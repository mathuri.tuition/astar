<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class NonAcademicClass extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guard_name = 'web';
    
    protected $fillable = [
        'class', 'price', 'created_by', 'modified_by', 'level_id'
    ];

    public function created_by()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }

    public function modified_by()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }

}
