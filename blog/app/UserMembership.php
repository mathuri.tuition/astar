<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class UserMembership extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $guard_name = 'web';

    protected $fillable = [
        'user_id', 'status', 'expiry', 'payment_status', 'payment_reference_no', 'remark'
    ];

    public function user_id()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }
}
