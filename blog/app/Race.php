<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Race extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guard_name = 'web';

    protected $fillable = [
        'race'
    ];

    public $timestamps = false;
}
