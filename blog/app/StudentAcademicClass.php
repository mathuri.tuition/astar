<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class StudentAcademicClass extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $guard_name = 'web';
    
    protected $fillable = [
        'user_id', 'academic_reference_no', 'academic_class_id', 'type' , 'pax','frequency', 'duration', 'days', 'time', 
        'state', 'city', 'postcode', 'area', 'tutor_gender', 'tutor_race', 'tutor_preference', 'status',
        'class_start_date', 'payment_received', 'payment_date', 'job_offer_active',
        'request_status', 'request_approved_by', 'request_approved_at', 'admin_remarks'
    ];

    public function user_id()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }

    public function academic_class_id()
    {
        return $this->hasOne('App\AcademicClass', 'foreign_key');
    }

    public function request_approved_by()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }

    
}
