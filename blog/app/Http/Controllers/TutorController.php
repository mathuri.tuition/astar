<?php

namespace App\Http\Controllers;

use App\Tutor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Mail\TutorEducationQualification;
use Auth;
use Session;
use App\Nationality;
use App\TutorApplyJob;
use App\User;
use App\TutorAcademicJob;
use Carbon\Carbon;
use Storage;
use App\Race;
use App\Religion;
use Illuminate\Support\Facades\Mail;
use App\TutorNonAcademicJob;
use App\UserMembership;
use Illuminate\Support\Facades\Response;
use App\TutorSpmResult;
use App\TutorExperience;

class TutorController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:tutor|admin|superadmin']); //middleware 

        $this->middleware('permission:tutor-dashboard', ['only' => ['dashboard']]);
        $this->middleware('permission:tutor-profile', ['only' => ['profile', 'updateProfile']]);
        $this->middleware('permission:tutor-job_list', ['only' => ['jobList']]);
    }

    public function dashboard()
    {
        $user_id = auth()->user()->id;

        $academic = DB::table('tutor_academic_jobs')
            ->join('student_academic_classes', 'student_academic_classes.id', '=', 'tutor_academic_jobs.student_academic_class_id')
            ->select(DB::raw('count(tutor_academic_jobs.id) AS academic_applied, 
            COUNT((CASE WHEN ((tutor_academic_jobs.tutor_status = 1) AND (tutor_academic_jobs.job_assigned = TRUE) AND (student_academic_classes.status = 1) AND (student_academic_classes.request_status = 1) ) THEN 1 ELSE NULL END)) AS academic_approve'))
            // ->groupBy('tutor_status')
            ->where([
                ['tutor_academic_jobs.user_id', '=', $user_id],
            ])
            ->first();


        $nonacademic = DB::table('tutor_non_academic_jobs')
            ->join('student_non_academic_classes', 'student_non_academic_classes.id', '=', 'tutor_non_academic_jobs.student_non_academic_class_id')
            ->select(DB::raw('count(tutor_non_academic_jobs.id) AS non_academic_applied, 
            COUNT((CASE WHEN ((tutor_non_academic_jobs.tutor_status = 1) AND (tutor_non_academic_jobs.job_assigned = TRUE) AND (student_non_academic_classes.status = 1) AND (student_non_academic_classes.request_status = 1))  THEN 1 ELSE NULL END)) AS non_academic_approve'))
            // ->groupBy('tutor_status')
            ->where([
                ['tutor_non_academic_jobs.user_id', '=', $user_id],
            ])
            ->first();

        $membership = DB::table('user_memberships')
            ->where('user_id',  $user_id)
            ->first();


        if (is_null($academic)) {
            $academic = (object) array(
                "academic_applied" => 0,
                "academic_approve" => 0
            );
        }

        if (is_null($nonacademic)) {
            $nonacademic = (object) array(
                "non_academic_applied" => 0,
                "non_academic_approve" => 0,
            );
        }

        return view('tutors.dashboard', compact('academic', 'nonacademic', 'membership'));
    }

    public function profile()
    {
        $id = Auth()->user()->id;
        $tutor = DB::table('tutors')
            ->join('users', 'users.id', '=', 'tutors.user_id')
            // ->leftJoin('marital_statuses', 'admins.marital_status_id', '=', 'marital_statuses.id')
            ->select([
                'tutors.*', 'users.name as user_name', 'users.email as user_email'
            ])
            ->where('tutors.user_id', $id)
            ->first();

        $nationalities = DB::table('nationalities')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $maritals = DB::table('marital_statuses')->get();
        $races = DB::table('races')->orderBy('race', 'ASC')->get();
        $religions = DB::table('religions')->orderBy('religion', 'ASC')->get();

        return view('tutors.profile', compact('tutor', 'nationalities', 'states', 'maritals', 'races', 'religions'));
    }

    // not using this function - start
    public function updateProfile(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            // 'email' => 'required|email|unique:users,email,' . $id,
            'nationality' => 'required',
            'gender' => 'required',
            'address_1' => 'required',
            'address_2' => 'required',
            // 'address_3' => 'required',
            'postcode' => 'required',
            'city' => 'required',
            'state' => 'required',
            // 'image' => 'required|mimes:jpeg,png',
            'race' => 'required',
            'religion' => 'required'
        ]);

        $user_id = Auth()->user()->id;

        $input = $request->all();
        // $path = $input('image')->store('images');

        $path = $request->file('image')->storeAs(
            'public/images/avatars/' . $user_id,
            $user_id
        );

        $user = User::find($id);
        $user->update($input);
        User::where('id', $id)->update(['email' => $input['email']]);
        Tutor::where('user_id', $id)->update([
            'nationality_id' => $input['nationality'],
            'gender_id' => $input['gender'],
            'address_1' => $input['address_1'],
            'address_2' => $input['address_2'],
            'address_3' => $input['address_3'],
            'address_4' => $input['address_4'],
            'postcode' => $input['postcode'],
            'city' => $input['city'],
            'state_code' => $input['state'],
            'image' => $path,
            'race' => $input['race'],
            'religion' => $input['religion']
        ]);


        return redirect()->route('tutors.dashboard')->with('success', 'User updated successfully.');
    }

    // not using function - end

    public function updateProfilePublicInfo(Request $request)
    {
        $rules = [
            'name'     => 'required|string|min:3|max:191',
            'nationality' => 'required',
            'phone' => 'required',
            'nationality' => 'required',
            'gender' => 'required',
            // 'email'    => 'required|email|min:3|max:191',
            'password' => 'nullable|string|min:5|max:191',
            // 'image'    => 'nullable|image|max:1999', //formats: jpeg, png, bmp, gif, svg
        ];
        $request->validate($rules);

        $user = Auth::user();
        $user->name = $request->name;
        if ($request->password) {
            $user->password = Hash::make($request->password);
        }
        $user->save();

        $admin = Tutor::where('user_id', $user->id)->first();
        $admin->phone_no = $request->phone;
        $admin->nationality_id = $request->nationality;
        $admin->mykad_no = $request->mykad;
        $admin->passport_no = $request->passport;
        $admin->gender_id = $request->gender;

        if ($request->hasFile('image')) {

            $rules = [
                'image'    => 'file|image|max:5000',
            ];
            $request->validate($rules);

            $profile_path = config('app.image.tutor.profile');

            $image = $request->image;
            $ext = $image->getClientOriginalExtension();
            $filename = uniqid() . '.' . $ext;
            $image->storeAs($profile_path, $filename);

            Storage::delete("$profile_path/{$admin->image}");

            $admin->image = $filename;
        }

        $admin->save();


        return redirect()
            ->route('tutors.profile')
            ->with('success', 'Your profile has been updated!');
    }

    public function updateProfilePrivateInfo(Request $request)
    {
        $rules = [
            'race'     => 'required',
            'religion'    => 'required',
            'marital_status' => 'required',
            'address_1' => 'required',
            'address_2' => 'required',
            'address_3' => 'nullable',
            'address_4' => 'nullable',
            'city' => 'required',
            'state' => 'required',
            'postcode' => 'required',

        ];
        $request->validate($rules);

        $user_id = Auth()->user()->id;

        $admin = Tutor::where('user_id', $user_id)->first();
        $admin->address_1 = $request->address_1;
        $admin->address_2 = $request->address_2;
        $admin->address_3 = $request->address_3;
        $admin->address_4 = $request->address_4;
        $admin->city = $request->city;
        $admin->state_code = $request->state;
        $admin->postcode = $request->postcode;
        $admin->race = $request->race;
        $admin->religion = $request->religion;
        $admin->marital_status_id = $request->marital_status;
        $admin->save();


        return redirect()
            ->route('tutors.profile')
            ->with('success', 'Your profile has been updated!');
    }

    public function changePassword(Request $request)
    {
        $rules = [
            'old_password' => 'required',
            'new_password' => 'required|string|min:5|max:191|different:old_password',
            'verify_password' => 'required|same:new_password'
        ];
        $request->validate($rules);


        if (Hash::check($request->get('old_password'), Auth::user()->password)) {

            $user = Auth::user();
            $user->password = Hash::make($request->verify_password);
            $user->save();

            // correct password and change success
            $code = 'success';
            $message = 'Your password has been updated successfully !';
        } else {
            // wrong current password
            $code = 'danger';
            $message = 'Sorry, current password is not valid !';
        }

        return redirect()
            ->route('tutors.profile')
            ->with($code, $message);
    }

    public function showQualification()
    {
        $user_id = Auth()->user()->id;
        $tutor = Tutor::where('user_id', $user_id)->first();
        $high = json_decode($tutor->highest_education_results);
        $spm = TutorSpmResult::where('user_id', $user_id)->first();

        return view(
            'tutors.qualification',
            compact('tutor', 'spm')
        );
    }

    public function updateSpmQualification(Request $request)
    {
        $rules = [
            'spm_year'     => 'required',
            'spm_image'    => 'required|file',
        ];
        $request->validate($rules);

        $user = Auth()->user();
        $tutor = Tutor::where('user_id', $user->id)->first();

        if ($request->hasFile('spm_image')) {
            $image = $request->spm_image;
            $ext = $image->getClientOriginalExtension();
            $filename = uniqid() . '.' . $ext;

            $spm_path = config('app.file.tutor.spm');

            $image->storeAs($spm_path, $filename);
            $spm = json_decode($tutor->spm_results);

            if (!is_null($spm)) {
                Storage::delete("$spm_path/{$spm->image}");
            }


            $spm_results = [
                'year' => $request->spm_year,
                'image' => $filename
            ];
            $tutor->spm_results = json_encode($spm_results);
        }

        $tutor->save();


        // working
        // $spm_image = $request->file('spm_image');
        // $tutor_email = $user->email;
        // $tutor_phone = $tutor->phone_no;
        // $tutor_name = $user->name;
        // $data = [
        //     'spm_image' => $spm_image,
        //     'tutor_name' => $tutor_name,
        //     'tutor_email' => $tutor_email,
        //     'tutor_phone' => $tutor_phone,
        //     'spm_year' => $request->input('spm_year'),
        // ];
        // Mail::to('lav12.deva@gmail.com')->send(new TutorEducationQualification($data));


        return redirect()
            ->route('tutors.qualification')
            ->with('success', 'Your qualifications has been updated!');
    }

    public function updateHigherQualification(Request $request)
    {
        $rules = [
            'education_level' => 'required',
            'graduation_year' => 'required',
            'certificate_image'    => 'required|file',
            'university' => 'required'
        ];
        $request->validate($rules);

        $user = Auth()->user();
        $tutor = Tutor::where('user_id', $user->id)->first();

        if ($request->hasFile('certificate_image')) {
            $image = $request->certificate_image;
            $ext = $image->getClientOriginalExtension();
            $filename = uniqid() . '.' . $ext;

            $higher_path = config('app.file.tutor.higher');

            $image->storeAs($higher_path, $filename);
            $high = json_decode($tutor->highest_education_results);
            if (!is_null($high)) {
                Storage::delete("$higher_path/{$high->image}");
            }


            $highest_education_results = [
                'level' => strip_tags($request->education_level),
                'year' => strip_tags($request->graduation_year),
                'image' => strip_tags($filename),
                'university' => strip_tags($request->university)
            ];
            $tutor->highest_education_results = json_encode($highest_education_results);
        }

        $tutor->save();

        return redirect()
            ->route('tutors.qualification')
            ->with('success', 'Your qualifications has been updated!');
    }

    public function membershipIndex()
    {
        $user = Auth()->user();
        $membership = DB::table('user_memberships')
            ->where('user_id', $user->id)
            ->first();

        $logs = DB::table('membership_logs')
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->paginate(5);

        return view('tutors.membership', compact('membership', 'logs'));
    }

    public function membershipRenewalRequest(Request $request)
    {
        $rules = [
            'payment_slip'    => 'nullable|image|max:1999', //formats: jpeg, png, bmp, gif, svg
        ];
        $request->validate($rules);

        $user = Auth()->user();

        $admin = UserMembership::where('user_id', $user->id)->first();
        $admin->status = 'requested';

        if ($request->hasFile('payment_slip')) {
            $image = $request->payment_slip;
            $ext = $image->getClientOriginalExtension();
            $filename = uniqid() . '.' . $ext;
            $membership_path = config('app.image.tutor.membership');
            $image->storeAs($membership_path, $filename);
            Storage::delete("$membership_path/{$admin->payment_reference_no}");
            $admin->payment_reference_no = $filename;
        }

        $admin->save();

        return redirect()
            ->route('tutors.membership')
            ->with('success', 'Your membership request has been sent!');
    }
    
     public function showClassPreference(Request $request)
    {
        $user = Auth()->user();
        $preference = TutorExperience::where('user_id', $user->id)->first();

        return view(
            'tutors.class_preference',
            compact('preference')
        );
    }
    
     public function updateSubjects(Request $request)
    {
        $rules = [
            'subjects' => 'required',
        ];
        $request->validate($rules);

        $user = Auth()->user();
        $tutor = TutorExperience::where('user_id', $user->id)->first();

        if(!is_null($tutor))
        {
            $tutor->subjects = $request->subjects;
            $tutor->save();

        }else{
            $insert = new TutorExperience();
            $insert->user_id = $user->id;
            $insert->subjects =  $request->subjects;
            $insert->save();
        }

        return redirect()
            ->route('tutors.class_preference')
            ->with('success', 'Your preference has been updated!');
    }

    public function updateExperience(Request $request)
    {
        $rules = [
            'experience' => 'required',
        ];
        $request->validate($rules);

        $user = Auth()->user();
        $tutor = TutorExperience::where('user_id', $user->id)->first();

        if (!is_null($tutor)) {

            $tutor->experience = $request->experience;
            $tutor->save();

        } else {
            $insert = new TutorExperience();
            $insert->user_id = $user->id;
            $insert->experience =  $request->experience;
            $insert->save();
        }

        return redirect()
            ->route('tutors.class_preference')
            ->with('success', 'Your preference has been updated!');
    }

    public function updateAreas(Request $request)
    {
        $rules = [
            'areas' => 'required',
        ];
        $request->validate($rules);

        $user = Auth()->user();
        $tutor = TutorExperience::where('user_id', $user->id)->first();

        if (!is_null($tutor)) {

            $tutor->areas = $request->areas;
            $tutor->save();

        } else {
            $insert = new TutorExperience();
            $insert->user_id = $user->id;
            $insert->areas =  $request->areas;
            $insert->save();
        }

        return redirect()
            ->route('tutors.class_preference')
            ->with('success', 'Your preference has been updated!');
    }




    /**
     * Academic Job
     */

    // job offer
    public function jobIndex(Request $request)
    {
        $user = Auth()->user();
        $tutor = DB::table('tutors')->where('user_id', $user->id)->first();

        if ($tutor->account_approved == 1) {
            $jobs = DB::table('student_academic_classes')
                ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
                ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
                ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
                ->where([
                    ['student_academic_classes.status', '=', 1],
                    ['student_academic_classes.request_status', '=', 1],
                    ['student_academic_classes.job_offer_active', '=', 1],
                    ['student_academic_classes.request_status', '=', 1],
                ])
                ->select(
                    'student_academic_classes.*',
                    'academic_classes.name AS class_name',
                    'curricula.curriculum AS curriculum',
                    'levels.level as level'
                )
                ->orderBy('student_academic_classes.created_at', 'DESC')
                ->paginate(5);
        } else {
            $jobs = [];
        }

        $membership = DB::table('user_memberships')->where('user_id', $user->id)->first();

        return view('tutors.academics.job_index', compact('jobs', 'tutor', 'membership'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // from job offer - show details
    public function jobShow(Request $request)
    {
        $user = Auth()->user();
        $class = DB::table('student_academic_classes')
            // ->join('student_academic_classes', 'student_academic_classes.id', '=', 'tutor_academic_jobs.student_academic_class_id')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->join('nationalities', 'nationalities.id', '=', 'students.nationality_id')
            ->where([
                ['student_academic_classes.academic_reference_no', '=', $request->ref_no],
                ['student_academic_classes.tutor_id', '=', NULL]
            ])->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.gender_id as gender',
                'students.race as race',
                'nationalities.nationality as nationality'
            )
            ->first();

        if (is_null($class)) {
            abort(403, 'Unauthorized action.');
        }

        $races = DB::table('races')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('tutors.academics.job_show', compact('class', 'races', 'states'));
    }

    // from job offer - show apply job page
    public function showJobApply(Request $request)
    {
        $user = Auth()->user();
        $class = DB::table('student_academic_classes')
            // ->join('student_academic_classes', 'student_academic_classes.id', '=', 'tutor_academic_jobs.student_academic_class_id')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->join('nationalities', 'nationalities.id', '=', 'students.nationality_id')
            ->where([
                ['student_academic_classes.academic_reference_no', '=', $request->ref_no],
                ['student_academic_classes.tutor_id', '=', NULL]
            ])->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.gender_id as gender',
                'students.race as race',
                'nationalities.nationality as nationality'
            )
            ->first();

        if (is_null($class)) {
            abort(403, 'Unauthorized action.');
        }

        $races = DB::table('races')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('tutors.academics.job_apply', compact('class', 'races', 'states'));
    }

    // from job offer - apply job submit
    public function jobApply(Request $request)
    {
        $input = $request->all();
        $user_id = Auth()->user()->id;

        $check = DB::table('tutor_academic_jobs')
            ->where([
                'user_id' => $user_id,
                'student_academic_class_id' => $input['class']
            ])
            ->first();

        if (is_null($check)) {
            $insert = new TutorAcademicJob();
            $insert->user_id =  $user_id;
            $insert->student_academic_class_id = $input['class'];
            $insert->tutor_remarks = $input['info'];
            $insert->save();

            $indicator = "success";
            $message = "Job application successful.";
        } else {
            $indicator = 'warning';
            $message = "You have applied for this job on " . Carbon::parse($check->created_at)->format('d-m-Y');
        }


        return redirect()->route('tutors.academics.job_index')->with($indicator, $message);
    }

    // job applied index
    public function showJobAppliedByUserIdHistory(Request $request)
    {
        $user_id = Auth()->user()->id;

        $jobs = DB::table('tutor_academic_jobs')
            ->join('student_academic_classes', 'student_academic_classes.id', '=', 'tutor_academic_jobs.student_academic_class_id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where([
                ['tutor_academic_jobs.user_id', '=', $user_id],
            ])
            ->select(
                'tutor_academic_jobs.id as id',
                'student_academic_classes.academic_reference_no as academic_reference_no',
                'student_academic_classes.city as city',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                'student_academic_classes.tutor_id as tutor_id',
                'tutor_academic_jobs.job_assigned as job_assigned',
                'student_academic_classes.status as status'
            )
            ->paginate(5);

        return view('tutors.academics.job_applied', compact('jobs'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    //job applied show- because wana restrict user
    public function academicJobAppliedShow(Request $request)
    {
        $user = Auth()->user();
        $class = DB::table('tutor_academic_jobs')
            ->join('student_academic_classes', 'student_academic_classes.id', '=', 'tutor_academic_jobs.student_academic_class_id')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->join('nationalities', 'nationalities.id', '=', 'students.nationality_id')
            ->where([
                ['student_academic_classes.academic_reference_no', '=', $request->ref_no],
                ['tutor_academic_jobs.user_id', '=', $user->id]
            ])->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',

                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.gender_id as gender',
                'students.race as race',
                'nationalities.nationality as nationality'
            )
            ->first();

        if (is_null($class)) {
            abort(403, 'Unauthorized action.');
        }

        $races = DB::table('races')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('tutors.academics.job_applied_show', compact('class', 'races', 'states'));
    }

    // job approved index / job assigned - both same 
    public function jobApprovedByUserIdIndex(Request $request)
    {
        $user_id = Auth()->user()->id;

        $jobs = DB::table('tutor_academic_jobs')
            ->join('student_academic_classes', 'student_academic_classes.id', '=', 'tutor_academic_jobs.student_academic_class_id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where([
                ['student_academic_classes.tutor_id', '=', $user_id],
                ['tutor_academic_jobs.job_assigned', '=', 1],
                ['tutor_academic_jobs.tutor_status', '=', 1],
                ['student_academic_classes.status', '=', 1],
                ['student_academic_classes.request_status', '=', 1]
            ])
            ->select(
                'tutor_academic_jobs.id as id',
                'student_academic_classes.id as sac_id',
                'student_academic_classes.academic_reference_no as academic_reference_no',
                'student_academic_classes.city as city',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                'tutor_academic_jobs.job_assigned as job_assigned'
            )
            ->paginate(5);


        return view('tutors.academics.job_approved', compact('jobs'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // job assigned/approved - show
    //job applied show- because wana restrict user
    public function academicJobAssignedShow(Request $request)
    {
        $user = Auth()->user();
        $class = DB::table('student_academic_classes')
            // ->join('student_academic_classes', 'student_academic_classes.id', '=', 'tutor_academic_jobs.student_academic_class_id')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->join('nationalities', 'nationalities.id', '=', 'students.nationality_id')
            ->where([
                ['student_academic_classes.academic_reference_no', '=', $request->ref_no],
                ['student_academic_classes.tutor_id', '=', $user->id]
            ])->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.gender_id as gender',
                'students.race as race',
                'nationalities.nationality as nationality'
            )
            ->first();

        if (is_null($class)) {
            abort(403, 'Unauthorized action.');
        }

        $races = DB::table('races')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('tutors.academics.job_assigned_show', compact('class', 'races', 'states'));
    }


    /**
     * tutor Non Academic Job
     */

    // job offer index
    public function nonAcademicJobIndex(Request $request)
    {
        $user = Auth()->user();
        $tutor = DB::table('tutors')->where('user_id', $user->id)->first();

        if ($tutor->account_approved == 1) {
            $jobs = DB::table('student_non_academic_classes')
                ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
                ->where([
                    ['student_non_academic_classes.status', '=', 1],
                    ['student_non_academic_classes.request_status', '=', 1],
                    ['student_non_academic_classes.job_offer_active', '=', 1],
                    ['student_non_academic_classes.status', '=', 1],
                ])
                ->select(
                    'student_non_academic_classes.*',
                    'non_academic_classes.class AS class_name'
                )
                ->orderBy('student_non_academic_classes.created_at', 'DESC')
                ->paginate(5);
        } else {
            $jobs = [];
        }

        $membership = DB::table('user_memberships')->where('user_id', $user->id)->first();

        return view('tutors.nonacademics.job_index', compact('jobs', 'tutor', 'membership'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // job offer show job
    public function nonAcademicJobShow(Request $request)
    {
        $user = Auth()->user();
        $class = DB::table('student_non_academic_classes')
            // ->join('student_non_academic_classes', 'student_non_academic_classes.id', '=', 'tutor_non_academic_jobs.student_non_academic_class_id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('nationalities', 'nationalities.id', '=', 'students.nationality_id')
            ->where([
                ['student_non_academic_classes.reference_no', '=', $request->ref_no],
                ['student_non_academic_classes.tutor_id', '=', NULL]
            ])
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name',
                'non_academic_classes.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.gender_id as gender',
                'students.race as race',
                'nationalities.nationality as nationality'
            )
            ->first();

        if (is_null($class)) {
            abort(403, 'Unauthorized action.');
        }


        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $races = DB::table('races')->get();

        return view('tutors.nonacademics.job_show', compact('class', 'states', 'races'));
    }

    // from job offer - apply job page
    public function nonAcademicJobApply(Request $request)
    {
        $user = Auth()->user();
        $class = DB::table('student_non_academic_classes')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('nationalities', 'nationalities.id', '=', 'students.nationality_id')
            ->where([
                ['student_non_academic_classes.reference_no', '=', $request->ref_no],
                ['student_non_academic_classes.tutor_id', '=', NULL]
            ])
            ->select(
                'users.name AS user_name',
                'users.email AS user_email',
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name',

                'non_academic_classes.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.gender_id as gender',
                'students.race as race',
                'nationalities.nationality as nationality'
            )
            ->first();

        if (is_null($class)) {
            abort(403, 'Unauthorized action.');
        }

        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $races = DB::table('races')->get();

        return view('tutors.nonacademics.job_apply', compact('class', 'states', 'races'));
    }

    // from job offer - apply job submit
    public function nonAcademicJobApplySubmit(Request $request)
    {
        $input = $request->all();
        $user_id = Auth()->user()->id;

        $check = DB::table('tutor_non_academic_jobs')
            ->where([
                'user_id' => $user_id,
                'student_non_academic_class_id' => $input['class']
            ])
            ->first();

        if (is_null($check)) {
            $insert = new TutorNonAcademicJob();
            $insert->user_id =  $user_id;
            $insert->student_non_academic_class_id = $input['class'];
            $insert->tutor_remarks = $input['info'];
            $insert->save();

            $indicator = "success";
            $message = "Job application successful.";
        } else {
            $indicator = 'warning';
            $message = "You have applied for this job on " . Carbon::parse($check->created_at)->format('d-m-Y');
        }


        return redirect()->route('tutors.nonacademics.job_index')->with($indicator, $message);
    }

    // job applied index
    public function showNonAcademicJobAppliedByUserIdHistory(Request $request)
    {
        $user_id = Auth()->user()->id;

        $jobs = DB::table('tutor_non_academic_jobs')
            ->join('student_non_academic_classes', 'student_non_academic_classes.id', '=', 'tutor_non_academic_jobs.student_non_academic_class_id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where([
                ['tutor_non_academic_jobs.user_id', '=', $user_id],
            ])
            ->select(
                'tutor_non_academic_jobs.id as id',
                'student_non_academic_classes.reference_no as reference_no',
                'student_non_academic_classes.city as city',
                'student_non_academic_classes.status as status',
                'non_academic_classes.class AS class_name',
                'student_non_academic_classes.id as snac_id',
                'student_non_academic_classes.tutor_id as tutor_id',
                'tutor_non_academic_jobs.job_assigned as job_assigned'
            )
            ->paginate(5);


        return view('tutors.nonacademics.job_applied', compact('jobs'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // job applied show
    public function nonAcademicJobAppliedShow(Request $request)
    {
        $user = Auth()->user();
        $class = DB::table('tutor_non_academic_jobs')
            ->join('student_non_academic_classes', 'student_non_academic_classes.id', '=', 'tutor_non_academic_jobs.student_non_academic_class_id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('nationalities', 'nationalities.id', '=', 'students.nationality_id')
            ->where([
                ['student_non_academic_classes.reference_no', '=', $request->ref_no],
                ['tutor_non_academic_jobs.user_id', '=', $user->id]
            ])
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name',
                'non_academic_classes.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.gender_id as gender',
                'students.race as race',
                'nationalities.nationality as nationality'
            )
            ->first();

        if (is_null($class)) {
            abort(403, 'Unauthorized action.');
        }


        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $races = DB::table('races')->get();

        return view('tutors.nonacademics.job_applied_show', compact('class', 'states', 'races'));
    }

    // job approved index
    public function nonAcademicJobApprovedByUserIdIndex(Request $request)
    {
        $user_id = Auth()->user()->id;

        $jobs = DB::table('tutor_non_academic_jobs')
            ->join('student_non_academic_classes', 'student_non_academic_classes.id', '=', 'tutor_non_academic_jobs.student_non_academic_class_id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where([
                ['student_non_academic_classes.tutor_id', '=', $user_id],
                ['tutor_non_academic_jobs.job_assigned', '=', '1'],
                ['tutor_non_academic_jobs.tutor_status', '=', '1'],
                ['student_non_academic_classes.status', '=', '1'],
                ['student_non_academic_classes.request_status', '=', '1'],
            ])
            ->select(
                'tutor_non_academic_jobs.id as id',
                'student_non_academic_classes.reference_no as reference_no',
                'student_non_academic_classes.city as city',
                'non_academic_classes.class AS class_name',
                'tutor_non_academic_jobs.job_assigned as job_assigned'
            )
            ->paginate(5);


        return view('tutors.nonacademics.job_approved', compact('jobs'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // job assigned show
    public function nonAcademicJobAssignedShow(Request $request)
    {
        $user = Auth()->user();
        $class = DB::table('student_non_academic_classes')
            // ->join('student_non_academic_classes', 'student_non_academic_classes.id', '=', 'tutor_non_academic_jobs.student_non_academic_class_id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('nationalities', 'nationalities.id', '=', 'students.nationality_id')
            ->where([
                ['student_non_academic_classes.reference_no', '=', $request->ref_no],
                ['student_non_academic_classes.tutor_id', '=', $user->id]
            ])
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name',
                'non_academic_classes.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.gender_id as gender',
                'students.race as race',
                'nationalities.nationality as nationality'
            )
            ->first();

        // dd("masuk", $class);

        if (is_null($class)) {
            abort(403, 'Unauthorized action.');
        }


        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $races = DB::table('races')->get();

        return view('tutors.nonacademics.job_assigned_show', compact('class', 'states', 'races'));
    }



    public function getHigher()
    {
        $user_id = Auth()->user()->id;
        $tutor = DB::table('tutors')->where(['user_id' => $user_id])->first();

        $json = json_decode($tutor->highest_education_results);

        $filename = $json->image;
        $path = config('app.file.tutor.higher');



        // file not found
        if (!Storage::exists($path . $filename)) {
            abort(404);
        }

        $pdfContent = Storage::get($path . $filename);
        // for pdf, it will be 'application/pdf'
        // $type       = Storage::mimeType($academic_price_path . 'academic_price.pdf');

        return Response::make($pdfContent, 200, [
            'Content-Type'        => 'application/pdf',
            'Content-Disposition' => 'inline; filename=' . $filename
        ]);
    }

    public function getSpm()
    {
        $user_id = Auth()->user()->id;

        $tutor = DB::table('tutors')->where(['user_id' => $user_id])->first();

        $json = json_decode($tutor->spm_results);

        $filename = $json->image;
        $path = config('app.file.tutor.spm');

        // file not found
        if (!Storage::exists($path . $filename)) {
            abort(404);
        }

        $pdfContent = Storage::get($path . $filename);
        // for pdf, it will be 'application/pdf'
        // $type       = Storage::mimeType($academic_price_path . 'academic_price.pdf');

        return Response::make($pdfContent, 200, [
            'Content-Type'        => 'application/pdf',
            'Content-Disposition' => 'inline; filename=' . $filename
        ]);
    }
    
     public function updateSpmResults(Request $request)
    {
        $user_id = Auth()->user()->id;

        $check = DB::table('tutor_spm_results')->where(['user_id' => $user_id])->first();

        $r_subjects = $request->subject;
        $r_scores = $request->score;

        $subjects = json_encode($r_subjects);
        $scores = json_encode($r_scores);

        if(!is_null($check)){
            // dd("Satu");
            
            TutorSpmResult::where('user_id', $user_id)->update([
                'subjects' =>  $subjects,
                'scores' =>  $scores
            ]);

        }else{
            // dd("dua");
            $insert = new TutorSpmResult();
            $insert->user_id =  $user_id;
            $insert->subjects =  $subjects;
            $insert->scores =  $scores;
            $insert->save();
        }

        return redirect()->route('tutors.qualification')->with('success', 'SPM results updated!');
    }
}
