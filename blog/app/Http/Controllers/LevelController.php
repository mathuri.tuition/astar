<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Level;
use Illuminate\Support\Facades\DB;

class LevelController extends Controller
{
    public function index()
    {
        $levels = DB::table('levels')
        ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
        ->select('levels.*', 'curricula.curriculum as curriculum')
        ->paginate(5);

        return view('levels.index', compact('levels'))->with('i', (request()->input('page', 1) - 1) * 5);
    }


  
    public function create()
    {
        $curricula = DB::table('curricula')->get();
        return view('levels.create', compact('curricula'));
    }


   
    public function store(Request $request)
    {
        request()->validate([
            'curriculum' => 'required',
            'level' => 'required',
            'price' => 'required'
        ]);

        $user_id = Auth()->user()->id;

        $input = $request->all();

        $insert_data = [
            'curriculum_id' => strip_tags($request->curriculum),
            'level' => strip_tags($request->level),
            'price' => strip_tags($request->price)
        ];

        Level::create($insert_data);

        return redirect()->route('levels.index')->with('success', 'Level added successfully.');
    }


  
    public function show($id)
    {
        $curricula = DB::table('curricula')->get();
        $level = DB::table('levels')->where('id', $id)->first();

        return view('levels.show', compact('level','curricula'));
    }


   
    public function edit($id)
    {
        $curricula = DB::table('curricula')->get();
        $level = DB::table('levels')->where('id', $id)->first();

        return view('levels.edit', compact('curricula', 'level'));
    }


   
    public function update(Request $request, Level $level)
    {
        request()->validate([
            'curriculum_id' => 'required',
            'level' => 'required',
            'price' => 'required'
        ]);

        $level->update($request->all());


        return redirect()->route('levels.index')->with('success', 'Curriculum updated successfully');
    }


    public function destroy(Level $level)
    {
        $level->delete();

        return redirect()->route('levels.index')->with('success', 'Curriculum deleted successfully');
    }
}
