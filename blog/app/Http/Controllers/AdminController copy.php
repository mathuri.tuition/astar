<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Admin;
use App\MembershipLog;
use App\Nationality;
use App\Student;
use App\StudentAcademicClass;
use App\User;
use Hash;
use App\StudentTutorAcademic;
use App\TutorAcademicJob;
use Carbon\Carbon;
use App\Tutor;
use Storage;
use Illuminate\Auth\Events\Registered;
use App\TutorNonAcademicJob;
use App\StudentNonAcademicClass;
use App\UserMembership;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|superadmin']); //middleware 

        // $this->middleware('permission:admin-dashboard', ['only' => ['dashboard']]);
        // $this->middleware('permission:admin-profile', ['only' => ['profile', 'update']]);
        // $this->middleware('permission:admin-job_list', ['only' => ['jobList', 'detailAcademicJobList']]);
    }

    public function dashboard()
    {
        $user = Auth()->user()->roles()->first()->name;

        return view('admins.dashboard', compact('user'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function profile()
    {
        $id = Auth()->user()->id;

        $admin = DB::table('admins')
            ->leftJoin('users', 'users.id', '=', 'admins.user_id')
            // ->leftJoin('marital_statuses', 'admins.marital_status_id', '=', 'marital_statuses.id')
            ->select([
                'admins.*', 'users.name as user_name', 'users.email as user_email'
            ])
            ->where('admins.user_id', $id)
            ->first();

        $nationalities = DB::table('nationalities')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $maritals = DB::table('marital_statuses')->get();
        $races = DB::table('races')->orderBy('race', 'ASC')->get();
        $religions = DB::table('religions')->orderBy('religion', 'ASC')->get();

        return view('admins.profile', compact('admin', 'nationalities', 'states', 'maritals', 'races', 'religions'));
    }

    public function updateProfile(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|regex:/^[A-Za-z ]+$/',
            'email' => 'required|email|unique:users,email,' . $id,
            'nationality' => 'required',
            'race' => 'required',
            'religion' => 'required',
            'gender' => 'required',
            'address_1' => 'required',
            'address_2' => 'required',
            'address_3' => 'required',
            'postcode' => 'required',
            'city' => 'required',
            'state' => 'required',
        ]);

        $input = $request->all();

        $user = User::find($id);
        $user->update($input);
        User::where('id', $id)->update(['email' => $input['email']]);
        Admin::where('user_id', $id)->update([
            'nationality_id' => $input['nationality'],
            'race' => $input['race'],
            'religion' => $input['religion'],
            'gender_id' => $input['gender'],
            'address_1' => $input['address_1'],
            'address_2' => $input['address_2'],
            'address_3' => $input['address_3'],
            'address_4' => $input['address_4'],
            'postcode' => $input['postcode'],
            'city' => $input['city'],
            'state_code' => $input['state'],
        ]);

        return redirect()->route('admins.dashboard')->with('success', 'User updated successfully.');
    }

    public function updateProfilePublicInfo(Request $request)
    {
        $user_id = Auth()->user()->id;

        $rules = [
            'name'     => 'required|string|min:3|max:191',
            'nationality' => 'required',
            'phone' => 'required',
            'nationality' => 'required',
            'gender' => 'required',
            // 'email'    => 'required|email|min:3|max:191',
            'password' => 'nullable|string|min:5|max:191',
            'image'    => 'nullable|image|max:1999', //formats: jpeg, png, bmp, gif, svg
        ];
        $request->validate($rules);

        $user = Auth::user();
        $user->name = $request->name;
        if ($request->password) {
            $user->password = Hash::make($request->password);
        }
        $user->save();


        $admin = Admin::where('user_id', $user->id)->first();
        $admin->phone_no = $request->phone;
        $admin->nationality_id = $request->nationality;
        $admin->mykad_no = $request->mykad;
        $admin->passport_no = $request->passport;
        $admin->gender_id = $request->gender;

        if ($request->hasFile('image')) {
            $image = $request->image;
            $ext = $image->getClientOriginalExtension();
            $filename = uniqid() . '.' . $ext;
            $image->storeAs('admins/pics', $filename);
            Storage::delete("admins/pics/{$admin->image}");
            $admin->image = $filename;
        }

        $admin->save();


        return redirect()
            ->route('admins.profile')
            ->with('success', 'Your profile has been updated!');
    }

    public function updateProfilePrivateInfo(Request $request)
    {
        $rules = [
            'race'     => 'required',
            'religion'    => 'required',
            'marital_status' => 'required',
            'address_1' => 'required',
            'address_2' => 'required',
            'address_3' => 'nullable',
            'address_4' => 'nullable',
            'city' => 'required',
            'state' => 'required',
            'postcode' => 'required',

        ];
        $request->validate($rules);

        $user_id = Auth()->user()->id;

        $admin = Admin::where('user_id', $user_id)->first();
        $admin->address_1 = $request->address_1;
        $admin->address_2 = $request->address_2;
        $admin->address_3 = $request->address_3;
        $admin->address_4 = $request->address_4;
        $admin->city = $request->city;
        $admin->state_code = $request->state;
        $admin->postcode = $request->postcode;
        $admin->race = $request->race;
        $admin->religion = $request->religion;
        $admin->marital_status_id = $request->marital_status;
        $admin->save();


        return redirect()
            ->route('admins.profile')
            ->with('success', 'Your profile has been updated!');
    }

    public function changePassword(Request $request)
    {
        $rules = [
            'old_password' => 'required',
            'new_password' => 'required|string|min:5|max:191|different:old_password',
            'verify_password' => 'required|same:new_password'
        ];
        $request->validate($rules);


        if (Hash::check($request->get('old_password'), Auth::user()->password)) {

            $user = Auth::user();
            $user->password = Hash::make($request->verify_password);
            $user->save();

            // correct password and change success
            $code = 'success';
            $message = 'Your password has been updated successfully !';
        } else {
            // wrong current password
            $code = 'danger';
            $message = 'Sorry, current password is not valid !';
        }

        return redirect()
            ->route('admins.profile')
            ->with($code, $message);
    }

    /** 
     * Tutor Account Validation - Approve or reject account request
     */
    public function tutorAccountValidation(Request $request, $id)
    {
        $rules = [
            'status' => 'required',
        ];
        $request->validate($rules);

        $user = Auth()->user();

        if ($request->status == 1) {
            $account_status = 1;
        } else {
            $account_status = 0;
        }

        $tutor = Tutor::where('id', $id)->first();
        $tutor->account_approved = $account_status;
        $tutor->account_approved_by = $user->id;
        $tutor->account_approved_at = Carbon::now();
        $tutor->save();

        return redirect()
            ->route('admins.users.tutors.index')
            ->with('success', 'Tutor status has been updated!');
    }


    /**
     * Tutor Membership Renewal - Admin change the status to renew and update the new expiry date
     */



    /**
     *  Academic Job Application by tutor that admin need to approve 
     */

    public function academicJobApplicationIndex(Request $request)
    {
        $applications = DB::table('tutor_academic_jobs')
            ->join('student_academic_classes', 'student_academic_classes.id', '=', 'tutor_academic_jobs.student_academic_class_id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->where([
                ['tutor_academic_jobs.tutor_status', '=', NULL],
                ['student_academic_classes.tutor_id', '=', NULL],
                ['student_academic_classes.status', '=', 1],
                ['student_academic_classes.request_status', '=', 1],
                ])
            ->select([
                'student_academic_classes.id as SAC_id',
                'student_academic_classes.academic_reference_no as academic_reference_no',
                'student_academic_classes.created_at as requested_date',
                'academic_classes.name as subject_name',
                DB::raw('COUNT(tutor_academic_jobs.student_academic_class_id) as no_of_tutor')
            ])
            ->groupBy(
                'student_academic_classes.id',
                'student_academic_classes.academic_reference_no',
                'tutor_academic_jobs.student_academic_class_id',
                'academic_classes.name',
                'student_academic_classes.created_at'
            )
            ->paginate(5);

        return view('admins.academics.job_application_index', compact('applications'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function academicJobApplicationShow(Request $request)
    {
        $sac = DB::table('student_academic_classes')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where('student_academic_classes.id', '=', $request->id)
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                'levels.price as price',
            'users.name AS user_name',
            'users.email AS user_email',
            'students.image as image',
            'students.phone_no as phone_no',
            'students.nationality_id as nationality_id',
            'students.mykad_no as mykad_no',
            'students.passport_no as passport_no',
            'students.gender_id as gender_id'
            )
            ->first();

        $tutors = DB::table('tutor_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_academic_jobs.student_academic_class_id', $request->id)
            ->select([
                'tutor_academic_jobs.*',
                'users.name as user_name',
                'users.email as user_email',
                'tutors.phone_no as user_phone',
            'tutors.gender_id as gender_id',
            'tutors.race as race',
            'tutors.marital_status_id as marital_status_id'
            ])
            ->get();

        $nationalities = Nationality::all();
        $races = DB::table('races')->get();
        $maritals = DB::table('marital_statuses')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        
        return view('admins.academics.job_application_show', compact('sac', 'tutors', 'nationalities','races', 'maritals', 'states'));
    }

    public function academicJobApplicationEdit(Request $request)
    {
        $sac = DB::table('student_academic_classes')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where('student_academic_classes.id', '=', $request->id)
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $tutors = DB::table('tutor_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_academic_jobs.student_academic_class_id', $request->id)
            ->orderBy('tutor_academic_jobs.created_at', 'asc')
            ->select([
                'tutor_academic_jobs.*',
                'users.name as user_name',
                'users.email as user_email',
                'tutors.phone_no as user_phone',
                'tutors.gender_id as gender_id',
                'tutors.race as race',
                'tutors.marital_status_id as marital_status_id'
            ])
            ->get();

        $nationalities = Nationality::all();
        $races = DB::table('races')->get();
        $maritals = DB::table('marital_statuses')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        
        return view('admins.academics.job_application_edit', compact('sac', 'tutors', 'nationalities','races','maritals', 'states'));
    }

    public function academicJobApplicationUpdate(Request $request)
    {
        //tutor_job gives tutor_academic_job->id
        $input = $request->all();       

        $tutor_job_id = strip_tags($input['tutor_job']);
        $job = DB::table('tutor_academic_jobs')->where('tutor_academic_jobs.id', $tutor_job_id);
        $result_job = $job->first();

        if ($job->count() > 0) {
           
            // find other applications by student_academic_class_id
            // make other applications false
            $user_id = Auth()->user()->id;

            //tutor status = the tutor is approved for the application
            // status > 1:approved, 2:rejected, 3: pending
            //job_assigned to mark that this application is settled

            $assign_job = TutorAcademicJob::where('id', $tutor_job_id)->update([
                'tutor_status' => 1,
                'job_assigned' => 1,
                'job_assigned_date' => Carbon::now(),
                'job_assigned_by' =>  $user_id
            ]);

            $update_student_academic_class = StudentAcademicClass::where('id', $result_job->student_academic_class_id)->update([
                'job_offer_active' => 0,
                'tutor_id' => $result_job->user_id
            ]);

            //update other application
            $others = DB::table('tutor_academic_jobs')->where([
                ['tutor_academic_jobs.id','!=',$tutor_job_id]
                ])->get();

            foreach ($others as $other) {
                $other_applications = TutorAcademicJob::where('id', $other->id)->update([
                    'job_assigned' => 1,
                ]);
            }

            $code = 'success';
            $message = 'Job assigned successfully.';
        } else {
            $code = 'warning';
            $message = 'Fail to assign job. Please try again later.';
        }

        return redirect()->route('admins.academics.job_application_index')->with($code, $message);
    }

    // jobs that has been assigned to tutors - list
    public function showAssignedJobList(Request $request)
    {
        $datas = DB::table('student_academic_classes')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where([
                ['student_academic_classes.request_status', '=', 1],
                ['student_academic_classes.job_offer_active', '=', 0],
                ['student_academic_classes.tutor_id','!=', NULL],
                ['student_academic_classes.job_settled', '=', NULL],
            ])
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'curricula.curriculum AS curriculum',
                'levels.level as level'
            )
            ->orderBy('student_academic_classes.created_at', 'DESC')
            ->paginate(5);

        return view('admins.academics.job_assigned_list', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // show details of job assigned
    public function showAssignedJobShow(Request $request,$id)
    {
        $class = DB::table('student_academic_classes')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where('student_academic_classes.id', '=', $id)
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                
                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $tutors = DB::table('tutor_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_academic_jobs.student_academic_class_id', $request->id)
            ->select([
                'tutor_academic_jobs.*',
                'users.name as user_name',
                'users.email as user_email',
                'tutors.phone_no as user_phone'
            ])
            ->get();

        $nationalities = Nationality::all();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('admins.academics.job_assigned_show', compact('class', 'tutors','nationalities','states'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // show page to update the assigned job
    public function showAssignedJobEdit(Request $request, $id)
    {
        $class = DB::table('student_academic_classes')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where('student_academic_classes.id', '=', $id)
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                
                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $tutors = DB::table('tutor_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_academic_jobs.student_academic_class_id', $request->id)
            ->select([
                'tutor_academic_jobs.*',
                'users.name as user_name',
                'users.email as user_email',
                'tutors.phone_no as user_phone'
            ])
            ->get();

        $nationalities = Nationality::all();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('admins.academics.job_assigned_edit', compact('class', 'tutors','nationalities', 'states'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // make job offer available again for tutors to apply
    public function updateAssignedJobStatus(Request $request)
    {
        // $ref_no = strip_tags($request->ref_no);
        $sac_id = strip_tags($request->sac_id);

        $update_student_academic_class = StudentAcademicClass::where('id', $sac_id)->update([
            'job_offer_active' => 1,
            'tutor_id' => NULL,
        ]);
        
        $find_tutor_assigned_job = TutorAcademicJob::where('student_academic_class_id', $sac_id);
        $results_taj =  $find_tutor_assigned_job->get();
        $count_taj =  $find_tutor_assigned_job->count();

        if ($count_taj > 0) {
            foreach ($results_taj as $ftaj) {
                $update_taj = TutorAcademicJob::where('id', $ftaj->id)->update([
                    'job_assigned' => 0,
                    'tutor_status' => NULL,
                    'job_assigned_date' => NULL,
                    'job_assigned_by' => NULL
                ]);
            }
        }

        return redirect()->route('admins.academics.job_assigned_list')
        ->with('success', 'Job updated successfully');
    }

    // after assigned to cancel class
    public function cancelAssignedJobByRefNo(Request $request)
    {
        $ref_no = strip_tags($request->ref_no);

        // 1: active | 2:cancel
        $update_student_academic_class = StudentAcademicClass::where('academic_reference_no', $ref_no)->update([
            'status' => 2,
            'job_offer_active' => 0,
        ]);

        // dd($update_student_academic_class);

        $find_tutor_assigned_job = TutorAcademicJob::where('student_academic_class_id', $update_student_academic_class);
        $results_taj =  $find_tutor_assigned_job->get();
        $count_taj =  $find_tutor_assigned_job->count();

        if($count_taj > 0)
        {
            foreach ($results_taj as $ftaj) 
            {
                $update_taj = TutorAcademicJob::where('student_academic_class_id', $update_student_academic_class)->update([
                    'job_assigned' => 0,
                    'tutor_status' => NULL
                ]);
            }
        }

        return redirect()->route('admins.academics.job_assigned_list')
            ->with('success', 'Job updated successfully');
    }

    // update class start date
    public function academicJobStartDateUpdate(Request $request)
    {
        $rules = [
            'start_date' => 'required',
            'sac_id' => 'required',
        ];
        $request->validate($rules);

        $sac_id = strip_tags($request->sac_id);
        $start_date = strip_tags($request->start_date);

        $update_student_academic_class = StudentAcademicClass::where('id', $sac_id)->update([
            'class_start_date' => $start_date,
        ]);

        return redirect()->route('admins.academics.job_assigned_list')->with('success', 'Job updated successfully');
    }

    // update payment received status & date
    public function academicJobPaymentUpdate(Request $request)
    {
        $rules = [
            'payment_date' => 'required',
            'sac_id' => 'required',
        ];
        $request->validate($rules);

        $sac_id = strip_tags($request->sac_id);
        $payment_date = strip_tags($request->payment_date);

        $update_student_academic_class = StudentAcademicClass::where('id', $sac_id)->update([
            'payment_date' => $payment_date,
           
        ]);

        return redirect()->route('admins.academics.job_assigned_list')->with('success', 'Job updated successfully');
    }

    // mark job settled - means tutor and student ngam
    public function academicJobSettledUpdate(Request $request)
    {
        $sac_id = strip_tags($request->sac_id);

        $update_student_academic_class = StudentAcademicClass::where('id', $sac_id)->update([
            'job_settled' => Carbon::today(),

        ]);

        return redirect()->route('admins.academics.job_assigned_list')->with('success', 'Job updated successfully');
    }

    // list job settled - index
    public function academicJobSettledIndex(Request $request)
    {
        $datas = DB::table('student_academic_classes')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where([
                ['student_academic_classes.request_status', '=', 1],
                ['student_academic_classes.job_offer_active', '=', 0],
                ['student_academic_classes.tutor_id', '!=', NULL],
                ['student_academic_classes.job_settled','!=',NULL]
            ])
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'curricula.curriculum AS curriculum',
                'levels.level as level'
            )
            ->orderBy('student_academic_classes.created_at', 'DESC')
            ->paginate(5);

        return view('admins.academics.job_settled_index', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // show job settled details - show
    public function academicJobSettledShow(Request $request, $id)
    {
        $class = DB::table('student_academic_classes')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where('student_academic_classes.id', '=', $id)
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                
                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $tutors = DB::table('tutor_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_academic_jobs.student_academic_class_id', $request->id)
            ->select([
                'tutor_academic_jobs.*',
                'users.name as user_name',
                'users.email as user_email',
                'tutors.phone_no as user_phone'
            ])
            ->get();

        $nationalities = Nationality::all();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('admins.academics.job_settled_show', compact('class', 'tutors', 'nationalities', 'states'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }







    /** 
     * Non Academic Job Application by tutor that admin need to approve
     *  
     */

    public function nonAcademicJobApplicationIndex(Request $request)
    {
        $applications = DB::table('tutor_non_academic_jobs')
            ->join('student_non_academic_classes', 'student_non_academic_classes.id', '=', 'tutor_non_academic_jobs.student_non_academic_class_id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where([
            'tutor_non_academic_jobs.job_assigned' => 0
            ])
            ->select([
            'student_non_academic_classes.id as SAC_id',
            'student_non_academic_classes.reference_no as reference_no',
            'student_non_academic_classes.created_at as requested_date',
            'non_academic_classes.class as class_name',
           
            'student_non_academic_classes.remarks as remarks',
                DB::raw('COUNT(tutor_non_academic_jobs.student_non_academic_class_id) as no_of_tutor')
            ])
            ->groupBy(
            'student_non_academic_classes.id',
            'tutor_non_academic_jobs.student_non_academic_class_id',
            'non_academic_classes.class',
            'student_non_academic_classes.created_at'
            )
            ->paginate(5);

        return view('admins.nonacademics.job_application_index', compact('applications'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function nonAcademicJobApplicationShow(Request $request)
    {
        $sac = DB::table('student_non_academic_classes')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')

            ->join('nationalities', 'nationalities.id', '=', 'students.nationality_id')
            ->where('student_non_academic_classes.id', '=', $request->id)
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name',

                'non_academic_classes.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        $tutors = DB::table('tutor_non_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_non_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_non_academic_jobs.student_non_academic_class_id', $request->id)
            ->select([
            'tutor_non_academic_jobs.*',
                'users.name as user_name',
                'users.email as user_email',
                'tutors.phone_no as user_phone'
            ])
            ->get();

        $nationalities = Nationality::all();
        
        return view('admins.nonacademics.job_application_show', compact('sac', 'tutors', 'nationalities', 'states'));
    }

    public function nonAcademicJobApplicationEdit(Request $request)
    {
        $sac = DB::table('student_non_academic_classes')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->join('nationalities', 'nationalities.id', '=', 'students.nationality_id')
            ->where('student_non_academic_classes.id', '=', $request->id)
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name',
                'non_academic_classes.price as price',
            'users.name AS user_name',
            'users.email AS user_email',
            'students.image as image',
            'students.phone_no as phone_no',
            'students.nationality_id as nationality_id',
            'students.mykad_no as mykad_no',
            'students.passport_no as passport_no',
            'students.gender_id as gender_id'
            )
            ->first();
        

        $tutors = DB::table('tutor_non_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_non_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_non_academic_jobs.student_non_academic_class_id', $request->id)
            ->orderBy('tutor_non_academic_jobs.created_at', 'asc')
            ->select([
            'tutor_non_academic_jobs.*',
            'users.name as user_name',
            'users.email as user_email',
            'tutors.phone_no as user_phone',
            'tutors.gender_id as gender_id',
            'tutors.race as race',
            'tutors.marital_status_id as marital_status_id'
            ])
            ->get();

        $nationalities = Nationality::all();
        $races = DB::table('races')->get();
        $maritals = DB::table('marital_statuses')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        
        return view('admins.nonacademics.job_application_edit', compact('sac', 'tutors', 'nationalities', 'states', 'maritals','races'));
    }

    public function nonAcademicJobApplicationUpdate(Request $request)
    {
        //tutor_job gives tutor_academic_job->id
        $input = $request->all();

        $tutor_job_id = strip_tags($input['tutor_job']);
        // dd($tutor_job_id);
        $job = DB::table('tutor_non_academic_jobs')->where('tutor_non_academic_jobs.id', $tutor_job_id);
        $result_job = $job->first();

        if ($job->count() > 0) {

            // find other applications by student_academic_class_id
            // make other applications false
            $user_id = Auth()->user()->id;

            //tutor status = the tutor is approved for the application
            // status > 1:approved, 2:rejected, 3: pending
            //job_assigned to mark that this application is settled

            $assign_job = TutorNonAcademicJob::where('id', $tutor_job_id)->update([
                'tutor_status' => 1,
                'job_assigned' => 1,
                'job_assigned_date' => Carbon::now(),
                'job_assigned_by' =>  $user_id
            ]);

            $update_student_academic_class = StudentNonAcademicClass::where('id', $result_job->student_non_academic_class_id)->update([
                'job_offer_active' => 0,
                'tutor_id' => $result_job->user_id
            ]);

            //update other application
            $others = DB::table('tutor_non_academic_jobs')->where([
                ['tutor_non_academic_jobs.id', '!=', $tutor_job_id]
            ])->get();

            foreach ($others as $other) {
                $other_applications = TutorNonAcademicJob::where('id', $other->id)->update([
                    'job_assigned' => 1,
                ]);
            }

            $code = 'success';
            $message = 'Job assigned successfully.';
        } else {
            $code = 'warning';
            $message = 'Fail to assign job. Please try again later.';
        }

        return redirect()->route('admins.nonacademics.job_application_index')->with($code, $message);
    }

    // to enable the job appear as active job offer again for tutors
    public function nonAshowAssignedJobList(Request $request)
    {
        $datas = DB::table('student_non_academic_classes')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
           
            ->where([
                ['student_non_academic_classes.request_status', '=', 1],
                ['student_non_academic_classes.job_offer_active', '=', 0],
                ['student_non_academic_classes.tutor_id', '!=', NULL],
                ['student_non_academic_classes.job_settled','=', NULL]
            ])
            ->select(
            'student_non_academic_classes.*',
            'non_academic_classes.class AS class_name'
            )
            ->orderBy('student_non_academic_classes.created_at', 'DESC')
            ->paginate(5);

        return view('admins.nonacademics.job_assigned_list', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function nonAshowAssignedJobShow(Request $request, $id)
    {
        $class = DB::table('student_non_academic_classes')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where('student_non_academic_classes.id', '=', $id)
            ->select(
            'student_non_academic_classes.*',
            'non_academic_classes.class AS class_name',
                
            'non_academic_classes.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        $tutors = DB::table('tutor_non_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_non_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_non_academic_jobs.student_non_academic_class_id', $request->id)
            ->select([
            'tutor_non_academic_jobs.*',
                'users.name as user_name',
                'users.email as user_email',
                'tutors.phone_no as user_phone'
            ])
            ->get();

        $nationalities = Nationality::all();

        return view('admins.nonacademics.job_assigned_show', compact('class', 'tutors', 'nationalities', 'states'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function nonAshowAssignedJobEdit(Request $request, $id)
    {
        $class = DB::table('student_non_academic_classes')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where('student_non_academic_classes.id', '=', $id)
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name',

                'non_academic_classes.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        $tutors = DB::table('tutor_non_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_non_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_non_academic_jobs.student_non_academic_class_id', $request->id)
            ->select([
            'tutor_non_academic_jobs.*',
                'users.name as user_name',
                'users.email as user_email',
                'tutors.phone_no as user_phone'
            ])
            ->get();

        $nationalities = Nationality::all();

        return view('admins.nonacademics.job_assigned_edit', compact('class', 'tutors', 'nationalities', 'states'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function nonAupdateAssignedJobStatus(Request $request)
    {
        // $ref_no = strip_tags($request->ref_no);
        $snac_id = strip_tags($request->snac_id);
        // dd($snac_id);

        $update_student_nonacademic_class = StudentNonAcademicClass::where('id', $snac_id)->update([
            'job_offer_active' => 1,
            'tutor_id' => NULL,
        ]);
        
        $find_tutor_assigned_job = TutorNonAcademicJob::where('student_non_academic_class_id', $snac_id);
        $results_taj =  $find_tutor_assigned_job->get();
        $count_taj =  $find_tutor_assigned_job->count();

        if ($count_taj > 0) {
            foreach ($results_taj as $ftaj) {
                $update_taj = TutorNonAcademicJob::where('id', $ftaj->id)->update([
                    'job_assigned' => 0,
                    'tutor_status' => NULL,
                    'job_assigned_date' => NULL,
                    'job_assigned_by' => NULL
                ]);
            }
        }

        return redirect()->route('admins.nonacademics.job_assigned_list')->with('success', 'Job updated successfully');
    }

    public function nonAcancelRequestByRefNo(Request $request)
    {
        $ref_no = strip_tags($request->ref_no);

        // 1: active | 2:cancel
        $update_student_nonacademic_class = StudentNonAcademicClass::where('reference_no', $ref_no)->update([
            'status' => 2,
            'job_offer_active' => 0,
        ]);

        // dd($update_student_academic_class);

        $find_tutor_assigned_job = TutorNonAcademicJob::where('student_non_academic_class_id', $update_student_nonacademic_class);
        $results_taj =  $find_tutor_assigned_job->get();
        $count_taj =  $find_tutor_assigned_job->count();

        if ($count_taj > 0) {
            foreach ($results_taj as $ftaj) {
                $update_taj = TutorNonAcademicJob::where('student_non_academic_class_id', $update_student_nonacademic_class)->update([
                    'job_assigned' => 0,
                    'tutor_status' => NULL
                ]);
            }
        }

        return redirect()->route('admins.nonacademics.job_assigned_list')
            ->with('success', 'Job updated successfully');
    }

    // update class start date
    public function nonAcademicJobStartDateUpdate(Request $request)
    {
        $rules = [
            'start_date' => 'required',
            'sac_id' => 'required',
        ];
        $request->validate($rules);

        $sac_id = strip_tags($request->sac_id);
        $start_date = strip_tags($request->start_date);

        $update_student_academic_class = StudentNonAcademicClass::where('id', $sac_id)->update([
            'class_start_date' => $start_date,
        ]);

        return redirect()->route('admins.nonacademics.job_assigned_list')->with('success', 'Job updated successfully');
    }

    // update payment received status & date
    public function nonAcademicJobPaymentUpdate(Request $request)
    {
        $rules = [
            'payment_date' => 'required',
            'sac_id' => 'required',
        ];
        $request->validate($rules);

        $sac_id = strip_tags($request->sac_id);
        $payment_date = strip_tags($request->payment_date);

        $update_student_academic_class = StudentNonAcademicClass::where('id', $sac_id)->update([
            'payment_date' => $payment_date,
            
        ]);

        return redirect()->route('admins.nonacademics.job_assigned_list')->with('success', 'Job updated successfully');
    }

    // mark job settled - means tutor and student ngam
    public function nonAcademicJobSettledUpdate(Request $request)
    {
        $sac_id = strip_tags($request->sac_id);

        $update_student_academic_class = StudentNonAcademicClass::where('id', $sac_id)->update([
            'job_settled' => Carbon::today(),

        ]);

        return redirect()->route('admins.nonacademics.job_assigned_list')->with('success', 'Job updated successfully');
    }

    // list job settled - index
    public function nonAcademicJobSettledIndex(Request $request)
    {
        $datas = DB::table('student_non_academic_classes')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
           
            ->where([
                ['student_non_academic_classes.request_status', '=', 1],
                ['student_non_academic_classes.job_offer_active', '=', 0],
                ['student_non_academic_classes.tutor_id', '!=', NULL],
                ['student_non_academic_classes.job_settled', '!=', NULL],
            ])
            ->select(
                'student_non_academic_classes.*',
            'non_academic_classes.class AS class_name'
              
            )
            ->orderBy('non_academic_classes.created_at', 'DESC')
            ->paginate(5);

        // dd($datas);

        return view('admins.nonacademics.job_settled_index', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // show job settled details - show
    public function nonAcademicJobSettledShow(Request $request, $id)
    {
        $class = DB::table('student_non_academic_classes')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where('student_non_academic_classes.id', '=', $id)
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name',

                'non_academic_classes.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        $tutors = DB::table('tutor_non_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_non_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_non_academic_jobs.student_non_academic_class_id', $request->id)
            ->select([
                'tutor_non_academic_jobs.*',
                'users.name as user_name',
                'users.email as user_email',
                'tutors.phone_no as user_phone'
            ])
            ->get();

        $nationalities = Nationality::all();

        return view('admins.nonacademics.job_settled_show', compact('class', 'tutors', 'nationalities', 'states'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }












    /** Reassign Job  - not using i guess */
    public function academicJobReassignIndex(Request $request)
    {
        $applications = DB::table('tutor_academic_jobs')
            ->join('student_academic_classes', 'student_academic_classes.id', '=', 'tutor_academic_jobs.student_academic_class_id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->where(['tutor_academic_jobs.job_assigned' => 1])
            ->select([
                'student_academic_classes.id as SAC_id',
                'student_academic_classes.academic_reference_no as academic_reference_no',
                'student_academic_classes.created_at as requested_date',
                'academic_classes.name as subject_name',
                DB::raw('COUNT( CASE WHEN tutor_academic_jobs.student_academic_class_id = 1 THEN 1 ELSE 0 END ) as no_of_tutor')
            ])
            ->groupBy(
                'student_academic_classes.id',
                'tutor_academic_jobs.student_academic_class_id',
                'academic_classes.name',
                'student_academic_classes.created_at'
            )
            ->get();


        return view('admins.academics.job_assigned_list', compact('applications'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function academicJobReassignShow(Request $request)
    {
        $sac = DB::table('student_academic_classes')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where('student_academic_classes.id', '=', $request->id)
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                
                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $tutors = DB::table('tutor_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_academic_jobs.student_academic_class_id', $request->id)
            ->select([
                'tutor_academic_jobs.*',
                'users.name as user_name',
                'users.email as user_email',
                'tutors.phone_no as user_phone'
            ])
            ->get();
        $nationalities = Nationality::all();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('admins.academics.job_reassign_show', compact('sac', 'tutors', 'nationalities', 'states'));
    }

    public function academicJobReassignEdit(Request $request)
    {
        $sac = DB::table('student_academic_classes')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where('student_academic_classes.id', '=', $request->id)
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                
                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $tutors = DB::table('tutor_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_academic_jobs.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutor_academic_jobs.student_academic_class_id', $request->id)
            ->orderBy('tutor_academic_jobs.created_at', 'asc')
            ->select([
                'tutor_academic_jobs.*',
                'users.name as user_name',
                'users.email as user_email',
                'tutors.phone_no as user_phone'
            ])
            ->get();
        $nationalities = Nationality::all();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('admins.academics.job_reassign_edit', compact('sac', 'tutors', 'nationalities', 'states'));
    }

    public function academicJobReassignUpdate(Request $request)
    {
        //tutor_job gives tutor_academic_job->id
        $input = $request->all();

        $tutor_job_id = strip_tags($input['tutor_job']);
        $job = DB::table('tutor_academic_jobs')
            ->where('tutor_academic_jobs.id', $tutor_job_id);
        $taj_job = $job->first();

        if ($job->count() > 0) {

            // find other applications by student_academic_class_id
            // make other applications false
            $user_id = Auth()->user()->id;

            //tutor status = the tutor is approved for the application
            // status > 1:approved, 2:rejected, 3: pending
            //job_assigned to mark that this application is settled

            $assign_job = TutorAcademicJob::where('id', $tutor_job_id)->update([
                'tutor_status' => 1,
                'job_assigned' => 1,
                'job_assigned_date' => Carbon::now(),
                // 'job_assigned_by' =>  $user_id
                'job_assigned_modified_by' => $user_id
            ]);

            $update_student_academic_class = StudentAcademicClass::where('id', $taj_job->student_academic_class_id)->update([
                'tutor_id' => $taj_job->user_id,
                'job_offer_active' => 0
            ]);

            //update other application
            $others = DB::table('tutor_academic_jobs')
                ->where([
                    ['tutor_academic_jobs.student_academic_class_id', '=', $taj_job->student_academic_class_id],
                    ['tutor_academic_jobs.id', '!=', $taj_job->id]
                ])
                ->get();

            foreach ($others as $other) {
                $other_applications = TutorAcademicJob::where('id', $other->id)->update([
                    'tutor_status' => NULL,
                    'job_assigned' => 1,
                ]);
            }

            $code = 'success';
            $message = 'Job assigned successfully.';
        } else {
            $code = 'warning';
            $message = 'Fail to assign job. Please try again later.';
        }

        return redirect()->route('admins.academics.job_assigned_list')->with($code, $message);
    }



    /** Tutors - CRUD */

    public function tutorIndex(Request $request)
    {
        $data = DB::table('users')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->join('user_memberships', 'user_memberships.user_id', '=', 'users.id')
            ->select('users.*', 'tutors.account_approved as account_status', 'user_memberships.status as status')
            ->paginate(5);

        return view('admins.users.tutors.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function tutorCreate()
    {
        $nationalities = DB::table('nationalities')->get();
        $roles = Role::pluck('name', 'name')->all();
        return view('admins.users.tutors.create', compact('roles', 'nationalities'));
    }

    public function tutorStore(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'same:confirm-password',
            'role' => 'required',
            'phone_no' => 'required',
            'nationality' => 'required',
            'gender' => 'required'
        ]);

        $input = $request->all();

        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);

        // to send email verification link
        event(new Registered($user));

        $user_id = $user->id;
        $user->assignRole($input['role']);

        if ($input['gender'] = 'male') {
            $gender = '1';
        } elseif ($input['gender'] = 'female') {
            $gender = '0';
        }

        if (!empty($input['mykad'])) {
            $mykad = $input['mykad'];
            $passport = null;
        } elseif (!empty($input['passport'])) {
            $passport = $input['passport'];
            $mykad = null;
        }

        //if save user success
        Tutor::create([
            'user_id' => $user_id,
            'mykad_no' => $mykad,
            'passport_no' => $passport,
            'nationality_id' => $input['nationality'],
            'gender_id' => $gender,
            'phone_no' => $input['phone_no']
        ]);

        UserMembership::create([
            'user_id' => $user_id,
            'status' => 'not active'
        ]);

        return redirect()->route('admins.users.tutors.index')->with('success', 'User created successfully');
    }

    public function tutorShow($id)
    {
        $tutor = DB::table('tutors')
            ->leftJoin('users', 'users.id', '=', 'tutors.user_id')
            // ->leftJoin('marital_statuses', 'admins.marital_status_id', '=', 'marital_statuses.id')
            ->select([
                'tutors.*', 'users.name as user_name', 'users.email as user_email'
            ])
            ->where('tutors.user_id', $id)
            ->first();

        $membership = DB::table('user_memberships')->where('user_id', $id)->first();
        $nationalities = DB::table('nationalities')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $maritals = DB::table('marital_statuses')->get();
        $races = DB::table('races')->get();
        $religions = DB::table('religions')->get();

        return view(
            'admins.users.tutors.show',
            compact('tutor', 'nationalities', 'states', 'maritals','races', 'religions', 'membership')
        );
    }

    public function tutorEdit($id)
    {
        $tutor = DB::table('tutors')
            ->leftJoin('users', 'users.id', '=', 'tutors.user_id')
            // ->leftJoin('marital_statuses', 'admins.marital_status_id', '=', 'marital_statuses.id')
            ->select([
                'tutors.*', 'users.name as user_name', 'users.email as user_email'
            ])
            ->where('tutors.user_id', $id)
            ->first();

        $membership = DB::table('user_memberships')->where('user_id', $id)->first();

        $nationalities = DB::table('nationalities')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $maritals = DB::table('marital_statuses')->get();

        return view(
            'admins.users.tutors.edit',
            compact('tutor', 'nationalities', 'states', 'maritals', 'membership')
        );
    }

    public function tutorMembershipUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
            'expiry_date' => 'required',
            'payment_date' => 'required',
        ]);

        $status_id = strip_tags($request->status);

        switch($status_id){
            case 1 :
                $status = 'active';
            break;

            case 2 :
                $status = 'not active';
            break;

            case 3:
                $status = 'revoke';
            break;
        }
       
        $user = Auth()->user();


        $membership = UserMembership::find($id);
        
        if(!is_null($membership['expiry_date'])){
            MembershipLog::create([
                'user_id' => $membership['user_id'],
                'last_expiry_date' => $membership['expiry_date'],
                'last_payment_date' => $membership['payment_date'],
                'admin_id' => $membership['admin_id'],
                'admin_remark' => $membership['admin_remark']
            ]);
        }
        $membership->status = $status;
        $membership->expiry_date = strip_tags($request->expiry_date);
        $membership->payment_date = strip_tags($request->payment_date);
        $membership->admin_id = $user->id;
        $membership->admin_remark = strip_tags($request->remark);
        $membership->save();

        return redirect()->route('admins.users.tutors.index')->with('success', 'User membership status updated successfully');
    }

    public function tutorUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
            'roles' => 'required',
            // 'phone_no' => 'required'
        ]);

        $input = $request->all();

        $user = User::find($id);
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = Hash::make($input['password']);
        $user->save();

        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($request->input('roles'));

        if ($input['gender'] = 'male') {
            $gender = '1';
        } elseif ($input['gender'] = 'female') {
            $gender = '0';
        }

        if (!empty($input['mykad'])) {
            $mykad = $input['mykad'];
            $passport = null;
        } elseif (!empty($input['passport'])) {
            $passport = $input['passport'];
            $mykad = null;
        }

        $tutor = Tutor::findByUserid($id);
        $tutor->mykad_no = $mykad;
        $$tutor->passport_no = $passport;
        $$tutor->nationality_id = $input['nationality'];
        $$tutor->gender_id = $gender;
        $$tutor->phone_no = $input['phone_no'];
        $tutor->save();

        return redirect()->route('admins.users.tutors.index')->with('success', 'User updated successfully');
    }

    public function tutorDelete($id)
    {
        $tutor = Tutor::where('user_id', $id)->first();
        $spm = json_decode($tutor->spm_results);
        $high = json_decode($tutor->highest_education_results);

        $membership = UserMembership::where('user_id', $id)->first();

        if (!is_null($spm->image)) {
            Storage::delete("tutors/pics/{$tutor->image}");
        }
       
        if (!is_null($spm)) {
            Storage::delete("tutors/education/{$spm->image}");
        }

        if(!is_null($high)){
            Storage::delete("tutors/education/{$high->image}");
        }
        

        if(!is_null($membership->payment_reference_no)){
        Storage::delete("tutors/membership/{$membership->payment_reference_no}");
        }

        $tutor->delete();
        User::find($id)->delete();

        return redirect()->route('admins.users.tutors.index')
            ->with('success', 'User deleted successfully');
    }

    /** Students - CRUD */

    public function studentIndex(Request $request)
    {
        $data = DB::table('users')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->select('users.*')
            ->paginate(5);

        return view('admins.users.students.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function studentCreate()
    {
        $nationalities = DB::table('nationalities')->get();
        $roles = Role::pluck('name', 'name')->all();
        return view('admins.users.students.create', compact('roles', 'nationalities'));
    }

    public function studentStore(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'same:confirm-password',
            'role' => 'required',
            'phone_no' => 'required',
            'nationality' => 'required',
            'gender' => 'required'
        ]);

        $input = $request->all();

        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);

        // to send email verification link
        event(new Registered($user));

        $user_id = $user->id;
        $user->assignRole($input['role']);

        if ($input['gender'] = 'male') {
            $gender = '1';
        } elseif ($input['gender'] = 'female') {
            $gender = '0';
        }

        if (!empty($input['mykad'])) {
            $mykad = $input['mykad'];
            $passport = null;
        } elseif (!empty($input['passport'])) {
            $passport = $input['passport'];
            $mykad = null;
        }

        //if save user success
        Student::create([
            'user_id' => $user_id,
            'mykad_no' => $mykad,
            'passport_no' => $passport,
            'nationality_id' => $input['nationality'],
            'gender_id' => $gender,
            'phone_no' => $input['phone_no']
        ]);

        return redirect()->route('admins.users.students.index')->with('success', 'User created successfully');
    }

    public function studentShow($id)
    {
        $student = DB::table('students')
            ->leftJoin('users', 'users.id', '=', 'students.user_id')
            ->select([
                'students.*', 'users.name as user_name', 'users.email as user_email'
            ])
            ->where('students.user_id', $id)
            ->first();

        $nationalities = DB::table('nationalities')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $maritals = DB::table('marital_statuses')->get();

        return view('admins.users.students.show', compact('student', 'nationalities', 'states', 'maritals'));
    }

    public function studentEdit($id)
    {
        $student = DB::table('students')
            ->leftJoin('users', 'users.id', '=', 'students.user_id')
            ->select([
                'students.*', 'users.name as user_name', 'users.email as user_email'
            ])
            ->where('students.user_id', $id)
            ->first();

        $nationalities = DB::table('nationalities')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $maritals = DB::table('marital_statuses')->get();

        return view('admins.users.students.edit', compact('student', 'nationalities', 'states', 'maritals'));
    }

    public function studentUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
            'roles' => 'required',
            // 'phone_no' => 'required'
        ]);

        $input = $request->all();

        $user = User::find($id);
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = Hash::make($input['password']);
        $user->save();

        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($request->input('roles'));

        if ($input['gender'] = 'male') {
            $gender = '1';
        } elseif ($input['gender'] = 'female') {
            $gender = '0';
        }

        if (!empty($input['mykad'])) {
            $mykad = $input['mykad'];
            $passport = null;
        } elseif (!empty($input['passport'])) {
            $passport = $input['passport'];
            $mykad = null;
        }

        $student = Student::findByUserid($id);
        $student->mykad_no = $mykad;
        $student->passport_no = $passport;
        $student->nationality_id = $input['nationality'];
        $student->gender_id = $gender;
        $student->phone_no = $input['phone_no'];
        $student->save();

        return redirect()->route('admins.users.students.index')->with('success', 'User updated successfully');
    }

    public function studentDelete($id)
    {
        User::find($id)->delete();
        $student = Student::where('user_id', $id)->first();
        $student->delete();

        return redirect()->route('admins.users.students.index')
            ->with('success', 'User deleted successfully');
    }

    /** Admins - CRUD */

    public function adminIndex(Request $request)
    {
        $data = DB::table('users')
            ->join('admins', 'admins.user_id', '=', 'users.id')
            ->where(
                'admins.id',
                '!=',
                1
            )
            ->select('users.*')
            ->paginate(5);

        return view('admins.users.admins.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function adminCreate()
    {
        $nationalities = DB::table('nationalities')->get();
        $roles = Role::pluck('name', 'name')->all();
        return view('admins.users.admins.create', compact('roles', 'nationalities'));
    }

    public function adminStore(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'same:confirm-password',
            'role' => 'required',
            'phone_no' => 'required',
            'nationality' => 'required',
            'gender' => 'required'
        ]);

        $input = $request->all();

        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);

        // to send email verification link
        event(new Registered($user));

        $user_id = $user->id;
        $user->assignRole($input['role']);

        if ($input['gender'] = 'male') {
            $gender = '1';
        } elseif ($input['gender'] = 'female') {
            $gender = '0';
        }

        if (!empty($input['mykad'])) {
            $mykad = $input['mykad'];
            $passport = null;
        } elseif (!empty($input['passport'])) {
            $passport = $input['passport'];
            $mykad = null;
        }

        //if save user success
        Admin::create([
            'user_id' => $user_id,
            'mykad_no' => $mykad,
            'passport_no' => $passport,
            'nationality_id' => $input['nationality'],
            'gender_id' => $gender,
            'phone_no' => $input['phone_no']
        ]);

        return redirect()->route('admins.users.admins.index')->with('success', 'User created successfully');
    }

    public function adminShow($id)
    {
        $admin = DB::table('admins')
            ->leftJoin('users', 'users.id', '=', 'admins.user_id')
            // ->leftJoin('marital_statuses', 'admins.marital_status_id', '=', 'marital_statuses.id')
            ->select([
                'admins.*', 'users.name as user_name', 'users.email as user_email'
            ])
            ->where('admins.user_id', $id)
            ->first();

        $nationalities = DB::table('nationalities')->get();
        $states = DB::table('states')
            ->orderBy('state', 'ASC')
            ->get();
        $maritals = DB::table('marital_statuses')->get();

        return view(
            'admins.users.admins.show',
            compact('admin', 'nationalities', 'states', 'maritals')
        );
    }

    public function adminEdit($id)
    {
        $admin = DB::table('admins')
            ->leftJoin('users', 'users.id', '=', 'admins.user_id')
            // ->leftJoin('marital_statuses', 'admins.marital_status_id', '=', 'marital_statuses.id')
            ->select([
                'admins.*', 'users.name as user_name', 'users.email as user_email'
            ])
            ->where('admins.user_id', $id)
            ->first();

        $nationalities = DB::table('nationalities')->get();
        $states = DB::table('states')
            ->orderBy('state', 'ASC')
            ->get();
        $maritals = DB::table('marital_statuses')->get();

        return view(
            'admins.users.admins.edit',
            compact('admin', 'nationalities', 'states', 'maritals')
        );
    }

    public function adminUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
            'roles' => 'required',
            // 'phone_no' => 'required'
        ]);

        $input = $request->all();

        $user = User::find($id);
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = Hash::make($input['password']);
        $user->save();

        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($request->input('roles'));

        if ($input['gender'] = 'male') {
            $gender = '1';
        } elseif ($input['gender'] = 'female') {
            $gender = '0';
        }

        if (!empty($input['mykad'])) {
            $mykad = $input['mykad'];
            $passport = null;
        } elseif (!empty($input['passport'])) {
            $passport = $input['passport'];
            $mykad = null;
        }

        $student = Student::findByUserid($id);
        $student->mykad_no = $mykad;
        $student->passport_no = $passport;
        $student->nationality_id = $input['nationality'];
        $student->gender_id = $gender;
        $student->phone_no = $input['phone_no'];
        $student->save();

        return redirect()->route('admins.users.admins.index')->with('success', 'User updated successfully');
    }

    public function adminDelete($id)
    {
        User::find($id)->delete();
        $student = Student::where('user_id', $id)->first();
        $student->delete();

        return redirect()->route('admins.users.admins.index')
            ->with('success', 'User deleted successfully');
    }


    /** OLD - start */
    public function academicsJobList()
    {
        $list = DB::table('student_academic_subjects')
            ->join('users', 'users.id', '=', 'student_academic_subjects.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->select('student_academic_subjects.id', 'student_academic_subjects.updated_at', 'student_academic_subjects.subject', 'student_academic_subjects.location', 'users.name')
            ->orderBy('updated_at', 'asc')
            ->get();

        // dd($list);

        return view('admins.academics.job_list', compact('list'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function detailAcademicJobList(Request $request)
    {
        $input = $request->all();
        $job = DB::table('student_academic_subjects')
            ->join('students', 'students.user_id', '=', 'student_academic_subjects.user_id')
            ->join('users', 'users.id', '=', 'students.user_id')
            ->join('nationalities', 'nationalities.id', '=', 'students.nationality_id')
            ->where('student_academic_subjects.id', $input['request_id'])
            ->first();
        // dd($job);

        $tutors = DB::table('tutors')
            ->join('users', 'users.id', '=', 'tutors.user_id')
            ->orderBy('users.name', 'asc')
            ->select('users.name', 'tutors.id')
            ->get();

        // dd($tutors);

        return view('admins.academics.detail_job_list', compact('job', 'tutors'));
    }

    public function assignJobTutor(Request $request)
    {
        $input = $request->all();
        $student_tutor = new StudentTutorAcademic();
        $student_tutor->tutor_id = request('tutor_id');
        $student_tutor->student_academic_subject_id = request('job_id');
        $student_tutor->save();

        return redirect()->route('admins.dashboard')->with('success', 'Successful! Tutor has been assigned for job.');
    }

    public function assignJobList()
    {
        $jobs = DB::table('student_tutor_academics')->orderBy('updated_at', 'desc')->get();

        return view('admins.academics.assign_job_list', compact('jobs'));
    }
}
