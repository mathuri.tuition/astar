<?php

namespace App\Http\Controllers;

use App\Curriculum;
use App\AcademicClass;
use App\EducationLevel;
use App\Standard;
use App\NonAcademicClass;
use App\StudentAcademicClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;


class ClassController extends Controller
{
    public function academicClassIndex(Request $request)
    {
        $datas = DB::table('academic_classes')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->select('academic_classes.name AS name', 'curricula.curriculum AS curriculum',
             'levels.level as level','levels.price AS price', 'academic_classes.id AS id')
            ->paginate(5);   

        return view('classes.academics.index', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function showCreateAcademicClasses()
    {
        $curricula = Curriculum::all();
        $levels = DB::table('levels')->orderBy('id', 'asc')->get();

        return view('classes.academics.create', compact('curricula', 'levels'));
    }

    public function storeAcademicClasses(Request $request)
    {
        request()->validate([
            'subject' => 'required',
            'level' => 'required',
        ]);

        $input = $request->all();

        $user_id = Auth()->user()->id;

        $insert_data = [
            'name' => strip_tags($input['subject']),
            'level_id' => strip_tags($input['level']),
            'created_by' => strip_tags($user_id),
            'modified_by' => strip_tags($user_id)
        ];

        AcademicClass::create($insert_data);
        
        return redirect()->route('classes.academics.index')->with('success', 'Class created successfully.');
    }

    public function showAcademicClass(Request $request)
    {
        $class = DB::table('academic_classes')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where('academic_classes.id', '=', $request->id)
            ->first();
        
        
        return view('classes.academics.show', compact('class'));
    }

    public function showAcademicClassEdit(Request $request)
    {
        $class = DB::table('academic_classes')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where('academic_classes.id', '=', $request->id)
            ->select(
                'academic_classes.name AS name',
                'curricula.id AS curriculum_id',
                'levels.id as level_id',
                'levels.price AS price',
                'academic_classes.id AS id'
            )
            ->first();

        $curricula = Curriculum::all();
        $levels = DB::table('levels')->orderBy('id', 'asc')->get();
      

        return view('classes.academics.edit', compact('class', 'curricula', 'levels'));
    }

    public function academicClassUpdate(Request $request, $id)
    {
        request()->validate([
            'subject' => 'required',
            'curriculum' => 'required',
            'level' => 'required',
        ]);

        $input = $request->all();

        $user_id = Auth()->user()->id;

        AcademicClass::where('id', $id)->update(
            [
                'name' => $input['subject'],
                'level_id' => $input['level'],
                'modified_by' => $user_id,
            ]
        );

        return redirect()->route('classes.academics.index')->with('success', 'Class updated successfully');
    }

    public function academicClassDelete($id)
    {
        AcademicClass::find($id)->delete();
        return redirect()->route('classes.academics.index')
            ->with('success', 'Class deleted successfully');
    }

    /** Non Academic */

    public function nonAcademicIndex(Request $request)
    {
        $datas = DB::table('non_academic_classes')
            // ->select(
            //     'academic_classes.name AS name',
            //     'curricula.curriculum AS curriculum',
            //     'levels.level AS level',
            //     'academic_classes.price AS price',
            //     'academic_classes.id AS id'
            // )
            ->paginate(5);

        return view('classes.nonacademics.index', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function nonAcademicCreate()
    {
        return view('classes.nonacademics.create');
    }

    public function nonAcademicStore(Request $request)
    {
        request()->validate([
            'class' => 'required',
            'level' => 'required',
            'price' => 'required',
        ]);

        $input = $request->all();
        $user_id = Auth()->user()->id;

        $insert_data = [
            'class' => strip_tags($input['class']),
            'price' => strip_tags($input['price']),
            'level_id' =>  strip_tags($input['level']),
            'created_by' => $user_id,
            'modified_by' => $user_id
        ];

        NonAcademicClass::create($insert_data);

        return redirect()->route('classes.nonacademics.index')->with('success', 'Class created successfully.');
    }

    public function nonAcademicShow(Request $request)
    {
        $class = DB::table('non_academic_classes')
            ->where('non_academic_classes.id', '=', $request->id)
            ->first();
        
        return view('classes.nonacademics.show', compact('class'));
    }

    public function nonAcademicEdit(Request $request)
    {
        $class = DB::table('non_academic_classes')
            ->where('non_academic_classes.id', '=', $request->id)
            ->first();
        return view('classes.nonacademics.edit', compact('class'));
    }

    public function nonAcademicUpdate(Request $request, $id)
    {
        request()->validate([
            'class' => 'required',
            'price' => 'required'
        ]);

        $input = $request->all();

        $user_id = Auth()->user()->id;

        NonAcademicClass::where('id', $id)->update(
            [
                'class' => strip_tags($input['class']),
                'price' => strip_tags($input['price']),
                'modified_by' => $user_id,
            ]
        );

        return redirect()->route('classes.nonacademics.index')->with('success', 'Class updated successfully');
    }

    public function nonAcademicDelete($id)
    {
        NonAcademicClass::find($id)->delete();
        return redirect()->route('classes.nonacademics.index')
            ->with('success', 'Class deleted successfully');
    }

}
