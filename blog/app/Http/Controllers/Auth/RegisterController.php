<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Student;
use App\Tutor;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Mail\VerifyMail;
use App\Membership;
use App\UserMembership;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
   

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required', 'min:8', 'string', 'same:password'],
            'role' => ['required'],
            'gender' => ['required'],
            'nationality' => ['required'],
            'phone_no' => ['required'],
            'terms' => ['required']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    // protected function create(Request $request)
    {
        // $data = $request->all();

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $user_id = $user->id;
        $user->assignRole($data['role']);

        if($data['gender'] = 'male'){
            $gender = '1';
        }
        if($data['gender'] = 'female'){
            $gender = '0';
        }

        if(!empty($data['mykad'])){
            $mykad = $data['mykad'];
            $passport = null;
        }
        if(!empty($data['passport'])){
            $passport = $data['passport'];
            $mykad = null;
        }

        //if save user success
        if($user){
            
            switch ($data['role']) {
                case "admin":
                    Admin::create([
                        'user_id' => $user_id,
                        'mykad_no' => $mykad,
                        'passport_no' => $passport,
                        'nationality_id' => $data['nationality'],
                        'gender_id' => $gender,
                        'phone_no' => $data['phone_no']
                    ]);

                    return $user;
                    break;

                case "tutor":
                    Tutor::create([
                        'user_id' => $user_id,
                        'mykad_no' => $mykad,
                        'passport_no' => $passport,
                        'nationality_id' => $data['nationality'],
                        'gender_id' => $gender,
                        'phone_no' => $data['phone_no']
                    ]);


                    UserMembership::create([
                        'user_id' => $user_id,
                        'status' => 'not active'
                    ]);

                    // $membership = new UserMembership();
                    // $membership->user_id = $user_id;
                    // $membership->status = 'not active';
                    // $membership->save();
                    
                    return $user;
                    break;

                case "student":
                Student::create([
                        'user_id' => $user_id,
                        'mykad_no' => $mykad,
                        'passport_no' => $passport,
                        'nationality_id' => $data['nationality'],
                        'gender_id' => $gender,
                        'phone_no' => $data['phone_no']
                    ]);
                    return $user;
                    break;
                default:
                    return redirect('/');
            }
        }
    }

    public function showRegistrationForm()
    {
        $nationalities = DB::table('nationalities')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $maritals = DB::table('marital_statuses')->get();

        return view('auth.register', compact('nationalities', 'states', 'maritals'));
    }

 




}
