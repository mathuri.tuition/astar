<?php


namespace App\Http\Controllers;

use App\Curriculum;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class SubjectController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware(['auth', 'role:superadmin']); //middleware 

         $this->middleware('permission:subject-list|subject-create|subject-edit|subject-delete', ['only' => ['index','show']]);
         $this->middleware('permission:subject-create', ['only' => ['create','store']]);
         $this->middleware('permission:subject-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:subject-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = DB::table('subjects')
            ->join('curricula', 'curricula.id', '=', 'subjects.curriculum_id')
            ->select('subjects.*', 'curricula.curriculum')
            ->paginate(5);    

        return view('subjects.index',compact('subjects'))->with('i', (request()->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $curricula = Curriculum::all();
        return view('subjects.create', compact('curricula'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'subject' => 'required',
            'curriculum' => 'required',
            'price' => 'required',
        ]);

        $input = $request->all();

        $user_id = Auth()->user()->id;

        $insert_data = [
            'name' => $input['subject'],
            'curriculum_id' => $input['curriculum'],
            'price' => $input['price'],
            'user_id' => $user_id 
        ];

        Subject::create($insert_data);

        return redirect()->route('subjects.index')->with('success','Subject created successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        $subject = DB::table('subjects')
        ->join('curricula', 'curricula.id', '=', 'subjects.curriculum_id')
        ->where('subjects.id', '=' , $subject->id)
        ->first();
        // dd($subject);
        return view('subjects.show',compact('subject'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        $curricula = Curriculum::all();
        return view('subjects.edit',compact('subject','curricula'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
         request()->validate([
            'subject' => 'required',
            'curriculum' => 'required',
            'price' => 'required'
        ]);

        $input = $request->all();

        $user_id = Auth()->user()->id;

        $insert_data = [
            'name' => $input['subject'],
            'curriculum_id' => $input['curriculum'],
            'price' => $input['price'],
            'user_id' => $user_id
        ];

        $subject->update($insert_data);

        return redirect()->route('subjects.index')->with('success','Subject updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        $subject->delete();

        return redirect()->route('subjects.index')->with('success','Subject deleted successfully');
    }
}