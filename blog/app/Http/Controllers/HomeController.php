<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        
        // this will only allow user that have verified email to login
        $this->middleware(['auth'=>'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home');
        $user = Auth::user();

        $role = $user->roles->first()->name;

        switch ($role) {
            case "superadmin" :
                return redirect()->route('superadmins.dashboard');
                break;
            case "admin":
                return redirect()->route('admins.dashboard');
                break;
            case "tutor":
                return redirect()->route('tutors.dashboard');
                break;
            case "student":
                return redirect()->route('students.dashboard');
                break;
            default:
                // return redirect('/');
                $this->guard()->logout();
                
        }

    }
}
