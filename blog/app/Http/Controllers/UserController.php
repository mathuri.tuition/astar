<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Nationality;
use App\Student;
use App\Tutor;
use App\Admin;
use Spatie\Permission\Models\Role;
use DB;
use Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id', 'DESC')->paginate(5);
        return view('users.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nationalities = DB::table('nationalities')->get();
        $roles = Role::pluck('name', 'name')->all();
        return view('users.create', compact('roles', 'nationalities'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'same:confirm-password',
            'role' => 'required',
            'phone_no' => 'required',
            'nationality' => 'required',
            'gender' => 'required'
        ]);

        $input = $request->all();

        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);
        $user_id = $user->id;
        $user->assignRole($input['role']);

        if ($input['gender'] = 'male') {
            $gender = '1';
        } elseif ($input['gender'] = 'female') {
            $gender = '0';
        }

        if (!empty($input['mykad'])) {
            $mykad = $input['mykad'];
            $passport = null;
        } elseif (!empty($input['passport'])) {
            $passport = $input['passport'];
            $mykad = null;
        }

        //if save user success
        switch ($input['role']) {
            
            case "admin":
                Admin::create([
                    'user_id' => $user_id,
                    'mykad_no' => $mykad,
                    'passport_no' => $passport,
                    'nationality_id' => $input['nationality'],
                    'gender_id' => $gender,
                    'phone_no' => $input['phone_no']
                ]);


                break;

            case "tutor":
                Tutor::create([
                    'user_id' => $user_id,
                    'mykad_no' => $mykad,
                    'passport_no' => $passport,
                    'nationality_id' => $input['nationality'],
                    'gender_id' => $gender,
                    'phone_no' => $input['phone_no']
                ]);

                break;

            case "student":
                $student = Student::create([
                    'user_id' => $user_id,
                    'mykad_no' => $mykad,
                    'passport_no' => $passport,
                    'phone_no' => $input['phone_no'],
                    'nationality_id' => $input['nationality'],
                    'gender_id' => $gender,

                ]);
                break;
            default:
                break;
        }

        return redirect()->route('users.index')->with('success', 'User created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show', compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();


        return view('users.edit', compact('user', 'roles', 'userRole'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
            'roles' => 'required',
            // 'phone_no' => 'required'
        ]);

        $input = $request->all();

        $user = User::find($id);
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = Hash::make($input['password']);
        $user->save();

        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($request->input('roles'));

        if ($input['gender'] = 'male') {
            $gender = '1';
        } elseif ($input['gender'] = 'female') {
            $gender = '0';
        }

        if (!empty($input['mykad'])) {
            $mykad = $input['mykad'];
            $passport = null;
        } elseif (!empty($input['passport'])) {
            $passport = $input['passport'];
            $mykad = null;
        }

        //if save user success
        switch ($input['roles']) {
            case "admin":

                $admin = Admin::findByUserid($id);
                $admin->mykad_no = $mykad;
                $admin->passport_no = $passport;
                $admin->nationality_id = $input['nationality'];
                $admin->gender_id = $gender;
                $admin->phone_no = $input['phone_no'];
                $admin->save();
                break;

            case "tutor":
                $tutor = Tutor::findByUserid($id);
                $tutor->mykad_no = $mykad;
                $$tutor->passport_no = $passport;
                $$tutor->nationality_id = $input['nationality'];
                $$tutor->gender_id = $gender;
                $$tutor->phone_no = $input['phone_no'];
                $tutor->save();
                break;

            case "student":
                $student = Student::findByUserid($id);
                $student->mykad_no = $mykad;
                $student->passport_no = $passport;
                $student->nationality_id = $input['nationality'];
                $student->gender_id = $gender;
                $student->phone_no = $input['phone_no'];
                $student->save();
                break;
            default:
                break;
        }


        return redirect()->route('users.index')->with('success', 'User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
            ->with('success', 'User deleted successfully');
    }
}
