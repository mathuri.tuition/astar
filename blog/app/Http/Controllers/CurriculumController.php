<?php


namespace App\Http\Controllers;


use App\Curriculum;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;


class CurriculumController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // function __construct()
    // {
    //     $this->middleware(['auth', 'role:superadmin|admin']); //middleware 
        
    //      $this->middleware('permission:curriculum-list|curriculum-create|curriculum-edit|curriculum-delete', ['only' => ['index','show']]);
    //      $this->middleware('permission:curriculum-create', ['only' => ['create','store']]);
    //      $this->middleware('permission:curriculum-edit', ['only' => ['edit','update']]);
    //      $this->middleware('permission:curriculum-delete', ['only' => ['destroy']]);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $curricula = DB::table('curricula')
        ->join('users', 'users.id', '=', 'curricula.user_id')
        ->select('curricula.*', 'users.name')
        ->paginate(5);
        
        return view('curricula.index',compact('curricula'))->with('i', (request()->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('curricula.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'curriculum' => 'required',
        ]);

        $user_id = Auth()->user()->id;

        $input = $request->all();

        $insert_data = [
            'curriculum' => $input['curriculum'],
            'user_id' => $user_id
        ];

        Curriculum::create($insert_data);

        return redirect()->route('curricula.index')->with('success','Curriculum created successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Curriculum  $curriculum
     * @return \Illuminate\Http\Response
     */
    public function show(Curriculum $curriculum)
    {
        return view('curricula.show',compact('curriculum'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Curriculum  $curriculum
     * @return \Illuminate\Http\Response
     */
    public function edit(Curriculum $curriculum)
    {
        return view('curricula.edit',compact('curriculum'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Curriculum  $curriculum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Curriculum $curriculum)
    {
         request()->validate([
            'curriculum' => 'required',
        ]);


        $curriculum->update($request->all());


        return redirect()->route('curricula.index')->with('success','Curriculum updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Curriculum  $curriculum
     * @return \Illuminate\Http\Response
     */
    public function destroy(Curriculum $curriculum)
    {
        $curriculum->delete();

        return redirect()->route('curricula.index')->with('success','Curriculum deleted successfully');
    }
}