<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use App\Nationality;
use App\NonAcademicClass;
use App\User;
use App\Student;
use App\StudentAcademicClass;
use App\StudentNonAcademicClass;
use Storage;
use App\Standard;
use App\State;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:student|superadmin']); //middleware 

        $this->middleware('permission:student-dashboard', ['only' => ['dashboard']]);
        $this->middleware('permission:student-profile', ['only' => ['profile', 'updateProfile']]);
        $this->middleware('permission:student-request_tutor', ['only' => ['requeststudent']]);
    }

    public function dashboard()
    {
        $user_id = auth()->user()->id;

        $request = DB::table('student_academic_classes')
            ->select(
                DB::raw('count(id) AS total, 
            COUNT((CASE WHEN request_status = 1 THEN 1 ELSE NULL END)) AS approve, 
            COUNT((CASE WHEN request_status = 2 THEN 1 ELSE NULL END)) AS reject, 
            COUNT((CASE WHEN request_status = 3 THEN 1 ELSE NULL END)) AS pending')
            )
            // ->groupBy('request_status')
            ->where('user_id', $user_id)
            ->first();

        // dd($request);

        if (is_null($request)) {
            $request = (object) array(
                "total" => 0,
                "approve" => 0,
                "pending" => 0,
                "reject" => 0,
            );
        }

        return view('students.dashboard', compact('request'));
    }

    public function profile()
    {
        $id = Auth()->user()->id;
        $student = DB::table('students')
            ->leftJoin('users', 'users.id', '=', 'students.user_id')
            ->select([
                'students.*', 'users.name as user_name', 'users.email as user_email'
            ])
            ->where('students.user_id', $id)
            ->first();

        $nationalities = DB::table('nationalities')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $maritals = DB::table('marital_statuses')->get();
        $races = DB::table('races')->orderBy('race', 'ASC')->get();
        $religions = DB::table('religions')->orderBy('religion', 'ASC')->get();

        return view('students.profile', compact('student', 'nationalities', 'states', 'maritals', 'races', 'religions'));
    }

    public function updateProfilePublicInfo(Request $request)
    {
        $user_id = Auth()->user()->id;

        $rules = [
            'name'     => 'required|string|min:3|max:191',
            'nationality' => 'required',
            'phone' => 'required',
            'nationality' => 'required',
            'gender' => 'required',
            // 'email'    => 'required|email|min:3|max:191',
            'password' => 'nullable|string|min:5|max:191',
            'image'    => 'nullable|image|max:1999', //formats: jpeg, png, bmp, gif, svg
        ];
        $request->validate($rules);

        $user = Auth::user();
        $user->name = $request->name;
        $user->save();


        $admin = Student::where('user_id', $user->id)->first();
        $admin->phone_no = $request->phone;
        $admin->nationality_id = $request->nationality;
        $admin->mykad_no = $request->mykad;
        $admin->passport_no = $request->passport;
        $admin->gender_id = $request->gender;

        if ($request->hasFile('image')) {
            $image = $request->image;
            $ext = $image->getClientOriginalExtension();
            $filename = uniqid() . '.' . $ext;
            $image->storeAs('students/pics', $filename);
            Storage::delete("students/pics/{$admin->image}");
            $admin->image = $filename;
        }

        $admin->save();


        return redirect()
            ->route('students.profile')
            ->with('success', 'Your profile has been updated!');
    }

    public function updateProfilePrivateInfo(Request $request)
    {
        $rules = [
            'race'     => 'required',
            'religion'    => 'required',
            'marital_status' => 'required',
            'address_1' => 'required',
            'address_2' => 'required',
            'address_3' => 'nullable',
            'address_4' => 'nullable',
            'city' => 'required',
            'state' => 'required',
            'postcode' => 'required',

        ];
        $request->validate($rules);

        $user_id = Auth()->user()->id;

        $admin = Student::where('user_id', $user_id)->first();
        $admin->address_1 = $request->address_1;
        $admin->address_2 = $request->address_2;
        $admin->address_3 = $request->address_3;
        $admin->address_4 = $request->address_4;
        $admin->city = $request->city;
        $admin->state_code = $request->state;
        $admin->postcode = $request->postcode;
        $admin->race = $request->race;
        $admin->religion = $request->religion;
        $admin->marital_status_id = $request->marital_status;
        $admin->save();


        return redirect()
            ->route('students.profile')
            ->with('success', 'Your profile has been updated!');
    }

    public function changePassword(Request $request)
    {
        $rules = [
            'old_password' => 'required',
            'new_password' => 'required|string|min:5|max:191|different:old_password',
            'verify_password' => 'required|same:new_password'
        ];
        $request->validate($rules);


        if (Hash::check($request->get('old_password'), Auth::user()->password)) {

            $user = Auth::user();
            $user->password = Hash::make($request->verify_password);
            $user->save();

            // correct password and change success
            $code = 'success';
            $message = 'Your password has been updated successfully !';
        } else {
            // dd("keluar");
            // wrong current password
            $code = 'danger';
            $message = 'Sorry, current password is not valid !';
        }

        return redirect()
            ->route('students.profile')
            ->with($code, $message);
    }

    // Academic
    public function showApplyForClass(Request $request)
    {
        $curricula = DB::table('curricula')->get();
        $classes = DB::table('academic_classes')->get();
        $levels =  DB::table('levels')->orderBy('id', 'asc')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('students.academics.request_class', compact('curricula', 'classes', 'states', 'levels'));
    }

    public function academicClassApplySubmit(Request $request)
    {
        request()->validate([
            'subject' => 'required',
            'state' => 'required',
            'city' => 'required',
            'area' => 'required',
            'postcode' => 'required|numeric',

            'type' => 'required',
            'pax' => 'required',
            'frequency' => 'required',
            'day' => 'required',
            'duration' => 'required',
            'time' => 'required',

            'gender' => 'required',
            'race' => 'required',
            'preference' => 'nullable',
        ]);

        $user_id = Auth()->user()->id;
        $input = $request->all();

        foreach ($input['day'] as $day) {
            $days[] = $day;
        }

        switch ($input['gender']) {
            case "male":
                $gender = 1;
                break;
            case "female":
                $gender = 2;
                break;
            case "any":
                $gender = 3;
                break;
            default:
                $gender = 3;
        }

        $insert = new StudentAcademicClass();
        $insert->user_id =  $user_id;
        $insert->academic_class_id = strip_tags($input['subject']);
        $insert->type = strip_tags($input['type']);
        $insert->pax = strip_tags($input['pax']);
        $insert->frequency = strip_tags($input['frequency']);
        $insert->duration = strip_tags($input['duration']);
        $insert->days = json_encode($days);
        $insert->time = strip_tags($input['time']);
        $insert->state = strip_tags($input['state']);
        $insert->city = strip_tags($input['city']);
        $insert->postcode = strip_tags($input['postcode']);
        $insert->area = strip_tags($input['area']);
        $insert->tutor_gender = $gender;
        $insert->tutor_race = strip_tags($input['race']);
        $insert->tutor_preference = strip_tags($input['preference']);
        // status pending because need admin approval
        //1 approve, 2 reject, 3 pending
        $insert->request_status = 3;
        $insert->save();


        $ref_no = sprintf("%'.06d", $insert->id);

        StudentAcademicClass::where('id', $insert->id)->update(
            [
                'academic_reference_no' => "AC_" . $ref_no,
            ]
        );
        return redirect()->route('students.academics.request_list')->with('success', 'Request successful.');
    }

    public function academicClassApplicationsIndex(Request $request)
    {
        $user_id = Auth()->user()->id;

        $applications = DB::table('student_academic_classes')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->where([
                ['student_academic_classes.user_id', '=', $user_id],
                ['student_academic_classes.status', '=', 1],
                ['student_academic_classes.tutor_id', '=', NULL]
            ])
            ->orderBy('student_academic_classes.created_at', 'DESC')
            ->select('student_academic_classes.*', 'academic_classes.name AS class_name', 'users.name AS user_name')
            ->paginate(5);

        return view('students.academics.request_list', compact('applications'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function showClassApplication(Request $request)
    {
        $user_id = Auth()->user()->id;

        $class = DB::table('student_academic_classes')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where([
                ['student_academic_classes.academic_reference_no', '=', $request->ref_no],
                ['student_academic_classes.user_id', '=', $user_id]
            ])
            ->select(
                'users.name AS user_name',
                'users.email AS user_email',
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                'levels.price as price'
            )
            ->first();

        if (is_null($class)) {
            abort(403, 'Unauthorized action.');
        }

        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('students.academics.request_show', compact('class', 'states'));
    }

    public function showAcademicClassApplicationEdit(Request $request)
    {
        $user_id = Auth()->user()->id;
        $class = DB::table('student_academic_classes')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where([
                'student_academic_classes.academic_reference_no' => $request->ref_no,
                'student_academic_classes.user_id' => $user_id,
                'student_academic_classes.status' => 1,
            ])
            ->whereNotIn('student_academic_classes.request_status', [1])
            ->select(
                'users.name AS user_name',
                'users.email AS user_email',
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                'levels.price as price'
            )
            ->first();

        if (is_null($class)) {
            abort(403, 'Unauthorized action.');
        }

        $curricula = DB::table('curricula')->get();
        $classes = DB::table('academic_classes')->get();
        $levels =  DB::table('levels')->orderBy('id', 'asc')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('students.academics.request_edit', compact('class', 'curricula', 'classes', 'levels', 'states'));
    }

    public function academicClassApplicationUpdate(Request $request, $id)
    {
        request()->validate([
            'state' => 'required',
            'city' => 'required',
            'area' => 'required',
            'postcode' => 'required|numeric',

            'type' => 'required',
            'pax' => 'required',
            'frequency' => 'required',
            'day' => 'required',
            'duration' => 'required',
            'time' => 'required',

            'gender' => 'required',
            'race' => 'required',
            'preference' => 'nullable',
        ]);

        $user_id = Auth()->user()->id;
        $input = $request->all();

        foreach ($input['day'] as $day) {
            $days[] = $day;
        }

        switch ($input['gender']) {
            case "male":
                $gender = 1;
                break;
            case "female":
                $gender = 2;
                break;
            case "any":
                $gender = 3;
                break;
            default:
                $gender = 3;
        }

        StudentAcademicClass::where('id', $id)->update(
            [
                'type' => strip_tags($input['type']),
                'pax' => strip_tags($input['pax']),
                'frequency' => strip_tags($input['frequency']),
                'duration' => strip_tags($input['duration']),
                'days' => json_encode($days),
                'time' => strip_tags($input['time']),
                'state' => strip_tags($input['state']),
                'city' => strip_tags($input['city']),
                'postcode' => strip_tags($input['postcode']),
                'area' => strip_tags($input['area']),
                'tutor_gender' => $gender,
                'tutor_race' => strip_tags($input['race']),
                'tutor_preference' => strip_tags($input['preference']),
                'request_status' => 3
            ]
        );

        return redirect()->route('students.academics.request_list')->with('success', 'Class updated successfully');
    }

    public function deleteClassApplication($id)
    {
        StudentAcademicClass::find($id)->delete();
        return redirect()->route('students.academics.request_list')
            ->with('success', 'Application deleted successfully');
    }

    public function cancelRequestByRefNo(Request $request)
    {
        $ref_no = strip_tags($request->ref_no);

        $user = Auth()->user();

        // 1: active | 2:cancel
        $update_student_academic_class = StudentAcademicClass::where([
            ['academic_reference_no', $ref_no],
            ['user_id', '=', $user->id]
        ])
            ->update([
                'status' => 2
            ]);

        if (is_null($update_student_academic_class)) {
            abort(403, 'Unauthorized action.');
        }

        return redirect()->route('students.academics.request_list')
            ->with('success', 'Job updated successfully');
    }

    public function showAcademicClassApplicationCancelledByUser(Request $request)
    {
        $user_id = Auth()->user()->id;

        $applications = DB::table('student_academic_classes')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->where([
                ['student_academic_classes.user_id', '=', $user_id],
                ['student_academic_classes.status', '=', 2]
            ])
            ->orderBy('student_academic_classes.created_at', 'DESC')
            ->select('student_academic_classes.*', 'academic_classes.name AS class_name', 'users.name AS user_name')
            ->paginate(5);

        return view('students.academics.request_cancel_list', compact('applications'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // show list of request which admin assigned tutor already - means found tutor la
    public function academicSuccessIndex(Request $request)
    {
        $user_id = Auth()->user()->id;

        $applications = DB::table('student_academic_classes')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->where([
                ['student_academic_classes.user_id', '=', $user_id],
                ['student_academic_classes.status', '=', 1],
                ['student_academic_classes.tutor_id', '!=', NULL]
            ])
            ->orderBy('student_academic_classes.created_at', 'DESC')
            ->select('student_academic_classes.*', 'academic_classes.name AS class_name', 'users.name AS user_name')
            ->paginate(5);

        return view('students.academics.request_success_index', compact('applications'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function getDocument()
    {
        $academic_price_path = config('app.file.student.academic_rate_list');

        // file not found
        if (!Storage::exists($academic_price_path . 'academic_price.pdf')) {
            abort(404);
        }

        $pdfContent = Storage::get($academic_price_path . 'academic_price.pdf');
        // for pdf, it will be 'application/pdf'
        // $type       = Storage::mimeType($academic_price_path . 'academic_price.pdf');

        return Response::make($pdfContent, 200, [
            'Content-Type'        => 'application/pdf',
            'Content-Disposition' => 'inline; filename="academic_price.pdf"'
        ]);
    }


    public function nonAcademicgetDocument()
    {
        $nonacademic_price_path = config('app.file.student.nonacademic_rate_list');
    
        // file not found
        if (!Storage::exists($nonacademic_price_path . 'nonacademic_price.pdf')) {
            abort(404);
        }
    
        $pdfContent = Storage::get($nonacademic_price_path . 'nonacademic_price.pdf');
        // for pdf, it will be 'application/pdf'
        // $type       = Storage::mimeType($academic_price_path . 'academic_price.pdf');
    
        return Response::make($pdfContent, 200, [
            'Content-Type'        => 'application/pdf',
            'Content-Disposition' => 'inline; filename="nonacademic_price.pdf"'
        ]);
    }




    /** Non Academic Class */
    public function showRequestNonAcademicClass()
    {
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $classes = DB::table('non_academic_classes')->orderBy('class', 'ASC')->get();

        return view('students.nonacademics.request_class', compact('classes', 'states'));
    }

    public function requestNonAcademicClassSubmit(Request $request)
    {
        request()->validate([
            'class' => 'required',
            'level' => 'required',
            'remarks' => 'nullable',

            'state' => 'required',
            'city' => 'required',
            'area' => 'required',
            'postcode' => 'required|numeric',

            'type' => 'required',
            'pax' => 'required',

            'frequency' => 'required',
            'day' => 'required',
            'duration' => 'required',
            'time' => 'required',

            'gender' => 'required',
            'race' => 'required',
            'preference' => 'nullable',
        ]);

        $user_id = Auth()->user()->id;
        $input = $request->all();

        if (strip_tags($request->class) == "") {
            $class_id = NULL;
        } else {
            $class_id = $request->class;
        }

        foreach ($input['day'] as $day) {
            $days[] = $day;
        }

        switch ($input['gender']) {
            case "male":
                $gender = 1;
                break;
            case "female":
                $gender = 2;
                break;
            case "any":
                $gender = 3;
                break;
            default:
                $gender = 3;
        }

        $insert = new StudentNonAcademicClass();
        $insert->user_id =  $user_id;
        $insert->non_academic_class_id = $class_id;

        $insert->level =  strip_tags($input['level']);
        $insert->remarks = strip_tags($input['remarks']);

        $insert->type = strip_tags($input['type']);
        $insert->pax = strip_tags($input['pax']);

        $insert->frequency = strip_tags($input['frequency']);
        $insert->duration = strip_tags($input['duration']);
        $insert->days = json_encode($days);
        $insert->time = strip_tags($input['time']);

        $insert->state = strip_tags($input['state']);
        $insert->city = strip_tags($input['city']);
        $insert->postcode = strip_tags($input['postcode']);
        $insert->area = strip_tags($input['area']);

        $insert->tutor_gender = $gender;
        $insert->tutor_race = strip_tags($input['race']);
        $insert->tutor_preference = strip_tags($input['preference']);
        // status pending because need admin approval
        //1 approve, 2 reject, 3 pending
        $insert->request_status = 3;
        $insert->save();


        $ref_no = sprintf("%'.06d", $insert->id);

        StudentNonAcademicClass::where('id', $insert->id)->update(
            [
                'reference_no' => "NAC_" . $ref_no,
            ]
        );

        // return view('students.dashboard');
        return redirect()->route('students.nonacademics.request_list')->with('success', 'Request successful.');
    }

    public function nonAcademicClassApplicationsIndex(Request $request)
    {
        $user_id = Auth()->user()->id;

        $applications = DB::table('student_non_academic_classes')
            ->leftJoin('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->where([
                ['student_non_academic_classes.user_id', '=', $user_id],
                ['student_non_academic_classes.status', '=', 1]
            ])
            ->orderBy('student_non_academic_classes.created_at', 'DESC')
            ->select('student_non_academic_classes.*', 'non_academic_classes.class AS class_name', 'users.name AS user_name')
            ->paginate(5);

        return view('students.nonacademics.request_list', compact('applications'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function nonAcademicClassApplicationsShow(Request $request)
    {
        $user_id = Auth()->user()->id;

        $class = DB::table('student_non_academic_classes')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where([
                ['student_non_academic_classes.reference_no', '=', $request->ref_no],
                ['student_non_academic_classes.user_id', '=', $user_id]
            ])
            ->select(
                'users.name AS user_name',
                'users.email AS user_email',
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name',
                'non_academic_classes.price as price'
            )
            ->first();

        if (is_null($class)) {
            abort(403, 'Unauthorized action.');
        }

        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('students.nonacademics.request_show', compact('class', 'states'));
    }

    public function nonAcademicClassApplicationsCancelByRefNo(Request $request)
    {
        $ref_no = strip_tags($request->ref_no);

        $user = Auth()->user();

        // 1: active | 2:cancel
        $update_student_nonacademic_class = StudentNonAcademicClass::where([
            ['reference_no', $ref_no],
            ['user_id', '=', $user->id]
        ])
            ->update([
                'status' => 2,
            ]);

        if (is_null($update_student_nonacademic_class)) {
            abort(403, 'Unauthorized action.');
        }

        return redirect()->route('students.nonacademics.request_list')
            ->with('success', 'Job updated successfully');
    }

    public function nonAcademicClassApplicationsCancelledByUser(Request $request)
    {
        $user_id = Auth()->user()->id;

        $applications = DB::table('student_non_academic_classes')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->where([
                ['student_non_academic_classes.user_id', '=', $user_id],
                ['student_non_academic_classes.status', '=', 2]
            ])
            ->orderBy('student_non_academic_classes.created_at', 'DESC')
            ->select('student_non_academic_classes.*', 'non_academic_classes.class AS class_name', 'users.name AS user_name')
            ->paginate(5);


        // dd($applications);

        return view('students.nonacademics.request_cancel_list', compact('applications'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // show list of request which admin assigned tutor already - means found tutor la
    public function nonAcademicSuccessIndex(Request $request)
    {
        $user_id = Auth()->user()->id;

        $applications = DB::table('student_non_academic_classes')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->where([
                ['student_non_academic_classes.user_id', '=', $user_id],
                ['student_non_academic_classes.status', '=', 1],
                ['student_non_academic_classes.tutor_id', '!=', NULL]
            ])
            ->orderBy('student_non_academic_classes.created_at', 'DESC')
            ->select('student_non_academic_classes.*', 'non_academic_classes.class AS class_name', 'users.name AS user_name')
            ->paginate(5);

        return view('students.nonacademics.request_success_index', compact('applications'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /** New */








    /** Old */

    public function updateProfile(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            // 'email' => 'required|email|unique:users,email,' . $id,
            'race' => 'required',
            'religion' => 'required',
            'nationality' => 'required',
            'gender' => 'required',
            'address_1' => 'required',
            'address_2' => 'required',
            // 'address_3' => 'required',
            'postcode' => 'required',
            'city' => 'required',
            'state' => 'required',
        ]);

        $input = $request->all();

        $user = User::find($id);
        $user->update($input);
        User::where('id', $id)->update(['email' => $input['email']]);
        Student::where('user_id', $id)->update([
            'mykad_no' => $input['mykad'],
            'passport_no' => $input['passport'],
            'nationality_id' => $input['nationality'],
            'gender_id' => $input['gender'],
            'marital_status_id' => $input['marital_status'],
            'address_1' => $input['address_1'],
            'address_2' => $input['address_2'],
            'address_3' => $input['address_3'],
            'address_4' => $input['address_4'],
            'postcode' => $input['postcode'],
            'city' => $input['city'],
            'state_code' => $input['state'],
        ]);


        return redirect()->route('students.dashboard')->with('success', 'User updated successfully.');
    }
}
