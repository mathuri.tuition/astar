<?php

namespace App\Http\Controllers;

use App\AcademicClass;
use App\StudentAcademicClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Curriculum;
use App\EducationLevel;
use App\Nationality;
use App\StudentNonAcademicClass;
use App\State;

class JobController extends Controller
{
    // admin to approve the class request posted by student so that tutor can apply
    public function academicIndex(Request $request)
    {
        $datas = DB::table('student_academic_classes')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where([
                ['student_academic_classes.request_status', '=', 3],
                ['student_academic_classes.status', '=', 1],
                ['student_academic_classes.job_offer_active', '=', 1],
                ['student_academic_classes.tutor_id', '=', NULL],
                ['student_academic_classes.job_settled', '=', NULL],
            ])
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'curricula.curriculum AS curriculum',
                'levels.level as level'
            )
            ->orderBy('student_academic_classes.created_at', 'DESC')
            ->paginate(5);

        return view('jobs.academics.index', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function academicShow(Request $request)
    {
        $class = DB::table('student_academic_classes')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where('student_academic_classes.id', '=', $request->id)
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $nationalities = Nationality::all();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('jobs.academics.show', compact('class', 'nationalities', 'states'));
    }

    public function showPageJobApproval(Request $request)
    {
        $class = DB::table('student_academic_classes')
            ->join('users', 'users.id', '=', 'student_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where('student_academic_classes.id', '=', $request->id)
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'levels.level AS edu_level',
                'curricula.curriculum as curriculum',
                'levels.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $nationalities = Nationality::all();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('jobs.academics.job_approval', compact('class', 'nationalities', 'states'));
    }

    public function approveJob(Request $request, $id)
    {
        request()->validate([
            'status' => 'required',
        ]);

        $input = $request->all();
        $user_id = Auth()->user()->id;

        $status = strip_tags($input['status']);
        $remarks = strip_tags($input['admin_remarks']);

        StudentAcademicClass::where('id', $id)->update(
            [
                'request_status' => $status,
                'request_approved_by' => $user_id,
                'request_approved_at' => Carbon::now(),
                'admin_remarks' => $remarks
            ]
        );

        switch ($status) {
            case 1:
                $code = 'success';
                $message = 'Job is approved !';
                break;
            case 2:
                $code = 'danger';
                $message = 'Job is rejected !';
                break;
            case 3:
                $code = 'warning';
                $message = 'Job is pending !';
                break;
            default:
                $code = 'info';
                $message = 'Something went wrong, please try again !';
        }

        return redirect()->route('jobs.academics.index')->with($code, $message);
    }

    public function jobApproved(Request $request)
    {
        $user_id = Auth()->user()->id;

        $datas = DB::table('student_academic_classes')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where([
                ['student_academic_classes.status', '=', 1],
                ['student_academic_classes.request_status', '=', 1],
                ['student_academic_classes.job_settled', '=', NULL],
                ['student_academic_classes.job_offer_active', '=', 1],
                ['student_academic_classes.tutor_id', '=', NULL],
            ])
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'curricula.curriculum AS curriculum',
                'levels.level as level'
            )
            ->orderBy('student_academic_classes.created_at', 'DESC')
            ->paginate(5);

        return view('jobs.academics.job_approved', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function jobRejected(Request $request)
    {
        $user_id = Auth()->user()->id;

        $datas = DB::table('student_academic_classes')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
            ->where([
                ['student_academic_classes.request_status', '=', 2],
                ['student_academic_classes.status', '=', 1],
                ['student_academic_classes.job_offer_active', '=', 1],
                ['student_academic_classes.tutor_id', '=', NULL],
                ['student_academic_classes.job_settled', '=', NULL],
            ])
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'curricula.curriculum AS curriculum',
                'levels.level as level'
            )
            ->orderBy('student_academic_classes.created_at', 'DESC')
            ->paginate(5);

        return view('jobs.academics.job_rejected', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function jobPending(Request $request)
    {
        $user_id = Auth()->user()->id;

        $datas = DB::table('student_academic_classes')
            ->join('academic_classes', 'academic_classes.id', '=', 'student_academic_classes.academic_class_id')
            ->join('curricula', 'curricula.id', '=', 'academic_classes.curriculum_id')
            ->join('levels', 'levels.id', '=', 'academic_classes.level_id')
            ->where([
                ['student_academic_classes.request_status', '=', 3],
                ['student_academic_classes.status', '=', 1],
                ['student_academic_classes.job_offer_active', '=', 0],
                ['student_academic_classes.tutor_id', '=', NULL],
                ['student_academic_classes.job_settled', '=', NULL],
            ])
            ->select(
                'student_academic_classes.*',
                'academic_classes.name AS class_name',
                'curricula.curriculum AS curriculum',
                'levels.level as level'
            )
            ->orderBy('student_academic_classes.created_at', 'DESC')
            ->get();

        return view('jobs.academics.job_pending', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /** Job Applications by tutors */

    public function jobApplicationIndex(Request $request)
    {
        $jobs = DB::table('tutor_academic_jobs')->get();

        return view('jobs.academics.job_application_index', compact('jobs'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function jobApplicationShow(Request $request)
    {
        $job = DB::table('tutor_academic_jobs')
            ->join('users', 'users.id', '=', 'tutor_academic_jobs.user_id')
            ->where('tutor_academic_jobs.id', '=', $request->id)
            ->select('tutor_academic_jobs.*', 'users.email AS user_email')
            ->first();

        return view('jobs.academics.job_application_show', compact('job'));
    }


    /** Non Academic Job */

    // initial class request by students that admin need to approve 
    // status = pending|3
    public function nonAcademicIndex(Request $request)
    {
        $datas = DB::table('student_non_academic_classes')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where([
                ['student_non_academic_classes.status', '=', 1],
                ['student_non_academic_classes.request_status', '=', 3],
                ['student_non_academic_classes.job_settled', '=', NULL],
                ['student_non_academic_classes.tutor_id', '=', NULL],
                ['student_non_academic_classes.job_offer_active', '=', 1],
            ])
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name'
            )
            ->orderBy('student_non_academic_classes.created_at', 'DESC')
            ->paginate(5);

        return view('jobs.nonacademics.index', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function nonAcademicShow(Request $request)
    {
        $class = DB::table('student_non_academic_classes')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where('student_non_academic_classes.id', '=', $request->id)
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name',
                'non_academic_classes.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $nationalities = Nationality::all();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();
        $races = DB::table('races')->get();

        return view('jobs.nonacademics.show', compact('class', 'nationalities', 'states', 'races'));
    }

    // job offers to approve - page to edit status
    public function showPageNonAcademicJobApproval(Request $request)
    {
        $class = DB::table('student_non_academic_classes')
            ->join('users', 'users.id', '=', 'student_non_academic_classes.user_id')
            ->join('students', 'students.user_id', '=', 'users.id')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where('student_non_academic_classes.id', '=', $request->id)
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name',
                'non_academic_classes.price as price',
                'users.name AS user_name',
                'users.email AS user_email',
                'students.image as image',
                'students.phone_no as phone_no',
                'students.nationality_id as nationality_id',
                'students.mykad_no as mykad_no',
                'students.passport_no as passport_no',
                'students.gender_id as gender_id'
            )
            ->first();

        $nationalities = Nationality::all();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();

        return view('jobs.nonacademics.job_approval', compact('class', 'nationalities', 'states'));
    }

    // job offers to approve - update status function
    public function approveNonAcademicJob(Request $request, $id)
    {
        request()->validate([
            'status' => 'required',
        ]);

        $input = $request->all();
        $user_id = Auth()->user()->id;

        $status = strip_tags($input['status']);
        $remarks = strip_tags($input['admin_remarks']);

        StudentNonAcademicClass::where('id', $id)->update(
            [
                'request_status' => $status,
                'request_approved_by' => $user_id,
                'request_approved_at' => Carbon::now(),
                'admin_remarks' => $remarks
            ]
        );

        switch ($status) {
            case 1:
                $code = 'success';
                $message = 'Job approved !';
                break;
            case 2:
                $code = 'warning';
                $message = 'Job pending !';
                break;
            case 3:
                $code = 'danger';
                $message = 'Job rejected !';
                break;
            default:
                $code = 'info';
                $message = 'Something went wrong, please try again !';
        }

        return redirect()->route('jobs.nonacademics.index')->with($code, $message);
    }

    // non academic jobs approved by admin
    public function nonAcademicJobApproved(Request $request)
    {
        $user_id = Auth()->user()->id;

        $datas = DB::table('student_non_academic_classes')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where([
                ['student_non_academic_classes.status', '=', 1],
                ['student_non_academic_classes.request_status', '=', 1],
                ['student_non_academic_classes.job_settled', '=', NULL],
                ['student_non_academic_classes.tutor_id', '=', NULL],
                ['student_non_academic_classes.job_offer_active', '=', 1],
            ])
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name'
            )
            ->orderBy('student_non_academic_classes.created_at', 'DESC')
            ->paginate(5);

        return view('jobs.nonacademics.job_approved', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    // non academic jobs rejected by admin
    public function nonAcademicJobRejected(Request $request)
    {
        $user_id = Auth()->user()->id;

        $datas = DB::table('student_non_academic_classes')
            ->join('non_academic_classes', 'non_academic_classes.id', '=', 'student_non_academic_classes.non_academic_class_id')
            ->where([
                ['student_non_academic_classes.status', '=', 1],
                ['student_non_academic_classes.request_status', '=', 2],
                ['student_non_academic_classes.job_settled', '=', NULL],
                ['student_non_academic_classes.tutor_id', '=', NULL],
                ['student_non_academic_classes.job_offer_active', '=', 1],
            ])
            ->select(
                'student_non_academic_classes.*',
                'non_academic_classes.class AS class_name'

            )
            ->orderBy('student_non_academic_classes.created_at', 'DESC')
            ->paginate(5);

        return view('jobs.nonacademics.job_rejected', compact('datas'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
}
