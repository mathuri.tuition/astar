<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Role;
use App\Admin;
use App\Nationality;
use App\User;
use Hash;
use App\Tutor;
use DB;

class SuperadminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:superadmin']); //middleware 

        $this->middleware('permission:superadmin-dashboard', ['only' => ['dashboard']]);
        $this->middleware('permission:superadmin-profile', ['only' => ['profile', 'update']]);
        $this->middleware('permission:superadmin-job_list', ['only' => ['jobList', 'detailAcademicJobList']]);
    }

    public function dashboard()
    {
        $user = Auth()->user()->roles()->first()->name;
        return view('superadmins.dashboard', compact('user'));
    }

    public function profile()
    {
        $id = Auth()->user()->id;

        $superadmin = DB::table('users')
            ->leftJoin('admins', 'users.id', '=', 'admins.user_id')
            ->leftJoin('marital_statuses', 'admins.marital_status_id', '=', 'marital_statuses.id')
            ->where('admins.user_id', $id)
            ->first();

        $nationalities = DB::table('nationalities')->get();
        $states = DB::table('states')->orderBy('state', 'ASC')->get();


        return view('superadmins.profile', compact('superadmin', 'nationalities', 'states'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function updateProfile(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'nationality' => 'required',
            'gender' => 'required',
            'address_1' => 'required',
            'address_2' => 'required',
            'address_3' => 'required',
            'postcode' => 'required',
            'city' => 'required',
            'state' => 'required',
        ]);

        $input = $request->all();

        $user = User::find($id);
        $user->update($input);
        User::where('id', $id)->update(['email' => $input['email']]);
        Admin::where('user_id', $id)->update([
            'nationality_id' => $input['nationality'],
            'gender_id' => $input['gender'],
            'address_1' => $input['address_1'],
            'address_2' => $input['address_2'],
            'address_3' => $input['address_3'],
            'address_4' => $input['address_4'],
            'postcode' => $input['postcode'],
            'city' => $input['city'],
            'state_code' => $input['state'],
        ]);

        return redirect()->route('superadmins.dashboard')->with('success', 'User updated successfully.');
    }

    public function changePassword(Request $request)
    {
        $rules = [
            'old_password' => 'required',
            'new_password' => 'required|string|min:5|max:191|different:old_password',
            'verify_password' => 'required|same:new_password'
        ];
        $request->validate($rules);


        if (Hash::check($request->get('old_password'), Auth::user()->password)) {

            $user = Auth::user();
            $user->password = Hash::make($request->verify_password);
            $user->save();

            // correct password and change success
            $code = 'success';
            $message = 'Your password has been updated successfully !';
        } else {
            // wrong current password
            $code = 'danger';
            $message = 'Sorry, current password is not valid !';
        }

        return redirect()
            ->route('superadmins.profile')
            ->with($code, $message);
    }


    

    public function tutorList(Request $request)
    {
        $tutors = DB::table('users')
        ->join('tutors', 'tutors.user_id', '=', 'users.id')
        ->select('tutors.id AS tutor_id', 'users.id AS user_id', 'users.name AS name', 'users.email AS email', 'tutors.created_at AS date_joined')
        ->paginate(5);
        // dd($tutors);

        return view('superadmins.tutor_list', compact('tutors'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function showTutor($id)
    {
        $tutor = DB::table('users')
            ->join('tutors', 'tutors.user_id', '=', 'users.id')
            ->where('tutors.id', '=', $id)
            ->first();

        $user = User::find($tutor->user_id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();
        $nationalities = DB::table('nationalities')->get();

        return view('superadmins.tutor_show', compact('tutor', 'roles', 'userRole', 'nationalities'));
    }

    public function showTutorAdd()
    {
        $nationalities = DB::table('nationalities')->get();      
        return view('superadmins.tutor_add', compact('nationalities'));
    }

    public function addTutor(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'same:confirm-password',
            'phone_no' => 'required',
            'nationality' => 'required',
            'gender' => 'required'
        ]);

        $input = $request->all();

        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);

        $user_id = $user->id;
        $role = 'tutor';
        $user->assignRole($role);

        if ($input['gender'] = 'male') {
            $gender = '1';
        } elseif ($input['gender'] = 'female') {
            $gender = '0';
        }

        if (!empty($input['mykad'])) {
            $mykad = $input['mykad'];
            $passport = null;
        } elseif (!empty($input['passport'])) {
            $passport = $input['passport'];
            $mykad = null;
        }

        Tutor::create([
            'user_id' => $user_id,
            'mykad_no' => $mykad,
            'passport_no' => $passport,
            'nationality_id' => $input['nationality'],
            'gender_id' => $gender,
            'phone_no' => $input['phone_no']
        ]);

        return redirect()->route('superadmins.tutor_list')->with('success', 'Tutor created successfully.');

    }

    public function deleteTutor($id)
    {
        $tutor = Tutor::find($id)->first();
        $uid = $tutor->user_id;
        Tutor::find($id)->delete();
        User::find($uid)->delete();

        return redirect()->route('superadmins.tutor_list')->with('success', 'User deleted successfully');
    }

    public function updateTutor(Request $request, $user_id)
    {
        $this->validate($request, [
            'name' => 'required',
            // 'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
            'phone_no' => 'required',
        ]);

        $input = $request->all();

        // dd($input);

        $user = User::find($user_id);
        $user->name = $input['name'];
        $user->save();

        if ($input['gender'] = 'male') {
            $gender = '1';
        } elseif ($input['gender'] = 'female') {
            $gender = '0';
        }

        if (!empty($input['mykad'])) {
            $mykad = $input['mykad'];
            $passport = null;
        } elseif (!empty($input['passport'])) {
            $passport = $input['passport'];
            $mykad = null;
        }

        $tutor_id = DB::table('tutors')->where('user_id', '=',$user_id)->first();
        // dd();
        $tutor = Tutor::find($tutor_id->id)->first();
        // dd($tutor);
        $tutor->mykad_no = $mykad;
        $tutor->passport_no = $passport;
        $tutor->nationality_id = $input['nationality'];
        $tutor->gender_id = $gender;
        $tutor->phone_no = $input['phone_no'];
        $tutor->save();

        return redirect()->route('superadmins.tutor_list')->with('success', 'Tutor details updated successfully');
    }

    public function showTutorEdit($id)
    {
        $tutor = DB::table('users')
        ->join('tutors', 'tutors.user_id', '=', 'users.id')
        ->where('tutors.id', '=', $id)
        ->first();

        $user = User::find($tutor->user_id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();
        $nationalities = DB::table('nationalities')->get();

        return view('superadmins.tutor_edit', compact('tutor', 'roles', 'userRole', 'nationalities'));
    }

    public function showAcademicRequestClass()
    {
        $curricula = DB::table('curricula')->get();
        $subjects = DB::table('subjects')->get();

        return view('superadmins.class_request', compact('curricula','subjects'));
    }

    public function academicClassRequestList()
    {
        $subjects = DB::table('student_academic_subjects')
        ->join('subjects', 'subjects.id', '=', 'student_academic_subjects.subject_id')
        ->join('users', 'users.id', '=', 'student_academic_subjects.user_id')
        ->select('student_academic_subjects.id AS id', 
        'users.name AS name', 'users.email AS email', 'subjects.name AS subject', 
        'student_academic_subjects.created_at AS date')
        ->paginate(5);
        // dd($subjects);
        
        return view('superadmins.class_list', compact( 'subjects'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

}
