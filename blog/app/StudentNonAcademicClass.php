<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class StudentNonAcademicClass extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $guard_name = 'web';

    protected $fillable = [
        'user_id', 'reference_no', 'non_academic_class_id', 'type', 'pax','frequency', 'duration', 'days', 'time', 'remarks',
        'state', 'city', 'postcode', 'area', 'tutor_gender', 'tutor_race', 'tutor_preference',
        'status', 'class_start_date', 'payment_received', 'payment_date', 'job_offer_active',
        'request_status', 'request_approved_by', 'request_approved_at', 'admin_remarks', 'tutor_id'
    ];

    public function user_id()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }

    public function non_academic_class_id()
    {
        return $this->hasOne('App\NonAcademicClass', 'foreign_key');
    }

    public function request_approved_by()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }
}
