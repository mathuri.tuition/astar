<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AcademicClass extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $guard_name = 'web';

    protected $fillable = [
        'name', 'level_id' , 'created_by', 'modified_by'
    ];

    public function created_by()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }

    public function modified_by()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }


    public function level_id()
    {
        return $this->hasOne('App\Level', 'foreign_key');
    }
    
}
