<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Nationality extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guard_name = 'web';
    
    protected $fillable = [
        'nationality'
    ];

    public $timestamps = false;
}
