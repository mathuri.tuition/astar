<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TutorEducationQualification extends Mailable
{
    use Queueable, SerializesModels;

    // protected $data;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data=[])
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.tutors.qualification.education')
          ->subject('Education Qualification')
                ->attach($this->data['spm_image']->getRealPath(),
                [
                    'as' => $this->data['spm_image']->getClientOriginalName(),
                    'mime' => $this->data['spm_image']->getClientMimeType(),
                ]);
    }
}
