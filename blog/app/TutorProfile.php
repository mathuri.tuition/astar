<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class TutorProfile extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $guard_name = 'web';

    protected $fillable = [
        'name', 'motto', 'image', 'email', 'phone_no', 'nationality_id', 'gender_id',
        'race', 'religion', 'marital_status_id', 'subjects', 'summary', 'higher_education_1',
        'higher_education_2', 'higher_education_3', 'experience'
    ];
}
