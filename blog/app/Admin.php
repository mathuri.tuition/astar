<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use OwenIt\Auditing\Contracts\Auditable;

class Admin extends Model implements Auditable
{
    use HasRoles;
    use \OwenIt\Auditing\Auditable;

    protected $guard_name = 'web';

    protected $fillable = [
        'user_id','mykad_no', 'passport_no', 'nationality_id', 'phone_no', 'gender_id', 'race', 'religion', 
        'marital_status_id', 'address_1', 'address_2', 'address_3', 'address_4',
        'postcode', 'city', 'state_code'
    ];

    public function marital_status_id()
    {
        return $this->hasOne('App\MaritalStatus', 'foreign_key');
    }

    public function user_id()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }

    public function state_code()
    {
        return $this->hasOne('App\State', 'foreign_key');
    }

   

    public function nationality_id()
    {
        return $this->hasOne('App\Nationality', 'foreign_key');
    }
}
