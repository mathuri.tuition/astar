<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class MembershipLog extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guard_name = 'web';
    
    protected $fillable = [
        'user_id', 'last_expiry_date', 'last_payment_date', 'admin_id', 'admin_remark'
    ];

    public function user_id()
    {
        return $this->hasMany('App\User', 'foreign_key');
    }
}
