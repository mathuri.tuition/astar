<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class TutorNonAcademicJob extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $guard_name = 'web';

    protected $fillable = [
        'student_non_academic_class_id', 'user_id', 'tutor_status', 'job_assigned', 'job_assigned_date',
        'job_assigned_by', 'job_assigned_modified_by', 'tutor_remarks'
    ];


    public function student_non_academic_class_id()
    {
        return $this->hasOne('App\StudentNonAcademicClass', 'foreign_key');
    }

    public function user_id()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }

    public function job_assigned_by()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }

    public function job_assigned_modified_by()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }
}
