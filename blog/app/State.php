<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class State extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $guard_name = 'web';

    protected $fillable = [
        'code', 'state'
    ];

    public $timestamps = false;

    protected $primaryKey = 'code';

}
