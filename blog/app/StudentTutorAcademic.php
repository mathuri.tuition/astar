<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class StudentTutorAcademic extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $guard_name = 'web';
    
    protected $fillable = [
        'tutor_id', 'student_academic_subject_id', 'payment_received', 'payment_reference', 'payment_date', 'remarks'
    ];

    public function tutor_id()
    {
        return $this->belongsToMany('App\Tutor', 'foreign_key');
    }
}
