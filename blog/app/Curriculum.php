<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Curriculum extends Model implements Auditable
{
  use \OwenIt\Auditing\Auditable;

  protected $guard_name = 'web';
  
  protected $fillable = [
    'curriculum', 'user_id'
  ];

  public function user_id()
  {
    return $this->hasOne('App\User', 'foreign_key');
  }
}
