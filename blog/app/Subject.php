<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
class Subject extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $guard_name = 'web';
    
    protected $fillable = [
        'name', 'curriculum_id', 'price', 'user_id'
    ];

    public function user_id()
    {
        return $this->hasOne('App\User', 'foreign_key');
    }

    public function curriculum_id()
    {
        return $this->hasOne('App\Curriculum', 'foreign_key');
    }
    
}
