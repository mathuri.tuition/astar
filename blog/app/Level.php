<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Level extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guard_name = 'web';

    protected $fillable = [
        'level', 'curriculum_id', 'price'
    ];

    public function curriculum_id()
    {
        return $this->hasOne('App\Curriculum', 'foreign_key');
    }

}
