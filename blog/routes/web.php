<?php

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use App\Newsletter;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;


Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    return "All cache cleared";
});

Route::get('/', function () {
    return view('welcome');
})->name('welcome');


Route::get('/about', function () {
    return view('about');
});


// Route::get('/teachers', function () {

//     $tutors = DB::table('tutor_profiles')->get();

//     return view('teachers', ['tutors' => $tutors]);
// });

Route::get('/courses', function () {

    $levels = DB::table('levels')
    ->join('curricula', 'curricula.id', '=', 'levels.curriculum_id')
    ->select('levels.*', 'curricula.curriculum as curriculum')
    ->paginate(6);

    return view('courses', ['levels' => $levels]);
});

Route::get('/contact-us', function () {
    return view('contact');
});

Route::post('/contact-us/send', function (Request $request) {
    
     $data = array(
                'name' => strip_tags($request->name),
                'email' => strip_tags($request->email),
                'subject' => strip_tags($request->subject),
                'message' => strip_tags($request->message)
            );
            
    // dd($data['name']);

    Mail::send(new ContactMail($data));
    
    return view('contact');
    //   return redirect()->url('/contact-us');
})->name('contactus');

Route::post('/newsletter/subscribe', function (Request $request) {

    $email = strip_tags($request->news_email);

    $check = Newsletter::where('email', $email)->first();

    if (is_null($check)) {
        $insert = new Newsletter();
        $insert->email =  $email;
        $insert->receive_updates = 1;
        $insert->save();
    }

    $data = [
        'success' => true,
        'message' => 'Your email processed correctly'
    ];
   
    return response()->json($data);
    
})->name('newsletter.subscribe');

Route::get('/terms-and-conditions', function () {
    return view('terms');
})->name('terms');



Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register/submit', 'Auth\RegisterController@register')->name('register.submit');
// Route::get('email/verify/{token}', 'Auth\RegisterController@verifyUser')->name('email.verify');


//set route for email verify link
Auth::routes(['verify' => true]);

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/**
 * New
 * 
 */

// superadmin
Route::group(
    [
        'middleware' => ['auth', 'role:superadmin', 'verified'],
    ],
    function () {
        Route::get('superadmin/dashboard', 'SuperadminController@dashboard')->name('superadmins.dashboard');
        Route::get('superadmin/profile', 'SuperadminController@profile')->name('superadmins.profile');
        Route::post('superadmin/profile/update/{id?}', 'SuperadminController@updateProfile')->name('superadmins.profile.update');
        Route::post('superadmin/change-password', 'SuperadminController@changePassword')->name('superadmins.change_password');

        Route::resource('roles', 'RoleController');
        Route::resource('permissions', 'PermissionController');
        Route::resource('superadmins', 'SuperadminController');
        Route::resource('users', 'UserController');
    }
);



//  admin
Route::group(
    [
        'middleware' => ['auth', 'role:admin|superadmin', 'verified'],
        'prefix' => 'admin'
    ],
    function () {

        Route::resource('curricula', 'CurriculumController');
        Route::resource('levels', 'LevelController');

        Route::get('/dashboard', 'AdminController@dashboard')->name('admins.dashboard');
        Route::get('/profile', 'AdminController@profile')->name('admins.profile');
        Route::post('/profile/update/{id}', 'AdminController@updateProfile')->name('admins.profile.update');

        Route::post('/profile/update-public-info', 'AdminController@updateProfilePublicInfo')->name('admins.profile.update_public_info');
        Route::post('/profile/update-private-info', 'AdminController@updateProfilePrivateInfo')->name('admins.profile.update_private_info');
        Route::post('/change-password', 'AdminController@changePassword')->name('admins.change_password');

        /** Create Tutors */
        Route::get('/tutor/index', 'AdminController@tutorIndex')->name('admins.users.tutors.index');
        Route::get('/tutor/show/{id}', 'AdminController@tutorShow')->name('admins.users.tutors.show');
        Route::get('/tutor/create', 'AdminController@tutorCreate')->name('admins.users.tutors.create');
        Route::post('/tutor/store', 'AdminController@tutorStore')->name('admins.users.tutors.store');
        Route::get('/tutor/edit/{id}', 'AdminController@tutorEdit')->name('admins.users.tutors.edit');
        Route::post('/tutor/update/{id}', 'AdminController@tutorUpdate')->name('admins.users.tutors.update');
        Route::delete('/tutor/delete/{id}', 'AdminController@tutorDelete')->name('admins.users.tutors.delete');

        Route::post('/tutor/validation/{id}', 'AdminController@tutorAccountValidation')->name('admins.users.tutors.validation');
        
        Route::get('/tutor/membership/show/{id}', 'AdminController@tutorMembershipShow')->name('admins.users.tutors.membership.show');
        Route::post('/tutor/membership/update/{id}', 'AdminController@tutorMembershipUpdate')->name('admins.users.tutors.membership.update');
        
        Route::post('/tutor/public-info/update/{id}', 'AdminController@tutorUpdateProfilePublicInfo')->name('admins.users.tutors.update_public_info');
        Route::post('/tutor/private-info/update/{id}', 'AdminController@tutorUpdateProfilePrivateInfo')->name('admins.users.tutors.update_private_info');
        Route::post('/tutor/spm-results/update/{id}', 'AdminController@tutorUpdateSpmResults')->name('admins.users.tutors.update_spm_results');
        Route::post('/tutor/spm-certificate/update/{id}', 'AdminController@tutorUpdateSpmQualification')->name('admins.users.tutors.update_spm_certificate');
        Route::post('/tutor/highest-education-results/update/{id}', 'AdminController@tutorUpdateHigherQualification')->name('admins.users.tutors.update_higher_qualification');
        
        
        Route::post('/tutor/class-preference/update-subjects/{id}', 'AdminController@updateTutorSubjects')->name('admins.users.tutors.class_preference.update_subjects');
        Route::post('/tutor/class-preference/update-experience/{id}', 'AdminController@updateTutorExperience')->name('admins.users.tutors.class_preference.update_experience');
        Route::post('/tutor/class-preference/update-areas/{id}', 'AdminController@updateTutorAreas')->name('admins.users.tutors.class_preference.update_areas');
        


        /** Create Students */
        Route::get('/student/index', 'AdminController@studentIndex')->name('admins.users.students.index');
        Route::get('/student/show/{id}', 'AdminController@studentShow')->name('admins.users.students.show');
        Route::get('/student/create', 'AdminController@studentCreate')->name('admins.users.students.create');
        Route::post('/student/store', 'AdminController@studentStore')->name('admins.users.students.store');
        Route::get('/student/edit/{id}', 'AdminController@studentEdit')->name('admins.users.students.edit');
        Route::post('/student/update/{id}', 'AdminController@studentUpdate')->name('admins.users.students.update');
        Route::delete('/student/delete/{id}', 'AdminController@studentDelete')->name('admins.users.students.delete');

        /** Create Admins */
        Route::get('/admin/index', 'AdminController@adminIndex')->name('admins.users.admins.index');
        Route::get('/admin/show/{id}', 'AdminController@adminShow')->name('admins.users.admins.show');
        Route::get('/admin/create', 'AdminController@adminCreate')->name('admins.users.admins.create');
        Route::post('/admin/store', 'AdminController@adminStore')->name('admins.users.admins.store');
        Route::get('/admin/edit/{id}', 'AdminController@adminEdit')->name('admins.users.admins.edit');
        Route::post('/admin/update/{id}', 'AdminController@adminUpdate')->name('admins.users.admins.update');
        Route::delete('/admin/delete/{id}', 'AdminController@adminDelete')->name('admins.users.admins.delete');


        /** Job Management */

        /** Jobs Offers for Admin to approve so that tutors can see the offers */
        Route::get('/jobs/academics/index', 'JobController@academicIndex')->name('jobs.academics.index');
        Route::get('/jobs/academics/show/{id}', 'JobController@academicShow')->name('jobs.academics.show');
        Route::get('/jobs/academics/applications/approval/{id}', 'JobController@showPageJobApproval')->name('jobs.academics.job_approval');
        Route::post('/jobs/academics/applications/approve/{id}', 'JobController@approveJob')->name('jobs.academics.approve_job');
        Route::get('/jobs/academics/applications/approved', 'JobController@jobApproved')->name('jobs.academics.job_approved');
        Route::get('/jobs/academics/applications/rejected', 'JobController@jobRejected')->name('jobs.academics.job_rejected');
        Route::get('/jobs/academics/applications/pending', 'JobController@jobPending')->name('jobs.academics.job_pending');

        /** Non Academic classes - for admin to approve request by students */
        Route::get('/jobs/non-academics/index', 'JobController@nonAcademicIndex')->name('jobs.nonacademics.index');
        Route::get('/jobs/non-academics/show/{id}', 'JobController@nonAcademicShow')->name('jobs.nonacademics.show');
        Route::get('/jobs/non-academics/applications/approval/{id}', 'JobController@showPageNonAcademicJobApproval')->name('jobs.nonacademics.job_approval');
        Route::post('/jobs/non-academics/applications/approve/{id}', 'JobController@approveNonAcademicJob')->name('jobs.nonacademics.approve_job');
        Route::get('/jobs/non-academics/applications/approved', 'JobController@nonAcademicJobApproved')->name('jobs.nonacademics.job_approved');
        Route::get('/jobs/non-academics/applications/rejected', 'JobController@nonAcademicJobRejected')->name('jobs.nonacademics.job_rejected');

        // Job Applications by tutors
        Route::get('/jobs/academics/application/index', 'JobController@jobApplicationIndex')->name('jobs.academics.job_application_index');
        Route::get('/jobs/academics/application/show/{id}', 'JobController@jobApplicationShow')->name('jobs.academics.job_application_show');
        Route::get('/jobs/academics/application/edit/{id}', 'JobController@jobApplicationIndex')->name('jobs.academics.job_application_edit');

        // list of job applications by tutors
        Route::get('/academic/job/application/index', 'AdminController@academicJobApplicationIndex')->name('admins.academics.job_application_index');
        // show job details and applicants - view only
        Route::get('/academic/job/application/show/{id}', 'AdminController@academicJobApplicationShow')->name('admins.academics.job_application_show');
        // Assign the job to which tutor
        Route::get('/academic/job/application/edit/{id}', 'AdminController@academicJobApplicationEdit')->name('admins.academics.job_application_edit');
        Route::post('/academic/job/application/update', 'AdminController@academicJobApplicationUpdate')->name('admins.academics.job_application_update');
       
        //show all the job that has been assigned 
        Route::get('/academic/job/assigned', 'AdminController@showAssignedJobList')->name('admins.academics.job_assigned_list');
        Route::get('/academic/job/assigned/show/{id}', 'AdminController@showAssignedJobShow')->name('admins.academics.job_assigned_show');
        Route::get('/academic/job/assigned/edit/{id}', 'AdminController@showAssignedJobEdit')->name('admins.academics.job_assigned_edit');
        Route::post('/academic/job/assigned/update', 'AdminController@updateAssignedJobStatus')->name('admins.academics.job_assigned_update');
        Route::post('/academic/job/assigned/cancel', 'AdminController@cancelAssignedJobByRefNo')->name('admins.academics.job_assigned_cancel');

        Route::post('/academic/job/assigned/updateStartDate', 'AdminController@academicJobStartDateUpdate')->name('admins.academics.job_assigned_updateStartDate');
        Route::post('/academic/job/assigned/updatePayment', 'AdminController@academicJobPaymentUpdate')->name('admins.academics.job_assigned_updatePayment');
        
        Route::post('/academic/job/reassign/update', 'AdminController@academicJobReassignUpdate')->name('admins.academics.job_reassign_update');
        
        Route::post('/academic/job/assigned/updateSettled', 'AdminController@academicJobSettledUpdate')->name('admins.academics.job_assigned_updateSettled');
        Route::get('/academic/job/settled/index', 'AdminController@academicJobSettledIndex')->name('admins.academics.job_settled_index');
        Route::get('/academic/job/settled/show/{id}', 'AdminController@academicJobSettledShow')->name('admins.academics.job_settled_show');

        Route::get('/academic/job/cancelled/index', 'AdminController@academicJobCancelledIndex')->name('admins.academics.job_cancelled_index');
        Route::get('/academic/job/cancelled/show/{id}', 'AdminController@academicJobCancelledShow')->name('admins.academics.job_cancelled_show');

        Route::get('/academic/class/price', 'AdminController@academicPriceIndex')->name('admins.academics.price');
        Route::post('/academic/class/upload', 'AdminController@academicPriceUpload')->name('admins.academics.price_upload');
        Route::get('/academic/class/price/show', 'AdminController@getPriceMenu')->name('admins.academics.show_pdf');
        
        Route::get('/non-academic/class/price', 'AdminController@nonAcademicPriceIndex')->name('admins.nonacademics.price');
        Route::post('/non-academic/class/upload', 'AdminController@nonAcademicPriceUpload')->name('admins.nonacademics.price_upload');
        Route::get('/non-academic/class/price/show', 'AdminController@nonAcademicGetPriceMenu')->name('admins.nonacademics.show_pdf');
        
        Route::get('/document/higher/{id}', 'AdminController@getHigher')->name('admins.higher');
        Route::get('/document/spm/{id}', 'AdminController@getSpm')->name('admins.spm');

        /** Non Academic */
        // list of job applications by tutors
        Route::get('/non-academic/job/application/index', 'AdminController@nonAcademicJobApplicationIndex')->name('admins.nonacademics.job_application_index');
        // show job details and applicants - view only
        Route::get('/non-academic/job/application/show/{id}', 'AdminController@nonAcademicJobApplicationShow')->name('admins.nonacademics.job_application_show');
        // Assign the job to which tutor
        Route::get('/non-academic/job/application/edit/{id}', 'AdminController@nonAcademicJobApplicationEdit')->name('admins.nonacademics.job_application_edit');
        Route::post('/non-academic/job/application/update', 'AdminController@nonAcademicJobApplicationUpdate')->name('admins.nonacademics.job_application_update');
        //show all the job that has been assigned 
        Route::get('/non-academic/job/assigned', 'AdminController@nonAshowAssignedJobList')->name('admins.nonacademics.job_assigned_list');
        Route::get('/non-academic/job/assigned/show/{id}', 'AdminController@nonAshowAssignedJobShow')->name('admins.nonacademics.job_assigned_show');
        Route::get('/non-academic/job/assigned/edit/{id}', 'AdminController@nonAshowAssignedJobEdit')->name('admins.nonacademics.job_assigned_edit');
        Route::post('/non-academic/job/assigned/update', 'AdminController@nonAupdateAssignedJobStatus')->name('admins.nonacademics.job_assigned_update');
        // cancel job after assigned tutor
        Route::post('/academic/job/cancel', 'AdminController@nonAcancelRequestByRefNo')->name('admins.nonacademics.request_cancel');

        Route::post('/non-academic/job/assigned/updateStartDate', 'AdminController@nonAcademicJobStartDateUpdate')->name('admins.nonacademics.job_assigned_updateStartDate');
        Route::post('/non-academic/job/assigned/updatePayment', 'AdminController@nonAcademicJobPaymentUpdate')->name('admins.nonacademics.job_assigned_updatePayment');
        
        Route::post('/non-academic/job/reassign/update', 'AdminController@nonAcademicJobReassignUpdate')->name('admins.nonacademics.job_reassign_update');

        Route::post('/non-academic/job/assigned/updateSettled', 'AdminController@nonAcademicJobSettledUpdate')->name('admins.nonacademics.job_assigned_updateSettled');
        Route::get('/non-academic/job/settled/index', 'AdminController@nonAcademicJobSettledIndex')->name('admins.nonacademics.job_settled_index');
        Route::get('/non-academic/job/settled/show/{id}', 'AdminController@nonAcademicJobSettledShow')->name('admins.nonacademics.job_settled_show');

        Route::get('/non-academic/job/cancelled/index', 'AdminController@nonAcademicJobCancelledIndex')->name('admins.nonacademics.job_cancelled_index');
        Route::get('/non-academic/job/cancelled/show/{id}', 'AdminController@nonAcademicJobCancelledShow')->name('admins.nonacademics.job_cancelled_show');


        /** Class Management for admin to create class and price */
        Route::get('/classes/academics/index', 'ClassController@academicClassIndex')->name('classes.academics.index');
        Route::get('/classes/academics/create', 'ClassController@showCreateAcademicClasses')->name('classes.academics.create');
        Route::post('/classes/academics/store', 'ClassController@storeAcademicClasses')->name('classes.academics.store');
        Route::get('/classes/academics/show/{id}', 'ClassController@showAcademicClass')->name('classes.academics.show');
        Route::get('/classes/academics/edit/{id}', 'ClassController@showAcademicClassEdit')->name('classes.academics.edit');
        Route::post('/classes/academics/update/{id}', 'ClassController@academicClassUpdate')->name('classes.academics.update');
        Route::delete('/classes/academics/delete/{id}', 'ClassController@academicClassDelete')->name('classes.academics.delete');



        /** Non Academic classes - for admin only */
        Route::get('/classes/non-academics/index', 'ClassController@nonAcademicIndex')->name('classes.nonacademics.index');
        Route::get('/classes/non-academics/show/{id}', 'ClassController@nonAcademicShow')->name('classes.nonacademics.show');
        Route::get('/classes/non-academics/edit/{id}', 'ClassController@nonAcademicEdit')->name('classes.nonacademics.edit');
        Route::get('/classes/non-academics/create', 'ClassController@nonAcademicCreate')->name('classes.nonacademics.create');
        Route::post('/classes/non-academics/store', 'ClassController@nonAcademicStore')->name('classes.nonacademics.store');
        Route::post('/classes/non-academics/update/{id}', 'ClassController@nonAcademicUpdate')->name('classes.nonacademics.update');
        Route::delete('/classes/non-academics/delete/{id}', 'ClassController@nonAcademicDelete')->name('classes.nonacademics.delete');
        
        
         /** Tutor Profile Management*/
        Route::get('/tutor-profile/index', 'AdminController@tutorProfileIndex')->name('admins.tutorprofiles.index');
        Route::get('/tutor-profile/create', 'AdminController@tutorProfileCreate')->name('admins.tutorprofiles.create');
        Route::post('/tutor-profile/store', 'AdminController@tutorProfileStore')->name('admins.tutorprofiles.store');
        Route::get('/tutor-profile/show/{id}', 'AdminController@tutorProfileShow')->name('admins.tutorprofiles.show');
        Route::get('/tutor-profile/edit/{id}', 'AdminController@tutorProfileEdit')->name('admins.tutorprofiles.edit');
        Route::post('/tutor-profile/update/{id}', 'AdminController@tutorProfileUpdate')->name('admins.tutorprofiles.update');
        Route::delete('/tutor-profile/delete/{id}', 'AdminController@tutorProfileDelete')->name('admins.tutorprofiles.delete');

       

    }
);

//  tutor
Route::group(
    [
        'middleware' => ['auth', 'role:tutor|admin|superadmin', 'verified'],
        'prefix' => 'tutor'
    ],
    function () {

        Route::get('/dashboard', 'TutorController@dashboard')->name('tutors.dashboard');
        Route::get('/profile', 'TutorController@profile')->name('tutors.profile');
        Route::post('/profile/update/{id?}', 'TutorController@updateProfile')->name('tutors.profile.update');

        Route::post('/profile/update-public-info', 'TutorController@updateProfilePublicInfo')->name('tutors.profile.update_public_info');
        Route::post('/profile/update-private-info', 'TutorController@updateProfilePrivateInfo')->name('tutors.profile.update_private_info');
        Route::post('/change-password', 'TutorController@changePassword')->name('tutors.change_password');

        Route::post('/spm-results/update', 'TutorController@updateSpmResults')->name('tutors.spmResults_update');

        Route::get('/qualifications', 'TutorController@showQualification')->name('tutors.qualification');
        Route::post('/spm-qualifications/update', 'TutorController@updateSpmQualification')->name('tutors.spmQualification_update');
        Route::post('/higher-qualifications/update', 'TutorController@updateHigherQualification')->name('tutors.higherQualification_update');
        
        Route::get('/document/higher', 'TutorController@getHigher')->name('tutors.higher');
        Route::get('/document/spm', 'TutorController@getSpm')->name('tutors.spm');

        Route::get('/membership', 'TutorController@membershipIndex')->name('tutors.membership');
        Route::post('/membership/request', 'TutorController@membershipRenewalRequest')->name('tutors.membership.request');
        
        Route::get('/class-preference', 'TutorController@showClassPreference')->name('tutors.class_preference');
        Route::post('/class-preference/update-subjects', 'TutorController@updateSubjects')->name('tutors.class_preference.update_subjects');
        Route::post('/class-preference/update-experience', 'TutorController@updateExperience')->name('tutors.class_preference.update_experience');
        Route::post('/class-preference/update-areas', 'TutorController@updateAreas')->name('tutors.class_preference.update_areas');

        // Tutor Apply for Academic JOB
        Route::get('/academic/job/index', 'TutorController@jobIndex')->name('tutors.academics.job_index');
        Route::get('/academic/job/show/{ref_no}', 'TutorController@jobShow')->name('tutors.academics.job_show');
        Route::get('/academic/job/apply/{ref_no}', 'TutorController@showJobApply')->name('tutors.academics.job_apply');
        Route::post('/academic/job/apply', 'TutorController@jobApply')->name('tutors.academics.job_apply_submit');

        Route::get('/academic/job/applied', 'TutorController@showJobAppliedByUserIdHistory')->name('tutors.academics.job_applied');
        Route::get('/academic/job/applied/{ref_no}', 'TutorController@academicJobAppliedShow')->name('tutors.academics.job_applied_show');
        Route::get('/academic/job/assigned/{ref_no}', 'TutorController@academicJobAssignedShow')->name('tutors.academics.job_assigned_show');
        Route::get('/academic/job/assigned', 'TutorController@jobApprovedByUserIdIndex')->name('tutors.academics.job_approved');

        // Tutor Apply for Non Academic JOB
        Route::get('/non-academic/job/index', 'TutorController@nonAcademicJobIndex')->name('tutors.nonacademics.job_index');
        Route::get('/non-academic/job/show/{ref_no}', 'TutorController@nonAcademicJobShow')->name('tutors.nonacademics.job_show');
        Route::get('/non-academic/job/apply/{ref_no}', 'TutorController@nonAcademicJobApply')->name('tutors.nonacademics.job_apply');
        Route::post('/non-academic/job/apply', 'TutorController@nonAcademicJobApplySubmit')->name('tutors.nonacademics.job_apply_submit');
        Route::get('/non-academic/job/applied', 'TutorController@showNonAcademicJobAppliedByUserIdHistory')->name('tutors.nonacademics.job_applied');
        Route::get('/non-academic/job/applied/{ref_no}', 'TutorController@nonAcademicJobAppliedShow')->name('tutors.nonacademics.job_applied_show');
        Route::get('/non-academic/job/assigned/{ref_no}', 'TutorController@nonAcademicJobAssignedShow')->name('tutors.nonacademics.job_assigned_show');
        Route::get('/non-academic/job/assigned', 'TutorController@nonAcademicJobApprovedByUserIdIndex')->name('tutors.nonacademics.job_approved');
    }
);

/**
 * Students Route
 * 
 */
Route::group(
    [
        'middleware' => ['auth', 'role:student|admin|superadmin', 'verified'],
        'prefix' => 'student'
    ],
    function () {

        Route::get('/dashboard', 'StudentController@dashboard')->name('students.dashboard');
        Route::get('/profile', 'StudentController@profile')->name('students.profile');
        Route::post('/profile/update/{id?}', 'StudentController@updateProfile')->name('students.profile.update');

        Route::post('/profile/update-public-info', 'StudentController@updateProfilePublicInfo')->name('students.profile.update_public_info');
        Route::post('/profile/update-private-info', 'StudentController@updateProfilePrivateInfo')->name('students.profile.update_private_info');
        Route::post('/change-password', 'StudentController@changePassword')->name('students.change_password');

        /** Academic Class - for students to apply for class */
        Route::get('/classes/academics/request', 'StudentController@showApplyForClass')->name('students.academics.request_class');
        Route::post('/classes/academics/request/submit', 'StudentController@academicClassApplySubmit')->name('students.academics.request_class.submit');

        Route::get('/classes/academics/applications', 'StudentController@academicClassApplicationsIndex')->name('students.academics.request_list');
        Route::get('/classes/academics/applications/show/{ref_no}', 'StudentController@showClassApplication')->name('students.academics.request_show');
        Route::delete('/classes/academics/applications/delete/{ref_no}', 'StudentController@deleteClassApplication')->name('students.academics.request_delete');
        Route::get('/classes/academics/applications/edit/{ref_no}', 'StudentController@showAcademicClassApplicationEdit')->name('students.academics.request_edit');
        Route::post('/classes/academics/applications/update/{ref_no}', 'StudentController@academicClassApplicationUpdate')->name('students.academics.request_update');

        Route::post('/classes/academics/cancel', 'StudentController@cancelRequestByRefNo')->name('students.academics.request_cancel');
        Route::get('/classes/academics/applications/cancelled', 'StudentController@showAcademicClassApplicationCancelledByUser')->name('students.academics.request_cancel_list');

        // to show list of request that got tutors assigned - means found tutor laaa
        Route::get('/classes/academics/applications/success', 'StudentController@academicSuccessIndex')->name('students.academics.request_success_index');

        Route::get('/academic/class/price/show', 'StudentController@getDocument')->name('students.academics.show_pdf');
        Route::get('/non-academic/class/price/show', 'StudentController@nonAcademicgetDocument')->name('students.nonacademics.show_pdf');
        
        /** Non Academic Class - for students to apply for class */
        Route::get('/classes/non-academics/request', 'StudentController@showRequestNonAcademicClass')->name('students.nonacademics.request_class');
        Route::post('/classes/non-academics/request/submit', 'StudentController@requestNonAcademicClassSubmit')->name('students.nonacademics.request_class.submit');

        Route::get('/classes/non-academics/applications', 'StudentController@nonAcademicClassApplicationsIndex')->name('students.nonacademics.request_list');
        Route::get('/classes/non-academics/applications/show/{ref_no}', 'StudentController@nonAcademicClassApplicationsShow')->name('students.nonacademics.request_show');
        
        Route::post('/classes/non-academics/cancel', 'StudentController@nonAcademicClassApplicationsCancelByRefNo')->name('students.nonacademics.request_cancel');
        Route::get('/classes/non-academics/applications/cancelled', 'StudentController@nonAcademicClassApplicationsCancelledByUser')->name('students.nonacademics.request_cancel_list');

        // to show list of request that got tutors assigned - means found tutor laaa
        Route::get('/classes/non-academics/applications/success', 'StudentController@nonAcademicSuccessIndex')->name('students.nonacademics.request_success_index');

        // not using edit functions
        Route::get('/classes/non-academics/applications/edit/{ref_no}', 'StudentController@nonAcademicClassApplicationsEdit')->name('students.nonacademics.request_edit');
        Route::post('/classes/non-academics/applications/update/{ref_no}', 'StudentController@nonAcademicClassApplicationsUpdate')->name('students.nonacademics.request_update');
        Route::delete('/classes/non-academics/applications/delete/{ref_no}', 'StudentController@nonAcademicClassApplicationsDelete')->name('students.nonacademics.request_delete');
       
    }
);
