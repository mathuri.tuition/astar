<?php 


Route::group(['middleware' => ['auth', 'role:tutor']], function () {





});






Route::group(['middleware' => ['auth', 'role:student']], function () {

Route::get('/student/academic/request-tutor', 'StudentController@showAcademicRequestTutor')->name('students.academics.request_tutor');
Route::post('/student/academic/request-tutor/submit', 'StudentController@academicRequestTutor')->name('students.academics.request_tutor.submit');

Route::get('/student/academic/request-tutor-list', 'StudentController@listRequestAcademicTutorsByUserId')->name('students.academics.list_request_tutor');

Route::get('/student/non-academic/request-tutor', 'StudentController@showNonAcademicRequestTutor')->name('students.non_academics.request_tutor');
Route::post('/student/non-academic/request-tutor/submit/{id?}', 'StudentController@nonAcademicRequestTutor')->name('students.non_academics.request_tutor.submit');

});






Route::group(['middleware' => ['auth','role:admin']], function () {

Route::post('/admin/academic/detail-job-list', 'AdminController@detailAcademicJobList')->name('admins.academics.detail_job_list');

Route::get('/admin/academic/job-applications', 'AdminController@jobApplicationsList')->name('admins.academics.job_applications');
Route::post('/admin/academic/detail-job-application', 'AdminController@detailJobApplicationByJobId')->name('admins.academics.detail_job_application');
Route::post('/admin/academic/update-job-application/{id?}', 'AdminController@updateJobApplicationByJobId')->name('admins.profile.update_job_detail');

Route::get('/admin/academic/assigned-job-list', 'AdminController@assignJobList')->name('admins.academics.assign_job_list');
Route::post('/admin/academic/assign-job-tutor', 'AdminController@assignJobTutor')->name('admins.academics.assign_job_tutor');

Route::get('/admin/academic/subject-list', 'AdminController@showSubjectsIndex')->name('admins.academics.subjects.index');
Route::get('/admin/academic/subject-create', 'AdminController@showSubjectsCreate')->name('admins.academics.subjects.create');
Route::post('/admin/academic/subject-add', 'AdminController@addSubject')->name('admins.academics.subjects.add');


    /** Apply class and manage classes that we apply */
        // Route::get('/classes/academics/apply', 'ClassController@showApplyForClass')->name('classes.academics.apply_class');
        // Route::post('/classes/academics/apply/submit', 'ClassController@academicClassApplySubmit')->name('classes.academics.apply_class.submit');
        // Route::get('/classes/academics/applications', 'ClassController@academicClassApplicationsIndex')->name('classes.academics.index_class_applications');
        // Route::get('/classes/academics/applications/show/{id}', 'ClassController@showClassApplication')->name('classes.academics.show_class_applications');
        // Route::delete('/classes/academics/applications/delete/{id}', 'ClassController@deleteClassApplication')->name('classes.academics.delete_class_applications');
        // Route::get('/classes/academics/applications/edit/{id}', 'ClassController@showAcademicClassApplicationEdit')->name('classes.academics.edit_class_applications');
        // Route::post('/classes/academics/applications/update/{id}', 'ClassController@academicClassApplicationUpdate')->name('classes.academics.update_class_applications');


    // Re-assign tutor list
    Route::get('/academic/job/reassign/index', 'AdminController@academicJobReassignIndex')->name('admins.academics.job_reassign_index');
    // show job details and applicants - view only
    Route::get('/academic/job/reassign/show/{id}', 'AdminController@academicJobReassignShow')->name('admins.academics.job_reassign_show');
    // re-Assign the job to which tutor
    Route::get('/academic/job/reassign/edit/{id}', 'AdminController@academicJobReassignEdit')->name('admins.academics.job_reassign_edit');
    Route::post('/academic/job/reassign/update', 'AdminController@academicJobReassignUpdate')->name('admins.academics.job_reassign_update');

});


Route::group(['middleware' => ['auth', 'role:admin']], function () {




Route::get('/superadmin/class/request', 'SuperadminController@showAcademicRequestClass')->name('superadmins.class_request');
Route::get('/superadmin/class/list', 'SuperadminController@academicClassRequestList')->name('superadmins.class_list');

/** Jobs */
Route::get('jobs/academics/index', 'JobController@academicIndex')->name('jobs.academics.index');
Route::get('jobs/academics/applications/approval/{id}', 'JobController@showPageJobApproval')->name('jobs.academics.job_approval');
Route::post('jobs/academics/applications/approve/{id}', 'JobController@approveJob')->name('jobs.academics.approve_job');

/** Tutors management */
    Route::get('/superadmin/tutors', 'SuperadminController@tutorList')->name('superadmins.tutor_list');
    Route::get('/superadmin/tutors/add', 'SuperadminController@showTutorAdd')->name('superadmins.tutor_add');
    Route::post('/superadmin/tutors/store', 'SuperadminController@addTutor')->name('superadmins.tutor_store');
    Route::get('/superadmin/tutors/edit/{id}', 'SuperadminController@showTutorEdit')->name('superadmins.tutor_edit');
    Route::post('/superadmin/tutors/update/{id}', 'SuperadminController@updateTutor')->name('superadmins.tutor_update');
    Route::get('/superadmin/tutors/show/{id}', 'SuperadminController@showTutor')->name('superadmins.tutor_show');
    Route::post('/superadmin/tutors/delete/{id}', 'SuperadminController@deleteTutor')->name('superadmins.tutor_delete');

Route::get('jobs/non-academics/index', 'JobController@nonAcademicIndex')->name('jobs.non_academics.index');

    /** Tutors management */
    Route::get('/superadmin/tutors', 'SuperadminController@tutorList')->name('superadmins.tutor_list');
    Route::get('/superadmin/tutors/add', 'SuperadminController@showTutorAdd')->name('superadmins.tutor_add');
    Route::post('/superadmin/tutors/store', 'SuperadminController@addTutor')->name('superadmins.tutor_store');
    Route::get('/superadmin/tutors/edit/{id}', 'SuperadminController@showTutorEdit')->name('superadmins.tutor_edit');
    Route::post('/superadmin/tutors/update/{id}', 'SuperadminController@updateTutor')->name('superadmins.tutor_update');
    Route::get('/superadmin/tutors/show/{id}', 'SuperadminController@showTutor')->name('superadmins.tutor_show');
    Route::post('/superadmin/tutors/delete/{id}', 'SuperadminController@deleteTutor')->name('superadmins.tutor_delete');


});



 <!-- <a href="{{ route('admins.academics.job_reassign_index') }}" class="collapse-item">Approved Offers</a> -->
                        <!-- <a href="{{ route('admins.academics.job_reassign_index') }}" class="collapse-item">Pending Offers</a> -->
                        <!-- <a href="{{ route('admins.academics.job_reassign_index') }}" class="collapse-item">Applied Job Offers</a> -->

                         <!-- <a href="{{ route('admins.academics.job_reassign_index') }}" class="collapse-item">Re-assign</a> -->