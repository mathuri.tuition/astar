<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'superadmin-dashboard',
            'superadmin-profile',

            'admin-dashboard',
            'admin-profile',

            'tutor-profile',
            'tutor-dashboard',

            'student-dashboard',
            'student-profile',

            'subject-list', 'subject-create', 'subject-edit', 'subject-delete',
            'curriculum-list', 'curriculum-create', 'curriculum-edit', 'curriculum-delete'
        ];


        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}
