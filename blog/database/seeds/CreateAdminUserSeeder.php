<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Admin;
use Illuminate\Auth\Events\Registered;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        	'name' => 'Super Admin', 
        	'email' => 'lav29.deva@gmail.com',
        	'password' => Hash::make('password1q2w3e$')
        ]);

        event(new Registered($user));

        $role = Role::create(['name' => 'superadmin']);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);

        $admin = Admin::create([
            'user_id' => $user->id,
            'mykad_no' => '940412145573',
            'phone_no' => '0133154730',
            'nationality_id' => '109',
            'gender_id' => '1',
            'race' => 1,
            'religion' => 1,
            'marital_status_id' => '1',
            'address_1' => '5/18',
            'address_2' => 'Lorong Udang Ketak 1',
            'address_3' => 'Taman Sri Segambut',
            'postcode' => '52000',
            'city' => 'KL',
            'state_code' => 'KUL',

        ]);
    }
}
