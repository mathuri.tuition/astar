<?php

use Illuminate\Database\Seeder;
use App\Religion;

class ReligionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $religions = [
            ['religion' => 'Hinduism'],
            ['religion' => 'Islam'],
            ['religion' => 'Christianity'],
            ['religion' => 'Buddhism'],
            ['religion' => 'Sikhism'],
            ['religion' => 'Others'],
        ];

        foreach ($religions as $religion) {
            Religion::create(array(
                'religion' => $religion["religion"]
            ));
        }
    }
}
