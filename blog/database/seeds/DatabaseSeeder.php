<?php

use App\Religion;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(MaritalStatusesTableSeeder::class);
        $this->call(NationalitiesTableSeeder::class);
        $this->call(StatesSeeder::class);
        $this->call(RacesSeeder::class);
        $this->call(ReligionsSeeder::class);
        $this->call(CreateAdminUserSeeder::class);
        $this->call(RolesSeeder::class);
    }
}
