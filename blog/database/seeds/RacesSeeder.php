<?php

use Illuminate\Database\Seeder;
use App\Race;

class RacesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levels = [
            ['race' => 'Malay'],
            ['race' => 'Indian'],
            ['race' => 'Chinese'],
            ['race' => 'Others'],
        ];

        foreach ($levels as $level) {
            Race::create(array(
                'race' => $level["race"]
            ));
        }
    }
}
