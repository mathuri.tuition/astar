<?php

use Illuminate\Database\Seeder;
use App\MaritalStatus;

class MaritalStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            ['reference' => 'single'],
            ['reference' => 'married'],
            ['reference' => 'divorced'],
            ['reference' => 'widowed']
        ];


        foreach ($status as $s) {
            MaritalStatus::create(array(
                'status' => $s["reference"],

            ));
        }
    }
}
