<?php

use Illuminate\Database\Seeder;
use App\State;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            ['code' => 'JHR' , 'state'=> 'Johor'],
            ['code' => 'KDH', 'state' => 'Kedah'],
            ['code' => 'KTN', 'state' => 'Kelantan'],
            ['code' => 'MLK', 'state' => 'Melaka'],

            ['code' => 'NSN', 'state' => 'Negeri Sembilan'],
            ['code' => 'PHG', 'state' => 'Pahang'],
            ['code' => 'PRK', 'state' => 'Perak'],
            ['code' => 'PLS', 'state' => 'Perlis'],

            ['code' => 'PNG', 'state' => 'Pulau Pinang'],
            ['code' => 'SBH', 'state' => 'Sabah'],
            ['code' => 'SWK', 'state' => 'Sarawak'],
            ['code' => 'SGR', 'state' => 'Selangor'],
            
            ['code' => 'TRG', 'state' => 'Terengganu'],
            ['code' => 'KUL', 'state' => 'W.P. Kuala Lumpur'],
            ['code' => 'LBN', 'state' => 'W.P. Labuan'],
            ['code' => 'PJY', 'state' => 'W.P. Putrajaya'],
        ];


        foreach ($states as $state) {
            State::create(array(
                'code' => $state["code"],
                'state' => $state["state"]
            ));
        }


    }
}
