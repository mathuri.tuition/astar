<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAcademicClasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_academic_classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');

            // class details - requested by student
            $table->string('academic_reference_no')->unique()->nullable();
            $table->integer('academic_class_id');

            $table->integer('type');
            $table->integer('pax');

            $table->string('frequency');
            $table->string('duration');
            $table->string('days');
            $table->string('time');

            $table->string('state');
            $table->string('city');
            $table->string('postcode');
            $table->string('area');

            $table->integer('tutor_gender');
            $table->string('tutor_race');
            $table->string('tutor_preference')->nullable();
            
            // if student plan to cancel this request, by default active
            // 1:active | 2:cancel
            $table->integer('status')->default(1);

            // to monitor on payments if both parties has agreed to start class
            $table->date('class_start_date')->nullable();
            $table->date('payment_date')->nullable();
            $table->date('job_settled')->nullable();

            // if tutor cancel after assign, active means other tutors will see job offer
            $table->boolean('job_offer_active')->default(1);

            // admin to approve this job is active for tutors to apply
            $table->integer('request_status')->default(0);
            $table->integer('request_approved_by')->nullable();
            $table->dateTime('request_approved_at')->nullable();
            $table->string('admin_remarks')->nullable();

            // if null means no tutor found, if not null means got tutor already la so can notify
            $table->integer('tutor_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_academic_classes');
    }
}
