<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorAcademicJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_academic_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('student_academic_class_id');
            // tutor ID - which tutor apply for this job
            $table->integer('user_id');

            $table->string('tutor_remarks')->nullable();
            // if this tutor is approved for this job then his status will be approved
            $table->integer('tutor_status')->nullable();

            // if the tutor is assigned for this job - 
            // eg : if got 5 application for same class, then this will show which tutor the job is assigned to
            $table->boolean('job_assigned')->default(0);
            $table->dateTime('job_assigned_date')->nullable();
            $table->integer('job_assigned_by')->nullable();
            $table->integer('job_assigned_modified_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_academic_jobs');
    }
}
