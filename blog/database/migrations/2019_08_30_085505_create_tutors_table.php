<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('mykad_no')->nullable();
            $table->string('passport_no')->nullable();
            $table->string('phone_no');
            $table->integer('nationality_id');
            $table->integer('gender_id');
            $table->string('image')->nullable();
            $table->integer('race')->nullable();
            $table->integer('religion')->nullable();
            $table->integer('marital_status_id')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('address_4')->nullable();
            $table->string('postcode')->nullable();
            $table->string('city')->nullable();
            $table->string('state_code')->nullable();
            $table->string('spm_results')->nullable();
            $table->string('highest_education_results')->nullable();
            
            $table->integer('account_approved')->nullable();
            $table->integer('account_approved_by')->nullable();
            $table->dateTime('account_approved_at')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutors');
    }
}
