<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('motto');
            $table->string('image');
            $table->string('email');
            $table->string('phone_no');
            $table->integer('nationality_id');
            $table->string('gender');
            $table->integer('race')->nullable();
            $table->integer('religion')->nullable();
            $table->integer('marital_status_id')->nullable();
            $table->string('subjects');
            $table->string('summary');
            $table->string('higher_education_1');
            $table->string('higher_education_2')->nullable();
            $table->string('higher_education_3')->nullable();
            $table->string('experience');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_profiles');
    }
}
