@extends('layouts.app_login')
<?php
$page = 'roles-index';
$title = 'Roles Management';
?>
@section('title', '| Roles')

@section('content')

@if ($message = Session::get('success'))
<div class="row">
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Role</th>
            <th>Permissions</th>
            <th width="280px">Action</th>
        </tr>

        <?php
        $i = 0;
        ?>
        @foreach ($roles as $role)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $role->name }}</td>
            <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>
            <td>
                <!-- <a href="{{ URL::to('roles/'.$role->id.'/edit') }}" class="btn btn-info float-left">Edit</a>
                {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!} -->

                <a class="btn btn-info" href="{{ route('roles.show', $role->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('roles.edit', "$role->id") }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>

        @endforeach
    </table>
</div>


@endsection