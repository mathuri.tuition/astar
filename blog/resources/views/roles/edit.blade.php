@extends('layouts.app_login')
<?php
$page = 'roles-edit';
$title = 'Edit Role';
?>
@section('title', '| Edit Role')

@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">
            <div class="card-body">
                {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

                <div class="row">
                    <div class="form-group">
                        <div class="col">
                            {{ Form::label('name', 'Role Name') }}
                            {{ Form::text('name', null, array('class' => 'form-control')) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <h5><b>Assign Permissions</b></h5>
                        <div class="col">
                            @foreach ($permissions as $permission)
                            {{Form::checkbox('permissions[]', $permission->id, $role->permissions ) }}
                            {{Form::label($permission->name, ucfirst($permission->name)) }}
                            <br>
                            @endforeach
                        </div>
                    </div>
                </div>
                <a class="btn btn-primary" href="{{ route('roles.index') }}"> Cancel</a>
                {{ Form::submit('Edit', array('class' => 'btn btn-success float-right')) }}
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>

@endsection