@extends('layouts.app_login')
<?php
$page = 'classes-index';
$title = 'Job Offers';
?>

@section('content')


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>ID </th>
            <th>Class ID</th>
            <th>User Id</th>
            <th>Remarks</th>
            <th>Status</th>
            <th>Action</th>
        </tr>

        @foreach ($jobs as $job)
        <tr>

            <td>{{ ++$i }}</td>
            <td>{{ $job->id }}</td>
            <td>{{ $job->student_academic_class_id }}</td>
            <td>{{ $job->user_id }}</td>
            <td>{{ $job->tutor_remarks }}</td>
            <td></td>

            <td>
                <a class="btn btn-info" href="{{ route('jobs.academics.job_application_show', $job->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('jobs.academics.job_application_edit', $job->id) }}">Edit</a>

            </td>
        </tr>
        @endforeach



    </table>
</div>

{!! $jobs->render() !!}

@endsection