@extends('layouts.app_login')
<?php
$page = 'classes-academics_show';
$title = 'Show Academic Job';
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">
            <div class="card-header">

                <div class="row">
                    <div class="col-6"> {{ __('Class Application') }}</div>

                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" border="0">

                        <tr>
                            <td>Requested At</td>
                            <td>:</td>
                            <td>{{ $job->created_at }}</td>
                        </tr>
                        <tr>
                            <td>Requested By</td>
                            <td>:</td>
                            <td>{{ $job->user_email }}</td>
                        </tr>

                        <tr>
                            <td>Class</td>
                            <td>:</td>
                            <td>{{ $job->class_name }}</td>
                        </tr>
                        <tr>
                            <td>Level</td>
                            <td>:</td>
                            <td>{{ $job->edu_level }}</td>
                        </tr>

                        <tr>
                            <td>Preferred Days</td>
                            <td>:</td>
                            <td>
                                <table class="table">
                                    <tr>
                                        <td>Day</td>
                                        <td>Start Time</td>
                                        <td>End Time</td>
                                    </tr>
                                    <?php
                                    $objs = json_decode($job->preferred_days);
                                    foreach ($objs as $obj) {
                                        echo "<tr>";
                                        echo "<td>" . ucwords($obj->day) . "</td>";
                                        echo "<td>" . $obj->start . "</td>";
                                        echo "<td>" . $obj->end . "</td>";
                                        echo "</tr>";
                                    }
                                    ?>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>Location</td>
                            <td>:</td>
                            <td>{{ $job->location }}</td>
                        </tr>

                        <tr>
                            <td>Request Remarks</td>
                            <td>:</td>
                            <td>{{ $job->student_remarks }}</td>
                        </tr>
                    </table>
                </div>


            </div>

            <div class="card-footer">
                <button class="btn btn-primary" onclick="goBack()"> Back</button>
            </div>

        </div>
        <br>
    </div>
</div>

@endsection