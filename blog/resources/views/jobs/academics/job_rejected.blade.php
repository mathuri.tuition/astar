@extends('layouts.app_login')
<?php
$page = 'classes-index';
$title = 'Rejected Academic Class Request';
?>

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Job Reference No </th>
            <th>Subject</th>
            <th>Level</th>
            <th>Curriculum</th>
            <th>Area</th>
            <th>Status</th>
            <th>Action</th>
        </tr>

        @foreach ($datas as $data)
        <tr>

            <td>{{ ++$i }}</td>
            <td>{{ $data->academic_reference_no }}</td>
            <td>{{$data->class_name}}</td>
            <td>{{ $data->level }}</td>
            <td>{{ $data->curriculum }}</td>
            <td>{{ $data->city }}</td>
            <td>
                @if($data->request_status == 1)
                <label class="badge badge-success">Approved</label>
                @elseif($data->request_status == 2)
                <label class="badge badge-danger">Rejected</label>
                @elseif($data->request_status == 3)
                <label class="badge badge-warning">Pending</label>
                @endif
            </td>

            <td>
                <a class="btn btn-info" href="{{ route('jobs.academics.show', $data->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('jobs.academics.job_approval', $data->id) }}">Edit</a>

            </td>
        </tr>
        @endforeach
    </table>
</div>
    {!! $datas->render() !!}
    @endsection