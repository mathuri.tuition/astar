@extends('layouts.app')
<?php $page = 'home'; ?>
@section('content')

<div class="hero-wrap" style="background-image: url('{{@genius}}/images/bg_1.jpg'); background-attachment:fixed;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-md-8 ftco-animate text-center">
                <h1 class="mb-4">No Nation Can Prosper In Life Without Education</h1>
                <p><a href="{{ route('register') }}" class="btn btn-primary px-4 py-3">Sign Up</a> <a href="{{ url('/courses') }}" class="btn btn-secondary px-4 py-3">View Courses</a></p>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services p-3 py-4 d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center mb-3"><span class="flaticon-exam"></span></div>
                    <div class="media-body px-3">
                        <h3 class="heading">Registration</h3>
                        <p>Register with your basic information after accepting the terms & conditions.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services p-3 py-4 d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center mb-3"><span class="flaticon-blackboard"></span></div>
                    <div class="media-body px-3">
                        <h3 class="heading">Dashboard</h3>
                        <p>Request management system to show a summary of request made.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services p-3 py-4 d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center mb-3"><span class="flaticon-books"></span></div>
                    <div class="media-body px-3">
                        <h3 class="heading">Comprehensive</h3>
                        <p>Offers you academic & non-academic classes.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="ftco-section-3 img" style="background-image: url({{@genius}}/images/bg_3.jpg);">
    <div class="overlay"></div>
    <div class="container">
        <div class="row d-md-flex justify-content-center">
            <div class="col-md-9 about-video text-center">
                <h2 class="ftco-animate">A Star Educators is a leading platform to discover your desired teachers and offers job opportunity for qualified teachers.</h2>
                <div class="video d-flex justify-content-center">
                    <!-- <a href="https://vimeo.com/45830194" class="button popup-vimeo d-flex justify-content-center align-items-center"><span class="ion-ios-play"></span></a> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate text-center">
                <h2 class="mb-4">Our Courses</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4  ftco-animate">
                <div class="course align-self-stretch">
                    <a  class="img" style="background-image: url({{@genius}}/images/course-1.jpg)"></a>
                    <div class="text p-4">
                        <p class="category"><span>National Syllabus</span></p>
                        <h3 class="mb-3"><a >Standard 1-6</a></h3>
                        <p></p>
                        <p><a href="{{ route('register') }}" class="btn btn-primary">Enroll now!</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="course align-self-stretch">
                    <a  class="img" style="background-image: url({{@genius}}/images/course-2.jpg)"></a>
                    <div class="text p-4">
                        <p class="category"><span>Cambridge Syllabus</span></p>
                        <h3 class="mb-3"><a>Year 1-11</a></h3>
                        <p></p>
                        <p><a href="{{ route('register') }}" class="btn btn-primary">Enroll now!</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="course align-self-stretch">
                    <a  class="img" style="background-image: url({{@genius}}/images/course-3.jpg)"></a>
                    <div class="text p-4">
                        <p class="category"><span>National Syllabus</span></p>
                        <h3 class="mb-3"><a href="#">Form 1-6</a></h3>
                        <p></p>
                        <p><a href="{{ route('register') }}" class="btn btn-primary">Enroll now!</a></p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4  ftco-animate">
                <div class="course align-self-stretch">
                    <a class="img" style="background-image: url({{@genius}}/images/baking_class.jpeg)"></a>
                    <div class="text p-4">
                        <p class="category"><span>Non-Academic</span></p>
                        <h3 class="mb-3"><a>Baking Classes</a></h3>
                        <p></p>
                        <p><a href="{{ route('register') }}" class="btn btn-primary">Enroll now!</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="course align-self-stretch">
                    <a class="img" style="background-image: url({{@genius}}/images/indian_sweets.jpeg)"></a>
                    <div class="text p-4">
                        <p class="category"><span>Non-Academic</span></p>
                        <h3 class="mb-3"><a>Indian Sweets Delicacies</a></h3>
                        <p></p>
                        <p><a href="{{ route('register') }}" class="btn btn-primary">Enroll now!</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="course align-self-stretch">
                    <a class="img" style="background-image: url({{@genius}}/images/online_tutor.jpg)"></a>
                    <div class="text p-4">
                        <p class="category"><span>Academic</span></p>
                        <h3 class="mb-3"><a>Online Tutoring</a></h3>
                        <p></p>
                        <p><a href="{{ route('register') }}" class="btn btn-primary">Enroll now!</a></p>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- <div class="row justify-content-center mt-5">
            <div class="col-md-10 ftco-animate">
                <p><strong>When she reached</strong> the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.</p>
                <p><span>Just Browsing?</span><a href="{{ url('/courses') }}"> View All Courses</a></p>
            </div>
        </div> -->
    </div>
</section>

<section class="ftco-freeTrial" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex align-items-center">
                    <div class="free-trial ftco-animate">
                        <h3>Don't miss this wonderful opportunity !</h3>
                        <p>With easy registration method, you can now reshape the future.</p>
                    </div>
                    <div class="btn-join ftco-animate">
                        <p><a href="{{ route('register') }}" class="btn btn-primary py-3 px-4">Join now!</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="ftco-section">
    <div class="container">
        
         <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate text-center">
                <h2 class="mb-4">How it works ?</h2>
            </div>
        </div>
        
        <div class="row">
            
            <div class="col-md-6 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services p-3 py-4 d-block text-center">
                    
                    <div class="media-body px-3">
                        <h3 class="heading">Teachers</h3>
                        <p align="left">
                            1) Register as a teacher. <br>
                            2) Verify your account by clicking on the link sent to your email. <br>
                            3) Login to update your personal informations. <br>
                            4) Activate your membership by following the steps given in your account. <br>
                            5) Once admin approves your account, you may start to apply for jobs.
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services p-3 py-4 d-block text-center">
                    
                    <div class="media-body px-3">
                        <h3 class="heading">Students</h3>
                        <p align="left">
                            1) Register as a students. <br>
                            2) Verify your account by clicking on the link sent to your email. <br>
                            3) Login to update your personal informations. <br>
                            4) Request for classes.<br>
                        </p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection