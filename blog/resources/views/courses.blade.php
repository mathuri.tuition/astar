@extends('layouts.app')
<?php $page = "courses"; ?>
@section('content')


<div class="hero-wrap hero-wrap-2" style="background-image: url('{{@genius}}/images/bg_2.jpg'); background-attachment:fixed;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-md-8 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span>Course</span></p>
                <h1 class="mb-3 bread">Courses</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section">
    <div class="container">

        <?php
        //Columns must be a factor of 12 (1,2,3,4,6,12)
        $numOfCols = 3;
        $rowCount = 0;
        $bootstrapColWidth = 12 / $numOfCols;
        ?>
         <div class="row">
            <div class="col text-center">
                <h2>Academic Courses</h2>
            </div>
        </div>
        
        <div class="row">

            @foreach ($levels as $level)

            <div class="col-md-4 col-sm-4 d-flex ftco-animate">
                <div class="card" style="width:300px;">
                    <div class="course align-self-stretch">
                        <a href="#" class="img" style="background-image: url({{@genius}}/images/course-1.jpg)"></a>
                        <div class="text p-4">

                            <p class="category"><span></span> <span class="price">RM{{$level->price}}</span></p>
                            <h3 class="mb-3">
                                <a>{{$level->level}}</a>
                            </h3>
                             <p>{{ $level->curriculum }}</p> 
                            <p><a href="{{route('register')}}" class="btn btn-primary">Enroll now!</a></p>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            $rowCount++; 
            ?>
            @if($rowCount % $numOfCols == 0)
        </div><br>
        <div class="row">
            @endif
            @endforeach
        </div>

        <div class="row mt-5">
            <div class="col text-center">
                {!! $levels->render() !!}
            </div>
        </div>
        
        <div class="row mt-5">
            <div class="col text-center">
                <h2>Non-Academic Courses</h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4  ftco-animate">
                <div class="course align-self-stretch">
                    <a class="img" style="background-image: url({{@genius}}/images/baking_class.jpeg)"></a>
                    <div class="text p-4">
                        <p class="category"><span>Non-Academic</span></p>
                        <h3 class="mb-3"><a>Baking Classes</a></h3>
                        <p></p>
                        <p><a href="{{ route('register') }}" class="btn btn-primary">Enroll now!</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="course align-self-stretch">
                    <a class="img" style="background-image: url({{@genius}}/images/indian_sweets.jpeg)"></a>
                    <div class="text p-4">
                        <p class="category"><span>Non-Academic</span></p>
                        <h3 class="mb-3"><a>Indian Sweets Delicacies</a></h3>
                        <p></p>
                        <p><a href="{{ route('register') }}" class="btn btn-primary">Enroll now!</a></p>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
</section>

@endsection