@extends('layouts.app_login')
<?php
$page = 'users-create';
$title = 'Create New User';
?>

@section('content')


@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">
            <div class="card-body">

                {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}

                <div class="row register-form ">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="fullName">{{ __('Full Name') }}</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Name" name="name" value="{{ old('name') }}" autocomplete="name" autofocus />
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="gender">{{ __('Gender') }}</label>
                            <div class="maxl">
                                <label class="radio inline">
                                    <input type="radio" name="gender" value="male" checked>
                                    <span> Male </span>
                                </label>
                                <label class="radio inline">
                                    <input type="radio" name="gender" value="female">
                                    <span>Female </span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phoneNo">{{ __('Phone No') }}</label>
                            <input type="text" minlength="10" maxlength="12" name="phone_no" class="form-control" placeholder="Your Phone" value="{{ old('phone_no') }}" />
                            @error('phone_no')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>
                            <input type="text" class="form-control" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password" value="12345678" />
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="confirmPassword">{{ __('Confirm Password') }}</label>
                            <input type="text" class="form-control" placeholder="Confirm Password" name="confirm-password" autocomplete="confirm-password" value="12345678" />
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="email">{{ __('Email') }}</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" autocomplete="email" autofocus />
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="nationality">{{ __('Nationality') }}</label>
                            <select onselect="myFunction()" class="form-control @error('nationality') is-invalid @enderror" name="nationality" id="nationality">
                                <option class="hidden" selected disabled>Select your nationality...</option>
                                <?php
                                foreach ($nationalities as $k) {
                                    echo  "<option value='$k->id'>$k->nationality</option>";
                                }
                                ?>
                            </select>

                            @error('nationality')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group" id="mykadDiv" style="display:none;">
                            <label for="nricNo">{{ __('NRIC No') }}</label>
                            <input id="mykad" name="mykad" maxlength="12" type="text" placeholder="MyKad" class="form-control @error('mykad') is-invalid @enderror" value="{{ old('mykad') }}" autocomplete="mykad" autofocus>
                            @error('mykad')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group" id="passportDiv" style="display:none;">
                            <label for="passportNo">{{ __('Passport No') }}</label>
                            <input id="passport" type="text" class="form-control @error('passport') is-invalid @enderror" placeholder="Passport Number" name="passport" value="{{ old('passport') }}" autocomplete="passport" autofocus>
                            @error('passport')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="role">{{ __('Role') }}</label>
                            <select name="role" class="form-control">
                                <option selected disabled>Choose role...</option>
                                @foreach($roles as $role)
                                <option value="{{$role}}">{{$role}}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary btnRegister" value="Create" />
                        </div>

                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#nationality").trigger("change");
        $("#nationality").change(function() {
            if ($(this).val() == "109") {
                $('#mykadDiv').show();
                $('#mykad').attr('required', '');
                $('#mykadDiv').attr('data-error', 'This field is required.');
                $('#passportDiv').hide();
                $('#passport').removeAttr('required');
                $('#passportDiv').removeAttr('data-error');
            } else {
                $('#passportDiv').show();
                $('#passport').attr('required', '');
                $('#passportDiv').attr('data-error', 'This field is required.');
                $('#mykadDiv').hide();
                $('#mykad').removeAttr('required');
                $('#mykadDiv').removeAttr('data-error');
            }
        });
    });
</script>

@endsection