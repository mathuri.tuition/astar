@extends('layouts.app_login')
<?php
$page = 'classes-index';
$title = 'Cancelled Requests';
?>

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <td>Reference No.</td>
            <th>Class </th>
            <th>Location</th>
            <th>Admin Approval</th>
            <th>Request Status</th>
            <th>Tutor Status</th>
            <th>Action</th>
        </tr>

        @foreach ($applications as $application)


        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $application->academic_reference_no }}</td>
            <td>{{ $application->class_name }}</td>
            <td>{{ $application->city }}</td>
            <td>
                @if($application->request_status == 1)
                <label class="badge badge-success">Approved</label>
                @elseif($application->request_status == 2)
                <label class="badge badge-danger">Rejected</label>
                @elseif($application->request_status == 3)
                <label class="badge badge-warning">Pending</label>
                @endif
            </td>
            <td>
                @if($application->status == 1)
                <label class="badge badge-success">Active</label>
                @elseif($application->status == 2)
                <label class="badge badge-danger">Cancelled</label>
                @endif
            </td>
            <td>
                @if(is_null($application->tutor_id) )
                <label class="badge badge-primary">Vacant</label>
                @else
                <label class="badge badge-success">Assigned</label>
                @endif
            </td>

            <td>
                <a class="btn btn-info" href="{{ route('students.academics.request_show', $application->academic_reference_no) }}">Show</a>


                {!! Form::open(['method' => 'DELETE','route' => ['students.academics.request_delete', $application->academic_reference_no],'style'=>'display:inline']) !!}
                <!-- {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!} -->
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach

    </table>
</div>


{!! $applications->render() !!}


@endsection