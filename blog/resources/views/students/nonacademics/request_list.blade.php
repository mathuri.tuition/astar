@extends('layouts.app_login')
<?php
$page = 'classes-index';
$title = 'Active Requests';
?>

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <td>Reference No.</td>
            <td>Class</td>
            <th>Location</th>
            <th>Admin Approval</th>
            <th>Request Status</th>
            <th>Tutor Status</th>
            <th>Action</th>
        </tr>

        @foreach ($applications as $application)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $application->reference_no }}</td>
            <td><?= (is_null($application->class_name)) ? $application->class_name : $application->class_name ?></td>
            <td>{{ $application->city }}</td>
            <td>
                @if($application->request_status == 1)
                <label class="badge badge-success">Approved</label>
                @elseif($application->request_status == 2)
                <label class="badge badge-danger">Rejected</label>
                @elseif($application->request_status == 3)
                <label class="badge badge-warning">Pending</label>
                @endif
            </td>
            <td>
                @if($application->status == 1)
                <label class="badge badge-success">Active</label>
                @elseif($application->status == 2)
                <label class="badge badge-danger">Cancelled</label>
                @endif
            </td>
            <td>
                @if(is_null($application->tutor_id) )
                <label class="badge badge-primary">Vacant</label>
                @else
                <label class="badge badge-success">Assigned</label>
                @endif
            </td>

            <td>
                <a class="btn btn-info" href="{{ route('students.nonacademics.request_show', $application->reference_no) }}">Show</a>
                <!-- @if($application->request_status == 3)
            <a class="btn btn-primary" href="{{ route('students.nonacademics.request_edit', $application->reference_no) }}">Edit</a>
            @endif -->

                @if(is_null($application->tutor_id))
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal_{{$application->id}}">Cancel</button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal_{{$application->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <span class="fa fa-random"> </span> Cancel My Request :
                            </div>
                            <div class="modal-body">
                                <h5>{{$application->reference_no}}</h5>
                                <p>This action is not reversible. Are you sure to perform this action ?</p>

                                <form method="POST" action="{{route('students.nonacademics.request_cancel')}}">
                                    @csrf
                                    <input type="text" hidden name="ref_no" id="ref_no" readonly value="{{$application->reference_no}}">
                            </div>
                            <div class="modal-footer content-align-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-success">Yes</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                {!! Form::open(['method' => 'DELETE','route' => ['students.nonacademics.request_delete', $application->id],'style'=>'display:inline']) !!}
                <!-- {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!} -->
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach



    </table>

</div>
    {!! $applications->render() !!}

    @endsection