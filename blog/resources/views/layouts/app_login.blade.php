<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>A Star Educators</title>

    <!-- Favicon - start -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon_io/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon_io/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon_io/favicon-16x16.png')}}">
    <link rel="manifest" href="{{ asset('favicon_io/site.webmanifest')}}">
    <!-- Favicon - end -->

    <!-- Custom fonts for this template-->
    <link href="{{ asset('sbadmin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('sbadmin/css/sb-admin-2.min.css')}}" rel="stylesheet">


    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->

    <!-- Latest compiled and minified CSS -->
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css"> -->

    <!-- Latest compiled and minified JavaScript -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script> -->
</head>

<body id="page-top">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow">

        <!-- Sidebar Toggler (Sidebar) -->
        <button class="btn btn-link" type="button" id="sidebarToggle">
            <span class="fa fa-bars"> Menu</span>
        </button>

        <div class="topbar-divider d-none d-sm-block"></div>

        <a class="logo" href="/">
            <img src="{{ asset('logo/astar_color.png')}}" style="width:10rem; height:auto;" alt="A Star Educators">
        </a>

        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link">
                    <span class="mr-2 d-none d-lg-inline text-gray-600 "> Hi, {{ Auth::user()->name }}</span>
                </a>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item">
                <a class="nav-link text-gray-600" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-600"></i>
                    Logout
                </a>
            </li>

        </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark " id="accordionSidebar">

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Start here New Side Bar -->

            <!-- Superadmin Menu -->
            @role('superadmin')
            <!-- Dashboard -->
            <li class="nav-item">
                <a href="{{ route('superadmins.dashboard') }}" class="nav-link">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <!-- Profile -->
            <li class="nav-item">
                <a href="{{ route('superadmins.profile') }}" class="nav-link">
                    <i class="fas fa-fw fa-user-alt"></i>
                    <span>Profile</span>
                </a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Admin Menu
            </div>
            <!-- Curricula -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSuperadminCurricula" aria-expanded="true" aria-controls="collapseSuperadminCurricula">
                    <i class="fas fa-fw fa-chalkboard"></i>
                    <span>Curricula</span>
                </a>
                <div id="collapseSuperadminCurricula" class="collapse" aria-labelledby="superadminCurricula" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('curricula.index') }}" class="collapse-item">Curricula List</a>
                        <a href="{{ route('curricula.create') }}" class="collapse-item">Add Curriculum</a>
                    </div>
                </div>
            </li>
            <!-- Classes -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSuperadminClass" aria-expanded="true" aria-controls="collapseSuperadminClass">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Academic Classes</span>
                </a>
                <div id="collapseSuperadminClass" class="collapse" aria-labelledby="superadminClass" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('classes.academics.index') }}" class="collapse-item">List Class</a>
                        <a href="{{ route('classes.academics.create') }}" class="collapse-item">Add Class</a>
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#superadminNonAcademicClass" aria-expanded="true" aria-controls="superadminNonAcademicClass">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Non Academic Classes</span>
                </a>
                <div id="superadminNonAcademicClass" class="collapse" aria-labelledby="superadminNonAcademicClass" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('classes.nonacademics.index') }}" class="collapse-item">List Class</a>
                        <a href="{{ route('classes.nonacademics.create') }}" class="collapse-item">Add Class</a>
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSuperadminJob" aria-expanded="true" aria-controls="collapseSuperadminJob">
                    <i class="fas fa-fw fa-briefcase"></i>
                    <span>Academic Job</span>
                </a>
                <div id="collapseSuperadminJob" class="collapse" aria-labelledby="superadminJob" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- By tutor: tutors need to apply for this job -->
                        <a href="{{ route('jobs.academics.index') }}" class="collapse-item">Job Offers To Approve</a>
                        <a href="{{ route('jobs.academics.job_approved') }}" class="collapse-item">Approved Job Offers</a>
                        <a href="{{ route('jobs.academics.job_rejected') }}" class="collapse-item">Rejected Job Offers</a>
                        <!-- <a href="{{ route('jobs.academics.job_pending') }}" class="collapse-item">Pending Job Offers</a> -->

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">

                        <a href="{{ route('admins.academics.job_assigned_list') }}" class="collapse-item">List Assigned Jobs</a>

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">

                        <a href="{{ route('admins.academics.job_application_index') }}" class="collapse-item">Tutor Applications</a>

                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#superadminNonAcademicJob" aria-expanded="true" aria-controls="superadminNonAcademicJob">
                    <i class="fas fa-fw fa-briefcase"></i>
                    <span>Non Academic Job</span>
                </a>
                <div id="superadminNonAcademicJob" class="collapse" aria-labelledby="superadminNonAcademicJob" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- By tutor: tutors need to apply for this job -->
                        <a href="{{ route('jobs.nonacademics.index') }}" class="collapse-item">Job Offers To Approve</a>
                        <a href="{{ route('jobs.nonacademics.job_approved') }}" class="collapse-item">Approved Job Offers</a>
                        <a href="{{ route('jobs.nonacademics.job_rejected') }}" class="collapse-item">Rejected Job Offers</a>
                        <!-- <a href="{{ route('jobs.academics.job_pending') }}" class="collapse-item">Pending Job Offers</a> -->

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">

                        <a href="{{ route('admins.academics.job_assigned_list') }}" class="collapse-item">List Assigned Jobs</a>

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">

                        <a href="{{ route('admins.academics.job_application_index') }}" class="collapse-item">Tutor Applications</a>

                    </div>
                </div>
            </li>

            <!-- Students -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSuperadminStudent" aria-expanded="true" aria-controls="collapseSuperadminStudent">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Students</span>
                </a>
                <div id="collapseSuperadminStudent" class="collapse" aria-labelledby="headingUser" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('admins.users.students.index') }}" class="collapse-item">Students List</a>
                        <a href="{{ route('admins.users.students.create') }}" class="collapse-item">Add Student</a>
                    </div>
                </div>
            </li>
            <!-- Tutors -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSuperadminTutor" aria-expanded="true" aria-controls="collapseSuperadminTutor">
                    </i><i class="fas fa-fw fa-chalkboard-teacher"></i>
                    <span>Tutors</span>
                </a>
                <div id="collapseSuperadminTutor" class="collapse" aria-labelledby="superadminTutor" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('admins.users.tutors.index') }}" class="collapse-item">List Tutor</a>
                        <a href="{{ route('admins.users.tutors.create') }}" class="collapse-item">Add Tutor</a>
                        <a href="{{ route('jobs.academics.job_application_index') }}" class="collapse-item">Academic Job Applications</a>
                    </div>
                </div>
            </li>
            <!-- Admins -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSuperadminAdmin" aria-expanded="true" aria-controls="collapseSuperadminAdmin">
                    </i><i class="fas fa-fw fa-chalkboard-teacher"></i>
                    <span>Admins</span>
                </a>
                <div id="collapseSuperadminAdmin" class="collapse" aria-labelledby="uperadminAdmin" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('admins.users.admins.index') }}" class="collapse-item">List Admin</a>
                        <a href="{{ route('admins.users.admins.create') }}" class="collapse-item">Add Admin</a>
                    </div>
                </div>
            </li>


            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Tutor Menu
            </div>

            <!-- Academic Jobs Offers -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#superadminTutorAcademicJob" aria-expanded="true" aria-controls="superadminTutorAcademicJob">
                    <i class="fas fa-fw fa-briefcase"></i>
                    <span>Academic</span>
                </a>
                <div id="superadminTutorAcademicJob" class="collapse" aria-labelledby="superadminTutorAcademicJob" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('admins.academics.job_application_index') }}" class="collapse-item">List Job Offers</a>
                    </div>
                </div>
            </li>

            <!-- Non Academic Jobs Offers -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#superadminTutorNonAcademicJob" aria-expanded="true" aria-controls="superadminTutorNonAcademicJob">
                    <i class="fas fa-fw fa-briefcase"></i>
                    <span>Non Academic</span>
                </a>
                <div id="superadminTutorNonAcademicJob" class="collapse" aria-labelledby="superadminTutorNonAcademicJob" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('admins.nonacademics.job_application_index') }}" class="collapse-item">List Job Offers</a>
                    </div>
                </div>
            </li>


            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Superadmin Menu
            </div>
            <!-- Roles -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseRole" aria-expanded="true" aria-controls="collapseRole">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Roles</span>
                </a>
                <div id="collapseRole" class="collapse" aria-labelledby="collapseRole" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('roles.index') }}" class="collapse-item">Roles List</a>
                        <a href="{{ route('roles.create') }}" class="collapse-item">Add Role</a>
                    </div>
                </div>
            </li>
            <!-- Permissions -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePermission" aria-expanded="true" aria-controls="collapsePermission">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Permissions</span>
                </a>
                <div id="collapsePermission" class="collapse" aria-labelledby="collapsePermission" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('permissions.index') }}" class="collapse-item">Permissions List</a>
                        <a href="{{ route('permissions.create') }}" class="collapse-item">Add Permission</a>
                    </div>
                </div>
            </li>
            <!-- Users -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUser" aria-expanded="true" aria-controls="collapseUser">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Users</span>
                </a>
                <div id="collapseUser" class="collapse" aria-labelledby="collapseUser" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('users.index') }}" class="collapse-item">User List</a>
                        <a href="{{ route('users.create') }}" class="collapse-item">Add User</a>
                    </div>
                </div>
            </li>
            @endrole


            <!-- Admin Menu -->
            @role('admin')
            <!-- Dashboard -->
            <li class="nav-item ">
                <a href="{{ route('admins.dashboard') }}" class="nav-link">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <!-- Profile -->
            <li class="nav-item ">
                <a href="{{ route('admins.profile') }}" class="nav-link">
                    <i class="fas fa-fw fa-user-alt"></i>
                    <span>Profile</span>
                </a>
            </li>
            <!-- Academic Jobs -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdminJob" aria-expanded="true" aria-controls="collapseAdminJob">
                    <i class="fas fa-fw fa-briefcase"></i>
                    <span>Academic Jobs</span>
                </a>
                <div id="collapseAdminJob" class="collapse" aria-labelledby="collapseAdminJob" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- By tutor: tutors need to apply for this job -->
                        <a href="{{ route('jobs.academics.index') }}" class="collapse-item">Job Offers To Approve</a>
                        <a href="{{ route('jobs.academics.job_approved') }}" class="collapse-item">Approved Job Offers</a>
                        <a href="{{ route('jobs.academics.job_rejected') }}" class="collapse-item">Rejected Job Offers</a>
                        <!-- <a href="{{ route('jobs.academics.job_pending') }}" class="collapse-item">Pending Job Offers</a> -->

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">
                        <a href="{{ route('admins.academics.job_application_index') }}" class="collapse-item">Tutor Applications</a>

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">
                        <a href="{{ route('admins.academics.job_assigned_list') }}" class="collapse-item">List Assigned Jobs</a>

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">
                        <a href="{{ route('admins.academics.job_settled_index') }}" class="collapse-item">List Settled Jobs</a>

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">
                        <a href="{{ route('admins.academics.job_cancelled_index') }}" class="collapse-item">Invalid Jobs</a>
                    </div>
                </div>
            </li>
            <!-- Non Academic Jobs -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseNonAcademicJob" aria-expanded="true" aria-controls="collapseNonAcademicJob">
                    <i class="fas fa-fw fa-briefcase"></i>
                    <span>Non-academic Jobs</span>
                </a>
                <div id="collapseNonAcademicJob" class="collapse" aria-labelledby="collapseNonAcademicJob" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!-- By tutor: tutors need to apply for this job -->
                        <a href="{{ route('jobs.nonacademics.index') }}" class="collapse-item">Job Offers To Approve</a>
                        <a href="{{ route('jobs.nonacademics.job_approved') }}" class="collapse-item">Approved Job Offers</a>
                        <a href="{{ route('jobs.nonacademics.job_rejected') }}" class="collapse-item">Rejected Job Offers</a>

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">
                        <a href="{{ route('admins.nonacademics.job_application_index') }}" class="collapse-item">Tutor Applications</a>

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">
                        <a href="{{ route('admins.nonacademics.job_assigned_list') }}" class="collapse-item">List Assigned Jobs</a>

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">
                        <a href="{{ route('admins.nonacademics.job_settled_index') }}" class="collapse-item">List Settled Jobs</a>

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">
                        <a href="{{ route('admins.nonacademics.job_cancelled_index') }}" class="collapse-item">Invalid Jobs</a>
                    </div>
                </div>
            </li>
            <!-- Curricula -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdminCurricula" aria-expanded="true" aria-controls="collapseAdminCurricula">
                    <i class="fas fa-fw fa-chalkboard"></i>
                    <span>Curricula</span>
                </a>
                <div id="collapseAdminCurricula" class="collapse" aria-labelledby="collapseAdminCurricula" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('curricula.index') }}" class="collapse-item">Curricula List</a>
                        <a href="{{ route('curricula.create') }}" class="collapse-item">Add Curriculum</a>
                    </div>
                </div>
            </li>

            <!-- Rates -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdminLevels" aria-expanded="true" aria-controls="collapseAdminLevels">
                    <i class="fas fa-fw fa-chalkboard"></i>
                    <span>Rates</span>
                </a>
                <div id="collapseAdminLevels" class="collapse" aria-labelledby="collapseAdminLevels" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('admins.academics.price') }}" class="collapse-item">Academic Rate PDF</a>
                        <a href="{{ route('admins.nonacademics.price') }}" class="collapse-item">Non Academic Rate PDF</a>
                        <a href="{{ route('levels.index') }}" class="collapse-item">Rate List</a>
                        <a href="{{ route('levels.create') }}" class="collapse-item">New Rate</a>
                    </div>
                </div>
            </li>
            <!-- Academic Classes -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdminClass" aria-expanded="true" aria-controls="collapseAdminClass">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Academic Classes</span>
                </a>
                <div id="collapseAdminClass" class="collapse" aria-labelledby="collapseAdminClass" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('classes.academics.index') }}" class="collapse-item">Class List</a>
                        <a href="{{ route('classes.academics.create') }}" class="collapse-item">Class Create</a>
                    </div>
                </div>
            </li>
            <!-- Non Academic Classes -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdminNonAcademicClass" aria-expanded="true" aria-controls="collapseAdminNonAcademicClass">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Non-academic Classes</span>
                </a>
                <div id="collapseAdminNonAcademicClass" class="collapse" aria-labelledby="collapseAdminNonAcademicClass" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('classes.nonacademics.index') }}" class="collapse-item">Class List</a>
                        <a href="{{ route('classes.nonacademics.create') }}" class="collapse-item">Class Create</a>
                    </div>
                </div>
            </li>
            <!-- Admins -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdminAdmin" aria-expanded="true" aria-controls="collapseAdminAdmin">
                    </i><i class="fas fa-fw fa-chalkboard-teacher"></i>
                    <span>Admins</span>
                </a>
                <div id="collapseAdminAdmin" class="collapse" aria-labelledby="collapseAdminAdmin" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('admins.users.admins.index') }}" class="collapse-item">List</a>
                        <a href="{{ route('admins.users.admins.create') }}" class="collapse-item">New</a>
                    </div>
                </div>
            </li>
            <!-- Tutors -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdminTutor" aria-expanded="true" aria-controls="collapseAdminTutor">
                    </i><i class="fas fa-fw fa-chalkboard-teacher"></i>
                    <span>Tutors</span>
                </a>
                <div id="collapseAdminTutor" class="collapse" aria-labelledby="collapseAdminTutor" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('admins.users.tutors.index') }}" class="collapse-item">List</a>
                        <a href="{{ route('admins.users.tutors.create') }}" class="collapse-item">New</a>
                    </div>
                </div>
            </li>
            <!-- Students -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdminStudent" aria-expanded="true" aria-controls="collapseAdminStudent">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Students</span>
                </a>
                <div id="collapseAdminStudent" class="collapse" aria-labelledby="collapseAdminStudent" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('admins.users.students.index') }}" class="collapse-item">List</a>
                        <a href="{{ route('admins.users.students.create') }}" class="collapse-item">New</a>
                    </div>
                </div>
            </li>
            <!-- Tutor Profiles -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTutorProfile" aria-expanded="true" aria-controls="collapseTutorProfile">
                    </i><i class="fas fa-fw fa-chalkboard-teacher"></i>
                    <span>Tutor Profiles</span>
                </a>
                <div id="collapseTutorProfile" class="collapse" aria-labelledby="collapseTutorProfile" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('admins.tutorprofiles.index') }}" class="collapse-item">List</a>
                        <a href="{{ route('admins.tutorprofiles.create') }}" class="collapse-item">New</a>
                    </div>
                </div>
            </li>
            @endrole

            <!-- Tutor Menu -->
            @role('tutor')
            <!-- Dashboard -->
            <li class="nav-item">
                <a href="{{ route('tutors.dashboard') }}" class="nav-link">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <!-- Profile -->
            <li class="nav-item">
                <a href="{{ route('tutors.profile') }}" class="nav-link">
                    <i class="fas fa-fw fa-user-alt"></i>
                    <span>Profile</span>
                </a>
            </li>
            <!-- Tutor Membership -->
            <li class="nav-item">
                <a href="{{ route('tutors.membership') }}" class="nav-link">
                    <i class="fas fa-fw fa-user-alt"></i>
                    <span>Membership</span>
                </a>
            </li>
            <!-- Tutor Qualifications -->
            <li class="nav-item">
                <a href="{{ route('tutors.qualification') }}" class="nav-link">
                    <i class="fas fa-fw fa-user-alt"></i>
                    <span>Qualifications</span>
                </a>
            </li>
            <!-- Tutor Class Preference -->
            <li class="nav-item">
                <a href="{{ route('tutors.class_preference') }}" class="nav-link">
                    <i class="fas fa-fw fa-user-alt"></i>
                    <span>Class Preference</span>
                </a>
            </li>


            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Academic Job -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTutorJob" aria-expanded="true" aria-controls="collapseTutorJob">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Academic Jobs</span>
                </a>
                <div id="collapseTutorJob" class="collapse" aria-labelledby="collapseTutorJob" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('tutors.academics.job_index') }}" class="collapse-item">Job Offers</a>
                        <a href="{{ route('tutors.academics.job_applied') }}" class="collapse-item">Applied Jobs</a>
                        <a href="{{ route('tutors.academics.job_approved') }}" class="collapse-item">My Offers</a>


                    </div>
                </div>
            </li>

            <!-- Non Academic Job -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTutorNonAcademicJob" aria-expanded="true" aria-controls="collapseTutorNonAcademicJob">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Non-academic Jobs</span>
                </a>
                <div id="collapseTutorNonAcademicJob" class="collapse" aria-labelledby="collapseTutorNonAcademicJob" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('tutors.nonacademics.job_index') }}" class="collapse-item">Job Offers</a>
                        <a href="{{ route('tutors.nonacademics.job_applied') }}" class="collapse-item">Applied Jobs</a>
                        <a href="{{ route('tutors.nonacademics.job_approved') }}" class="collapse-item">My Offers</a>


                    </div>
                </div>
            </li>
            @endrole

            <!-- Student Menu -->
            @role('student')
            <!-- Dashboard -->
            <li class="nav-item">
                <a href="{{ route('students.dashboard') }}" class="nav-link">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <!-- Profile -->
            <li class="nav-item">
                <a href="{{ route('students.profile') }}" class="nav-link">
                    <i class="fas fa-fw fa-user-alt"></i>
                    <span>Profile</span>
                </a>
            </li>
            <!-- Academic Class -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#studentAcademicClass" aria-expanded="true" aria-controls="studentAcademicClass">
                    <i class="fas fa-fw fa-briefcase"></i>
                    <span>Academic Class</span>
                </a>
                <div id="studentAcademicClass" class="collapse" aria-labelledby="studentAcademicClass" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a href="{{ route('students.academics.show_pdf') }}" target="_blank" class="collapse-item">Fees</a>
                        <a href="{{ route('students.academics.request_class') }}" class="collapse-item">New Request</a>
                        <a href="{{ route('students.academics.request_list') }}" class="collapse-item">Active Request</a>
                        <a href="{{ route('students.academics.request_success_index') }}" class="collapse-item">Successful Request</a>
                        <a href="{{ route('students.academics.request_cancel_list') }}" class="collapse-item">Cancelled Request</a>

                    </div>
                </div>
            </li>
            <!-- Non Academic Class -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#studentNonAcademicClass" aria-expanded="true" aria-controls="studentNonAcademicClass">
                    <i class="fas fa-fw fa-briefcase"></i>
                    <span>Non-academic Class</span>
                </a>
                <div id="studentNonAcademicClass" class="collapse" aria-labelledby="studentNonAcademicClass" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <!--<a href="{{ route('students.nonacademics.show_pdf') }}" target="_blank" class="collapse-item">Fees</a>-->
                        <a href="{{ route('students.nonacademics.request_class') }}" class="collapse-item">New Request</a>
                        <a href="{{ route('students.nonacademics.request_list') }}" class="collapse-item">Active Request</a>
                        <a href="{{ route('students.nonacademics.request_success_index') }}" class="collapse-item">Successful Request</a>
                        <a href="{{ route('students.nonacademics.request_cancel_list') }}" class="collapse-item">Cancelled Request</a>
                    </div>
                </div>
            </li>

            @endrole

            <!-- End here New Side Bar -->

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column" style="padding-top: 1.5rem;">

            <!-- Main Content -->
            <div id="content">



                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">{{ $title }}</h1>
                    </div>

                    <!-- Content Row -->
                    <!-- <div class="row"> -->
                    <div class="container">
                        @yield('content')
                    </div>
                    <!-- </div> -->
                    <br>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; A'Star Educators 2019</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Logout Confirmation</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Are you sure to logout ?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();"> {{ __('Logout') }}</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </div>
            </div>
        </div>
    </div>

    <script>
        function goBack() {
            window.history.back();
        }
    </script>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('sbadmin/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('sbadmin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('sbadmin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('sbadmin/js/sb-admin-2.min.js')}} "></script>

    <!-- Page level plugins -->
    <!-- <script src="vendor/chart.js/Chart.min.js"></script> -->

    <!-- Page level custom scripts -->
    <!-- <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script> -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> -->
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.11/js/bootstrap-select.js"></script> -->

</body>

</html>