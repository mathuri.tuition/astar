<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>A Star Educators</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon - start -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon_io/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon_io/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon_io/favicon-16x16.png')}}">
    <link rel="manifest" href="{{ asset('favicon_io/site.webmanifest')}}">
    <!-- Favicon - end -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('genius/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('genius/css/animate.css')}}">

    <link rel="stylesheet" href="{{ asset('genius/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('genius/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('genius/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{ asset('genius/css/aos.css')}}">

    <link rel="stylesheet" href="{{ asset('genius/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{ asset('genius/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('genius/css/jquery.timepicker.css')}}">


    <link rel="stylesheet" href="{{ asset('genius/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{ asset('genius/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{ asset('genius/css/style.css')}}">

   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('sbadmin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('sbadmin/css/sb-admin-2.min.css')}}" rel="stylesheet">


   
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light ftco_navbar bg-light ftco-navbar-light" id="ftco-navbar">
        <!-- <div class="container"> -->

        <a class="logo" href="/"><img src="{{ asset('logo/astar_color.png')}}" style="width:10rem; height:auto;" alt="A Star Educators"></a>

        <div class="float-right">
            <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="oi oi-menu"></span> Menu
            </button>
        </div>

        <div class="collapse navbar-collapse " id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item <?= (($page == "home") ? 'active' : '') ?>"><a href="/" class="nav-link">Home</a></li>
                <li class="nav-item <?= (($page == "about") ? 'active' : '') ?>"><a href="about" class="nav-link">About</a></li>
                <li class="nav-item <?= (($page == "courses") ? 'active' : '') ?>"><a href="courses" class="nav-link">Courses</a></li>
                <!--<li class="nav-item <?= (($page == "teachers") ? 'active' : '') ?>"><a href="teachers" class="nav-link">Teachers</a></li>-->
                <li class="nav-item <?= (($page == "contact") ? 'active' : '') ?>"><a href="contact-us" class="nav-link">Contact Us</a></li>
                <li class="nav-item <?= (($page == "terms") ? 'active' : '') ?>"><a href="terms-and-conditions" class="nav-link">Terms & Conditions</a></li>
                <li class="nav-item <?= (($page == "login") ? 'active' : '') ?>"><a href="{{ route('login') }}" class="nav-link">{{ __('Login') }}</a></li>
                <li class="nav-item <?= (($page == "login") ? 'active' : '') ?>"><a href="{{ route('register') }}" class="nav-link">{{ __('Register') }}</a></li>
                <!-- <li class="nav-item cta"><a href="{{ route('register') }}" class="nav-link"><span>Sign Up!</span></a></li> -->
                <li class="nav-item cta"><a href="{{ route('request_class.nonregister') }}" class="nav-link"><span>Request Now !</span></a></li>
            </ul>
        </div>

        <!-- </div> -->
    </nav>

    <div class="container-fluid">
        @yield('content')
    </div>

    <section class="ftco-section-parallax">
        <div class="parallax-img d-flex align-items-center">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                        <h2>Subscribe to our Newsletter</h2>
                        <p>Subscribe now to receive latest updates and promotions from our website.</p>
                        <div class="row d-flex justify-content-center mt-5">
                            <div class="col-sm-8 col-md-10">
                                <form id="newsletterForm" class="subscribe-form newsletter">
                                    @csrf
                                    <div class="form-group d-flex">
                                        <input type="email" class="form-control" name="news_email" required id="news_email" placeholder="Enter email address">

                                        <input type="submit" id="news_submit" value="Subscribe" class="submit px-3">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Button trigger modal -->
    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
        Launch demo modal
    </button> -->

    <!-- Newsletter Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Subscription successful</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Congratulations! <br> You will be receiving updates and promotions from us. <br>
                    Thank you for your subscription.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalError" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Subscription failed</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Opppsss.. Something went wrong. Please try again.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <footer class="ftco-footer ftco-bg-dark ftco-section img" style="background-image: url({{@genius}}/images/bg_2.jpg); background-attachment:fixed;">
        <div class="overlay"></div>
        <div class="container">
            <div class="row mb-5">

                <div class="col-lg-5 col-md-5 col-sm-12">
                    <div class="ftco-footer-widget mb-4">
                        <h2><a class="navbar-brand" href="/"><img src="{{ asset('logo/v5_white.png')}}" style="width:9rem; height:auto;" alt="A Star Educators"></a></h2>
                        <p>A Star Educators is a platform for students to search for desired teachers and also offers job opportunity to qualified teachers.</p>
                        <!-- <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                            <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                            <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                            <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                        </ul> -->
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="ftco-footer-widget mb-4 ml-md-4">
                        <h2 class="ftco-heading-2">Site Links</h2>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('/') }}" class="py-2 d-block">Home</a></li>
                            <li><a href="{{ url('/about') }}" class="py-2 d-block">About</a></li>
                            <li><a href="{{ url('/courses') }}" class="py-2 d-block">Courses</a></li>
                            <li><a href="{{ url('/contact-us') }}" class="py-2 d-block">Contact Us</a></li>
                            <li><a href="{{ url('/terms-and-conditions') }}" class="py-2 d-block">Terms & Conditions</a></li>
                            <li><a href="{{ url('/login') }}" class="py-2 d-block">Login</a></li>
                            <li><a href="{{ url('/register') }}" class="py-2 d-block">Sign Up</a></li>
                            <!--<li><a href="{{ url('/teachers') }}" class="py-2 d-block">Teachers</a></li>-->
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Have a Questions?</h2>
                        <div class="block-23 mb-3">
                            <ul>
                                <li><span class="icon icon-map-marker"></span><span class="text">Kuala Lumpur</span></li>
                                <li><a><span class="icon icon-phone"></span><span class="text">+6011-1070 4756</span></a></li>
                                <li><a><span class="icon icon-envelope"></span><span class="text">admin@stareducators.my</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">

                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | by <a>Lav1</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </footer>


    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen">
        <svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" />
        </svg>
    </div>




    <script src="{{ asset('genius/js/jquery.min.js')}}"></script>
    <script src="{{ asset('genius/js/jquery-migrate-3.0.1.min.js')}}"></script>
    <script src="{{ asset('genius/js/popper.min.js')}}"></script>
    <script src="{{ asset('genius/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('genius/js/jquery.easing.1.3.js')}}"></script>
    <script src="{{ asset('genius/js/jquery.waypoints.min.js')}}"></script>
    <script src="{{ asset('genius/js/jquery.stellar.min.js')}}"></script>
    <script src="{{ asset('genius/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('genius/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('genius/js/aos.js')}}"></script>
    <script src="{{ asset('genius/js/jquery.animateNumber.min.js')}}"></script>
    <script src="{{ asset('genius/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{ asset('genius/js/jquery.timepicker.min.js')}}"></script>
    <script src="{{ asset('genius/js/scrollax.min.js')}}"></script>
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script> -->
    <!-- <script src="{{ asset('genius/js/google-map.js')}}"></script> -->
    <script src="{{ asset('genius/js/main.js')}}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $(document).ready(function() {
            $('#newsletterForm').submit(function(e) {
                e.preventDefault(); //**** to prevent normal form submission and page reload

                $.ajax({
                    type: 'POST',
                    url: "{{url('/newsletter/subscribe') }} ",
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(result) {
                        $('#exampleModalCenter').modal('show');
                        setTimeout(function() {
                            $('#exampleModalCenter').modal('hide');
                        }, 3000);
                        $('#newsletterForm').find('input:not([type="submit"])').val('');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        $('#exampleModalError').modal('show');
                        setTimeout(function() {
                            $('#exampleModalError').modal('hide');
                        }, 3000);
                    }
                });
            });
        });
    </script>
</body>

</html>