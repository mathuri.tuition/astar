@extends('layouts.app')
<?php $page = "terms"; ?>
@section('content')

<style>
    .btn-width {
        width: 150px;
    }
</style>

<div class="hero-wrap hero-wrap-2" style="background-image: url('{{@genius}}/images/bg_2.jpg'); background-attachment:fixed;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-md-8 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span>Terms & Conditions</span></p>
                <h1 class="mb-3 bread">Terms & Conditions</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section contact-section ftco-degree-bg">
    <div class="container">
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-primary btn-width collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Students
                        </button>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p>Please be informed that terms used such as (We, Us, Our) refers to A'Star Educators and terms such as (You, your) refers to Students.</p>
                        <p>
                            1) There is no registration fees for students or parents. <br>
                            2) You will have to pay only the first month tuition fees to us and pay the subsequent fees directly to the tutor. <br>
                            3) We will charge RM100 as service fees to match a tutor with your preference. <br>
                            4) Upon receiving the payment, the tutor's details will be given to the student or parent. <br>
                            5) In case you are unhappy with the tutor, we will change the tutor within 2 weeks without any extra charges. <br>
                            6) Any changes regarding the tutor is only allowed within the first month. Any changes request upon that will have to go through a new request. <br>
                            7) All tutors will be interviewed by our staff to ensure that we provide the best for you. <br>
                            8) Any request to change tutor will be reviewed by our staff for validation. <br>
                            
                            <br>
                            Bank : CIMB <br>
                            Name : Astar Agency <br>
                            Acc.no : 8010268445<br>

                        </p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-primary btn-width collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Tutors
                        </button>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <p>Please be informed that terms such as (We, Us, Our) refers to A'Star Educators and terms such as (You, your) refers to Tutors.</p>
                        <p>
                            1) You need to update your profile & education qualifications upon successful registration. <br>
                            2) Your account will be reviewed and approved by our staffs. Accounts with complete details will be approved fast. <br>
                            3) You will have to pay RM50 for an annual membership with us. <br>
                            4) Accounts with active membership only are allowed to view job offers. <br>
                            5) Our agency will charge a commission of 50% from the first month tuition fees paid by the students. <br>
                            6) Your details will be given to the student or parents only after receiving payment from students. <br>
                            7) If student or parents request to change tutor within the first month, only 20% of fees will be paid to you. <br>
                            8) Any request to change tutor will be reviewed by our staff for validation. <br>
                            9) Please do note that annual membership fees is not refundable. <br>
                            
                            <br>
                            Bank : CIMB <br>
                            Name : Astar Agency <br>
                            Acc.no : 8010268445<br>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>



@endsection