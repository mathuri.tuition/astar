@extends('layouts.app_login')
<?php
$page = 'tutor-class_preference';
$title = 'Class Preferences';

if(is_null($preference))
{
    $subjects = '';
    $areas = '';
    $experience = '';
}else{
    $subjects = $preference->subjects;
    $areas = $preference->areas;
    $experience = $preference->experience;
}
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Whoops!</strong> There were some problems with your input.<br><br>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif


<div class="col-12">
    <div class="tab-content">
        <div class="tab-pane fade show active" id="account" role="tabpanel">

            <div class="card" id="subjects">
                <div class="card-header">
                    <div class="float-right">
                        <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#subjectsInfo" aria-expanded="false" aria-controls="subjectsInfo">
                            <span class="fa fa-caret-down"></span>
                        </button>
                    </div>
                    <h5 class="card-title mb-0">Preferred subjects or skills</h5>
                </div>
                <div class="card-body collapse" id="subjectsInfo">
                    <form method="POST" action="{{route('tutors.class_preference.update_subjects')}}">
                        @csrf
                        <div class="form-group">
                            <label>You may list the subjects or skills you can teach. Add comma (,) to continue with the list.</label>
                            <textarea maxlength="500" name="subjects" id="subjects" class="form-control">{{ $subjects }}</textarea>
                        </div>
                        <input type="submit" name="submit" id="submit" class="btn btn-success" value="Save" />
                    </form>
                </div>
            </div>

            <br><br>

            <div class="card" id="experience">
                <div class="card-header">
                    <div class="float-right">
                        <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#experienceInfo" aria-expanded="false" aria-controls="experienceInfo">
                            <span class="fa fa-caret-down"></span>
                        </button>
                    </div>
                    <h5 class="card-title mb-0">Teaching Experience</h5>
                </div>
                <div class="card-body collapse" id="experienceInfo">

                    <form method="POST" action="{{ route('tutors.class_preference.update_experience') }}">
                        @csrf
                        <div class="form-group">
                            <label>What are you experience in teaching ?</label>
                            <textarea maxlength="500" name="experience" id="experience" class="form-control">{{ $experience }}</textarea>
                        </div>
                        <input type="submit" name="submit" id="submit" class="btn btn-success" value="Save" />
                    </form>
                </div>
            </div>

            <br><br>

            <div class="card" id="areas">
                <div class="card-header">
                    <div class="float-right">
                        <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#areasInfo" aria-expanded="false" aria-controls="areasInfo">
                            <span class="fa fa-caret-down"></span>
                        </button>
                    </div>
                    <h5 class="card-title mb-0">Preferred Areas</h5>
                </div>
                <div class="card-body collapse" id="areasInfo">
                    <form method="POST" action="{{ route('tutors.class_preference.update_areas') }}">
                        @csrf
                        <div class="form-group">
                            <label>Which are the areas you prefer to conduct classes?</label>
                            <textarea maxlength="500" name="areas" id="areas" class="form-control">{{ $areas }}</textarea>
                        </div>
                        <input type="submit" name="submit" id="submit" class="btn btn-success" value="Save" />
                    </form>
                </div>
            </div>

        </div>

    </div>
</div>


@endsection