@extends('layouts.app_login')
<?php
$page = 'tutor-academic-job_apply';
$title = $class->academic_reference_no;

$mon = '';
$tue = '';
$wed = '';
$thu = '';
$fri = '';
$sat = '';
$sun = '';

$days = json_decode($class->days);

foreach ($days as $day) {

    if ($day == "mon") {
        $mon = $day;
    }
    if ($day == "tue") {
        $tue = $day;
    }
    if ($day == "wed") {
        $wed = $day;
    }
    if ($day == "thu") {
        $thu = $day;
    }
    if ($day == "fri") {
        $fri = $day;
    }
    if ($day == "sat") {
        $sat = $day;
    }
    if ($day == "sun") {
        $sun = $day;
    }
}
?>
@section('content')
<style>
    .fa {
        padding-right: 10px;
    }

    .col-sm-12,
    .col-md-6 {
        margin-bottom: 25px;
    }

    .td {
        width: 32%;
    }

    .mark_space {
        width: 5%;
    }

    .weekDays-selector input {
        display: none !important;
    }

    .weekDays-selector input[type=checkbox]+label {
        display: inline-block;
        border-radius: 6px;
        background: #dddddd;
        height: 40px;
        width: 45px;
        margin-right: 3px;
        line-height: 40px;
        text-align: center;
        cursor: pointer;
    }

    .weekDays-selector input[type=checkbox]:checked+label {
        background: #2AD705;
        color: #ffffff;
    }
</style>

<!-- Student Details -->
<div class="row">
    <div class="col-sm-12 col-md-12 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-user-graduate"> </span> Student Info :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Gender</td>
                        <td class="mark_space">:</td>
                        <td><?= ($class->gender == 1 ? 'Male' : 'Female') ?></td>
                    </tr>
                    <tr>
                        <td class="td">Race</td>
                        <td class="mark_space">:</td>
                        <td>
                            @foreach($races as $race)
                            @if($race->id == $class->race)
                            {{$race->race}}
                            @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td class="td">Nationality</td>
                        <td class="mark_space">:</td>
                        <td>{{$class->nationality}}</td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>

<!-- Tutor location -->
<div class="row">
    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-map-marked"> </span> Tuition Job Location :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">State</td>
                        <td class="mark_space">:</td>
                        <td>
                            @foreach($states as $state)
                            @if($state->code == $class->state)
                            {{ $state->state }}
                            @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td class="td">City</td>
                        <td class="mark_space">:</td>
                        <td>{{ $class->city }}</td>
                    </tr>
                    <tr>
                        <td class="td">Postcode</td>
                        <td class="mark_space">:</td>
                        <td>{{ $class->postcode }}</td>
                    </tr>
                    <tr>
                        <td class="td">Area</td>
                        <td class="mark_space">:</td>
                        <td>{{ $class->area }}

                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>

    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-chalkboard-teacher"> </span> Tuition Job Description :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Syllabus</td>
                        <td class="mark_space">:</td>
                        <td>{{$class->curriculum}}</td>
                    </tr>
                    <tr>
                        <td class="td">Level</td>
                        <td class="mark_space">:</td>
                        <td>{{$class->edu_level}}</td>
                    </tr>
                    <tr>
                        <td class="td">Subject</td>
                        <td class="mark_space">:</td>
                        <td>{{$class->class_name}}</td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>


<!-- Timing -->
<div class="row">
    <div class="col-sm-12 col-md-12 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-clock"> </span> Tuition Preferred Timing :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Frequency</td>
                        <td class="mark_space">:</td>
                        <td>
                            <select id="frequency" readonly disabled class="form-control ">
                                <option disabled selected>Choose...</option>
                                <option <?= $class->frequency == 1 ? 'selected' : '' ?> value="1">1 class per week</option>
                                <option <?= $class->frequency == 2 ? 'selected' : '' ?> value="2">2 class per week</option>
                                <option <?= $class->frequency == 3 ? 'selected' : '' ?> value="3">3 class per week</option>
                                <option <?= $class->frequency == 4 ? 'selected' : '' ?> value="4">4 class per week</option>
                                <option <?= $class->frequency == 5 ? 'selected' : '' ?> value="5">5 class per week</option>
                                <option <?= $class->frequency == 6 ? 'selected' : '' ?> value="6">6 class per week</option>
                                <option <?= $class->frequency == 7 ? 'selected' : '' ?> value="7">7 class per week</option>
                                <option <?= $class->frequency == 8 ? 'selected' : '' ?> value="8">More than 7 class</option>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Days</td>
                        <td class="mark_space">:</td>
                        <td>
                            <div class="weekDays-selector">
                                <input type="checkbox" id="weekday-mon" name="day[]" value="mon" <?= $mon == 'mon' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-mon">Mon</label>
                                <input type="checkbox" id="weekday-tue" name="day[]" value="tue" <?= $tue == 'tue' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-tue">Tue</label>
                                <input type="checkbox" id="weekday-wed" name="day[]" value="wed" <?= $wed == 'wed' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-wed">Wed</label>
                                <input type="checkbox" id="weekday-thu" name="day[]" value="thu" <?= $thu == 'thu' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-thu">Thu</label>
                                <input type="checkbox" id="weekday-fri" name="day[]" value="fri" <?= $fri == 'fri' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-fri">Fri</label>
                                <input type="checkbox" id="weekday-sat" name="day[]" value="sat" <?= $sat == 'sat' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-sat">Sat</label>
                                <input type="checkbox" id="weekday-sun" name="day[]" value="sun" <?= $sun == 'sun' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-sun">Sun</label>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Duration per class (hour)</td>
                        <td class="mark_space">:</td>
                        <td>
                            <input id="duration" type="number" readonly class="form-control" value="{{$class->duration}}">
                        </td>
                    </tr>
                    <tr>
                        <td class="td">Time</td>
                        <td class="mark_space">:</td>
                        <td>{{$class->time}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Tuition Fees -->
<div class="row">
    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-comment-dollar"> </span> Tuition Fees :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Fees per hour</td>
                        <td class="mark_space">:</td>
                        <td>
                            <input id="fees" type="text" class="form-control" value="{{$class->price}}" readonly />
                        </td>
                    </tr>

                    <tr>
                        <td class="td">Fees per month</td>
                        <td class="mark_space">:</td>
                        <td>
                            <input id="total" type="text" class="form-control" value="" readonly />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-user-friends"> </span> Tuition Type :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Type</td>
                        <td class="mark_space">:</td>
                        <td>
                            <select class="form-control " disabled readonly>
                                <option <?= $class->type == 1 ? 'selected' : '' ?> value="1">Personal</option>
                                <option <?= $class->type == 2 ? 'selected' : '' ?> value="2">Group</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="td">No. of students</td>
                        <td class="mark_space">:</td>
                        <td>{{ $class->pax }}

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Tutor Preference -->
<div class="row">
    <div class="col-sm-12 col-md-12 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-id-card"> </span> Tutor Preferences :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Gender</td>
                        <td class="mark_space">:</td>
                        <td>
                            <input type="radio" <?= $class->tutor_gender == 1 ? 'checked' : '' ?> class="radio" id="gender" name="gender" value="male"> Male
                            <input type="radio" <?= $class->tutor_gender == 2 ? 'checked' : '' ?> class="radio" id="gender" name="gender" value="female"> Female
                            <input type="radio" <?= $class->tutor_gender == 3 ? 'checked' : '' ?> class="radio" id="gender" name="gender" value="any"> Any

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Race</td>
                        <td class="mark_space">:</td>
                        <td>
                            <select class="form-control " disabled readonly>

                                <option <?= $class->tutor_race == 'none' ? 'checked' : '' ?> value="none">No preference</option>
                                <option <?= $class->tutor_race == 'malay' ? 'checked' : '' ?> value="malay">Malay</option>
                                <option <?= $class->tutor_race == 'indian' ? 'checked' : '' ?> value="indian">Indian</option>
                                <option <?= $class->tutor_race == 'chinese' ? 'checked' : '' ?> value="chinese">Chinese</option>
                                <option <?= $class->tutor_race == 'others' ? 'checked' : '' ?> value="others">Others</option>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Preferences</td>
                        <td class="mark_space">:</td>
                        <td>
                            <textarea class="form-control" id="preference" name="preference" readonly disabled>{{$class->tutor_preference}}</textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <a href="{{ route('tutors.academics.job_index') }}" class="btn btn-primary float-right">Back</a>

    </div>

    <div class="col-6">
        <button class="btn btn-success float-left" href="#" data-toggle="modal" data-target="#applyJobModal"> Apply</button>
    </div>
</div>


<!-- Modal for apply job confimation -->
<div class="modal fade" id="applyJobModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are you sure to apply for this job?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                Click 'Apply' if you are sure or click 'Cancel' to go back to previous page. <br><br>

                <form id="job-apply-form" action="{{ route('tutors.academics.job_apply_submit') }}" method="POST">
                    <input type="hidden" name="class" value="{{$class->id}}">
                    <div class="form-group">
                        <label for="info">Tutor Remarks</label>
                        <textarea name="info" id="info" rows="4" cols="50" class="form-control"></textarea>
                    </div>
                    @csrf
                </form>


            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="#" onclick="event.preventDefault(); document.getElementById('job-apply-form').submit();"> {{ __('Apply') }}</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function calculateRate() {
        var price = $('#fees').val();
        var frequency = $('#frequency').val();
        var duration = $('#duration').val();
        var total = ((price * duration) * frequency) * 4;
        $('#total').val(total.toFixed(2));
        // console.log(price, frequency, duration, total);
    }

    window.onload = function() {
        calculateRate();
    };
</script>


@endsection