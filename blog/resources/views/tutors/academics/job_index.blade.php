@extends('layouts.app_login')
<?php
$page = 'tutor-academic-job_index';
$title = 'Academic Job Offers';

use Illuminate\Support\Carbon;
?>

@section('content')


@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if($tutor->account_approved == 1)

@if($membership->status == 'active' && (Carbon::today() < $membership->expiry_date))


    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <tr>
                <th>#</th>
                <th>Job Reference No </th>
                <th>Subject</th>
                <th>Level</th>
                <th>Curriculum</th>
                <th>Area</th>
                <th>Status</th>
                <th>Action</th>
            </tr>

            @foreach ($jobs as $job)
            <tr>

                <td>{{ ++$i }}</td>
                <td>{{ $job->academic_reference_no }}</td>
                <td>{{$job->class_name}}</td>
                <td>{{ $job->level }}</td>
                <td>{{ $job->curriculum }}</td>
                <td>{{ $job->city }}</td>
                <td>
                    @if($job->job_offer_active == 1)
                    <label class="badge badge-success">Available</label>
                    @else
                    <label class="badge badge-danger">Taken</label>
                    @endif
                </td>

                <td>
                    <a class="btn btn-info" href="{{ route('tutors.academics.job_show', $job->academic_reference_no) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('tutors.academics.job_apply', $job->academic_reference_no) }}">Apply</a>

                </td>
            </tr>
            @endforeach
        </table>
    </div>

    {!! $jobs->render() !!}

    @else
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8">
            <div class="card">
                <div class="card-body">
                    <p>
                        We're sorry, you are unable to view job offers because your membership is not active.<br>
                        Click here to request for membership renewal.
                        <br>
                        <a type="button" class="btn btn-primary" href="{{ route('tutors.membership') }}">Membership</a>
                        <br>
                        Please contact admin if problem persist.
                    </p>


                </div>
            </div>

        </div>

    </div>

    @endif

    @else

    <div class="row justify-content-center">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    Your account is pending approval from admin. <br>
                    Be sure to update your profile for fast approval.
                </div>
            </div>

        </div>

    </div>

    @endif




    @endsection