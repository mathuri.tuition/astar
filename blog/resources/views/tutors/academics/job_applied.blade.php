@extends('layouts.app_login')
<?php
$page = 'tutor-academic-job_applied_index';
$title = 'Applied Job History';
?>

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Job Reference No</th>
            <th>Subject</th>
            <th>Level</th>
            <th>Curriculum</th>
            <th>Area</th>
            <th>Job Vacancy</th>
            <th>Status</th>
            <th>Action</th>
        </tr>

        @foreach ($jobs as $job)
        <tr>

            <td>{{ ++$i }}</td>
            <td>{{ $job->academic_reference_no }}</td>
            <td>{{$job->class_name}}</td>
            <td>{{ $job->edu_level }}</td>
            <td>{{ $job->curriculum }}</td>
            <td>{{ $job->city }}</td>
            <td>
                <?php
                if(is_null($job->tutor_id))
                {
                    echo '<label class="badge badge-primary">Vacant</label>';
                }else{
                    if($job->tutor_id == Auth()->user()->id)
                    {
                        echo '<label class="badge badge-success">Success</label>';
                    }else{
                        echo '<label class="badge badge-danger">Taken</label>';
                    }
                }
                ?>
            </td>
            <td>
                <?= ($job->status == 1 ? '<label class="badge badge-success">Available</label>' : '<label class="badge badge-warning">Cancelled By Student</label>') ?>
            </td>
            <td>
                <a class="btn btn-info" href="{{ route('tutors.academics.job_applied_show', $job->academic_reference_no) }}">Show</a>
            </td>
        </tr>
        @endforeach

    </table>
</div>

    {!! $jobs->render() !!}

    @endsection