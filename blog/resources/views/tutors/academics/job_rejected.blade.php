@extends('layouts.app_login')
<?php
$page = 'tutor-academic-job_rejected';
$title = 'Rejected Academic Job Offers';
?>

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Job </th>
            <th>Level</th>
            <th>Curriculum</th>
            <th>Area</th>
            <th>Status</th>
            <th>Action</th>
        </tr>

        @foreach ($jobs as $job)
        <tr>

            <td>{{ ++$i }}</td>
            <td>{{ $job->class_name }}</td>
            <td>{{ $job->edu_level }}</td>
            <td>{{ $job->curriculum }}</td>
            <td>{{ $job->location }}</td>
            <td><?= ($job->request_status == 1 ? ' <label class="badge badge-success">Approved</label>' : '') ?></td>

            <td>
                <a class="btn btn-info" href="{{ route('tutors.academics.job_show', $job->academic_reference_no) }}">Show</a>
            </td>
        </tr>
        @endforeach

    </table>
</div>

    {!! $jobs->render() !!}
    @endsection