@extends('layouts.app_login')
<?php
$page = 'tutor-qualification';
$title = 'Profile';

// $tutor_spm_path = config('app.image.tutor.spm');
$tutor_spm_path = config('app.file.tutor.spm');
// $tutor_higher_path = config('app.image.tutor.higher');
$tutor_higher_path = config('app.file.tutor.higher');

?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Whoops!</strong> There were some problems with your input.<br><br>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif


<div class="col-12">
    <div class="tab-content">
        <div class="tab-pane fade show active" id="account" role="tabpanel">
            
            <div class="card" id="spmResultsCard">
            <div class="card-header">
                <div class="float-right">
                    <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#spmResults" aria-expanded="false" aria-controls="spmResults">
                        <span class="fa fa-caret-down"></span>
                    </button>
                </div>
                <h5 class="card-title mb-0">SPM Results / High School Results</h5>
            </div>
            <div class="card-body collapse" id="spmResults">
                <div class="form-group">
                    <button type="button" name="add" id="add" class="btn btn-success float-right">Add More</button>
                    <br>
                </div>

                <div class="form-group">
                    <form name="add_name" id="add_name" method="POST" action="{{route('tutors.spmResults_update')}}">
                        @csrf
                        <div class="table-responsive">


                            <table class="table table-bordered" id="dynamic_field">
                                <tr>
                                    <th>Subject</th>
                                    <th>Grade</th>
                                    <th>Action</th>
                                </tr>

                                <?php
                                if (!is_null($spm)) {
                                    $subjects = json_decode($spm->subjects);
                                    $scores = json_decode($spm->scores);
                                    $i = 1;
                                    $lastElement = end($subjects);
                                }

                                ?>
                                @if(!is_null($spm))
                                @foreach($subjects as $key=>$subject)
                                <tr id="row{{$i}}">
                                    <td><input type="text" name="subject[]" value="{{$subject}}" class="form-control name_list" /></td>
                                    <td><input type="text" name="score[]" value="{{$scores[$key]}}" class="form-control score_list" /></td>
                                    @if($subject == $lastElement)
                                    <td>
                                        <button type="button" name="remove" id="{{$i}}" class="btn btn-danger btn_remove">X</button>
                                        <input type="hidden" name="count" id="count" value="{{$i}}">
                                    </td>
                                    @else
                                    <td>
                                        <button type="button" name="remove" id="{{$i}}" class="btn btn-danger btn_remove">X</button>
                                    </td>
                                    @endif

                                </tr>
                                <?php
                                $i++;
                                ?>
                                @endforeach

                                @else
                                <tr>
                                    <td><input type="text" name="subject[]" class="form-control name_list" required="required"/></td>
                                    <td><input type="text" name="score[]" maxlength="2" class="form-control score_list" required="required"/></td>
                                    <td>
                                        <button type="button" name="remove" id="1" class="btn btn-danger btn_remove" disabled>X</button>
                                        <input type="hidden" name="count" id="count" value="1">
                                    </td>
                                </tr>
                                @endif


                            </table>

                            <input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
            
            <br><br>

            <div class="card" id="spmQualificationCard">
                <div class="card-header">
                    <div class="float-right">
                        <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#publicInfo" aria-expanded="false" aria-controls="publicInfo">
                            <span class="fa fa-caret-down"></span>
                        </button>
                    </div>
                    <h5 class="card-title mb-0">SPM Certificate</h5>
                </div>
                <div class="card-body collapse" id="publicInfo">
                    <input type="hidden" id="nationality" value="{{$tutor->nationality_id}}">
                    <form method="POST" action="{{ route('tutors.spmQualification_update') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-row">
                                    <div class="col-md-3 col-sm-12">
                                        <label for="inputUsername">SPM Year</label>
                                        <?php
                                        $spm_year = '';
                                        $spm_image = '';
                                        $spm = json_decode($tutor->spm_results);

                                        if (!is_null($spm)) {
                                            $spm_year = $spm->year;
                                            $spm_image = $spm->image;
                                        }
                                        ?>
                                        <input type="text" name="spm_year" maxlength="4" id="spm_year" class="form-control" value="{{ $spm_year }}">
                                    </div>



                                    <div class="col-md-4 col-sm-12">
                                        <label for="inputUsername">Certificate File (PDF)</label>
                                        <span class="btn btn-secondary col-sm" style="outline:none;">

                                            <!--<input type="file" style="outline:none;" accept="application/pdf" name="spm_image" id="spm_image" style="visibility:hidden;position:absolute;">-->
                                            <input type="file" style="outline:none;"  name="spm_image" id="spm_image" style="visibility:hidden;position:absolute;">

                                        </span>
                                    </div>

                                    <?php

                                    if (!is_null($tutor->spm_results)) { 
                                     $spm_format = explode('.', $spm_image);
                                    ?>
                                        <div class="col-md-4 col-sm-12">
                                            <label for="inputUsername">File</label>
                                            <br>
                                            
                                            <!-- Button trigger modal -->
                                            @if($spm_format[1] == 'pdf')
                                           <a href="{{ route('tutors.spm') }}" target="_blank" class="btn btn-primary pop">Show PDF</a>
                                            @else
                                            <!-- Button trigger modal -->
                                            <a href="#" class="btn btn-primary pop">Show Image
                                                <img hidden src="{{asset('storage/'.$tutor_spm_path.$spm_image)}}" style="width: 200px; height: 264px;">
                                            </a>
                                            @endif

                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-4 col-sm-12">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <br><br>

            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#privateInfo" aria-expanded="false" aria-controls="privateInfo">
                            <span class="fa fa-caret-down"></span>
                        </button>
                    </div>
                    <h5 class="card-title mb-0">Highest Education Qualifications</h5>
                </div>
                <div class="card-body collapse" id="privateInfo">
                    <form method="POST" action="{{ route('tutors.higherQualification_update') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 col-sm-12">

                                <?php
                                $level = '';
                                $grad_year = '';
                                $cert = '';
                                $university = '';

                                $high = json_decode($tutor->highest_education_results);

                                if (!is_null($high)) {
                                    $level = $high->level;
                                    $grad_year = $high->year;
                                    $cert = $high->image;
                                    $university = $high->university;
                                }
                                ?>

                                <div class="form-row">
                                    <div class="col-md-4 col-sm-12">
                                        <label for="inputUsername">Highest Education</label>
                                        <select name="education_level" id="education_level" class="form-control">
                                            <option selected disabled>Choose...</option>
                                            <option <?= ($level == 1 ? 'selected' : '') ?> value="1">A-Level/Foundation/Matriculation/STPM</option>
                                            <option <?= ($level == 2 ? 'selected' : '') ?> value="2">Vocational qualification</option>
                                            <option <?= ($level == 3 ? 'selected' : '') ?> value="3">Bachelor's degree</option>
                                            <option <?= ($level == 4 ? 'selected' : '') ?> value="4">Master's degree</option>
                                            <option <?= ($level == 5 ? 'selected' : '') ?> value="5">Doctorate or higher</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <label for="inputUsername">Graduation Year</label>
                                        <input type="text" name="graduation_year" maxlength="4" id="graduation_year" class="form-control" value="{{ $grad_year }}">
                                    </div>
                                    <div class="col-md-5 col-sm-12">
                                        <label for="inputUsername">University Name</label>
                                        <input type="text" name="university" id="university" class="form-control" value="{{ $university }}">
                                    </div>

                                </div>

                                <div class="form-row">
                                    <div class="col-md-5 col-sm-12">
                                        <label for="inputUsername">Certificate File (PDF)</label>
                                        <span class="btn btn-secondary col-sm" style="outline:none;">
                                            <i class="">
                                                <!--<input style="outline:none;" accept="application/pdf" name="certificate_image" id="certificate_image" type="file" style="visibility:hidden;position:absolute;">-->
                                                <input style="outline:none;"  name="certificate_image" id="certificate_image" type="file" style="visibility:hidden;position:absolute;">
                                            </i>
                                        </span>
                                    </div>

                                    <?php

                                    if (!is_null($tutor->highest_education_results)) { 
                                     $format = explode('.', $cert);
                                    ?>
                                        <div class="col-md-2 col-sm-12">
                                            <label for="inputUsername">File</label>
                                            <br>
                                           
                                            @if($format[1] == 'pdf')
                                             <a href="{{ route('tutors.higher') }}" target="_blank" class="btn btn-primary ">Show PDF</a>
                                            @else
                                            <!-- Button trigger modal -->
                                            <a href="#" class="btn btn-primary pop">Show Image
                                                <img hidden src="{{asset('storage/'.$tutor_higher_path.$cert)}}" style="width: 200px; height: 264px;">
                                            </a>
                                            @endif

                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>

                            </div>
                        </div>

                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-4 col-sm-12">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
</div>

 <!--Modal -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <img src="" class="imagepreview" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $(document).ready(function() {
        // var i = 1;
        var i = document.getElementById("count").value;

        $('#add').click(function() {
            i++;
            $('#dynamic_field').append('<tr id="row' + i + '"><td><input type="text" name="subject[]" class="form-control name_list" /></td><td><input type="text" name="score[]" maxlength="2" class="form-control score_list" /></td><td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
        });

        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            var new_button_id = button_id - 1;

            $('#row' + button_id + '').remove();


        });
        // $('#submit').click(function() {
        //     $.ajax({
        //         url: "{{route('tutors.spmResults_update')}}",
        //         method: "POST",
        //         data: $('#add_name').serialize(),
        //         success: function(data) {
        //             // alert(data);
        //             $('#add_name')[0].reset();
        //         }
        //     });
        // });
    });

    $(function() {
        $('.pop').on('click', function() {
            $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#imagemodal').modal('show');
        });

        if (document.getElementById('nationality').value != "109") {
            $('#spmQualificationCard').hide();
            // $('#spmResultsCard').hide();
        }
    });
</script>

@endsection