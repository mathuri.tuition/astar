@extends('layouts.app_login')
<?php
$page = 'tutor-membership';
$title = 'Membership';

use Illuminate\Support\Carbon;
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Whoops!</strong> There were some problems with your input.<br><br>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif


<div class="col-12">
    <div class="tab-content">
        <div class="tab-pane fade show active" id="account" role="tabpanel">

            <div class="card" id="spmQualificationCard">
                <div class="card-header">
                    <div class="float-right">
                        <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#publicInfo" aria-expanded="false" aria-controls="publicInfo">
                            <span class="fa fa-caret-down"></span>
                        </button>
                    </div>
                    <h5 class="card-title mb-0">Request renewal</h5>
                </div>
                <div class="card-body collapse" id="publicInfo">

                    @if(Carbon::today() <= $membership->expiry_date)
                        <p>Congratulations! your membership is ACTIVE and expires on {{$membership->expiry_date}}. <br> Stay Active to receive new job offers.</p>
                        @else
                        <p>Oppsss..! Your <?= (is_null($membership->expiry_date)) ? 'membership has not been activated yet' : 'membership has expired on' . $membership->expiry_date ?> , please renew by sending a request to admin. <br> You can follow the steps below to renew your membership;</p>

                        <p>
                            1) Transfer/Cash Deposit the amount to this bank account. <br>
                                    Bank : CIMB <br>
                                    Name : Astar Agency <br>
                                    Acc.no : 8010268445 <br>
                                    <br>
                            2) Take a CLEAR picture of the transaction slip and upload here;
                        </p>

                        <form method="POST" action="{{ route('tutors.membership.request')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-sm-12 col-md-12">

                                    <div class="form-row">

                                        <div class="col-md-4 col-sm-12">
                                            <label for="inputUsername">Payment Slip</label>
                                            <span class="btn btn-secondary col-sm" style="outline:none;">
                                                <input type="file" style="outline:none;" name="payment_slip" id="payment_slip" style="visibility:hidden;position:absolute;">
                                            </span>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top:20px;">
                                <div class="col-sm-12 col-md-4">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                        @endif


                </div>
            </div>

            <br><br>

            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#privateInfo" aria-expanded="false" aria-controls="privateInfo">
                            <span class="fa fa-caret-down"></span>
                        </button>
                    </div>
                    <h5 class="card-title mb-0">Membership Renewal History</h5>
                </div>
                <div class="card-body collapse" id="privateInfo">
                    <div class="table-responsive">
                        <table class="table ">
                            <tr>
                                <td>#</td>
                                <td>Requested Date</td>
                                <td>Last Payment Date</td>
                                <td>Last Expiry Date</td>
                                
                            </tr>
                             <?php 
                            $i=1; 
                            ?>
                            @foreach($logs as $log)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$log->created_at}}</td>
                                <td>{{$log->last_payment_date}}</td>
                                <td>{{$log->last_expiry_date}}</td>
                            </tr>
                            <?php 
                            $i++; 
                            ?>
                            @endforeach
                        </table>
                    </div>

                </div>
            </div>


            {!! $logs->render() !!}

        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <img src="" class="imagepreview" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>

@endsection