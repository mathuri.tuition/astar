@extends('layouts.app_login')
<?php
$page = 'classes-index';
$title = 'Non Academic Job Offers';

use Illuminate\Support\Carbon;
?>

@section('content')


@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if($tutor->account_approved == 1)
@if($membership->status == 'active' && (Carbon::today() < $membership->expiry_date))
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <tr>
                <th>#</th>
                <th>Job Reference No </th>
                <th>Class</th>

                <th>Area</th>
                <th>Status</th>
                <th>Action</th>
            </tr>

            @foreach ($jobs as $job)

            <tr>

                <td>{{ ++$i }}</td>
                <td>{{ $job->reference_no }}</td>
                <td><?= (is_null($job->class_name)) ? $job->class_name : $job->class_name ?></td>

                <td>{{ $job->city }}</td>
                <td>
                    @if($job->job_offer_active == 1)
                    <label class="badge badge-success">Available</label>
                    @else
                    <label class="badge badge-danger">Taken</label>
                    @endif
                </td>

                <td>
                    <a class="btn btn-info" href="{{ route('tutors.nonacademics.job_show', $job->reference_no) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('tutors.nonacademics.job_apply', $job->reference_no) }}">Apply</a>

                </td>
            </tr>
            @endforeach
        </table>
    </div>

    {!! $jobs->render() !!}

    @else
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8">
            <div class="card">
                <div class="card-body">
                    We're sorry, you are unable to view job offers because your membership is not active.<br>
                    Click here to request for membership renewal.
                    <br>
                    <a type="button" class="btn btn-primary" href="{{ route('tutors.membership') }}">Membership</a>
                    <br>
                    Please contact admin if problem persist.
                </div>
            </div>
        </div>
    </div>

    @endif

    @else

    <div class="row justify-content-center">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    Your account is pending approval from admin. <br>
                    Be sure to update your profile for fast approval.
                </div>
            </div>
        </div>
    </div>

    @endif


    @endsection