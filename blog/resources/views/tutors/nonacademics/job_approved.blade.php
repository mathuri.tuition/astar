@extends('layouts.app_login')
<?php
$page = 'classes-index';
$title = 'My Offers';
?>

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Job Reference No </th>
            <th>Class</th>

            <th>Area</th>
            <th>Job Status</th>
            <th>Action</th>
        </tr>

        @foreach ($jobs as $job)
        <tr>

            <td>{{ ++$i }}</td>
            <td>{{ $job->reference_no }}</td>
            <td><?= (is_null($job->class_name)) ? $job->class_name : $job->class_name ?></td>

            <td>{{ $job->city }}</td>
            <td>
                <?= ($job->job_assigned == 1 ? '<label class="badge badge-success">Active</label>' : '<label class="badge badge-warning">Cancelled By Student</label>') ?>
            </td>
            <td>
                <a class="btn btn-info" href="{{ route('tutors.nonacademics.job_assigned_show', $job->reference_no) }}">Show</a>
            </td>
        </tr>
        @endforeach

    </table>
</div>

    {!! $jobs->render() !!}
    @endsection