@extends('layouts.app_login')
<?php
$page = 'tutor-dashboard';
$title = 'Dashboard';

use Illuminate\Support\Carbon;

?>
@section('content')

<div class="row">

    <!-- Border Left Utilities -->
    <div class="col-lg-4 col-md-6 col-sm-12">

        <div class="card mb-4 py-3 border-left-primary">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Academic Job Applied</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $academic->academic_applied }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-list fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mb-4 py-3 border-left-primary">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Academic Job Approved</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $academic->academic_approve }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-check fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>



    </div>

    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card mb-4 py-3 border-left-info">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Non-academic Job Applied</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $nonacademic->non_academic_applied }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-list fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mb-4 py-3 border-left-info">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Non-academic Job Approved</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $nonacademic->non_academic_approve }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-check fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Membership Info</h6>

            </div>
            <!-- Card Body -->
            <div class="card-body">
                <h2 style="text-align: center"><?= (Carbon::today() <= $membership->expiry_date) ? 'Active' : 'Expired' ?></h2>
                <hr>
                Expires on : {{$membership->expiry_date}}<br>
                <?php
                    $date = Carbon::parse($membership->expiry_date);
                    $now = Carbon::today();
                    $diff = $date->diffInDays($now);
                ?>
                {{ $diff }} day(s) more to go<br>
            </div>

            <div class="card-footer text-center">
                <a style="text-align: center" href="{{ route('tutors.membership') }}" class="btn btn-success">Renew Now</a>
            </div>
        </div>
    </div>


</div>


<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">

    </div>

    <!-- Pie Chart -->
    <div class="col-xl-4 col-lg-5">

    </div>
</div>


@endsection