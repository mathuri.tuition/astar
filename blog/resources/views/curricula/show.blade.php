@extends('layouts.app_login')
<?php
$page = 'curricula-show';
$title = 'Show Curriculum';
?>


@section('content')

<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">

            <div class="card-body">
                <div class="form-group">
                    <label>Curriculum Name</label>
                    <input class="form-control" disabled value="{{$curriculum->curriculum}}">
                </div>
            </div>

            <div class="card-footer">
                <a class="btn btn-primary" href="{{ route('curricula.index') }}"> Back</a>
            </div>

        </div>
    </div>
</div>

@endsection