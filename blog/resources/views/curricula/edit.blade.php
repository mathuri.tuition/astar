@extends('layouts.app_login')
<?php
$page = 'curricula-edit';
$title = 'Edit Curriculum';
?>


@section('content')

<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">

            <div class="card-body">
                {{ Form::model($curriculum, array('route' => array('curricula.update', $curriculum->id), 'method' => 'PUT')) }}
                <div class="form-group">
                    {{ Form::label('name', 'Curriculum Name') }}
                    {{ Form::text('curriculum', $curriculum->curriculum, array('class' => 'form-control')) }}
                </div>

                <br>
                <a class="btn btn-primary float-left" href="{{ route('curricula.index') }}"> Cancel</a>
                {{ Form::submit('Edit', array('class' => 'btn btn-success float-right')) }}
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>

@endsection