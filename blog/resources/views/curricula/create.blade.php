@extends('layouts.app_login')
<?php
$page = 'curricula-create';
$title = 'Create New Curriculum';
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">

            <div class="card-body">

                <form method="POST" action="{{ route('curricula.store') }}">
                    @csrf

                    <div class="form-group">
                        <div class="col">
                            <label for="inputSyllabus">Curriculum</label>
                            <input type="text" name="curriculum" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col">
                            <a class="btn btn-primary float-left" href="{{ route('curricula.index') }}"> Cancel</a>
                            <button type="submit" class="btn btn-success float-right">Submit</button>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>
</div>

@endsection