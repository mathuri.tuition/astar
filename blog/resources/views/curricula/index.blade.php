@extends('layouts.app_login')
<?php
$page = 'curricula-index';
$title = 'Curricula Management';
?>

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Curriculum</th>
            <th>User</th>
            <th>Last Updated</th>
            <th>Action</th>
        </tr>

        @foreach ($curricula as $curriculum)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $curriculum->curriculum }}</td>
            <td>{{ $curriculum->name }}</td>
            <td>{{ $curriculum->updated_at }}</td>
            <td>
                <a class="btn btn-info" href="{{ route('curricula.show', $curriculum->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('curricula.edit', "$curriculum->id") }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['curricula.destroy', $curriculum->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach

    </table>
</div>

    {!! $curricula->render() !!}

    @endsection