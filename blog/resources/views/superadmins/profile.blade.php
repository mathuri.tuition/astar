@extends('layouts.app_login')
<?php
$page = 'superadmin-profile';
$title = 'Profile';
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<div class="row row justify-content-center">
    <div class="col-10">
        <div class="card">
            <div class="card-header">{{ __('Profile') }}</div>

            <div class="card-body">

                {{ Form::model($superadmin, array('route' => array('superadmins.profile.update', $superadmin->user_id), 'method' => 'POST')) }}
                <div class="form-row">
                    <div class="col">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Name" value="<?= $superadmin->name; ?>">
                    </div>
                    <div class="col">
                        <label for="inputEmail4">Email</label>
                        <input type="email" class="form-control" name="email" id="inputEmail4" placeholder="Email" value="<?= $superadmin->email; ?>">
                    </div>
                </div>

                <div class="form-row">

                    <div class="col" style="border-left-width: 10px;padding-left: 45px;">
                        <label for="gender">Gender</label>
                        <br>
                        <input type="radio" name="gender" value="1" <?php echo ($superadmin->gender_id  == '1') ? 'checked' : '' ?>> Male

                        <input type="radio" name="gender" value="0" <?php echo ($superadmin->gender_id  == '0') ? 'checked' : '' ?>> Female
                    </div>

                    <div class="col" style="border-left-width: 10px;padding-left: 45px;">
                        <label for="gender">Race</label>
                        <select class="form-control" id="race" name="race">
                            <option selected disabled>Choose..</option>
                            <option>Chinese</option>
                            <option>Indian</option>
                            <option>Malay</option>
                            <option>Others</option>
                        </select>


                    </div>

                    <div class="col" style="border-left-width: 10px;padding-left: 45px;">
                        <label for="gender">Religion</label>
                        <select class="form-control" id="religion" name="religion">
                            <option selected disabled>Choose..</option>
                            <option>Buddhist</option>
                            <option>Cristian</option>
                            <option>Hindu</option>
                            <option>Islam</option>
                            <option>Sikh</option>
                            <option>Others</option>
                        </select>

                    </div>



                </div>

                <div class="form-row">
                    <div class="col">
                        <label for="inputEmail4">Nationality</label>
                        <select class="form-control" id="nationality" name="nationality">
                            <?php
                            foreach ($nationalities as $nationality) {
                                $n = $nationality->id;
                                if ($n == $superadmin->nationality_id) {
                                    echo "<option selected value='$n'>$nationality->nationality</option>";
                                } else {
                                    echo "<option value='$n'>$nationality->nationality</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class='col' id='mykadDiv' style="display:none;">
                        <label for='mykad'>Mykad No.</label>
                        <input type='text' class='form-control' name='mykad' id='mykad' value='<?= $superadmin->mykad_no; ?>'>
                    </div>

                    <div class='col' id='passportDiv' style="display:none;">
                        <label for='passport'>Passport No.</label>
                        <input type='text' class='form-control' name='passport' id='passport' value='<?= $superadmin->passport_no; ?>'>
                    </div>

                </div>


                <div class="form-group">
                    <label for="inputAddress">Address</label>
                    <input type="text" class="form-control" name="address_1" id="inputAddress" placeholder="1234 Main St" value="<?= $superadmin->address_1 ?>">
                </div>
                <div class="form-group">
                    <label for="inputAddress2">Address 2</label>
                    <input type="text" class="form-control" name="address_2" id="inputAddress2" placeholder="Apartment, studio, or floor" value="<?= $superadmin->address_2 ?>">
                </div>
                <div class="form-group">
                    <label for="inputAddress3">Address 3</label>
                    <input type="text" class="form-control" name="address_3" id="inputAddress3" placeholder="Apartment, studio, or floor" value="<?= $superadmin->address_3 ?>">
                </div>
                <div class="form-group">
                    <label for="inputAddress4">Address 4</label>
                    <input type="text" class="form-control" name="address_4" id="inputAddress4" placeholder="Apartment, studio, or floor" value="<?= $superadmin->address_4 ?>">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="inputZip">Postcode</label>
                        <input type="text" class="form-control" name="postcode" id="inputZip" maxlength="5" value="<?= $superadmin->postcode ?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputCity">City</label>
                        <input type="text" class="form-control" name="city" id="inputCity" value="<?= $superadmin->city ?>">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="state">State</label>
                        <select class="form-control" id="state" name="state">
                            <?php
                            foreach ($states as $state) {
                                if ($state->code == $superadmin->state_code) {
                                    echo "<option selected value='$state->code'>$state->state</option>";
                                } else {
                                    echo "<option value='$state->code'>$state->state</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>

                </div>


                <div class="form-group float-right">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        // console.log(document.getElementById('nationality').value);

        if (document.getElementById('nationality').value == "109") {
            $('#mykadDiv').show();
            $('#mykad').attr('required', '');
            $('#mykadDiv').attr('data-error', 'This field is required.');
            $('#passportDiv').hide();
            $('#passport').removeAttr('required');
            $('#passportDiv').removeAttr('data-error');
        } else {
            $('#passportDiv').show();
            $('#passport').attr('required', '');
            $('#passportDiv').attr('data-error', 'This field is required.');
            $('#mykadDiv').hide();
            $('#mykad').removeAttr('required');
            $('#mykadDiv').removeAttr('data-error');
        }

        $("#nationality").trigger("change");

    });
</script>

<br><br>
@endsection