@extends('layouts.app_login')
<?php
$page = 'superadmin-class_request';
$title = 'Request Class';
?>

@section('content')


<div class="row justify-content-center">
    <div class="col-10">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p>{{ $message }}</p>
        </div>
        @endif
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-12">
        <div class="card">
            <div class="card-header">Student Request Tutor</div>

            <div class="card-body">

                <form method="POST" action="{{ route('students.academics.request_tutor.submit') }}">
                    <!-- {{ Form::model( array('route' => array('students.academics.request_tutor.submit'), 'method' => 'POST')) }} -->
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="subject">Curriculum</label>
                        <select class="form-control" id="choice1">
                            <option selected disabled>Choose curriculum...</option>
                            <?php
                            foreach ($curricula as $curriculum) {
                                echo "<option value='$curriculum->id'>$curriculum->curriculum</option>";
                            }
                            ?>

                        </select>
                        <br>
                        <label for="subject">Subject</label>
                        <select name="subject" class="form-control selectpicker" disabled id="choice2">
                            <option selected disabled>Choose subject...</option>
                            <?php
                            foreach ($classes as $subject) {
                                echo "<option data-option='$subject->curriculum_id' value='$subject->id'>$subject->name</option>";
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">

                        <label for="days">Preferred Day & Time</label> <br>
                        <div class="table-responsive">
                            <table border="1">
                                <tr>
                                    <th>Select</th>
                                    <th>Days</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputMon" checked name="day[]" value="mon"></td>
                                    <td> Monday</td>
                                    <td> <input type="time" class="input" id="startTimeMon" name="startTimeMon"></td>
                                    <td><input type="time" class="input" id="endTimeMon" name="endTimeMon"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputTue" name="day[]" value="tue"></td>
                                    <td> Tuesday</td>
                                    <td> <input type="time" class="input" id="startTimeTue" name="startTimeTue"></td>
                                    <td><input type="time" class="input" id="startTimeTue" name="endTimeTue"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputWed" name="day[]" value="wed"></td>
                                    <td> Wednesday</td>
                                    <td> <input type="time" class="input" id="startTimeWed" name="startTimeWed"></td>
                                    <td><input type="time" class="input" id="startTimeWed" name="endTimeWed"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputThu" name="day[]" value="thu"></td>
                                    <td> Thursday</td>
                                    <td> <input type="time" class="input" id="startTimeThu" name="startTimeThu"></td>
                                    <td><input type="time" class="input" id="startTimeThu" name="endTimeThu"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputFri" name="day[]" value="fri"></td>
                                    <td> Friday</td>
                                    <td> <input type="time" class="input" id="startTimeFri" name="startTimeFri"></td>
                                    <td><input type="time" class="input" id="startTimeFri" name="endTimeFri"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputSat" name="day[]" value="sat"></td>
                                    <td> Saturday</td>
                                    <td> <input type="time" class="input" id="startTimeSat" name="startTimeSat"></td>
                                    <td><input type="time" class="input" id="startTimeSat" name="endTimeSat"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputSun" name="day[]" value="sun"></td>
                                    <td> Sunday</td>
                                    <td> <input type="time" class="input" id="startTimeSun" name="startTimeSun"></td>
                                    <td><input type="time" class="input" id="startTimeSun" name="endTimeSun"></td>
                                </tr>

                            </table>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="location">Preferred Location</label>
                        <input type="text" name="location" id="location" class="form-control" placeholder="Location" value="try">
                    </div>

                    <div class="form-group">
                        <label for="info">Additional Information</label>
                        <textarea name="info" rows="4" cols="50" id="info" class="form-control"> Try </textarea>
                    </div>

                    <div class="form-group float-right">
                        <button type="submit" class="btn btn-success">Request</button>
                    </div>

                    <!-- </form> -->
                    {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        $('#inputMon').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeMon').attr('required', '');
                $('#startTimeMon').attr('data-error', 'This field is required.');

                $('#endTimeMon').attr('required', '');
                $('#endTimeMon').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeMon').removeAttr('required');
                $('#startTimeMon').removeAttr('data-error');

                $('#endTimeMon').removeAttr('required');
                $('#endTimeMon').removeAttr('data-error');
            }
        });

        $('#inputTue').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeTue').attr('required', '');
                $('#startTimeTue').attr('data-error', 'This field is required.');

                $('#endTimeTue').attr('required', '');
                $('#endTimeTue').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeTue').removeAttr('required');
                $('#startTimeTue').removeAttr('data-error');

                $('#endTimeTue').removeAttr('required');
                $('#endTimeTue').removeAttr('data-error');
            }
        });

        $('#inputWed').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeWed').attr('required', '');
                $('#startTimeWed').attr('data-error', 'This field is required.');

                $('#endTimeWed').attr('required', '');
                $('#endTimeWed').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeWed').removeAttr('required');
                $('#startTimeWed').removeAttr('data-error');

                $('#endTimeWed').removeAttr('required');
                $('#endTimeWed').removeAttr('data-error');
            }
        });

        $('#inputThu').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeThu').attr('required', '');
                $('#startTimeThu').attr('data-error', 'This field is required.');

                $('#endTimeThu').attr('required', '');
                $('#endTimeThu').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeThu').removeAttr('required');
                $('#startTimeThu').removeAttr('data-error');

                $('#endTimeThu').removeAttr('required');
                $('#endTimeThu').removeAttr('data-error');
            }
        });

        $('#inputFri').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeFri').attr('required', '');
                $('#startTimeFri').attr('data-error', 'This field is required.');

                $('#endTimeFri').attr('required', '');
                $('#endTimeFri').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeFri').removeAttr('required');
                $('#startTimeFri').removeAttr('data-error');

                $('#endTimeFri').removeAttr('required');
                $('#endTimeFri').removeAttr('data-error');
            }
        });

        $('#inputSat').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeSat').attr('required', '');
                $('#startTimeSat').attr('data-error', 'This field is required.');

                $('#endTimeSat').attr('required', '');
                $('#endTimeSat').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeSat').removeAttr('required');
                $('#startTimeSat').removeAttr('data-error');

                $('#endTimeSat').removeAttr('required');
                $('#endTimeSat').removeAttr('data-error');
            }
        });

        $('#inputSun').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeSun').attr('required', '');
                $('#startTimeSun').attr('data-error', 'This field is required.');

                $('#endTimeSun').attr('required', '');
                $('#endTimeSun').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeSun').removeAttr('required');
                $('#startTimeSun').removeAttr('data-error');

                $('#endTimeSun').removeAttr('required');
                $('#endTimeSun').removeAttr('data-error');
            }
        });


        $("#choice1").trigger("change");
        $("#choice1").change(function() {
            if (typeof $(this).data('options') === "undefined") {
                /*Taking an array of all options-2 and kind of embedding it on the select1*/
                $(this).data('options', $('#choice2 option').clone());
            }
            var id = $(this).val();
            var options = $(this).data('options').filter('[data-option=' + id + ']');
            $('#choice2').html(options);
            $('#choice2').prop('disabled', false);
            $('#choice2').selectpicker('refresh');
        });

    });
</script>
@endsection