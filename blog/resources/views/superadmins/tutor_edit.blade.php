@extends('layouts.app_login')
<?php
$page = 'superadmin-tutor_add';
$title = 'Edit Tutor';
?>

@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">
            <div class="card-body">

              
                {!! Form::model($tutor, ['method' => 'POST','route' => ['superadmins.tutor_update', $tutor->user_id]]) !!}
                
                <div class="row register-form ">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="fullName">{{ __('Full Name') }}</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Name" name="name" value="<?= $tutor->name; ?>" autocomplete="name" autofocus />
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="gender">{{ __('Gender') }}</label>
                            <div class="maxl">
                                <label class="radio inline">
                                    <input type="radio" name="gender" value="male" checked>
                                    <span> Male </span>
                                </label>
                                <label class="radio inline">
                                    <input type="radio" name="gender" value="female">
                                    <span>Female </span>
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>
                            <input type="text" class="form-control" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password" value="<?= $tutor->password; ?>" />
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="confirmPassword">{{ __('Confirm Password') }}</label>
                            <input type="text" class="form-control" placeholder="Confirm Password" name="confirm-password" autocomplete="confirm-password" value="<?= $tutor->password; ?>" />
                        </div>

                    </div>
                    <div class=" col-6">
                        <div class="form-group">
                            <label for="email">{{ __('Email') }}</label>
                            <input disabled type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="<?= $tutor->email; ?>" autocomplete=" email" autofocus />
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="phoneNo">{{ __('Phone No') }}</label>
                            <input type="text" minlength="10" maxlength="12" name="phone_no" class="form-control" placeholder="Your Phone" value="<?= $tutor->phone_no; ?>" />
                            @error('phone_no')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group" onload="myFunction()">
                            <label for="nationality">{{ __('Nationality') }}</label>
                            <select class="form-control @error('nationality') is-invalid @enderror" name="nationality" id="nationality">
                                <option class="hidden" selected disabled>Select your nationality...</option>
                                <?php
                                foreach ($nationalities as $nationality) {
                                    $n = $nationality->id;
                                    if ($n == $tutor->nationality_id) {
                                        echo "<option selected value='$n'>$nationality->nationality</option>";
                                    } else {
                                        echo "<option value='$n'>$nationality->nationality</option>";
                                    }
                                }
                                ?>
                            </select>

                            @error('nationality')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group" id="mykadDiv" style="display:none;">
                            <label for="nricNo">{{ __('NRIC No') }}</label>
                            <input id="mykad" name="mykad" maxlength="12" type="text" placeholder="MyKad" class="form-control @error('mykad') is-invalid @enderror" value="{{ $tutor->mykad_no }}" autocomplete="mykad" autofocus>
                            @error('mykad')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group" id="passportDiv" style="display:none;">
                            <label for="passportNo">{{ __('Passport No') }}</label>
                            <input id="passport" type="text" class="form-control @error('passport') is-invalid @enderror" placeholder="Passport Number" name="passport" value="{{ $tutor->passport_no }}" autocomplete="passport" autofocus>
                            @error('passport')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <a class="btn btn-primary float-left" href="{{ route('users.index') }}"> Cancel</a>
                            <input type="submit" class="btn btn-success btnRegister float-right" value="Submit" />
                        </div>

                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        function myFunction() {
            var x = document.getElementById("nationality").value;
            // alert(x + " is loaded");
            if (x == "109") {
                $('#mykadDiv').show();
                $('#mykad').attr('required', '');
                $('#mykadDiv').attr('data-error', 'This field is required.');
                $('#passportDiv').hide();
                $('#passport').removeAttr('required');
                $('#passportDiv').removeAttr('data-error');
            } else {
                $('#passportDiv').show();
                $('#passport').attr('required', '');
                $('#passportDiv').attr('data-error', 'This field is required.');
                $('#mykadDiv').hide();
                $('#mykad').removeAttr('required');
                $('#mykadDiv').removeAttr('data-error');
            }
        }
        window.onload = function() {
            myFunction();
        }

        $("#nationality").trigger("change");
        $("#nationality").change(function() {
            if ($(this).val() == "109") {
                $('#mykadDiv').show();
                $('#mykad').attr('required', '');
                $('#mykadDiv').attr('data-error', 'This field is required.');
                $('#passportDiv').hide();
                $('#passport').removeAttr('required');
                $('#passportDiv').removeAttr('data-error');
            } else {
                $('#passportDiv').show();
                $('#passport').attr('required', '');
                $('#passportDiv').attr('data-error', 'This field is required.');
                $('#mykadDiv').hide();
                $('#mykad').removeAttr('required');
                $('#mykadDiv').removeAttr('data-error');
            }
        });
    });
</script>

@endsection