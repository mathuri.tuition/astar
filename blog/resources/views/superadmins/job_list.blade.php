@extends('layouts.app_login')
<?php $page = 'tutor-job_list'; ?>
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tutor Job List</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    @role('Admin')
                    You are logged in as Admin !
                    @endrole

                    @role('Student')
                    You are logged in as Student !
                    @endrole

                    @role('Tutor')
                    You are logged in as Tutor !
                    @endrole
                </div>
            </div>
        </div>
    </div>
</div>
@endsection