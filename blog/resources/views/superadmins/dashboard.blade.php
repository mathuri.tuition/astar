@extends('layouts.app_login')
<?php
$page = 'superadmin-dashboard';
$title = 'Dashboard';
?>

@section('content')


<div class="row justify-content-center">
    <div class="col-10">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p>{{ $message }}</p>
        </div>
        @endif
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-10">
        <div class="card">
            <div class="card-header">Superadmin Dashboard</div>

            <div class="card-body">

                {{$user}}
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif

                @role('superadmin')
                You are logged in as Super Admin !
                @endrole

                @role('admin')
                You are logged in as Adminsss !
                @endrole

                @role('student')
                You are logged in as Student !
                @endrole

                @role('tutor')
                You are logged in as Tutors !
                @endrole
            </div>
        </div>
    </div>
</div>

@endsection