@extends('layouts.app_login')
<?php
$page = 'superadmin-class_list';
$title = 'Class Management';
?>
@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Date Requested</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($subjects as $key => $subject)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $subject->subject }}</td>
            <td>{{ $subject->email }}</td>
            <td>
                {{ $subject->date }}
            </td>
            <td>
                <a class="btn btn-info" href="{{ route('superadmins.tutor_show',$subject->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('superadmins.tutor_edit',$subject->id) }}">Edit</a>
                {!! Form::open(['method' => 'POST','route' => ['superadmins.tutor_delete', $subject->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </table>
</div>

{!! $subjects->render() !!}

@endsection