@extends('layouts.app_login')
<?php
$page = 'superadmin-tutor_show';
$title = 'Show Tutor Details';
?>

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2> Show User</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('superadmins.tutor_list') }}"> Back</a>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {{ $tutor->name }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Email:</strong>
            {{ $tutor->email }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Roles:</strong>
           
        </div>
    </div>
</div>
@endsection