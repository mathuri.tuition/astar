@extends('layouts.app_login')
<?php
$page = 'superadmin-tutor_list';
$title = 'Tutor Management';
?>
@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Date Joined</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($tutors as $key => $tutor)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $tutor->name }}</td>
            <td>{{ $tutor->email }}</td>
            <td>
                {{ $tutor->date_joined }}
            </td>
            <td>
                <a class="btn btn-info" href="{{ route('superadmins.tutor_show',$tutor->tutor_id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('superadmins.tutor_edit',$tutor->tutor_id) }}">Edit</a>
                {!! Form::open(['method' => 'POST','route' => ['superadmins.tutor_delete', $tutor->tutor_id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </table>
</div>

{!! $tutors->render() !!}

@endsection