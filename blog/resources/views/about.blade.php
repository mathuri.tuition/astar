@extends('layouts.app')
<?php $page = "about"; ?>
@section('content')


<div class="hero-wrap hero-wrap-2" style="background-image: url('{{@genius}}/images/bg_2.jpg'); background-attachment:fixed;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-md-8 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span>About</span></p>
                <h1 class="mb-3 bread">About</h1>
            </div>
        </div>
    </div>
</div>


<section class="ftco-section">
    <div class="container">
        <div class="row d-flex">
            <div class="col-md-6 d-flex ftco-animate">
                <div class="img img-about align-self-stretch" style="background-image: url({{@genius}}/images/bg_3.jpg); width: 100%;"></div>
            </div>
            <div class="col-md-6 pl-md-5 ftco-animate">
                <h2 class="mb-4">Welcome to A Star Educators</h2>
                <p>We are a home tuition agency that provides a platform for students to achieve excellent results by matching them with the best tutors.</p>
                
                <p>We provide a wide range of tuition services that covers academic and non academic classes.</p>
                
                <p>We are committed to match the most suitable tutors in Malaysia based on the students need. Besides, our tutors are determined to build strong rapport with each student and focused to provide the students with the key skills that will lead them towards success in all future endeavours.
We strongly believe that every child in this world has the potential to excel and we are here to bring out the best in all of them.</p>
            </div>
        </div>
    </div>
</section>


@endsection