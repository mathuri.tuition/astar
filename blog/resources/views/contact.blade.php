@extends('layouts.app')
<?php 
$page = "contact";

$site_key = config('app.recaptcha.site_key');
$secret_key = config('app.recaptcha.secret_key');
?>


@section('content')

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<style>
    .hidden {
        display: none;
    }
</style>

<div class="hero-wrap hero-wrap-2" style="background-image: url('{{@genius}}/images/bg_2.jpg'); background-attachment:fixed;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-md-8 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span>Contact</span></p>
                <h1 class="mb-3 bread">Contact Us</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section contact-section ftco-degree-bg">
    <div class="container">
        <div class="row d-flex mb-5 contact-info">
            <div class="col-md-12 mb-4">
                <h2 class="h4">Contact Information</h2>
            </div>
            <div class="w-100"></div>
            
            <div class="col-lg-4 col-md-4 col-sm-12">
                <p><span>Phone:</span> <a >+60 11 1070 4756</a></p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <p><span>Email:</span> <a >admin@stareducators.my</a></p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <p><span>Website</span> <a >www.stareducators.my</a></p>
            </div>
        </div>

        <!-- Starting of successful form message -->
        <div class="row">
            <div class="col-6">
                <div class="alert alert-success contact__msg" style="display: none" role="alert">
                    Your message was sent successfully.
                </div>
            </div>
        </div>
        <!-- Ending of successful form message -->

        <div class="row block-9">
            <div class="col-md-6 pr-md-5">
                <h4 class="mb-4">Do you have any questions?</h4>
                <form id="contact_form" name="contact_form" class="contact__form">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="name" id="name" required class="form-control" placeholder="Your Name">
                    </div>
                    <div class="form-group">
                        <input type="text" name="email" id="email" required class="form-control" placeholder="Your Email">
                    </div>
                    <div class="form-group">
                        <input type="text" name="subject" id="subject" required class="form-control" placeholder="Subject">
                    </div>
                    <div class="form-group">
                        <textarea name="message" id="message" required cols="30" rows="7"  class="form-control" placeholder="Message"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <span id="required_captcha" style="color:red;">* This action is required.</span>
                        <div class="g-recaptcha" data-callback="captchaVerified" data-sitekey="{{ $site_key }}"></div>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Send Message"  id="submit" class="btn btn-primary py-3 px-5">
                        <button class="btn btn-primary py-3 px-5 hidden" type="button" id="loading" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Sending...
                        </button>
                    </div>
                </form>

            </div>

            <div class="col-md-6" id="map"></div>
        </div>
    </div>
</section>

 <script>
 
      window.onload = function() {
            var $recaptcha = document.querySelector('#g-recaptcha-response');
    
            if ($recaptcha) {
                $recaptcha.setAttribute("required", "required");
            }
        };
    
        function captchaVerified() {
            var submitBtn = document.querySelector('#submit');
            submitBtn.removeAttribute('disabled');
        }
       

     (function($) {
        'use strict';
        var form = $('.contact__form'),
            message = $('.contact__msg'),
            form_data;
        // Success function
        function done_func(response) {

            message.fadeIn().removeClass('alert-danger').addClass('alert-success');
            // message.text(response);
            message.text('Your message has been sent. Our team will contact you soon.');
            setTimeout(function() {
                message.fadeOut();
            }, 5000);
            form.find('input:not([type="submit"]), textarea').val('');
            grecaptcha.reset();
            $('#loading').addClass('hidden');
            $('#submit').removeClass('hidden');
        }
        // fail function
        function fail_func(data) {
            message.fadeIn().removeClass('alert-success').addClass('alert-danger');
            // message.text(data.responseText);
            message.text('Oppss ! Seems like something went wrong, please try again.');
            setTimeout(function() {
                message.fadeOut();
            }, 5000);
            $('#loading').addClass('hidden');
            $('#submit').removeClass('hidden');
        }

        form.submit(function(e) {
            e.preventDefault();

            $('#submit').addClass('hidden');
            $('#loading').removeClass('hidden');

            $.ajax({
                    type: 'POST',
                    url: "{{url('/contact-us/send') }}",
                    data: $(this).serialize()
                })
                .done(done_func)
                .fail(fail_func);
        });

    })(jQuery);
    </script>


@endsection