@extends('layouts.app_login')
<?php
$page = 'classes-apply_class';
$title = 'Apply Class';
?>

@section('content')


<div class="row justify-content-center">
    <div class="col-10">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p>{{ $message }}</p>
        </div>
        @endif
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-sm-12 col-md-8">
        <div class="card">
            <div class="card-header">Student Request Tutor</div>

            <div class="card-body">

                <form id="myForm" method="POST" action="{{ route('classes.academics.apply_class.submit') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <div class="col">
                            <label for="inputCurriculum">Curriculum</label>

                            <select class="form-control" required="required" name="curriculum" id="curriculum">
                                <option selected disabled>Choose one...</option>
                                @foreach ($curricula as $key => $curriculum)
                                <option value="{{$curriculum->id}}"> {{$curriculum->curriculum}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="inputLevel">Level</label>
                                <select class="form-control " required="required" name="level" disabled id="level">
                                    <option selected disabled>Choose one...</option>
                                    @foreach ($levels as $key => $level)
                                    <option value="{{$level->id}}"> {{$level->level}}</option>
                                    @endforeach
                                </select>
                                @error('level')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="subject">Subject</label>
                                <select name="subject" class="form-control fees @error('subject') is-invalid @enderror" disabled id="subject">
                                    <option selected disabled data-standard="default">Choose subject...</option>
                                    @foreach($classes as $subject)
                                    <option data-standard="{{$subject->standard_id}}" data-selected="{{$subject->id}}" data-fees="{{$subject->price}}" value="{{$subject->id}}">{{$subject->name}}</option>
                                    @endforeach
                                </select>
                                @error('subject')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <label for="subject">Fees per hour (RM)</label>
                                <input type="text" class="form-control feesCapacity" readonly />
                            </div>
                        </div>

                    </div>


                    <div class="form-group">

                        <label for="days">Preferred Day & Time</label> <br>
                        <div class="table-responsive  @error('day[]') is-invalid @enderror">
                            <table class="table" border="1">
                                <tr>
                                    <th>Select</th>
                                    <th>Days</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputMon" name="day[]" value="mon"></td>
                                    <td> Monday</td>
                                    <td> <input type="time" class="input" id="startTimeMon" name="startTimeMon"></td>
                                    <td><input type="time" class="input" id="endTimeMon" name="endTimeMon"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputTue" name="day[]" value="tue"></td>
                                    <td> Tuesday</td>
                                    <td> <input type="time" class="input" id="startTimeTue" name="startTimeTue"></td>
                                    <td><input type="time" class="input" id="startTimeTue" name="endTimeTue"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputWed" name="day[]" value="wed"></td>
                                    <td> Wednesday</td>
                                    <td> <input type="time" class="input" id="startTimeWed" name="startTimeWed"></td>
                                    <td><input type="time" class="input" id="startTimeWed" name="endTimeWed"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputThu" name="day[]" value="thu"></td>
                                    <td> Thursday</td>
                                    <td> <input type="time" class="input" id="startTimeThu" name="startTimeThu"></td>
                                    <td><input type="time" class="input" id="startTimeThu" name="endTimeThu"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputFri" name="day[]" value="fri"></td>
                                    <td> Friday</td>
                                    <td> <input type="time" class="input" id="startTimeFri" name="startTimeFri"></td>
                                    <td><input type="time" class="input" id="startTimeFri" name="endTimeFri"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputSat" name="day[]" value="sat"></td>
                                    <td> Saturday</td>
                                    <td> <input type="time" class="input" id="startTimeSat" name="startTimeSat"></td>
                                    <td><input type="time" class="input" id="startTimeSat" name="endTimeSat"></td>
                                </tr>

                                <tr>
                                    <td style="text-align:center"> <input type="checkbox" id="inputSun" name="day[]" value="sun"></td>
                                    <td> Sunday</td>
                                    <td> <input type="time" class="input" id="startTimeSun" name="startTimeSun"></td>
                                    <td><input type="time" class="input" id="startTimeSun" name="endTimeSun"></td>
                                </tr>

                            </table>
                        </div>
                        @error('day[]')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="location">Preferred Location</label>
                        <input type="text" name="location" id="location" class="form-control  @error('location') is-invalid @enderror" placeholder="Location" value="{{ old('location') }}">
                        @error('location')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="info">Tutor Preference / Additional Information</label>
                        <textarea name="info" rows="4" cols="50" id="info" class="form-control  @error('info') is-invalid @enderror"></textarea>
                        @error('info')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group float-right">
                        <button type="submit" class="btn btn-success">Request</button>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        $('#inputMon').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeMon').attr('required', '');
                $('#startTimeMon').attr('data-error', 'This field is required.');

                $('#endTimeMon').attr('required', '');
                $('#endTimeMon').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeMon').removeAttr('required');
                $('#startTimeMon').removeAttr('data-error');

                $('#endTimeMon').removeAttr('required');
                $('#endTimeMon').removeAttr('data-error');
            }
        });

        $('#inputTue').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeTue').attr('required', '');
                $('#startTimeTue').attr('data-error', 'This field is required.');

                $('#endTimeTue').attr('required', '');
                $('#endTimeTue').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeTue').removeAttr('required');
                $('#startTimeTue').removeAttr('data-error');

                $('#endTimeTue').removeAttr('required');
                $('#endTimeTue').removeAttr('data-error');
            }
        });

        $('#inputWed').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeWed').attr('required', '');
                $('#startTimeWed').attr('data-error', 'This field is required.');

                $('#endTimeWed').attr('required', '');
                $('#endTimeWed').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeWed').removeAttr('required');
                $('#startTimeWed').removeAttr('data-error');

                $('#endTimeWed').removeAttr('required');
                $('#endTimeWed').removeAttr('data-error');
            }
        });

        $('#inputThu').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeThu').attr('required', '');
                $('#startTimeThu').attr('data-error', 'This field is required.');

                $('#endTimeThu').attr('required', '');
                $('#endTimeThu').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeThu').removeAttr('required');
                $('#startTimeThu').removeAttr('data-error');

                $('#endTimeThu').removeAttr('required');
                $('#endTimeThu').removeAttr('data-error');
            }
        });

        $('#inputFri').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeFri').attr('required', '');
                $('#startTimeFri').attr('data-error', 'This field is required.');

                $('#endTimeFri').attr('required', '');
                $('#endTimeFri').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeFri').removeAttr('required');
                $('#startTimeFri').removeAttr('data-error');

                $('#endTimeFri').removeAttr('required');
                $('#endTimeFri').removeAttr('data-error');
            }
        });

        $('#inputSat').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeSat').attr('required', '');
                $('#startTimeSat').attr('data-error', 'This field is required.');

                $('#endTimeSat').attr('required', '');
                $('#endTimeSat').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeSat').removeAttr('required');
                $('#startTimeSat').removeAttr('data-error');

                $('#endTimeSat').removeAttr('required');
                $('#endTimeSat').removeAttr('data-error');
            }
        });

        $('#inputSun').click(function() {
            if ($(this).prop("checked") == true) {
                // alert("Checkbox is checked.");
                // console.log("yes checked");
                $('#startTimeSun').attr('required', '');
                $('#startTimeSun').attr('data-error', 'This field is required.');

                $('#endTimeSun').attr('required', '');
                $('#endTimeSun').attr('data-error', 'This field is required.');
            } else {
                $('#startTimeSun').removeAttr('required');
                $('#startTimeSun').removeAttr('data-error');

                $('#endTimeSun').removeAttr('required');
                $('#endTimeSun').removeAttr('data-error');
            }
        });


        $("#curriculum").trigger("change");
        $("#level").trigger("change");
        $("#standard").trigger("change");

        $("#curriculum").change(function() {
            $('#level').prop('disabled', false);
        });

        $("#level").change(function() {
            if ($(this).data('options') === undefined) {
                /*Taking an array of all options-2 and kind of embedding it on the level*/
                $(this).data('options', $('#standard option').clone());
            }
            var id = $(this).val();
            var options = $(this).data('options').filter('[data-level=' + id + ']');
            $('#standard').html(options);
            $('#standard').prop('disabled', false);

        });

        $("#standard").change(function() {
            if ($(this).data('options') === undefined) {
                /*Taking an array of all options-2 and kind of embedding it on the level*/
                $(this).data('options', $('#subject option').clone());
            }
            var id = $(this).val();
            $("#standard").data('options').filter('[data-standard=default]').show();
            var options = $(this).data('options').filter('[data-standard=' + id + ']');
            $('#subject').html(options);
            $('#subject').prop('disabled', false);
            // $("#standard").children("option[value^=default]").show();

        });

        $('#standard').change(function() {
            var fees = $('select.fees').find(':selected').data('fees');
            $('.feesCapacity').val(fees);
        });

    });
</script>
@endsection