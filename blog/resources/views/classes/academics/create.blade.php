@extends('layouts.app_login')
<?php
$page = 'classes-academics_create';
$title = 'Create Academic Class';
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">

            <div class="card-body">

                <form method="POST" action="{{ route('classes.academics.store') }}">
                    @csrf

                    <div class="form-group">
                        <div class="col">
                            <label for="inputCurriculum">Curriculum</label>

                            <select class="form-control" required="required" name="curriculum" id="curriculum">
                                <option selected disabled>Choose...</option>
                                @foreach ($curricula as $key => $curriculum)
                                <option value="{{$curriculum->id}}"> {{$curriculum->curriculum}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col">
                            <label for="inputLevel">Level</label>

                            <select class=" form-control" required="required" name="level"  id="level">
                                <option selected disabled>Choose...</option>
                                @foreach ($levels as $key => $level)
                                <option data-curriculum="{{$level->curriculum_id}}" value="{{$level->id}}">{{$level->level}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col">
                            <label for="inputName">Subject Name</label>
                            <input type="text" name="subject" class="form-control" placeholder="Subject" value="{{old('subject')}}">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col">
                            <button type="submit" class="btn btn-success float-right">Submit</button>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {

        $("#curriculum").change(function() {
            if ($(this).data('options') === undefined) {
                /*Taking an array of all options-2 and kind of embedding it on the level*/
                $(this).data('options', $('#level option').clone());
            }
            var id = $(this).val();
            var options = $(this).data('options').filter('[data-curriculum=' + id + ']');
            $('#level').html(options);
            $("#level").prepend("<option disabled selected='selected'>Choose...</option>");
        });

    });
</script>

@endsection