@extends('layouts.app_login')
<?php
$page = 'classes-academics_create';
$title = 'Edit Academic Class';
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">

            <div class="card-body">

                {!! Form::model($class, ['method' => 'POST','route' => ['classes.academics.update', $class->id]]) !!}
                @csrf

                <div class="form-group">
                    <div class="col">
                        <label for="inputCurriculum">Curriculum</label>

                        <select class="form-control" required="required" name="curriculum" id="curriculum">
                            <option selected disabled>Choose one...</option>
                            @foreach ($curricula as $key => $curriculum)
                            @if($curriculum->id == $class->curriculum_id)
                            <option selected value="{{$curriculum->id}}"> {{$curriculum->curriculum}}</option>
                            @else
                            <option value="{{$curriculum->id}}"> {{$curriculum->curriculum}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col">
                        <label for="inputLevel">Level</label>

                        <select class=" form-control" required="required" name="level" id="level">
                            <option selected disabled>Choose one...</option>
                            @foreach ($levels as $key => $level)
                            @if($level->id == $class->level_id)
                            <option selected value="{{$level->id}}"> {{$level->level}}</option>
                            @else
                            <option value="{{$level->id}}"> {{$level->level}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>



                <div class="form-group">
                    <div class="col">
                        <label for="inputName">Subject Name</label>
                        <input type="text" name="subject" class="form-control" placeholder="Subject" value="{{$class->name}}">
                    </div>
                </div>

               
            </div>

            <div class="card-footer">
                <div class="form-group">
                    <div class="col">
                        <a class="btn btn-primary float-left" href="{{ route('classes.academics.index') }}"> Back</a>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-success float-right">Submit</button>
                    </div>
                </div>
                </form>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $("#curriculum").change(function() {
            $('#level').prop('disabled', false);
        });

        $("#level").change(function() {
            if ($(this).data('options') === undefined) {
                /*Taking an array of all options-2 and kind of embedding it on the level*/
                $(this).data('options', $('#standard option').clone());
            }
            var id = $(this).val();
            var options = $(this).data('options').filter('[data-level=' + id + ']');
            $('#standard').html(options);
            $('#standard').prop('disabled', false);
        });

    });
</script>

@endsection