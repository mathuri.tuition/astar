@extends('layouts.app_login')
<?php
$page = 'classes-apply_class';
$title = 'Apply Class';
?>

@section('content')

<style>
    .fa {
        padding-right: 10px;
    }

    /* .row { */
    /* padding-bottom: 20px; */
    /* } */

    .col-sm-12,
    .col-md-6 {
        margin-bottom: 25px;
    }

    .td {
        width: 32%;
    }

    .mark_space {
        width: 5%;
    }

    .weekDays-selector input {
        display: none !important;
    }

    .weekDays-selector input[type=checkbox]+label {
        display: inline-block;
        border-radius: 6px;
        background: #dddddd;
        height: 40px;
        width: 45px;
        margin-right: 3px;
        line-height: 40px;
        text-align: center;
        cursor: pointer;
    }

    .weekDays-selector input[type=checkbox]:checked+label {
        background: #2AD705;
        color: #ffffff;
    }
</style>

<!-- Job Title / ID -->

<form id="myForm" method="POST" action="{{ route('classes.academics.apply_class.submit') }}">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-sm-12 col-md-6 ">
            <div class="card">
                <div class="card-header">
                    <span class="fa fa-map-marked"> </span> Tuition Job Location :
                </div>
                <div class="card-body">
                    <table class="table table-sm table-borderless">
                        <tr>
                            <td class="td">State</td>
                            <td class="mark_space">:</td>
                            <td>
                                <input type="text" class="form-control @error('state') is-invalid @enderror" id="state" name="state" value="{{ old('state') }}">
                                @error('state')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td class="td">City</td>
                            <td class="mark_space">:</td>
                            <td>
                                <input type="text" class="form-control @error('city') is-invalid @enderror" id="city" name="city" value="{{ old('city') }}">
                                @error('city')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td class="td">Postcode</td>
                            <td class="mark_space">:</td>
                            <td>
                                <input type="text" maxlength="5" class="form-control @error('postcode') is-invalid @enderror" id="postcode" name="postcode" value="{{ old('postcode') }}">
                                @error('postcode')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td class="td">Area</td>
                            <td class="mark_space">:</td>
                            <td>
                                <input type="text" class="form-control @error('area') is-invalid @enderror" id="area" name="area" value="{{ old('area') }}">
                                @error('area')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>

        <div class="col-sm-12 col-md-6 ">
            <div class="card">
                <div class="card-header">
                    <span class="fa fa-chalkboard-teacher"> </span> Tuition Job Description :
                </div>
                <div class="card-body">
                    <table class="table table-sm table-borderless">
                        <tr>
                            <td class="td">Syllabus</td>
                            <td class="mark_space">:</td>
                            <td>
                                <select class="form-control" required="required" name="curriculum" id="curriculum">
                                    <option selected disabled>Choose...</option>
                                    @foreach ($curricula as $key => $curriculum)
                                    <option value="{{$curriculum->id}}"> {{$curriculum->curriculum}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="td">Level</td>
                            <td class="mark_space">:</td>
                            <td>
                                <select class="form-control " required="required" name="level" disabled id="level">
                                    <option selected disabled>Choose...</option>
                                    @foreach ($levels as $key => $level)
                                    <option value="{{$level->id}}"> {{$level->level}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>


                        <tr>
                            <td class="td">Subject</td>
                            <td class="mark_space">:</td>
                            <td>
                                <select name="subject" class="form-control fees @error('subject') is-invalid @enderror" disabled id="subject">
                                    <option selected disabled data-standard="default">Choose...</option>
                                    @foreach($classes as $subject)
                                    <option data-standard="{{$subject->standard_id}}" data-selected="{{$subject->id}}" data-fees="{{$subject->price}}" value="{{$subject->id}}">{{$subject->name}}</option>
                                    @endforeach
                                </select>
                                @error('subject')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-6 ">
            <div class="card">
                <div class="card-header">
                    <span class="fa fa-comment-dollar"> </span> Tuition Fees :
                </div>
                <div class="card-body">
                    <table class="table table-sm table-borderless">
                        <tr>
                            <td class="td">Fees per hour</td>
                            <td class="mark_space">:</td>
                            <td>
                                RM <input type="text" class=" feesCapacity" readonly />
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-6 ">
            <div class="card">
                <div class="card-header">
                    <span class="fa fa-user-friends"> </span> Tuition Type :
                </div>
                <div class="card-body">
                    <table class="table table-sm table-borderless">
                        <tr>
                            <td class="td">Type</td>
                            <td class="mark_space">:</td>
                            <td>
                                <select class="form-control @error('type') is-invalid @enderror" id="type" name="type">
                                    <option disabled selected>Choose...</option>
                                    <option value="1">Personal</option>
                                    <option value="2">Group</option>
                                </select>
                                @error('type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td class="td">No. of students</td>
                            <td class="mark_space">:</td>
                            <td>
                                <input type="number" class="form-control @error('pax') is-invalid @enderror" id="pax" name="pax" value="{{ old('pax') }}">
                                @error('pax')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Timing -->
    <div class="row">
        <div class="col-sm-12 col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <span class="fa fa-clock"> </span> Tuition Preferred Timing :
                </div>
                <div class="card-body">
                    <table class="table table-sm table-borderless">
                        <tr>
                            <td class="td">Frequency</td>
                            <td class="mark_space">:</td>
                            <td>
                                <select id="frequency" name="frequency" class="form-control @error('frequency') is-invalid @enderror">
                                    <option disabled selected>Choose...</option>
                                    <option value="1">1 class per week</option>
                                    <option value="2">2 class per week</option>
                                    <option value="3">3 class per week</option>
                                    <option value="4">4 class per week</option>
                                    <option value="5">5 class per week</option>
                                    <option value="6">6 class per week</option>
                                    <option value="7">7 class per week</option>
                                    <option value="8">More than 7 class</option>
                                </select>
                                @error('frequency')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td class="td">Days</td>
                            <td class="mark_space">:</td>
                            <td>
                                <div class="weekDays-selector">
                                    <input type="checkbox" id="weekday-mon" name="day[]" value="mon" class="weekday" />
                                    <label for="weekday-mon">Mon</label>
                                    <input type="checkbox" id="weekday-tue" name="day[]" value="tue" class="weekday" />
                                    <label for="weekday-tue">Tue</label>
                                    <input type="checkbox" id="weekday-wed" name="day[]" value="wed" class="weekday" />
                                    <label for="weekday-wed">Wed</label>
                                    <input type="checkbox" id="weekday-thu" name="day[]" value="thu" class="weekday" />
                                    <label for="weekday-thu">Thu</label>
                                    <input type="checkbox" id="weekday-fri" name="day[]" value="fri" class="weekday" />
                                    <label for="weekday-fri">Fri</label>
                                    <input type="checkbox" id="weekday-sat" name="day[]" value="sat" class="weekday" />
                                    <label for="weekday-sat">Sat</label>
                                    <input type="checkbox" id="weekday-sun" name="day[]" value="sun" class="weekday" />
                                    <label for="weekday-sun">Sun</label>
                                </div>
                                @error('days')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td class="td">Duration per class (hour)</td>
                            <td class="mark_space">:</td>
                            <td>
                                <input type="text" id="duration" name="duration" class="form-control @error('duration') is-invalid @enderror" placeholder="2.5 hours" value="{{ old('duration') }}">
                                @error('duration')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td class="td">Time</td>
                            <td class="mark_space">:</td>
                            <td>
                                <input type="text" id="time" name="time" class="form-control @error('time') is-invalid @enderror" placeholder="morning/evening" value="{{ old('time') }}">
                                @error('time')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Tutor Preference -->
    <div class="row">
        <div class="col-sm-12 col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <span class="fa fa-id-card"> </span> Tutor Preferences :
                </div>
                <div class="card-body">
                    <table class="table table-sm table-borderless">
                        <tr>
                            <td class="td">Gender</td>
                            <td class="mark_space">:</td>
                            <td>
                                <input type="radio" class="radio @error('gender') is-invalid @enderror" id="gender" name="gender" value="male"> Male
                                <input type="radio" class="radio" id="gender" name="gender" value="female"> Female
                                <input type="radio" class="radio" id="gender" name="gender" value="any"> Any
                                @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td class="td">Race</td>
                            <td class="mark_space">:</td>
                            <td>
                                <select class="form-control @error('race') is-invalid @enderror" id="race" name="race">
                                    <option selected disabled>Choose...</option>
                                    <option value="none">No preference</option>
                                    <option value="malay">Malay</option>
                                    <option value="indian">Indian</option>
                                    <option value="chinese">Chinese</option>
                                    <option value="others">Others</option>
                                </select>
                                @error('race')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td class="td">Preferences</td>
                            <td class="mark_space">:</td>
                            <td>
                                <textarea class="form-control" id="preference" name="preference" value="{{old('preference')}}"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="form-group">
            <button type="submit" class="btn btn-success">Request</button>
        </div>
    </div>
</form>


<script type="text/javascript">
    $(document).ready(function() {

        $("#curriculum").trigger("change");
        $("#level").trigger("change");
        $("#standard").trigger("change");

        $("#curriculum").change(function() {
            $('#level').prop('disabled', false);
        });

        $("#level").change(function() {
            if ($(this).data('options') === undefined) {
                /*Taking an array of all options-2 and kind of embedding it on the level*/
                $(this).data('options', $('#standard option').clone());
            }
            var id = $(this).val();
            var options = $(this).data('options').filter('[data-level=' + id + ']');
            $('#standard').html(options);
            $('#standard').prop('disabled', false);

        });

        $("#standard").change(function() {
            if ($(this).data('options') === undefined) {
                /*Taking an array of all options-2 and kind of embedding it on the level*/
                $(this).data('options', $('#subject option').clone());
            }
            var id = $(this).val();
            $("#standard").data('options').filter('[data-standard=default]').show();
            var options = $(this).data('options').filter('[data-standard=' + id + ']');
            $('#subject').html(options);
            $('#subject').prop('disabled', false);
            // $("#standard").children("option[value^=default]").show();

        });

        $('#standard').change(function() {
            var fees = $('select.fees').find(':selected').data('fees');
            $('.feesCapacity').val(fees);
        });

    });
</script>
@endsection