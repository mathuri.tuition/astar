@extends('layouts.app_login')
<?php
$page = 'classes-academics_show';
$title = 'Show Academic Class';
?>
@section('content')


<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <div class="col">
                        <label for="inputCurriculum">Curriculum</label>
                        <input type="text" class="form-control" readonly disabled value=" {{$class->curriculum}}">

                    </div>
                </div>

                <div class="form-group">
                    <div class="col">
                        <label for="inputLevel">Level</label>
                        <input type="text" class="form-control" readonly disabled value="{{ $class->level }}">
                    </div>
                </div>

    

                <div class="form-group">
                    <div class="col">
                        <label for="inputName">Subject Name</label>
                        <input disabled type="text" name="subject" class="form-control" placeholder="Subject (Level)" value="{{ $class->name }}">
                    </div>
                </div>

                
            </div>

            <div class="card-footer">
                <a class="btn btn-primary float-left" href="{{ route('classes.academics.index') }}"> Back</a>
            </div>

        </div>
    </div>
</div>

@endsection