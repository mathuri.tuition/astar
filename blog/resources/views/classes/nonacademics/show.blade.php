@extends('layouts.app_login')
<?php
$page = 'classes-academics_show';
$title = 'Show Non Academic Class';
?>
@section('content')


<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">

            <div class="card-body">

              

                <div class="form-group">
                    <div class="col">
                        <label for="inputName">Class</label>
                        <input disabled type="text" name="subject" class="form-control"  value="{{ $class->class }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col">
                        <label for="inputPrice">Price per hour (RM)</label>
                        <input disabled type="number" class="form-control" name="price"  value="{{ $class->price }}">
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <a class="btn btn-primary float-left" href="{{ route('classes.nonacademics.index') }}"> Back</a>
            </div>

        </div>
    </div>
</div>

@endsection