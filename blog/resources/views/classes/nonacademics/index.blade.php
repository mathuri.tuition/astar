@extends('layouts.app_login')
<?php
$page = 'classes-index';
$title = 'Non Academic Class Management';
?>

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Name </th>
            <th>Level </th>
            <th>Price</th>
            <th>Action</th>
        </tr>

        @foreach ($datas as $data)
        <tr>

            <td>{{ ++$i }}</td>
            <td>{{ $data->class }}</td>
            <td>
                <?php 
                
                $levels = [
                    ["id" => 1,"level"=>"Beginner"],
                    ["id" => 2, "level" => "Intermediate"],
                    ["id" => 3, "level" => "Advanced"],
                ];
                ?>

                @foreach($levels as $level)
                @if($level['id'] == $data->level_id)
                {{ $level['level'] }}
                @endif
                @endforeach
            </td>
            <td>{{ $data->price }}</td>
            <td>
                <a class="btn btn-info" href="{{ route('classes.nonacademics.show', $data->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('classes.nonacademics.edit', "$data->id") }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['classes.nonacademics.delete', $data->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach

    </table>
</div>
@endsection