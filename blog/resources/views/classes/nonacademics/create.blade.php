@extends('layouts.app_login')
<?php
$page = 'classes-academics_create';
$title = 'Create Non Academic Class';
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">

            <div class="card-body">

                <form method="POST" action="{{ route('classes.nonacademics.store') }}">
                    @csrf
                    <div class="form-group">
                        <div class="col">
                            <label for="inputName">Class Name</label>
                            <input type="text" name="class" id="class" class="form-control" value="{{old('class')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col">
                            <label for="inputName">Level</label>
                            <select name="level" id="level" class="form-control @error('level') is-invalid @enderror">
                                <option selected disabled>Choose...</option>
                                <option value="1">Beginner</option>
                                <option value="2">Intermediate</option>
                                <option value="3">Advanced</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col">
                            <label for="inputPrice">Price per hour (RM)</label>
                            <input type="text" class="form-control" name="price" value="{{ old('price') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col">
                            <button type="submit" class="btn btn-success float-right">Submit</button>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>
</div>



@endsection