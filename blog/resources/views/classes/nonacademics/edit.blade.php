@extends('layouts.app_login')
<?php
$page = 'classes-academics_create';
$title = 'Create Academic Class';
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">

            <div class="card-body">

                {!! Form::model($class, ['method' => 'POST','route' => ['classes.nonacademics.update', $class->id]]) !!}
                @csrf
                <div class="form-group">
                    <div class="col">
                        <label for="inputName">Subject Name</label>
                        <input type="text" name="class" id="class" class="form-control" value="{{$class->class}}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col">
                        <label for="inputPrice">Price per hour (RM)</label>
                        <input type="text" class="form-control" name="price" id="price"  value="{{ $class->price }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col">
                        <button type="submit" class="btn btn-success float-right">Submit</button>
                    </div>
                </div>

                </form>

            </div>

        </div>
    </div>
</div>

@endsection