@extends('layouts.app_login')
<?php
$page = 'curricula-edit';
$title = 'Edit Level';
?>


@section('content')

<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">

            <div class="card-body">
                {{ Form::model($level, array('route' => array('levels.update', $level->id), 'method' => 'PUT')) }}
                <div class="form-group">
                    <div class="col">
                        <label for="inputSyllabus">Curriculum</label>
                        <select name="curriculum_id" id="curriculum_id" class="form-control">
                            <option selected disabled>Choose...</option>
                            @foreach($curricula as $curriculum)
                            @if($curriculum->id == $level->curriculum_id)
                            <option selected  value="{{$curriculum->id}}">{{$curriculum->curriculum}}</option>
                            @else
                            <option value="{{$curriculum->id}}">{{$curriculum->curriculum}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col">
                        <label for="inputSyllabus">Level</label>
                        <input type="text" name="level" class="form-control" value="{{$level->level}}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col">
                        <label for="inputSyllabus">Price per hour</label>
                        <input type="text" name="price" class="form-control" value="{{$level->price}}">
                    </div>
                </div>

                <br>
                <a class="btn btn-primary float-left" href="{{ route('levels.index') }}"> Cancel</a>
                        {{ Form::submit('Edit', array('class' => 'btn btn-success float-right')) }}
                        {{ Form::close() }}
                    </div>

                </div>
            </div>
        </div>

        @endsection