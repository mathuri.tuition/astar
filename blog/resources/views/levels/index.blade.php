@extends('layouts.app_login')
<?php
$page = 'levels-index';
$title = 'Levels Management';
?>

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Level</th>
            <th>Curriculum</th>
            <th>Price per hour</th>
            <th>Action</th>
        </tr>

        @foreach ($levels as $level)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $level->level }}</td>
            <td>{{ $level->curriculum }}</td>
            <td>RM {{ $level->price }}</td>
            <td>
                <a class="btn btn-info" href="{{ route('levels.show', $level->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('levels.edit', "$level->id") }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['levels.destroy', $level->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach

    </table>
</div>

{!! $levels->render() !!}

@endsection