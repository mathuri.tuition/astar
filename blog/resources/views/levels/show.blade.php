@extends('layouts.app_login')
<?php
$page = 'curricula-show';
$title = 'Show Level';
?>


@section('content')

<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">

            <div class="card-body">
                <div class="form-group">
                    <label>Curriculum Name</label>

                    <select class="form-control">
                        @foreach($curricula as $curriculum)
                        @if($curriculum->id == $level->curriculum_id)
                        <option selected disabled value="{{$curriculum->id}}">{{$curriculum->curriculum}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <div class="col">
                        <label for="inputSyllabus">Level</label>
                        <input type="text" name="level" class="form-control" value="{{$level->level}}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col">
                        <label for="inputSyllabus">Price per hour</label>
                        <input type="text" name="price" class="form-control" value="{{$level->price}}">
                    </div>
                </div>

            </div>

            <div class="card-footer">
                <a class="btn btn-primary" href="{{ route('levels.index') }}"> Back</a>
            </div>

        </div>
    </div>
</div>

@endsection