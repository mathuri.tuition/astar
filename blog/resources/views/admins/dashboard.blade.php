@extends('layouts.app_login')
<?php 
$page = 'admin-dashboard';
$title = 'Dashboard';
?>
@section('sidebar')

@endsection

@section('content')

<div class="row">


    <div class="col-lg-4 col-md-6 col-sm-12">

        <!-- Total tutors -->
        <div class="card mb-4 py-3 border-left-primary">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Tutors</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ number_format($tutors_count) }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-lg-4 col-md-6 col-sm-12">

        <!-- Total students -->
        <div class="card mb-4 py-3 border-left-primary">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Students</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ number_format($students_count) }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="col-lg-4 col-md-6 col-sm-12">

        <!-- Total students -->
        <div class="card mb-4 py-3 border-left-primary">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Admins</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ number_format($admins_count) }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>


@endsection