@extends('layouts.app_login')
<?php 
$page = 'admin-profile'; 
$title = 'Profile'; 
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">{{ __('Profile') }}</div>

            <div class="card-body">

                {{ Form::model($admin, array('route' => array('admins.profile.update', $admin->user_id), 'method' => 'POST')) }}
                <div class="form-row">
                    <div class="col">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Name" value="<?= $admin->name; ?>">
                    </div>
                    <div class="col">
                        <label for="inputEmail4">Email</label>
                        <input disabled type="email" class="form-control" name="email" id="inputEmail4" placeholder="Email" value="<?= $admin->email; ?>">
                    </div>
                </div>

                <div class="form-row">

                    <div class="col" style="border-left-width: 10px;padding-left: 45px;">
                        <label for="gender">Gender</label>
                        <br>
                        <input type="radio" name="gender" value="1" <?php echo ($admin->gender_id  == '1') ? 'checked' : '' ?>> Male
                        <br>
                        <input type="radio" name="gender" value="0" <?php echo ($admin->gender_id  == '0') ? 'checked' : '' ?>> Female
                    </div>

                    <div class="col">
                        <label for="inputRace">Race</label>
                        <input type="text" class="form-control" name="race" id="inputRace" placeholder="Race" value="<?= $admin->race; ?>">
                    </div>

                    <div class="col">
                        <label for="inputReligion">Religion</label>
                        <input type="text" class="form-control" name="religion" id="inputRace" placeholder="Religion" value="<?= $admin->religion; ?>">
                    </div>

                </div>

                <div class="form-row">
                    <div class="col">
                        <label for="inputEmail4">Nationality</label>
                        <select class="form-control" id="nationality" name="nationality">
                            <?php
                            foreach ($nationalities as $nationality) {
                                $n = $nationality->id;
                                if ($n == $admin->nationality_id) {
                                    echo "<option selected value='$n'>$nationality->nationality</option>";
                                } else {
                                    echo "<option value='$n'>$nationality->nationality</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class='col' id='mykadDiv' style="display:none;">
                        <label for='mykad'>Mykad No.</label>
                        <input type='text' class='form-control' name='mykad' id='mykad' value='<?= $admin->mykad_no; ?>'>
                    </div>

                    <div class='col' id='passportDiv' style="display:none;">
                        <label for='passport'>Passport No.</label>
                        <input type='text' class='form-control' name='passport' id='passport' value='<?= $admin->passport_no; ?>'>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <label for="maritalStatus">Marital Status</label>
                        <select class="form-control" id="marital_status" name="marital_status">
                            <?php
                            foreach ($marital_statuses as $marital_status) {

                                if ($marital_status->id == $admin->marital_status_id) {
                                    echo "<option selected value='$marital_status->id'>" . ucfirst($marital_status->status) . "</option>";
                                } else {
                                    echo "<option value='$marital_status->id'>" . ucfirst($marital_status->status) . "</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>

                </div>

                <div class="form-group">
                    <label for="inputAddress">Address</label>
                    <input type="text" class="form-control" name="address_1" id="inputAddress" placeholder="1234 Main St" value="<?= $admin->address_1 ?>">
                </div>
                <div class="form-group">
                    <label for="inputAddress2">Address 2</label>
                    <input type="text" class="form-control" name="address_2" id="inputAddress2" placeholder="Apartment, studio, or floor" value="<?= $admin->address_2 ?>">
                </div>
                <div class="form-group">
                    <label for="inputAddress3">Address 3</label>
                    <input type="text" class="form-control" name="address_3" id="inputAddress3" placeholder="Apartment, studio, or floor" value="<?= $admin->address_3 ?>">
                </div>
                <div class="form-group">
                    <label for="inputAddress4">Address 4</label>
                    <input type="text" class="form-control" name="address_4" id="inputAddress4" placeholder="Apartment, studio, or floor" value="<?= $admin->address_4 ?>">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputCity">City</label>
                        <input type="text" class="form-control" name="city" id="inputCity" value="<?= $admin->city ?>">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="state">State</label>
                        <select class="form-control" id="state" name="state">
                            <?php
                            foreach ($states as $state) {
                                if ($state->code == $admin->state_code) {
                                    echo "<option selected value='$state->code'>$state->state</option>";
                                } else {
                                    echo "<option value='$state->code'>$state->state</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputZip">Zip</label>
                        <input type="text" class="form-control" name="postcode" maxlength="5" id="inputZip" value="<?= $admin->postcode ?>">
                    </div>
                </div>


                <div class="form-group float-right">
                    <button type="submit" class="btn btn-success">Update</button>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        // console.log(document.getElementById('nationality').value);

        if (document.getElementById('nationality').value == "109") {
            $('#mykadDiv').show();
            $('#mykad').attr('required', '');
            $('#mykadDiv').attr('data-error', 'This field is required.');
            $('#passportDiv').hide();
            $('#passport').removeAttr('required');
            $('#passportDiv').removeAttr('data-error');
        } else {
            $('#passportDiv').show();
            $('#passport').attr('required', '');
            $('#passportDiv').attr('data-error', 'This field is required.');
            $('#mykadDiv').hide();
            $('#mykad').removeAttr('required');
            $('#mykadDiv').removeAttr('data-error');
        }

        $("#nationality").trigger("change");
        $("#nationality").change(function() {
            if ($(this).val() == "109") {
                $('#mykadDiv').show();
                $('#mykad').attr('required', '');
                $('#mykadDiv').attr('data-error', 'This field is required.');
                $('#passportDiv').hide();
                $('#passport').removeAttr('required');
                $('#passportDiv').removeAttr('data-error');
            } else {
                $('#passportDiv').show();
                $('#passport').attr('required', '');
                $('#passportDiv').attr('data-error', 'This field is required.');
                $('#mykadDiv').hide();
                $('#mykad').removeAttr('required');
                $('#mykadDiv').removeAttr('data-error');
            }
        });
        // $("#nationality").trigger("change");

        function myFunction() {
            if (document.getElementById('nationality').value == "109") {
                $('#mykadDiv').show();
                $('#mykad').attr('required', '');
                $('#mykadDiv').attr('data-error', 'This field is required.');
                $('#passportDiv').hide();
                $('#passport').removeAttr('required');
                $('#passportDiv').removeAttr('data-error');
            } else {
                $('#passportDiv').show();
                $('#passport').attr('required', '');
                $('#passportDiv').attr('data-error', 'This field is required.');
                $('#mykadDiv').hide();
                $('#mykad').removeAttr('required');
                $('#mykadDiv').removeAttr('data-error');
            }
        }
    });
</script>

<br><br>
@endsection