@extends('layouts.app_login')
<?php
$page = 'jobs-active_status_index';
$title = 'Academic Class Price';
?>

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="card">
            <div class="card-body">
                <a type="button" class="btn btn-primary" target="_blank" href="{{ route('admins.academics.show_pdf') }}">View</a>
            </div>
        </div>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="card">
            <div class="card-body">
                <form id="upload_price" action="{{ route('admins.academics.price_upload') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label>Attachement File (PDF)</label>
                        <input id="price" type="file" accept="application/pdf" class="form-control" name="price">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>


@endsection