@extends('layouts.app_login')
<?php
$page = 'tutor-profile';
$title = 'Profile';

$profile_path = config('app.image.student.profile');
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Whoops!</strong> There were some problems with your input.<br><br>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif


<div class="row">
<div class="col-12">
    <div class="tab-content">
        <div class="tab-pane fade show active" id="account" role="tabpanel">

            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#publicInfo" aria-expanded="false" aria-controls="publicInfo">
                            <span class="fa fa-caret-down"></span>
                        </button>
                    </div>
                    <h5 class="card-title mb-0">Basic info</h5>
                </div>
                <div class="card-body collapse" id="publicInfo">

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="inputUsername">Full Name</label>
                                    <input readonly type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{$student->user_name}}">
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="col-md-6 col-sm-12">
                                    <label for="inputUsername">E-Mail</label>
                                    <input readonly type="text" class="form-control" disabled value="{{$student->user_email}}">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <label for="inputUsername">Phone Number</label>
                                    <input readonly type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="phone" value="{{$student->phone_no}}">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-6 col-sm-12">
                                    <label for="inputEmail4">Nationality</label>
                                    <select readonly class="form-control @error('nationality') is-invalid @enderror" id="nationality" name="nationality">
                                        <?php
                                        foreach ($nationalities as $nationality) {
                                            $n = $nationality->id;
                                            if ($n == $student->nationality_id) {
                                                echo "<option selected value='$n'>$nationality->nationality</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class='col-md-6 col-sm-12' id='mykadDiv' style="display:none;">
                                    <label for='mykad'>Mykad No.</label>
                                    <input readonly type='text' class="form-control @error('mykad') is-invalid @enderror" name='mykad' id='mykad' value='<?= $student->mykad_no; ?>'>
                                </div>

                                <div class='col-md-6 col-sm-12' id='passportDiv' style="display:none;">
                                    <label for='passport'>Passport No.</label>
                                    <input readonly type='text' class="form-control @error('passport') is-invalid @enderror" name='passport' id='passport' value='<?= $student->passport_no; ?>'>
                                </div>
                            </div>

                            <div class="row" style="margin-top:10px;">
                                <div class="col-md-6 col-sm-12">
                                    <label for='gender'>Gender &nbsp </label>
                                    <input readonly type="radio" class="@error('gender') is-invalid @enderror" name="gender" value="1" <?php echo ($student->gender_id  == '1') ? 'checked' : '' ?>> Male
                                    <input readonly type="radio" name="gender" value="0" <?php echo ($student->gender_id  == '0') ? 'checked' : '' ?>> Female
                                </div>
                            </div>


                        </div>
                        <div class="col-md-4">
                            <div class="text-center">


                                <div class="mt-2">
                                    <img id="preview" alt="{{ $student->user_name }}" src="{{asset('storage/'.$profile_path.$student->image)}}" class="rounded-circle img-responsive mt-2" width="128" height="128">
                                    <span class="btn btn-secondary col-sm" style="outline:none;">
                                        <i class="">
                                            <input readonly style="outline:none;" name="image" id="image" type="file" style="visibility:hidden;position:absolute;">
                                        </i>
                                    </span>
                                </div>
                                <small>For best results, use an image at least 128px by 128px in .jpg format</small>
                            </div>
                        </div>
                    </div>



                </div>
            </div>

            <br><br>

            <div class="card">
                <div class="card-header">
                    <div class="float-right">
                        <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#privateInfo" aria-expanded="false" aria-controls="privateInfo">
                            <span class="fa fa-caret-down"></span>
                        </button>
                    </div>
                    <h5 class="card-title mb-0">Private info</h5>
                </div>
                <div class="card-body collapse" id="privateInfo">

                    <div class="form-row">
                        <div class="col">
                            <label for="inputMaritalStatus">Marital Status</label>
                            <select readonly class="form-control @error('marital_status') is-invalid @enderror" name="marital_status" id="marital_status" required>
                                <option selected disabled>Choose...</option>
                                <?php
                                foreach ($maritals as $maritial) {
                                    if ($maritial->id == $student->marital_status_id) {
                                        echo "<option selected value='$maritial->id'>" . ucfirst($maritial->status) . "</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col">
                            <label for="inputRace">Race</label>
                            <select readonly class="form-control @error('race') is-invalid @enderror" name="race" id="race" required>
                                <option selected disabled>Choose...</option>
                                <option <?= ($student->race == 1 ? 'selected' : '') ?> value="1">Malay</option>
                                <option <?= ($student->race == 2 ? 'selected' : '') ?> value="2">Indian</option>
                                <option <?= ($student->race == 3 ? 'selected' : '') ?> value="3">Chinese</option>
                                <option <?= ($student->race == 4 ? 'selected' : '') ?> value="4">Others</option>
                            </select>
                        </div>

                        <div class="col">
                            <label for="inputReligion">Religion</label>
                            <select readonly class="form-control @error('religion') is-invalid @enderror" name="religion" id="religion" required>
                                <option selected disabled>Choose...</option>
                                <option <?= ($student->race == 1 ? 'selected' : '') ?> value="1">Buddhist</option>
                                <option <?= ($student->race == 2 ? 'selected' : '') ?> value="2">Cristian</option>
                                <option <?= ($student->race == 3 ? 'selected' : '') ?> value="3">Hindu</option>
                                <option <?= ($student->race == 4 ? 'selected' : '') ?> value="4">Islam</option>
                                <option <?= ($student->race == 5 ? 'selected' : '') ?> value="5">Sikh</option>
                                <option <?= ($student->race == 6 ? 'selected' : '') ?> value="6">Others</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputAddress">Address</label>
                        <input readonly type="text" class="form-control @error('address_1') is-invalid @enderror" name="address_1" id="address_1" placeholder="1234 Main St" value="<?= $student->address_1 ?>">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress2">Address 2</label>
                        <input readonly type="text" class="form-control @error('address_2') is-invalid @enderror" name="address_2" id="address_2" placeholder="Apartment, studio, or floor" value="<?= $student->address_2 ?>">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress3">Address 3</label>
                        <input readonly type="text" class="form-control @error('address_3') is-invalid @enderror" name="address_3" id="address_3" placeholder="Apartment, studio, or floor" value="<?= $student->address_3 ?>">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress4">Address 4</label>
                        <input readonly type="text" class="form-control @error('address_4') is-invalid @enderror" name="address_4" id="address_4" placeholder="Apartment, studio, or floor" value="<?= $student->address_4 ?>">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputCity">City</label>
                            <input readonly type="text" class="form-control @error('city') is-invalid @enderror" name="city" id="city" value="<?= $student->city ?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="state">State</label>
                            <select readonly class="form-control @error('state') is-invalid @enderror" id="state" name="state">
                                <option selected disabled>Choose...</option>
                                <?php
                                foreach ($states as $state) {
                                    if ($state->code == $student->state_code) {
                                        echo "<option selected value='$state->code'>$state->state</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputZip">Zip</label>
                            <input readonly type="text" class="form-control @error('postcode') is-invalid @enderror" name="postcode" maxlength="5" id="postcode" value="<?= $student->postcode ?>">
                        </div>
                    </div>




                </div>
            </div>

        </div>

    </div>
</div>
</div>

<div class="row" style="margin-top: 30px;">
    <div class="col-12">
        <a class="btn btn-primary float-left" href="{{route('admins.users.students.index')}}">Back</a>
    </div>
</div>

<script type="text/javascript">
    $(document).on("click", ".browse", function() {
        var file = $(this).parents().find(".file");
        file.trigger("click");
    });

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            document.getElementById("preview").src = e.target.result;
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    $(document).ready(function() {
        // console.log(document.getElementById('nationality').value);

        if (document.getElementById('nationality').value == "109") {
            $('#mykadDiv').show();
            $('#mykad').attr('required', '');
            $('#mykadDiv').attr('data-error', 'This field is required.');
            $('#passportDiv').hide();
            $('#passport').removeAttr('required');
            $('#passportDiv').removeAttr('data-error');
        } else {
            $('#passportDiv').show();
            $('#passport').attr('required', '');
            $('#passportDiv').attr('data-error', 'This field is required.');
            $('#mykadDiv').hide();
            $('#mykad').removeAttr('required');
            $('#mykadDiv').removeAttr('data-error');
        }

        $("#nationality").trigger("change");
        $("#nationality").change(function() {
            if ($(this).val() == "109") {
                $('#mykadDiv').show();
                $('#mykad').attr('required', '');
                $('#mykadDiv').attr('data-error', 'This field is required.');
                $('#passportDiv').hide();
                $('#passport').removeAttr('required');
                $('#passportDiv').removeAttr('data-error');
            } else {
                $('#passportDiv').show();
                $('#passport').attr('required', '');
                $('#passportDiv').attr('data-error', 'This field is required.');
                $('#mykadDiv').hide();
                $('#mykad').removeAttr('required');
                $('#mykadDiv').removeAttr('data-error');
            }
        });
        // $("#nationality").trigger("change");


    });
</script>

@endsection