@extends('layouts.app_login')
<?php
$page = 'users-index';
$title = 'Student Management';
?>

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
  <p>{{ $message }}</p>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="table-responsive">
  <table class="table table-bordered">
    <tr>
      <th>No</th>
      <th>Name</th>
      <th>Email</th>
      <th>Roles</th>
      <th width="280px">Action</th>
    </tr>
    @foreach ($data as $key => $user)
    <tr>
      <td>{{ ++$i }}</td>
      <td>{{ $user->name }}</td>
      <td>{{ $user->email }}</td>
      <td>Student</td>
      <td>
        <a class="btn btn-info" href="{{ route('admins.users.students.show',$user->id) }}">Show</a>
        <a class="btn btn-primary" href="{{ route('admins.users.students.edit',$user->id) }}">Edit</a>
        {!! Form::open(['method' => 'DELETE','route' => ['admins.users.students.delete', $user->id],'style'=>'display:inline']) !!}
        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
      </td>
    </tr>
    @endforeach
  </table>
</div>

{!! $data->render() !!}

@endsection