@extends('layouts.app_login')
<?php
$page = 'tutor-profile';
$title = 'Profile';

$profile_path = config('app.image.tutor.profile');
$spm_path = config('app.file.tutor.spm');
$higher_path = config('app.file.tutor.higher');
$membership_path = config('app.image.tutor.membership');


if(is_null($preference))
{
    $t_subjects = '';
    $areas = '';
    $experience = '';
}else{
    $t_subjects = $preference->subjects;
    $areas = $preference->areas;
    $experience = $preference->experience;
}

// dd($tutor);
?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Whoops!</strong> There were some problems with your input.<br><br>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif


<div class="row">
    <div class="col-12">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="account" role="tabpanel">

                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#publicInfo" aria-expanded="false" aria-controls="publicInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Basic info</h5>
                    </div>
                    <div class="card-body collapse" id="publicInfo">
                        
                        <form method="POST" action="{{ route('admins.users.tutors.update_public_info', $tutor->user_id) }}" enctype="multipart/form-data">
                            @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-row">
                                    <div class="col-md-12 col-sm-12">
                                        <label for="inputUsername">Full Name</label>
                                        <input  type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{$tutor->user_name}}">
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="inputUsername">E-Mail</label>
                                        <input type="email" name="email" id="email" class="form-control" value="{{$tutor->user_email}}">
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label for="inputUsername">Phone Number</label>
                                        <input  type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="phone" value="{{$tutor->phone_no}}">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="inputEmail4">Nationality</label>
                                        <select  class="form-control @error('nationality') is-invalid @enderror" id="nationality" name="nationality">
                                            <?php
                                            foreach ($nationalities as $nationality) {
                                                $n = $nationality->id;
                                                if ($n == $tutor->nationality_id) {
                                                    echo "<option selected value='$n'>$nationality->nationality</option>";
                                                }else{
                                                    echo "<option value='$n'>$nationality->nationality</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class='col-md-6 col-sm-12' id='mykadDiv' style="display:none;">
                                        <label for='mykad'>Mykad No.</label>
                                        <input  type='text' class="form-control @error('mykad') is-invalid @enderror" name='mykad' id='mykad' value='<?= $tutor->mykad_no; ?>'>
                                    </div>

                                    <div class='col-md-6 col-sm-12' id='passportDiv' style="display:none;">
                                        <label for='passport'>Passport No.</label>
                                        <input  type='text' class="form-control @error('passport') is-invalid @enderror" name='passport' id='passport' value='<?= $tutor->passport_no; ?>'>
                                    </div>
                                </div>

                                <div class="row" style="margin-top:10px;">
                                    <div class="col-md-6 col-sm-12">
                                        <label for='gender'>Gender &nbsp </label>
                                        <input  type="radio" class="@error('gender') is-invalid @enderror" name="gender" value="1" <?php echo ($tutor->gender_id  == '1') ? 'checked' : '' ?>> Male
                                        <input  type="radio" name="gender" value="0" <?php echo ($tutor->gender_id  == '0') ? 'checked' : '' ?>> Female
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-4">
                                <div class="text-center">
                                    
                                      <div class="mt-2">
                                            <img id="preview" alt="{{ $tutor->user_name }}" src="{{asset('storage/'.$profile_path.$tutor->image)}}" class="rounded-circle img-responsive mt-2" width="128" height="128">
                                            <span class="btn btn-secondary col-sm" style="outline:none;">
                                                <i class="">
                                                    <input style="outline:none;" name="image" id="image" type="file" style="visibility:hidden;position:absolute;">
                                                </i>
                                            </span>
                                        </div>
                                        <small>For best results, use an image at least 128px by 128px in .jpg format</small>

                                    <!--<div class="mt-2">-->
                                    <!--    <img alt="{{ $tutor->user_name }}" src="{{asset('storage/'.$profile_path.$tutor->image)}}" class="rounded-circle img-responsive mt-2" width="128" height="128">-->
                                    <!--</div>-->

                                </div>
                            </div>
                        </div>
                        
                        <div class="row" style="margin-top:20px;">
                                <div class="col-md-4 col-sm-12">
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <br><br>

                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#privateInfo" aria-expanded="false" aria-controls="privateInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Private info</h5>
                    </div>
                    <div class="card-body collapse" id="privateInfo">
                        
                        <form method="POST" action="{{ route('admins.users.tutors.update_private_info', $tutor->user_id) }}">
                            @csrf
                        <div class="form-row">
                            <div class="col">
                                <label for="inputMaritalStatus">Marital Status</label>
                                <select  class="form-control @error('marital_status') is-invalid @enderror" name="marital_status" id="marital_status" required>
                                    <option selected disabled>Choose...</option>
                                    <?php
                                    foreach ($maritals as $maritial) {
                                        if ($maritial->id == $tutor->marital_status_id) {
                                            echo "<option selected value='$maritial->id'>" . ucfirst($maritial->status) . "</option>";
                                        }else {
                                            echo "<option value='$maritial->id'>" . ucfirst($maritial->status) . "</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col">
                                <label for="inputRace">Race</label>
                                <select  class="form-control @error('race') is-invalid @enderror" name="race" id="race" required>
                                     <option selected disabled>Choose...</option>
                                        @foreach($races as $race)
                                        @if($race->id == $tutor->race)
                                        <option selected value="{{$race->id}}">{{$race->race}}</option>
                                        @else
                                        <option value="{{$race->id}}">{{$race->race}}</option>
                                        @endif
                                        @endforeach
                                </select>
                            </div>

                            <div class="col">
                                <label for="inputReligion">Religion</label>
                                <select  class="form-control @error('religion') is-invalid @enderror" name="religion" id="religion" required>
                                    <option selected disabled>Choose...</option>
                                     @foreach($religions as $religion)
                                        @if($religion->id == $tutor->religion)
                                        <option selected value="{{$religion->id}}">{{$religion->religion}}</option>
                                        @else
                                        <option value="{{$religion->id}}">{{$religion->religion}}</option>
                                        @endif
                                        @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputAddress">Address</label>
                            <input  type="text" class="form-control @error('address_1') is-invalid @enderror" name="address_1" id="address_1" placeholder="1234 Main St" value="<?= $tutor->address_1 ?>">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress2">Address 2</label>
                            <input  type="text" class="form-control @error('address_2') is-invalid @enderror" name="address_2" id="address_2" placeholder="Apartment, studio, or floor" value="<?= $tutor->address_2 ?>">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress3">Address 3</label>
                            <input  type="text" class="form-control @error('address_3') is-invalid @enderror" name="address_3" id="address_3" placeholder="Apartment, studio, or floor" value="<?= $tutor->address_3 ?>">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress4">Address 4</label>
                            <input  type="text" class="form-control @error('address_4') is-invalid @enderror" name="address_4" id="address_4" placeholder="Apartment, studio, or floor" value="<?= $tutor->address_4 ?>">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputCity">City</label>
                                <input  type="text" class="form-control @error('city') is-invalid @enderror" name="city" id="city" value="<?= $tutor->city ?>">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="state">State</label>
                                <select  class="form-control @error('state') is-invalid @enderror" id="state" name="state">
                                    <option selected disabled>Choose...</option>
                                    <?php
                                    foreach ($states as $state) {
                                        if ($state->code == $tutor->state_code) {
                                            echo "<option selected value='$state->code'>$state->state</option>";
                                        } else {
                                                echo "<option value='$state->code'>$state->state</option>";
                                            }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputZip">Zip</label>
                                <input  type="text" class="form-control @error('postcode') is-invalid @enderror" name="postcode" maxlength="5" id="postcode" value="<?= $tutor->postcode ?>">
                            </div>
                        </div>

                        <div class="row" style="margin-top:20px;">
                                <div class="col-md-4 col-sm-12">
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </div>
                        </form>


                    </div>
                </div>

                <br><br>
                
                 <!-- SPM Results -->
                <div class="card" id="spmResultsCard">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#spmResults" aria-expanded="false" aria-controls="spmResults">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0"><?= ($tutor->nationality_id == 109)?'SPM Results':'High School Results' ?></h5>
                    </div>
                    <div class="card-body collapse" id="spmResults">

                        <div class="form-group">
                            <button type="button" name="add" id="add" class="btn btn-success float-right">Add More</button>
                            <br>
                        </div>
                        
                        <div class="form-group">
                            
                             <form name="add_name" id="add_name" method="POST" action="{{route('admins.users.tutors.update_spm_results', $tutor->user_id)}}">
                                @csrf
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dynamic_field">
                                        <tr>
                                            <th>Subject</th>
                                            <th>Score</th>
                                            <th>Action</th>
                                        </tr>

                                        <?php
                                        if (!is_null($spm)) {
                                            $subjects = json_decode($spm->subjects);
                                            $scores = json_decode($spm->scores);
                                            $i = 1;
                                            $lastElement = end($subjects);
                                        }

                                        ?>
                                        @if(!is_null($spm))
                                        @foreach($subjects as $key=>$subject)
                                        <tr id="row{{$i}}">
                                            <td><input type="text" name="subject[]" value="{{$subject}}" class="form-control name_list" /></td>
                                            <td><input type="text" name="score[]" value="{{$scores[$key]}}" class="form-control score_list" /></td>
                                            @if($subject == $lastElement)
                                            <td>
                                                <button type="button" name="remove" id="{{$i}}" class="btn btn-danger btn_remove">X</button>
                                                <input type="hidden" name="count" id="count" value="{{$i}}">
                                            </td>
                                            @else
                                            <td>
                                                <button type="button" name="remove" id="{{$i}}" class="btn btn-danger btn_remove">X</button>
                                            </td>
                                            @endif

                                        </tr>
                                        <?php
                                        $i++;
                                        ?>
                                        @endforeach

                                        @else
                                        <tr>
                                            <td><input type="text" name="subject[]" class="form-control name_list" /></td>
                                            <td><input type="text" name="score[]" maxlength="2" class="form-control score_list" /></td>
                                            <td>
                                                <button type="button" name="remove" id="1" class="btn btn-danger btn_remove" disabled>X</button>
                                                <input type="hidden" name="count" id="count" value="1">
                                            </td>
                                        </tr>
                                        @endif
                                    </table>

                                    <input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit" />
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                
                <br><br>

                <!--SPM Certificate-->
                <div class="card" id="spmQualificationCard">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#spmInfo" aria-expanded="false" aria-controls="spmInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">SPM Certificate</h5>
                    </div>
                    <div class="card-body collapse" id="spmInfo">
                    
                    <form method="POST" action="{{ route('admins.users.tutors.update_spm_certificate', $tutor->user_id) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-row">
                                    <div class="col-md-3 col-sm-12">
                                        <label for="inputUsername">SPM Year</label>
                                        <?php
                                        $spm_year = '';
                                        $spm_image = '';
                                        $spm = json_decode($tutor->spm_results);

                                        if (!is_null($spm)) {
                                            $spm_year = $spm->year;
                                            $spm_image = $spm->image;
                                        }
                                        ?>
                                        <input type="text" name="spm_year" maxlength="4" id="spm_year" class="form-control" value="{{ $spm_year }}">
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-12">
                                        <label for="inputUsername">Certificate File (Image/PDF)</label>
                                        <span class="btn btn-secondary col-sm" style="outline:none;">
                                            <input type="file" style="outline:none;" name="spm_image" id="spm_image" style="visibility:hidden;position:absolute;">
                                        </span>
                                    </div>


                                    <?php

                                    if (!is_null($tutor->spm_results)) { 
                                     $spm_format = explode('.', $spm_image);
                                    ?>
                                        <div class="col-md-4 col-sm-12">
                                            <label for="inputUsername">File</label>
                                            <br>
                                            
                                            <!-- Button trigger modal -->
                                            @if($spm_format[1] == 'pdf')
                                            <a href="{{ route('admins.spm', $tutor->id) }}" target="_blank" class="btn btn-primary ">Show PDF</a>
                                            @else
                                            <!-- Button trigger modal -->
                                            <a href="#" class="btn btn-primary pop">Show Image
                                                <img hidden src="{{asset('storage/'.$spm_path.$spm_image)}}" style="width: 200px; height: 264px;">
                                            </a>
                                            @endif

                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-4 col-sm-12">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                    </form>

                    </div>
                </div>

                <br><br>

                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#highInfo" aria-expanded="false" aria-controls="highInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Highest Education Qualifications</h5>
                    </div>
                    <div class="card-body collapse" id="highInfo">
                        
                        <form method="POST" action="{{ route('admins.users.tutors.update_higher_qualification', $tutor->user_id) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-md-12 col-sm-12">

                                <?php
                                $level = '';
                                $grad_year = '';
                                $cert = '';
                                $university = '';

                                $high = json_decode($tutor->highest_education_results);

                                if (!is_null($high)) {
                                    $level = $high->level;
                                    $grad_year = $high->year;
                                    $cert = $high->image;
                                    $university = $high->university;
                                }

                                ?>

                                <div class="form-row">
                                    <div class="col-md-4 col-sm-12">
                                        <label for="inputUsername">Highest Education</label>
                                        <select  name="education_level" id="education_level" class="form-control">
                                            <option selected disabled>Choose...</option>
                                            <option <?= ($level == 1 ? 'selected' : '') ?> value="1">A-Level/Foundation/Matriculation/STPM</option>
                                            <option <?= ($level == 2 ? 'selected' : '') ?> value="2">Vocational qualification</option>
                                            <option <?= ($level == 3 ? 'selected' : '') ?> value="3">Bachelor's degree</option>
                                            <option <?= ($level == 4 ? 'selected' : '') ?> value="4">Master's degree</option>
                                            <option <?= ($level == 5 ? 'selected' : '') ?> value="5">Doctorate or higher</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <label for="inputUsername">Graduation Year</label>
                                        <input type="text" name="graduation_year"  maxlength="4" id="graduation_year" class="form-control" value="{{ $grad_year }}">
                                    </div>

                                    <div class="col-md-3 col-sm-12">
                                        <label for="inputUsername">University/College/Institute</label>
                                        <input type="text" name="university" id="university" class="form-control" value="{{ $university }}">
                                    </div>
                                    
                                    <div class="col-md-5 col-sm-12">
                                        <label for="inputUsername">Certificate File (Image/PDF)</label>
                                        <span class="btn btn-secondary col-sm" style="outline:none;">
                                            <i class="">
                                                <input style="outline:none;" name="certificate_image" id="certificate_image" type="file" style="visibility:hidden;position:absolute;">
                                            </i>
                                        </span>
                                    </div>

                                    <?php
                                    if (!is_null($tutor->highest_education_results)) { 
                                     $format = explode('.', $cert);
                                    ?>
                                        <div class="col-md-2 col-sm-12">
                                            <label for="inputUsername">Image</label>
                                            <br>
                                        
                                            @if($format[1] == 'pdf')
                                            <a href="{{ route('admins.higher', $tutor->id) }}" target="_blank" class="btn btn-primary">Show PDF</a>
                                            @else
                                            <!-- Button trigger modal -->
                                            <a href="#" class="btn btn-primary pop">Show Image
                                                <img hidden src="{{asset('storage/'.$higher_path.$cert)}}" style="width: 200px; height: 264px;">
                                            </a>
                                            @endif

                                        </div>
                                    <?php
                                    }
                                    ?>

                                </div>



                            </div>
                        </div>

                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-4 col-sm-12">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                        </form>

                    </div>
                </div>

                <br><br>
                <!-- Subjects Can teach -->
                <div class="card" id="subjects">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#subjectsInfo" aria-expanded="false" aria-controls="subjectsInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Preferred subjects or skills</h5>
                    </div>
                    <div class="card-body collapse" id="subjectsInfo">
                        <form method="POST" action="{{route('admins.users.tutors.class_preference.update_subjects', $tutor->user_id)}}">
                            @csrf
                            <div class="form-group">
                                <label>You may list the subjects or skills you can teach. Add comma (,) to continue with the list.</label>
                                <textarea maxlength="500" name="subjects" id="subjects" class="form-control">{{ $t_subjects }}</textarea>
                            </div>
                            <input type="submit" name="submit" id="submit" class="btn btn-success" value="Save" />
                        </form>
                    </div>
                </div>

                <br><br>
                <!-- Teaching Experience -->
                <div class="card" id="experience">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#experienceInfo" aria-expanded="false" aria-controls="experienceInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Teaching Experience</h5>
                    </div>
                    <div class="card-body collapse" id="experienceInfo">

                        <form method="POST" action="{{ route('admins.users.tutors.class_preference.update_experience', $tutor->user_id) }}">
                            @csrf
                            <div class="form-group">
                                <label>What are you experience in teaching ?</label>
                                <textarea maxlength="500" name="experience" id="experience" class="form-control">{{ $experience }}</textarea>
                            </div>
                            <input type="submit" name="submit" id="submit" class="btn btn-success" value="Save" />
                        </form>
                    </div>
                </div>

                <br><br>
                <!-- Areas Can teach -->
                <div class="card" id="areas">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#areasInfo" aria-expanded="false" aria-controls="areasInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Preferred Areas</h5>
                    </div>
                    <div class="card-body collapse" id="areasInfo">
                        <form method="POST" action="{{ route('admins.users.tutors.class_preference.update_areas', $tutor->user_id) }}">
                            @csrf
                            <div class="form-group">
                                <label>Which are the areas you prefer to conduct classes?</label>
                                <textarea maxlength="500" name="areas" id="areas" class="form-control">{{ $areas }}</textarea>
                            </div>
                            <input type="submit" name="submit" id="submit" class="btn btn-success" value="Save" />
                        </form>
                    </div>
                </div>

                <br><br>

                <!-- Account Validation  -->
                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#accountValidation" aria-expanded="false" aria-controls="accountValidation">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Account Validation</h5>
                    </div>
                    <div class="card-body collapse" id="accountValidation">
                        <form method="POST" action="{{ route('admins.users.tutors.validation', $tutor->id) }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-12 col-sm-12">

                                    <div class="form-row">
                                        <div class="col-md-4 col-sm-12">
                                            <label for="inputUsername">Account Status</label>
                                            <select name="status" class="form-control selectpicker  @error('status') is-invalid @enderror">
                                                <option selected disabled>Choose..</option>
                                                <option <?= ($tutor->account_approved == 1 ? 'selected' : '') ?> value="1">Approve</option>
                                                <option <?= ($tutor->account_approved == 2 ? 'selected' : '') ?> value="2">Reject</option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="row" style="margin-top:20px;">
                                        <div class="col-md-4 col-sm-12">
                                            <button type="submit" class="btn btn-success">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>



                    </div>
                </div>

                <br><br>
                <!-- Membership -->
                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#membership" aria-expanded="false" aria-controls="membership">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Membership</h5>
                    </div>
                    <div class="card-body collapse" id="membership">
                        <form method="POST" action="{{ route('admins.users.tutors.membership.update', $tutor->id) }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-row">
                                        <div class="col-md-3 col-sm-12">
                                            <label for="inputUsername">Status</label>
                                            <input type="text" name="m_status" id="m_status" class="form-control" readonly disabled value="{{ ucfirst($membership->status) }}">
                                        </div>

                                        <div class="col-md-3 col-sm-12">
                                            <label for="inputUsername">Expiry Date</label>
                                            <input type="date" name="m_expiry" id="m_expiry" class="form-control" readonly disabled value="{{ ucfirst($membership->expiry_date) }}">
                                        </div>

                                        <div class="col-md-3 col-sm-12">
                                            <label for="inputUsername">Payment Date</label>
                                            <input type="date" name="m_payment" id="m_payment" class="form-control" readonly disabled value="{{ ucfirst($membership->payment_date) }}">
                                        </div>

                                        <div class="col-md-3 col-sm-12">
                                            
                                            <?php
                                            if (!is_null($membership->payment_reference_no)) { ?>
                                                <div class="col-md-3 col-sm-12">
                                                    <label for="inputUsername">Image</label>
                                                    <br>
                                                    <!-- Button trigger modal -->

                                                    <a href="#" class="btn btn-primary pop"> View
                                                        <img hidden src="{{asset('storage/'.$membership_path.$membership->payment_reference_no)}}" style="width: 200px; height: 264px;">
                                                    </a>

                                                </div>
                                            <?php
                                            } else {
                                                echo "<label for='inputUsername'>Payment Reference</label>";
                                                echo "<input class='form-control' readonly disabled value='No record found!'>";
                                            }
                                            ?>
                                        </div>
                                    </div>


                                    <br>
                                    <hr>
                                    <h5>Renewal Details</h5>
                                    <div class="form-row">
                                        <div class="col-md-3 col-sm-12">
                                            <label for="inputUsername">Status</label>
                                            <select class="form-control" name="status" id="status">
                                                <option selected disabled>Choose...</option>
                                                <option value="1">Active</option>
                                                <option value="2">Not Active</option>
                                                <option value="3">Revoke</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-sm-12">
                                            <label for="inputUsername">Start Date</label>
                                            <input type="date" name="start_date" id="start_date" class="form-control">
                                        </div>

                                        <div class="col-md-3 col-sm-12">
                                            <label for="inputUsername">Expiry Date</label>
                                            <input type="date" name="expiry_date" id="expiry_date" readonly class="form-control">
                                        </div>

                                        <div class="col-md-3 col-sm-12">
                                            <label for="inputUsername">Payment Date</label>
                                            <input type="date" name="payment_date" id="payment_date" class="form-control">
                                        </div>


                                    </div>

                                    <div class="form-row">
                                        <div class="col-sm-12 col-md-6">
                                            <label for="inputUsername">Remarks (optional)</label>
                                            <textarea id="remark" name="remark" class="form-control"></textarea>
                                        </div>
                                    </div>

                                    <div class="row " style="margin-top:20px;">
                                        <div class="col-md-4 col-sm-12">
                                            <button type="submit" class="btn btn-success">Activate</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>



                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="row" style="margin-top: 30px;">
    <div class="col-12">
        <a class="btn btn-primary float-left" href="{{route('admins.users.tutors.index')}}">Back</a>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <img src="" class="imagepreview" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $(document).ready(function() {
        // var i = 1;
        var i = document.getElementById("count").value;

        $('#add').click(function() {
            i++;
            $('#dynamic_field').append('<tr id="row' + i + '"><td><input type="text" name="subject[]" class="form-control name_list" /></td><td><input type="text" name="score[]" maxlength="2" class="form-control score_list" /></td><td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
        });

        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            var new_button_id = button_id - 1;

            $('#row' + button_id + '').remove();
        });
    });
    
    $(function() {
        $('.pop').on('click', function() {
            $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#imagemodal').modal('show');
        });
        
        if (document.getElementById('nationality').value != "109") {
            $('#spmQualificationCard').hide();
            // $('#spmResultsCard').hide();
        }
    });


    $(document).on("click", ".browse", function() {
        var file = $(this).parents().find(".file");
        file.trigger("click");
    });

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            document.getElementById("preview").src = e.target.result;
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    $(document).ready(function() {
        // console.log(document.getElementById('nationality').value);


        //membership - start
        $("#start_date").on('change', function() {
            var value = $("#start_date").val();

            var d = new Date(value);
            var n = d.getDate();
            var d = new Date(value);
            d.setDate(n + 365);

            // console.log(d);

            var new_date = '0' + d.getDate();
            var new_month = d.getMonth() + 1;
            // console.log(new_month);
            var new_year = d.getFullYear();

            $("#expiry_date").val(new_year + '-' + new_month + '-' + new_date.slice(-2)); //this is how u set the value
        });
        //membership - end

        if (document.getElementById('nationality').value == "109") {
            $('#mykadDiv').show();
            $('#mykad').attr('required', '');
            $('#mykadDiv').attr('data-error', 'This field is required.');
            $('#passportDiv').hide();
            $('#passport').removeAttr('required');
            $('#passportDiv').removeAttr('data-error');
        } else {
            $('#passportDiv').show();
            $('#passport').attr('required', '');
            $('#passportDiv').attr('data-error', 'This field is required.');
            $('#mykadDiv').hide();
            $('#mykad').removeAttr('required');
            $('#mykadDiv').removeAttr('data-error');
        }

        $("#nationality").trigger("change");
        $("#nationality").change(function() {
            if ($(this).val() == "109") {
                $('#mykadDiv').show();
                $('#mykad').attr('required', '');
                $('#mykadDiv').attr('data-error', 'This field is required.');
                $('#passportDiv').hide();
                $('#passport').removeAttr('required');
                $('#passportDiv').removeAttr('data-error');
            } else {
                $('#passportDiv').show();
                $('#passport').attr('required', '');
                $('#passportDiv').attr('data-error', 'This field is required.');
                $('#mykadDiv').hide();
                $('#mykad').removeAttr('required');
                $('#mykadDiv').removeAttr('data-error');
            }
        });
        // $("#nationality").trigger("change");


    });
</script>

@endsection