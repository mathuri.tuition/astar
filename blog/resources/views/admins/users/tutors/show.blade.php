@extends('layouts.app_login')
<?php
$page = 'tutor-profile';
$title = 'Profile';

$profile_path = config('app.image.tutor.profile');
$spm_path = config('app.file.tutor.spm');
$higher_path = config('app.file.tutor.higher');
$membership_path = config('app.image.tutor.membership');

if(is_null($preference))
{
    $t_subjects = '';
    $areas = '';
    $experience = '';
}else{
    $t_subjects = $preference->subjects;
    $areas = $preference->areas;
    $experience = $preference->experience;
}

?>
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Whoops!</strong> There were some problems with your input.<br><br>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="row">
    <div class="col-12">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="account" role="tabpanel">

                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#publicInfo" aria-expanded="false" aria-controls="publicInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Basic info</h5>
                    </div>
                    <div class="card-body collapse" id="publicInfo">

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-row">
                                    <div class="col-md-12 col-sm-12">
                                        <label for="inputUsername">Full Name</label>
                                        <input readonly type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{$tutor->user_name}}">
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="inputUsername">E-Mail</label>
                                        <input readonly type="text" class="form-control" disabled value="{{$tutor->user_email}}">
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label for="inputUsername">Phone Number</label>
                                        <input readonly type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="phone" value="{{$tutor->phone_no}}">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="inputEmail4">Nationality</label>
                                        <select readonly class="form-control @error('nationality') is-invalid @enderror" id="nationality" name="nationality">
                                            <?php
                                            foreach ($nationalities as $nationality) {
                                                $n = $nationality->id;
                                                if ($n == $tutor->nationality_id) {
                                                    echo "<option selected value='$n'>$nationality->nationality</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class='col-md-6 col-sm-12' id='mykadDiv' style="display:none;">
                                        <label for='mykad'>Mykad No.</label>
                                        <input readonly type='text' class="form-control @error('mykad') is-invalid @enderror" name='mykad' id='mykad' value='<?= $tutor->mykad_no; ?>'>
                                    </div>

                                    <div class='col-md-6 col-sm-12' id='passportDiv' style="display:none;">
                                        <label for='passport'>Passport No.</label>
                                        <input readonly type='text' class="form-control @error('passport') is-invalid @enderror" name='passport' id='passport' value='<?= $tutor->passport_no; ?>'>
                                    </div>
                                </div>

                                <div class="row" style="margin-top:10px;">
                                    <div class="col-md-6 col-sm-12">
                                        <label for='gender'>Gender &nbsp </label>
                                        <input readonly type="radio" class="@error('gender') is-invalid @enderror" name="gender" value="1" <?php echo ($tutor->gender_id  == '1') ? 'checked' : '' ?>> Male
                                        <input readonly type="radio" name="gender" value="0" <?php echo ($tutor->gender_id  == '0') ? 'checked' : '' ?>> Female
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-4">
                                <div class="text-center">

                                    <div class="mt-2">
                                        <img alt="{{ $tutor->user_name }}" src="{{asset('storage/'.$profile_path.$tutor->image)}}" class="rounded-circle img-responsive mt-2" width="128" height="128">
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <br><br>

                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#privateInfo" aria-expanded="false" aria-controls="privateInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Private info</h5>
                    </div>
                    <div class="card-body collapse" id="privateInfo">

                        <div class="form-row">
                            <div class="col">
                                <label for="inputMaritalStatus">Marital Status</label>
                                <select readonly class="form-control @error('marital_status') is-invalid @enderror" name="marital_status" id="marital_status" required>
                                    <option selected disabled>Choose...</option>
                                    <?php
                                    foreach ($maritals as $maritial) {
                                        if ($maritial->id == $tutor->marital_status_id) {
                                            echo "<option selected value='$maritial->id'>" . ucfirst($maritial->status) . "</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col">
                                <label for="inputRace">Race</label>
                                <select disabled class="form-control">
                                    @foreach($races as $race)
                                    @if($race->id == $tutor->race)
                                    <option>{{$race->race}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="col">
                                <label for="inputReligion">Religion</label>
                                <select disabled class="form-control">
                                    @foreach($religions as $religion)
                                    @if($religion->id == $tutor->religion)
                                    <option>{{$religion->religion}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputAddress">Address</label>
                            <input readonly type="text" class="form-control @error('address_1') is-invalid @enderror" name="address_1" id="address_1" placeholder="1234 Main St" value="<?= $tutor->address_1 ?>">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress2">Address 2</label>
                            <input readonly type="text" class="form-control @error('address_2') is-invalid @enderror" name="address_2" id="address_2" placeholder="Apartment, studio, or floor" value="<?= $tutor->address_2 ?>">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress3">Address 3</label>
                            <input readonly type="text" class="form-control @error('address_3') is-invalid @enderror" name="address_3" id="address_3" placeholder="Apartment, studio, or floor" value="<?= $tutor->address_3 ?>">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress4">Address 4</label>
                            <input readonly type="text" class="form-control @error('address_4') is-invalid @enderror" name="address_4" id="address_4" placeholder="Apartment, studio, or floor" value="<?= $tutor->address_4 ?>">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputCity">City</label>
                                <input readonly type="text" class="form-control @error('city') is-invalid @enderror" name="city" id="city" value="<?= $tutor->city ?>">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="state">State</label>
                                <select readonly class="form-control @error('state') is-invalid @enderror" id="state" name="state">
                                    <option selected disabled>Choose...</option>
                                    <?php
                                    foreach ($states as $state) {
                                        if ($state->code == $tutor->state_code) {
                                            echo "<option selected value='$state->code'>$state->state</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputZip">Zip</label>
                                <input readonly type="text" class="form-control @error('postcode') is-invalid @enderror" name="postcode" maxlength="5" id="postcode" value="<?= $tutor->postcode ?>">
                            </div>
                        </div>




                    </div>
                </div>

                <br><br>
                
                <!-- SPM Results -->
                <div class="card" id="spmResultsCard">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#spmResults" aria-expanded="false" aria-controls="spmResults">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0"><?= ($tutor->nationality_id == 109)?'SPM Results':'High School Results' ?></h5>
                    </div>
                    <div class="card-body collapse" id="spmResults">

                        <div class="form-group">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dynamic_field">
                                    <tr>
                                        <th>Subject</th>
                                        <th>Grade</th>

                                    </tr>

                                    <?php
                                    if (!is_null($spm)) {
                                        $subjects = json_decode($spm->subjects);
                                        $scores = json_decode($spm->scores);
                                        $i = 1;
                                        $lastElement = end($subjects);
                                    }

                                    ?>
                                    @if(!is_null($spm))
                                    @foreach($subjects as $key=>$subject)
                                    <tr id="row{{$i}}">
                                        <td>{{$subject}}</td>
                                        <td>{{$scores[$key]}}</td>
                                    </tr>
                                    <?php
                                    $i++;
                                    ?>
                                    @endforeach

                                    @else
                                    <tr>
                                        <td><input type="text" name="subject[]" class="form-control name_list" /></td>
                                        <td><input type="text" name="score[]" maxlength="2" class="form-control score_list" /></td>
                                       
                                    </tr>
                                    @endif
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

                <br><br>
                <!--SPM Certificate-->
                <div class="card" id="spmQualificationCard">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#spmInfo" aria-expanded="false" aria-controls="spmInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">SPM Certificate</h5>
                    </div>
                    <div class="card-body collapse" id="spmInfo">

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-row">
                                    <div class="col-md-3 col-sm-12">
                                        <label for="inputUsername">SPM Year</label>
                                        <?php
                                        $spm_year = '';
                                        $spm_image = '';
                                        $spm = json_decode($tutor->spm_results);

                                        if (!is_null($spm)) {
                                            $spm_year = $spm->year;
                                            $spm_image = $spm->image;
                                        }
                                        ?>
                                        <input type="text" readonly name="spm_year" maxlength="4" id="spm_year" class="form-control" value="{{ $spm_year }}">
                                    </div>


                                    <?php

                                    if (!is_null($tutor->spm_results)) { 
                                    $spm_format = explode('.', $spm_image);
                                    ?>
                                        <div class="col-md-4 col-sm-12">
                                            <label for="inputUsername">File</label>
                                            <br>
                                            
                                            <!-- Button trigger modal -->
                                                
                                            @if($spm_format[1] == 'pdf')
                                            <a href="{{ route('admins.spm', $tutor->id) }}" target="_blank" class="btn btn-primary ">Show PDF</a>
                                            @else
                                            <!-- Button trigger modal -->
                                            <a href="#" class="btn btn-primary pop">Show Image
                                                <img hidden src="{{asset('storage/'.$spm_path.$spm_image)}}" style="width: 200px; height: 264px;">
                                            </a>
                                            @endif
                                            
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>



                            </div>
                        </div>


                    </div>
                </div>

                <br><br>

                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#highInfo" aria-expanded="false" aria-controls="highInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Highest Education Qualifications</h5>
                    </div>
                    <div class="card-body collapse" id="highInfo">

                        <div class="row">
                            <div class="col-md-12 col-sm-12">

                                <?php
                                $level = '';
                                $grad_year = '';
                                $cert = '';
                                $university = '';

                                $high = json_decode($tutor->highest_education_results);

                                if (!is_null($high)) {
                                    $level = $high->level;
                                    $grad_year = $high->year;
                                    $cert = $high->image;
                                    $university = $high->university;
                                }
                                ?>

                                <div class="form-row">
                                    <div class="col-md-4 col-sm-12">
                                        <label for="inputUsername">Highest Education</label>
                                        <select readonly name="education_level" id="education_level" class="form-control">
                                            <option selected disabled>Choose...</option>
                                            <option <?= ($level == 1 ? 'selected' : '') ?> value="1">A-Level/Foundation/Matriculation/STPM</option>
                                            <option <?= ($level == 2 ? 'selected' : '') ?> value="2">Vocational qualification</option>
                                            <option <?= ($level == 3 ? 'selected' : '') ?> value="3">Bachelor's degree</option>
                                            <option <?= ($level == 4 ? 'selected' : '') ?> value="4">Master's degree</option>
                                            <option <?= ($level == 5 ? 'selected' : '') ?> value="5">Doctorate or higher</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <label for="inputUsername">Graduation Year</label>
                                        <input type="text" name="graduation_year" readonly maxlength="4" id="graduation_year" class="form-control" value="{{ $grad_year }}">
                                    </div>

                                    <div class="col-md-3 col-sm-12">
                                        <label for="inputUsername">University/College/Institute</label>
                                        <input type="text" name="university" readonly maxlength="4" id="university" class="form-control" value="{{ $university }}">
                                    </div>

                                    <?php

                                    if (!is_null($tutor->highest_education_results)) { 
                                    $format = explode('.', $cert);
                                    ?>
                                        <div class="col-md-2 col-sm-12">
                                            <label for="inputUsername">File</label>
                                            <br>
                                            
                                            @if($format[1] == 'pdf')
                                            <a href="{{ route('admins.higher', $tutor->id) }}" target="_blank" class="btn btn-primary">Show PDF</a>
                                            @else
                                            <!-- Button trigger modal -->
                                            <a href="#" class="btn btn-primary pop">Show Image
                                                <img hidden src="{{asset('storage/'.$higher_path.$cert)}}" style="width: 200px; height: 264px;">
                                            </a>
                                            @endif
                                           
                                        </div>
                                    <?php
                                    }
                                    ?>

                                </div>



                            </div>
                        </div>



                    </div>
                </div>

                <br><br>
                <!-- Subjects Can teach -->
                <div class="card" id="subjects">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#subjectsInfo" aria-expanded="false" aria-controls="subjectsInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Preferred subjects or skills</h5>
                    </div>
                    <div class="card-body collapse" id="subjectsInfo">
                            <div class="form-group">
                                <label>You may list the subjects or skills you can teach. Add comma (,) to continue with the list.</label>
                                <textarea readonly maxlength="500" name="subjects" id="subjects" class="form-control">{{ $t_subjects }}</textarea>
                            </div>
                    </div>
                </div>

                <br> <br>
                <!-- Teaching Experience -->
                <div class="card" id="experience">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#experienceInfo" aria-expanded="false" aria-controls="experienceInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Teaching Experience</h5>
                    </div>
                    <div class="card-body collapse" id="experienceInfo">
                            <div class="form-group">
                                <label>What are you experience in teaching ?</label>
                                <textarea readonly maxlength="500" name="experience" id="experience" class="form-control">{{ $experience }}</textarea>
                            </div>
                    </div>
                </div>

                <br> <br>
                <!-- Areas Can teach -->
                <div class="card" id="areas">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#areasInfo" aria-expanded="false" aria-controls="areasInfo">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Preferred Areas</h5>
                    </div>
                    <div class="card-body collapse" id="areasInfo">
                       
                            <div class="form-group">
                                <label>Which are the areas you prefer to conduct classes?</label>
                                <textarea readonly maxlength="500" name="areas" id="areas" class="form-control">{{ $areas }}</textarea>
                            </div>
                          
                    </div>
                </div>

                <br> <br>

                <!-- Membership -->
                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-sm btn-grey" type="button" data-toggle="collapse" data-target="#membership" aria-expanded="false" aria-controls="membership">
                                <span class="fa fa-caret-down"></span>
                            </button>
                        </div>
                        <h5 class="card-title mb-0">Membership</h5>
                    </div>
                    <div class="card-body collapse" id="membership">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-row">
                                    <div class="col-md-3 col-sm-12">
                                        <label for="inputUsername">Status</label>
                                        <input type="text" name="m_status" id="m_status" class="form-control" readonly disabled value="{{ ucfirst($membership->status) }}">
                                    </div>

                                    <div class="col-md-3 col-sm-12">
                                        <label for="inputUsername">Expiry Date</label>
                                        <input type="date" name="m_expiry" id="m_expiry" class="form-control" readonly disabled value="{{ $membership->expiry_date }}">
                                    </div>

                                    <div class="col-md-3 col-sm-12">
                                        <label for="inputUsername">Payment Date</label>
                                        <input type="date" name="m_payment" id="m_payment" class="form-control" readonly disabled value="{{ $membership->payment_date }}">
                                    </div>

                                    <div class="col-md-3 col-sm-12">

                                        <?php
                                        if (!is_null($membership->payment_reference_no)) { ?>
                                            <div class="col-md-3 col-sm-12">
                                                <label for="inputUsername">Image</label>
                                                <br>
                                                <!-- Button trigger modal -->

                                                <a href="#" class="btn btn-primary pop"> View
                                                    <img hidden src="{{asset('storage/'.$membership_path.$membership->payment_reference_no)}}" style="width: 200px; height: 264px;">
                                                </a>

                                            </div>
                                        <?php
                                        } else {
                                            echo "<label for='inputUsername'>Payment Reference</label>";
                                            echo "<input class='form-control' readonly disabled value='No record found!'>";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-top: 30px;">
    <div class="col-12">
        <a class="btn btn-primary float-left" href="{{route('admins.users.tutors.index')}}">Back</a>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <img src="" class="imagepreview" style="width: 100%;">
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function() {
        $('.pop').on('click', function() {
            $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#imagemodal').modal('show');
        });
        
         if (document.getElementById('nationality').value != "109") {
            $('#spmQualificationCard').hide();
            // $('#spmResultsCard').hide();
        }
    });

    $(document).on("click", ".browse", function() {
        var file = $(this).parents().find(".file");
        file.trigger("click");
    });

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            document.getElementById("preview").src = e.target.result;
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    $(document).ready(function() {
        // console.log(document.getElementById('nationality').value);

        if (document.getElementById('nationality').value == "109") {
            $('#mykadDiv').show();
            $('#mykad').attr('required', '');
            $('#mykadDiv').attr('data-error', 'This field is required.');
            $('#passportDiv').hide();
            $('#passport').removeAttr('required');
            $('#passportDiv').removeAttr('data-error');
        } else {
            $('#passportDiv').show();
            $('#passport').attr('required', '');
            $('#passportDiv').attr('data-error', 'This field is required.');
            $('#mykadDiv').hide();
            $('#mykad').removeAttr('required');
            $('#mykadDiv').removeAttr('data-error');
        }

        $("#nationality").trigger("change");
        $("#nationality").change(function() {
            if ($(this).val() == "109") {
                $('#mykadDiv').show();
                $('#mykad').attr('required', '');
                $('#mykadDiv').attr('data-error', 'This field is required.');
                $('#passportDiv').hide();
                $('#passport').removeAttr('required');
                $('#passportDiv').removeAttr('data-error');
            } else {
                $('#passportDiv').show();
                $('#passport').attr('required', '');
                $('#passportDiv').attr('data-error', 'This field is required.');
                $('#mykadDiv').hide();
                $('#mykad').removeAttr('required');
                $('#mykadDiv').removeAttr('data-error');
            }
        });
        // $("#nationality").trigger("change");


    });
</script>

@endsection