@extends('layouts.app_login')
<?php
$page = 'users-index';
$title = 'Tutor Management';
?>

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
  <p>{{ $message }}</p>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="table-responsive">
  <table class="table table-bordered">
    <tr>
      <th>No</th>
      <th>Name</th>
      <th>Email</th>
      <th>Membership Status</th>
      <th>Account Status</th>
      <th width="280px">Action</th>
    </tr>
    @foreach ($data as $key => $user)
    <tr>
      <td>{{ ++$i }}</td>
      <td>{{ $user->name }}</td>
      <td>{{ $user->email }}</td>
      <td><?= $user->status == 'active' ?  "<label class='badge badge-success'>Active</label>" : "" ?></td>
      <td><?= $user->account_status == 1 ?  "<label class='badge badge-success'>Approved</label>" : "" ?></td>
      <td>
        <a class="btn btn-info" href="{{ route('admins.users.tutors.show',$user->id) }}">Show</a>
        <a class="btn btn-primary" href="{{ route('admins.users.tutors.edit',$user->id) }}">Edit</a>
        {!! Form::open(['method' => 'DELETE','route' => ['admins.users.tutors.delete', $user->id],'style'=>'display:inline']) !!}
        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
      </td>
    </tr>
    @endforeach
  </table>
</div>

{!! $data->render() !!}

@endsection