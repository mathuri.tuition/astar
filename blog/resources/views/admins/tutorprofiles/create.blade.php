@extends('layouts.app_login')
<?php
$page = 'tutorprofiles-create';
$title = 'Create Tutor Profile';
?>

@section('content')


@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row justify-content-center">
    <div class="col-sm-12 col-md-12">
        <div class="card">
            <div class="card-body">

               
                <form action="{{ route('admins.tutorprofiles.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row ">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="fullName">{{ __('Full Name') }}</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus />
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email">{{ __('Email') }}</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus />
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="nationality">{{ __('Nationality') }}</label>
                                <select  class="form-control @error('nationality') is-invalid @enderror" name="nationality" id="nationality">
                                    <option class="hidden" selected disabled>Select...</option>
                                    <?php
                                    foreach ($nationalities as $k) {
                                        echo  "<option value='$k->id'>$k->nationality</option>";
                                    }
                                    ?>
                                </select>

                                @error('nationality')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>


                            <div class="form-group">
                                <label for="gender">{{ __('Gender') }}</label>
                                <div class="maxl">
                                    <label class="radio inline">
                                        <input type="radio" name="gender" value="male" checked>
                                        <span> Male </span>
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="gender" value="female">
                                        <span>Female </span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="phoneNo">{{ __('Phone No') }}</label>
                                <input type="text" minlength="10" maxlength="12" name="phone_no" placeholder="60133154730" id="phone_no" class="form-control @error('phone_no') is-invalid @enderror" value="{{ old('phone_no') }}" />
                                @error('phone_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="phoneNo">{{ __('Race') }}</label>
                                <select class="form-control @error('race') is-invalid @enderror" name="race" id="race" required>
                                    <option selected disabled>Choose...</option>
                                    @foreach($races as $race)
                                    <option value="{{$race->id}}">{{$race->race}}</option>
                                    @endforeach
                                </select>
                                @error('race')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="religion">{{ __('Religion') }}</label>
                                <select class="form-control @error('religion') is-invalid @enderror" name="religion" id="religion" required>
                                    <option selected disabled>Choose...</option>
                                    @foreach($religions as $religion)
                                    <option value="{{$religion->id}}">{{$religion->religion}}</option>
                                    @endforeach
                                </select>
                                @error('religion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="marital_status">{{ __('Marital Status') }}</label>
                                <select class="form-control @error('marital_status') is-invalid @enderror" name="marital_status" id="marital_status" required>
                                    <option selected disabled>Choose...</option>
                                    @foreach($maritals as $marital)
                                    <option value="{{$marital->id}}">{{ucfirst($marital->status)}}</option>
                                    @endforeach
                                </select>
                                @error('marital_status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <div class="text-center">
                                    <div class="mt-2">
                                        <img id="preview" alt="" src="" class="rounded-circle img-responsive mt-2" width="128" height="128">
                                        <span class="btn btn-secondary col-sm" style="outline:none;">
                                            <i class="">
                                                <input style="outline:none;" name="image" id="image" type="file" style="visibility:hidden;position:absolute;">
                                            </i>
                                        </span>
                                    </div>
                                    <small>For best results, use an image at least 128px by 128px in .jpg format</small>
                                </div>
                            </div>



                        </div>

                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="motto">{{ __('Motto') }}</label>
                                <input name="motto" id="motto" class="form-control @error('motto') is-invalid @enderror" value="{{ old('motto') }}">
                                @error('motto')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="education_1">{{ __('Higher Education 1') }}</label>
                                <input name="education_1" id="education_1" class="form-control @error('education_1') is-invalid @enderror" value="{{ old('education_1') }}">
                                @error('education_1')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="education_2">{{ __('Higher Education 2') }}</label>
                                <input name="education_2" id="education_2" class="form-control @error('education_2') is-invalid @enderror" value="{{ old('education_2') }}">
                                @error('education_2')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="education_3">{{ __('Higher Education 3') }}</label>
                                <input name="education_3" id="education_3" class="form-control @error('education_3') is-invalid @enderror" value="{{ old('education_3') }}">
                                @error('education_3')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>



                            <div class="form-group">
                                <label for="summary">{{ __('Summary') }}</label>
                                <textarea name="summary" id="summary" class="form-control @error('summary') is-invalid @enderror" value="{{ old('summary') }}"></textarea>
                                @error('summary')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="subjects">{{ __('Subjects') }}</label>
                                <textarea name="subjects" id="subjects" class="form-control @error('subjects') is-invalid @enderror" value="{{ old('subjects') }}"></textarea>
                                @error('subjects')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="experience">{{ __('Teaching Experience') }}</label>
                                <textarea name="experience" id="experience" class="form-control  @error('experience') is-invalid @enderror" value="{{ old('experience') }}"></textarea>
                                @error('experience')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>



                            <div class="form-group float-right">
                                <input type="submit" class="btn btn-success btnRegister " value="Create" />
                            </div>

                        </div>
                    </div>

                    {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on("click", ".browse", function() {
        var file = $(this).parents().find(".file");
        file.trigger("click");
    });

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            document.getElementById("preview").src = e.target.result;
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    $(document).ready(function() {
        $("#nationality").trigger("change");
        $("#nationality").change(function() {
            if ($(this).val() == "109") {
                $('#mykadDiv').show();
                $('#mykad').attr('required', '');
                $('#mykadDiv').attr('data-error', 'This field is required.');
                $('#passportDiv').hide();
                $('#passport').removeAttr('required');
                $('#passportDiv').removeAttr('data-error');
            } else {
                $('#passportDiv').show();
                $('#passport').attr('required', '');
                $('#passportDiv').attr('data-error', 'This field is required.');
                $('#mykadDiv').hide();
                $('#mykad').removeAttr('required');
                $('#mykadDiv').removeAttr('data-error');
            }
        });
    });
</script>

@endsection