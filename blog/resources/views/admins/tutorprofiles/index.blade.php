@extends('layouts.app_login')
<?php
$page = 'tutorprofiles-index';
$title = 'Tutor Profile Management';
?>

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
  <p>{{ $message }}</p>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="table-responsive">
  <table class="table table-bordered">
    <tr>
      <th>No</th>
      <th>Name</th>
      <th>Email</th>
      <th>Phone No.</th>
      <th width="280px">Action</th>
    </tr>
    @foreach ($datas as $key => $user)
    <tr>
      <td>{{ ++$i }}</td>
      <td>{{ $user->name }}</td>
      <td>{{ $user->email }}</td>
      <td>{{ $user->phone_no }}</td>
      <td>
        <a class="btn btn-info" href="{{ route('admins.tutorprofiles.show',$user->id) }}">Show</a>
        <a class="btn btn-primary" href="{{ route('admins.tutorprofiles.edit',$user->id) }}">Edit</a>
        {!! Form::open(['method' => 'DELETE','route' => ['admins.tutorprofiles.delete', $user->id],'style'=>'display:inline']) !!}
        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
      </td>
    </tr>
    @endforeach
  </table>
</div>

{!! $datas->render() !!}

@endsection