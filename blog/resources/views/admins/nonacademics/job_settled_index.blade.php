@extends('layouts.app_login')
<?php
$page = 'jobs-active_status_index';
$title = 'Settled Job';
?>

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="table-responsive">

    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Job Reference No.</th>
            <th>Subject</th>
            <th>Level</th>
            <th>Job Status</th>
            <th>Request Status</th>
            <th>Action</th>
        </tr>

        @foreach ($datas as $data)
        <tr>

            <td>{{ ++$i }}</td>
            <td>{{ $data->reference_no }}</td>
            <td>{{$data->class_name}}</td>
            <td><?php

                if ($data->level == 1) {
                    echo 'Beginner';
                }
                if ($data->level == 2) {
                    echo 'Intermediate';
                }
                if ($data->level == 3) {
                    echo 'Advanced';
                }
                ?></td>

            <td>
                @if($data->status == 1)
                <label class="badge badge-success">Approved</label>
                @elseif($data->status == 2)
                <label class="badge badge-warning">Cancelled</label>
                @endif
            </td>
            <td>
                @if($data->request_status == 1)
                <label class="badge badge-success">Approved</label>
                @elseif($data->request_status == 2)
                <label class="badge badge-danger">Rejected</label>
                @elseif($data->request_status == 3)
                <label class="badge badge-warning">Pending</label>
                @endif
            </td>

            <td>
                <a class="btn btn-info" href="{{ route('admins.nonacademics.job_settled_show', $data->id) }}">Show</a>
                <!-- <a class="btn btn-primary" href="{{ route('admins.academics.job_assigned_edit', $data->id) }}">Edit</a> -->
            </td>
        </tr>
        @endforeach
    </table>
</div>
    {!! $datas->render() !!}
    @endsection