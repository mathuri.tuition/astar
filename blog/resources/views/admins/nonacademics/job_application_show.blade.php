@extends('layouts.app_login')
<?php
$page = 'classes-academics_show';
$title = $sac->reference_no;

$student_profile_path = config('app.image.student.profile');

$mon = '';
$tue = '';
$wed = '';
$thu = '';
$fri = '';
$sat = '';
$sun = '';

$days = json_decode($sac->days);

foreach ($days as $day) {

    if ($day == "mon") {
        $mon = $day;
    }
    if ($day == "tue") {
        $tue = $day;
    }
    if ($day == "wed") {
        $wed = $day;
    }
    if ($day == "thu") {
        $thu = $day;
    }
    if ($day == "fri") {
        $fri = $day;
    }
    if ($day == "sat") {
        $sat = $day;
    }
    if ($day == "sun") {
        $sun = $day;
    }
}
?>
@section('content')

<style>
    .fa {
        padding-right: 10px;
    }

    /* .row { */
    /* padding-bottom: 20px; */
    /* } */

    .col-sm-12,
    .col-md-6 {
        margin-bottom: 25px;
    }

    .td {
        width: 32%;
    }

    .mark_space {
        width: 5%;
    }

    .weekDays-selector input {
        display: none !important;
    }

    .weekDays-selector input[type=checkbox]+label {
        display: inline-block;
        border-radius: 6px;
        background: #dddddd;
        height: 40px;
        width: 45px;
        margin-right: 3px;
        line-height: 40px;
        text-align: center;
        cursor: pointer;
    }

    .weekDays-selector input[type=checkbox]:checked+label {
        background: #2AD705;
        color: #ffffff;
    }
</style>

<!-- Student information -->
<div class="row">
    <div class="col-sm-12 col-md-12 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-user-graduate"> </span> Student Information :
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6 col-md-8">
                        <div class="form-row">

                            <label for="inputUsername">Full Name</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" readonly name="name" id="name" value="{{$sac->user_name}}">


                        </div>
                        <div class="form-row">
                            <div class="col-md-6 col-sm-12">
                                <label for="inputUsername">E-Mail</label>
                                <input type="text" class="form-control" readonly value="{{$sac->user_email}}">
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label for="inputUsername">Phone Number</label>
                                <input type="text" class="form-control" readonly name="phone" id="phone" value="{{$sac->phone_no}}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 col-sm-12">
                                <label for="inputEmail4">Nationality</label>
                                <select class="form-control" readonly disabled id="nationality" name="nationality">
                                    <?php
                                    foreach ($nationalities as $nationality) {
                                        $n = $nationality->id;
                                        if ($n == $sac->nationality_id) {
                                            echo "<option selected value='$n'>$nationality->nationality</option>";
                                        } else {
                                            echo "<option value='$n'>$nationality->nationality</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class='col-md-6 col-sm-12' id='mykadDiv' style="display:none;">
                                <label for='mykad'>Mykad No.</label>
                                <input type='text' class="form-control" readonly name='mykad' id='mykad' value='<?= $sac->mykad_no; ?>'>
                            </div>

                            <div class='col-md-6 col-sm-12' id='passportDiv' style="display:none;">
                                <label for='passport'>Passport No.</label>
                                <input type='text' class="form-control" readonly name='passport' id='passport' value='<?= $sac->passport_no; ?>'>
                            </div>
                        </div>

                        <div class="row" style="margin-top:10px;">
                            <div class="col-sm-12 col-md-12">
                                <label for='gender'>Gender :&nbsp </label>
                                <input type="checkbox" readonly disabled class="radio" name="gender" value="1" <?= ($sac->gender_id  == '1') ? 'checked' : '' ?>> Male
                                <input type="checkbox" readonly disabled class="radio" name="gender" value="0" <?= ($sac->gender_id  == '0') ? 'checked' : '' ?>> Female
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="text-center">


                            <div class="mt-2">
                                <img id="preview" alt="{{ $sac->user_name }}" src="{{asset('storage/'.$student_profile_path.$sac->image)}}" class="rounded-circle img-responsive mt-2" width="128" height="128">

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Job Location -->
<div class="row">
    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-map-marked"> </span> Tuition Job Location :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">State</td>
                        <td class="mark_space">:</td>
                        <td>
                            @foreach($states as $state)
                            @if($state->code == $sac->state )
                            {{ $state->state }}
                            @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td class="td">City</td>
                        <td class="mark_space">:</td>
                        <td>{{ $sac->city }}</td>
                    </tr>
                    <tr>
                        <td class="td">Postcode</td>
                        <td class="mark_space">:</td>
                        <td>{{ $sac->postcode }}</td>
                    </tr>
                    <tr>
                        <td class="td">Area</td>
                        <td class="mark_space">:</td>
                        <td>{{ $sac->area }}

                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>

    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-chalkboard-teacher"> </span> Tuition Job Description :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Class</td>
                        <td class="mark_space">:</td>
                        <td>
                            {{$sac->class_name}}

                        </td>
                    </tr>

                    

                    <tr>
                        <td class="td">Remarks</td>
                        <td class="mark_space">:</td>
                        <td>
                            <textarea class="form-control" id="remarks" readonly disabled name="remarks">{{ $sac->remarks }}</textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>

<!-- Job Fees -->
<div class="row">
    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-comment-dollar"> </span> Tuition Fees :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Fees per hour</td>
                        <td class="mark_space">:</td>
                        <td>
                            RM {{$sac->price}}
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-user-friends"> </span> Tuition Type :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Type</td>
                        <td class="mark_space">:</td>
                        <td>
                            <select class="form-control " disabled readonly>
                                <option <?= $sac->type == 1 ? 'selected' : '' ?> value="1">Personal</option>
                                <option <?= $sac->type == 2 ? 'selected' : '' ?> value="2">Group</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="td">No. of students</td>
                        <td class="mark_space">:</td>
                        <td>{{ $sac->pax }}

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Timing -->
<div class="row">
    <div class="col-sm-12 col-md-12 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-clock"> </span> Tuition Preferred Timing :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Frequency</td>
                        <td class="mark_space">:</td>
                        <td>
                            <select readonly disabled class="form-control ">
                                <option disabled selected>Choose...</option>
                                <option <?= $sac->frequency == 1 ? 'selected' : '' ?> value="1">1 class per week</option>
                                <option <?= $sac->frequency == 2 ? 'selected' : '' ?> value="2">2 class per week</option>
                                <option <?= $sac->frequency == 3 ? 'selected' : '' ?> value="3">3 class per week</option>
                                <option <?= $sac->frequency == 4 ? 'selected' : '' ?> value="4">4 class per week</option>
                                <option <?= $sac->frequency == 5 ? 'selected' : '' ?> value="5">5 class per week</option>
                                <option <?= $sac->frequency == 6 ? 'selected' : '' ?> value="6">6 class per week</option>
                                <option <?= $sac->frequency == 7 ? 'selected' : '' ?> value="7">7 class per week</option>
                                <option <?= $sac->frequency == 8 ? 'selected' : '' ?> value="8">More than 7 class</option>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Days</td>
                        <td class="mark_space">:</td>
                        <td>
                            <div class="weekDays-selector">
                                <input type="checkbox" id="weekday-mon" name="day[]" value="mon" <?= $mon == 'mon' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-mon">Mon</label>
                                <input type="checkbox" id="weekday-tue" name="day[]" value="tue" <?= $tue == 'tue' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-tue">Tue</label>
                                <input type="checkbox" id="weekday-wed" name="day[]" value="wed" <?= $wed == 'wed' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-wed">Wed</label>
                                <input type="checkbox" id="weekday-thu" name="day[]" value="thu" <?= $thu == 'thu' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-thu">Thu</label>
                                <input type="checkbox" id="weekday-fri" name="day[]" value="fri" <?= $fri == 'fri' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-fri">Fri</label>
                                <input type="checkbox" id="weekday-sat" name="day[]" value="sat" <?= $sat == 'sat' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-sat">Sat</label>
                                <input type="checkbox" id="weekday-sun" name="day[]" value="sun" <?= $sun == 'sun' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-sun">Sun</label>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Duration per class (hour)</td>
                        <td class="mark_space">:</td>
                        <td>{{$sac->duration}}</td>
                    </tr>
                    <tr>
                        <td class="td">Time</td>
                        <td class="mark_space">:</td>
                        <td>{{$sac->time}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Tutor Preference -->
<div class="row">
    <div class="col-sm-12 col-md-12 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-id-card"> </span> Tutor Preferences :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Gender</td>
                        <td class="mark_space">:</td>
                        <td>
                            <input type="radio" <?= $sac->tutor_gender == 1 ? 'checked' : '' ?> class="radio" id="gender" name="gender" value="male"> Male
                            <input type="radio" <?= $sac->tutor_gender == 2 ? 'checked' : '' ?> class="radio" id="gender" name="gender" value="female"> Female
                            <input type="radio" <?= $sac->tutor_gender == 3 ? 'checked' : '' ?> class="radio" id="gender" name="gender" value="any"> Any

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Race</td>
                        <td class="mark_space">:</td>
                        <td>
                            <select class="form-control " disabled readonly>

                                <option <?= $sac->tutor_race == 'none' ? 'checked' : '' ?> value="none">No preference</option>
                                <option <?= $sac->tutor_race == 'malay' ? 'checked' : '' ?> value="malay">Malay</option>
                                <option <?= $sac->tutor_race == 'indian' ? 'checked' : '' ?> value="indian">Indian</option>
                                <option <?= $sac->tutor_race == 'chinese' ? 'checked' : '' ?> value="chinese">Chinese</option>
                                <option <?= $sac->tutor_race == 'others' ? 'checked' : '' ?> value="others">Others</option>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Preferences</td>
                        <td class="mark_space">:</td>
                        <td>
                            <textarea class="form-control" id="preference" name="preference" readonly disabled>{{$sac->tutor_preference}}</textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Tutors - Applicants -->
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="card">
            <div class="card-header" id="headingTwo">
                <span class="fa fa-tasks"> </span> Applications :
            </div>
            <div id="collapseTwo" class="" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Application Date</th>
                                <th>Remarks</th>
                            </tr>

                            @foreach($tutors as $tutor)
                            <tr>
                                <td>{{ $tutor->user_name }}</td>
                                <td>{{ $tutor->user_email }}</td>
                                <td>{{ $tutor->user_phone }}</td>
                                <td>{{ $tutor->created_at }}</td>
                                <td>{{ $tutor->tutor_remarks }}</td>
                            </tr>
                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <a class="btn btn-primary" href="{{ route('admins.nonacademics.job_application_index') }}"> Back</a>
</div>



@endsection