@extends('layouts.app_login')
<?php
$page = 'classes-index';
$title = 'Non Academic Job Applications by Tutor';
?>

@section('content')


@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show">
    <p>{{ $message }}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Reference No.</th>
            <th>Class </th>
            <th>Requested At</th>
            <th>No of Applications</th>
            <th>Action</th>
        </tr>

        @foreach ($applications as $application)
        <tr>

            <td>{{ ++$i }}</td>
            <td>{{ $application->reference_no }}</td>
            <td><?= (is_null($application->class_name)) ? $application->class_name : $application->class_name ?></td>
            <td>{{ $application->requested_date }}</td>
            <td>{{ $application->no_of_tutor }}</td>

            <td>
                <a class="btn btn-info" href="{{ route('admins.nonacademics.job_application_show', $application->SAC_id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('admins.nonacademics.job_application_edit', $application->SAC_id) }}">Edit</a>
            </td>
        </tr>
        @endforeach


    </table>
</div>

{!! $applications->render() !!}

@endsection