<!-- Admin View. View the job in detail-->
@extends('layouts.app_login')
<?php
$page = 'classes-academics_show';
$title = 'Job : ' . $class->reference_no;

$student_profile_path = config('app.image.student.profile');

$mon = '';
$tue = '';
$wed = '';
$thu = '';
$fri = '';
$sat = '';
$sun = '';

$days = json_decode($class->days);

foreach ($days as $day) {

    if ($day == "mon") {
        $mon = $day;
    }
    if ($day == "tue") {
        $tue = $day;
    }
    if ($day == "wed") {
        $wed = $day;
    }
    if ($day == "thu") {
        $thu = $day;
    }
    if ($day == "fri") {
        $fri = $day;
    }
    if ($day == "sat") {
        $sat = $day;
    }
    if ($day == "sun") {
        $sun = $day;
    }
}
?>
@section('content')

<style>
    .fa {
        padding-right: 10px;
    }

    /* .row { */
    /* padding-bottom: 20px; */
    /* } */

    .col-sm-12,
    .col-md-6 {
        margin-bottom: 25px;
    }

    .td {
        width: 32%;
    }

    .mark_space {
        width: 5%;
    }

    .weekDays-selector input {
        display: none !important;
    }

    .weekDays-selector input[type=checkbox]+label {
        display: inline-block;
        border-radius: 6px;
        background: #dddddd;
        height: 40px;
        width: 45px;
        margin-right: 3px;
        line-height: 40px;
        text-align: center;
        cursor: pointer;
    }

    .weekDays-selector input[type=checkbox]:checked+label {
        background: #2AD705;
        color: #ffffff;
    }
</style>

<!-- Student information -->
<div class="row">
    <div class="col-sm-12 col-md-12 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-user-graduate"> </span> Student Information :
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6 col-md-8">
                        <div class="form-row">

                            <label for="inputUsername">Full Name</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" readonly name="name" id="name" value="{{$class->user_name}}">


                        </div>
                        <div class="form-row">
                            <div class="col-md-6 col-sm-12">
                                <label for="inputUsername">E-Mail</label>
                                <input type="text" class="form-control" readonly value="{{$class->user_email}}">
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label for="inputUsername">Phone Number</label>
                                <input type="text" class="form-control @error('phone') is-invalid @enderror" readonly name="phone" id="phone" value="{{$class->phone_no}}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 col-sm-12">
                                <label for="inputEmail4">Nationality</label>
                                <select class="form-control @error('nationality') is-invalid @enderror" readonly disabled id="nationality" name="nationality">
                                    <?php
                                    foreach ($nationalities as $nationality) {
                                        $n = $nationality->id;
                                        if ($n == $class->nationality_id) {
                                            echo "<option selected value='$n'>$nationality->nationality</option>";
                                        } else {
                                            echo "<option value='$n'>$nationality->nationality</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class='col-md-6 col-sm-12' id='mykadDiv' style="display:none;">
                                <label for='mykad'>Mykad No.</label>
                                <input type='text' class="form-control @error('mykad') is-invalid @enderror" readonly name='mykad' id='mykad' value='<?= $class->mykad_no; ?>'>
                            </div>

                            <div class='col-md-6 col-sm-12' id='passportDiv' style="display:none;">
                                <label for='passport'>Passport No.</label>
                                <input type='text' class="form-control @error('passport') is-invalid @enderror" readonly name='passport' id='passport' value='<?= $class->passport_no; ?>'>
                            </div>
                        </div>

                        <div class="row" style="margin-top:10px;">
                            <div class="col-sm-12 col-md-12">
                                <label for='gender'>Gender :&nbsp </label>
                                <input type="checkbox" readonly disabled class="radio" name="gender" value="1" <?= ($class->gender_id  == '1') ? 'checked' : '' ?>> Male
                                <input type="checkbox" readonly disabled class="radio" name="gender" value="0" <?= ($class->gender_id  == '0') ? 'checked' : '' ?>> Female
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="text-center">


                            <div class="mt-2">
                                <img id="preview" alt="{{ $class->user_name }}" src="{{asset('storage/'.$student_profile_path.$class->image)}}" class="rounded-circle img-responsive mt-2" width="128" height="128">

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>


</div>

<div class="row">
    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-map-marked"> </span> Tuition Job Location :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">State</td>
                        <td class="mark_space">:</td>
                        <td>
                            @foreach($states as $state)
                            @if($state->code == $class->state )
                            {{ $state->state }}
                            @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td class="td">City</td>
                        <td class="mark_space">:</td>
                        <td>{{ $class->city }}</td>
                    </tr>
                    <tr>
                        <td class="td">Postcode</td>
                        <td class="mark_space">:</td>
                        <td>{{ $class->postcode }}</td>
                    </tr>
                    <tr>
                        <td class="td">Area</td>
                        <td class="mark_space">:</td>
                        <td>{{ $class->area }}

                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>

    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-chalkboard-teacher"> </span> Tuition Job Description :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Class</td>
                        <td class="mark_space">:</td>
                        <td>
                            {{$class->class_name}}

                        </td>
                    </tr>

                    

                    <tr>
                        <td class="td">Remarks</td>
                        <td class="mark_space">:</td>
                        <td>
                            <textarea class="form-control" id="remarks" readonly disabled name="remarks">{{ $class->remarks }}</textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-comment-dollar"> </span> Tuition Fees :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Fees per hour</td>
                        <td class="mark_space">:</td>
                        <td>
                            RM {{$class->price}}
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-6 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-user-friends"> </span> Tuition Type :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Type</td>
                        <td class="mark_space">:</td>
                        <td>
                            <select class="form-control " disabled readonly>
                                <option <?= $class->type == 1 ? 'selected' : '' ?> value="1">Personal</option>
                                <option <?= $class->type == 2 ? 'selected' : '' ?> value="2">Group</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="td">No. of students</td>
                        <td class="mark_space">:</td>
                        <td>{{ $class->pax }}

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Timing -->
<div class="row">
    <div class="col-sm-12 col-md-12 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-clock"> </span> Tuition Preferred Timing :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Frequency</td>
                        <td class="mark_space">:</td>
                        <td>
                            <select readonly disabled class="form-control ">
                                <option disabled selected>Choose...</option>
                                <option <?= $class->frequency == 1 ? 'selected' : '' ?> value="1">1 class per week</option>
                                <option <?= $class->frequency == 2 ? 'selected' : '' ?> value="2">2 class per week</option>
                                <option <?= $class->frequency == 3 ? 'selected' : '' ?> value="3">3 class per week</option>
                                <option <?= $class->frequency == 4 ? 'selected' : '' ?> value="4">4 class per week</option>
                                <option <?= $class->frequency == 5 ? 'selected' : '' ?> value="5">5 class per week</option>
                                <option <?= $class->frequency == 6 ? 'selected' : '' ?> value="6">6 class per week</option>
                                <option <?= $class->frequency == 7 ? 'selected' : '' ?> value="7">7 class per week</option>
                                <option <?= $class->frequency == 8 ? 'selected' : '' ?> value="8">More than 7 class</option>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Days</td>
                        <td class="mark_space">:</td>
                        <td>
                            <div class="weekDays-selector">
                                <input type="checkbox" id="weekday-mon" name="day[]" value="mon" <?= $mon == 'mon' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-mon">Mon</label>
                                <input type="checkbox" id="weekday-tue" name="day[]" value="tue" <?= $tue == 'tue' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-tue">Tue</label>
                                <input type="checkbox" id="weekday-wed" name="day[]" value="wed" <?= $wed == 'wed' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-wed">Wed</label>
                                <input type="checkbox" id="weekday-thu" name="day[]" value="thu" <?= $thu == 'thu' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-thu">Thu</label>
                                <input type="checkbox" id="weekday-fri" name="day[]" value="fri" <?= $fri == 'fri' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-fri">Fri</label>
                                <input type="checkbox" id="weekday-sat" name="day[]" value="sat" <?= $sat == 'sat' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-sat">Sat</label>
                                <input type="checkbox" id="weekday-sun" name="day[]" value="sun" <?= $sun == 'sun' ? 'checked' : '' ?> class="weekday" readonly disabled />
                                <label for="weekday-sun">Sun</label>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Duration per class (hour)</td>
                        <td class="mark_space">:</td>
                        <td>{{$class->duration}}</td>
                    </tr>
                    <tr>
                        <td class="td">Time</td>
                        <td class="mark_space">:</td>
                        <td>{{$class->time}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Tutor Preference -->
<div class="row">
    <div class="col-sm-12 col-md-12 ">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-id-card"> </span> Tutor Preferences :
            </div>
            <div class="card-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <td class="td">Gender</td>
                        <td class="mark_space">:</td>
                        <td>
                            <input type="radio" <?= $class->tutor_gender == 1 ? 'checked' : '' ?> class="radio" id="gender" name="gender" value="male"> Male
                            <input type="radio" <?= $class->tutor_gender == 2 ? 'checked' : '' ?> class="radio" id="gender" name="gender" value="female"> Female
                            <input type="radio" <?= $class->tutor_gender == 3 ? 'checked' : '' ?> class="radio" id="gender" name="gender" value="any"> Any

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Race</td>
                        <td class="mark_space">:</td>
                        <td>
                            <select class="form-control " disabled readonly>

                                <option <?= $class->tutor_race == 'none' ? 'checked' : '' ?> value="none">No preference</option>
                                <option <?= $class->tutor_race == 'malay' ? 'checked' : '' ?> value="malay">Malay</option>
                                <option <?= $class->tutor_race == 'indian' ? 'checked' : '' ?> value="indian">Indian</option>
                                <option <?= $class->tutor_race == 'chinese' ? 'checked' : '' ?> value="chinese">Chinese</option>
                                <option <?= $class->tutor_race == 'others' ? 'checked' : '' ?> value="others">Others</option>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td class="td">Preferences</td>
                        <td class="mark_space">:</td>
                        <td>
                            <textarea class="form-control" id="preference" name="preference" readonly disabled>{{$class->tutor_preference}}</textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Tutors - Applicants -->
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="card">
            <div class="card-header" id="headingTwo">
                <span class="fa fa-tasks"> </span> Applications :
            </div>
            <div id="collapseTwo" class="" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Application Date</th>
                                <th>Name</th>
                                <th>Details</th>
                                <th>Status</th>
                                <th>Assign Job</th>
                                <th>Action</th>
                            </tr>


                            @foreach($tutors as $tutor)

                            <tr>
                                <td>{{ $tutor->created_at }}</td>
                                <td>{{ $tutor->user_name }}</td>
                                <td>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal_{{$tutor->id}}"><span class="fa fa-angle-double-right"></span></button>
                                </td>
                                <td>
                                    <?= (!is_null($tutor->tutor_status) ? 'Approved' : '') ?>
                                </td>

                                <td>
                                    <form method="POST" action="{{route('admins.nonacademics.job_reassign_update')}}">
                                        @csrf
                                        <input id="tutor_job" <?= ($tutor->tutor_status == 1 ? 'checked' : '') ?> required="required" name="tutor_job" value="{{ $tutor->id }}" type="checkbox" class="radio @error('Assign Job') is-invalid @enderror">
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-success">Re-Assign Tutor</button>
                                    </form>
                                </td>
                            </tr>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal_{{$tutor->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-md" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Application Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row top-buffer">
                                                <div class="col-5">Name</div>
                                                <div class="col-2">:</div>
                                                <div class="col-5">{{$tutor->user_name}}</div>
                                            </div>
                                            <div class="row top-buffer">
                                                <div class="col-5">Email</div>
                                                <div class="col-2">:</div>
                                                <div class="col-5">{{$tutor->user_email}}</div>
                                            </div>
                                            <div class="row top-buffer">
                                                <div class="col-5">Phone No</div>
                                                <div class="col-2">:</div>
                                                <div class="col-5">{{$tutor->user_phone}}</div>
                                            </div>
                                            <div class="row top-buffer">
                                                <div class="col-5">Application Date & Time</div>
                                                <div class="col-2">:</div>
                                                <div class="col-5">{{$tutor->created_at}}</div>
                                            </div>
                                            <div class="row top-buffer">
                                                <div class="col-5">Remarks</div>
                                                <div class="col-2">:</div>
                                                <div class="col-5">{{$tutor->tutor_remarks}}</div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Update the start date for first class - trial month begins -->
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-calendar"> </span> Class Start Date :
            </div>
            <div class="card-body">
                <p>This class start date will be used to calculate the trial month.</p>
                <p>One month from now, if there is no problem, admin has to update the status to 'SETTLED', else need to update the new start date.</p>
                <form method="POST" action="{{route('admins.nonacademics.job_assigned_updateStartDate')}}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <input type="text" name="ref_no" id="ref_no" class="form-control" readonly value="{{$class->reference_no}}">
                            <input type="hidden" name="sac_id" id="sac_id" value="{{$class->id}}">
                        </div>

                        <div class="col-sm-12 col-md-4">
                            <input type="date" name="start_date" id="start_date" class="form-control @error('start_date') is-invalid @enderror" value="{{$class->class_start_date}}">
                            @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-sm-12 col-md-4">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Update the payment received from students -->
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-money-bill-wave"> </span> Payment :
            </div>
            <div class="card-body">
                <p>This is to update the payment received from students.</p>
                <p>Once payment is received, enter the payment date to update record.</p>
                <form method="POST" action="{{route('admins.nonacademics.job_assigned_updatePayment')}}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <input type="text" name="ref_no" id="ref_no" class="form-control " readonly value="{{$class->reference_no}}">
                            <input type="hidden" name="sac_id" id="sac_id" value="{{$class->id}}">
                        </div>

                        <div class="col-sm-12 col-md-4">
                            <input type="date" name="payment_date" id="payment_date" class="form-control @error('payment_date') is-invalid @enderror" value="{{$class->payment_date}}">
                            @error('payment_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-sm-12 col-md-4">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Update the job settled -->
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-check-circle"> </span> Update Job Status to Settled :
            </div>
            <div class="card-body">
                <p>This is to update that this job has been settled.</p>
                <p>After finishing 1 month trial between student and tutor, mark this as settled.</p>
                <form method="POST" action="{{route('admins.nonacademics.job_assigned_updateSettled')}}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <input type="text" name="ref_no" id="ref_no" class="form-control " readonly value="{{$class->reference_no}}">
                            <input type="hidden" name="sac_id" id="sac_id" value="{{$class->id}}">
                        </div>

                        <div class="col-sm-12 col-md-4">
                            <button type="submit" class="btn btn-success">Settled</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-random"> </span> Make Job Offer Available :
            </div>
            <div class="card-body">
                <p>This job is not available in JOB OFFERS for tutors.</p>
                <p>Click on the button below to make this job available in JOB OFFERS.</p>
                <form method="POST" action="{{route('admins.nonacademics.job_assigned_update')}}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <input type="text" name="ref_no" id="ref_no" class="form-control" readonly value="{{$class->reference_no}}">
                            <input type="hidden" name="snac_id" id="snac_id" value="{{$class->id}}">
                        </div>

                        <div class="col-sm-12 col-md-4">
                            <button type="submit" class="btn btn-success">Change to Active</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <span class="fa fa-random"> </span> Cancel Job :
            </div>
            <div class="card-body">
                <p>This job is approved and already assigned. Please be sure of your action.</p>
                <p>This action is not reversible.</p>
                <form method="POST" action="{{route('admins.nonacademics.request_cancel')}}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <input type="text" name="ref_no" id="ref_no" class="form-control" readonly value="{{$class->reference_no}}">
                        </div>

                        <div class="col-sm-12 col-md-4">
                            <button type="submit" class="btn btn-warning">Cancel Job</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row justify-content-center">
    <div class="form-group">
        <a href="{{ route('admins.nonacademics.job_assigned_list') }}" class="btn btn-primary">Back</a>
    </div>
</div>

<script type="text/javascript">
    function beforeSubmit() {
        event.preventDefault();
        var ck_box = $('input[type="checkbox"]:checked').length;

        if (ck_box == 0) {
            alert("You need to assign the tutor. Please select one tutor.");
            return false;
        }

        $('#submitForm').modal('show');
    }

    function formSubmit() {
        event.preventDefault();
        document.getElementById('myForm').submit();
    }

    $("input:checkbox").on('click', function() {
        // in the handler, 'this' refers to the box clicked on
        var $box = $(this);
        if ($box.is(":checked")) {
            // the name of the box is retrieved using the .attr() method
            // as it is assumed and expected to be immutable
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            // the checked state of the group/box on the other hand will change
            // and the current value is retrieved using .prop() method
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", false);
        }
    });
    $(document).on("click", ".browse", function() {
        var file = $(this).parents().find(".file");
        file.trigger("click");
    });

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            document.getElementById("preview").src = e.target.result;
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    $(document).ready(function() {
        // console.log(document.getElementById('nationality').value);

        if (document.getElementById('nationality').value == "109") {
            $('#mykadDiv').show();
            $('#mykad').attr('required', '');
            $('#mykadDiv').attr('data-error', 'This field is required.');
            $('#passportDiv').hide();
            $('#passport').removeAttr('required');
            $('#passportDiv').removeAttr('data-error');
        } else {
            $('#passportDiv').show();
            $('#passport').attr('required', '');
            $('#passportDiv').attr('data-error', 'This field is required.');
            $('#mykadDiv').hide();
            $('#mykad').removeAttr('required');
            $('#mykadDiv').removeAttr('data-error');
        }

        $("#nationality").trigger("change");
        $("#nationality").change(function() {
            if ($(this).val() == "109") {
                $('#mykadDiv').show();
                $('#mykad').attr('required', '');
                $('#mykadDiv').attr('data-error', 'This field is required.');
                $('#passportDiv').hide();
                $('#passport').removeAttr('required');
                $('#passportDiv').removeAttr('data-error');
            } else {
                $('#passportDiv').show();
                $('#passport').attr('required', '');
                $('#passportDiv').attr('data-error', 'This field is required.');
                $('#mykadDiv').hide();
                $('#mykad').removeAttr('required');
                $('#mykadDiv').removeAttr('data-error');
            }
        });
        // $("#nationality").trigger("change");


    });
</script>

@endsection