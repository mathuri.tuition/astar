@extends('layouts.app_login')


@section('content')
<div class="row justify-content-center">
    <div class="col-lg-8 margin-tb">
        <div class="float-left">
            <h2>Profile</h2>
        </div>
    </div>
</div>


@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


{!! Form::model($admin, ['method' => 'POST','route' => ['admins.profile.update', $admin->id]]) !!}
<div class="row justify-content-center">

    <div class="col-xs-8 col-sm-8 col-md-8">
        <div class="form-group">
            <strong>Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-8 col-sm-8 col-md-8">
        <div class="form-group">
            <strong>Email:</strong>
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-8 col-sm-8 col-md-8">
        <div class="form-group">
            <strong>Nationality:</strong>
            <select class="form-control" name="nationality" id="nationality">
                <?php

                foreach ($nationalities as $k) { ?>

                    <option value="<?= $k->id; ?>" <?php if ($k->id == $admin->nationality_id) echo "selected"; ?>><?= $k->nationality; ?></option>;


                <?php

                }
                ?>

            </select>
        </div>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-xs-5 col-sm-5 col-md-5">

        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('admins.dashboard') }}"> Back</a>
        </div>
    </div>


    <div class="col-xs-2 col-sm-2 col-md-2">

    </div>

    <div class="col-xs-5 col-sm-5 col-md-5">
        <div class="float-left">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>


</div>
{!! Form::close() !!}


<br><br>
@endsection