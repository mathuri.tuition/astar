@extends('layouts.app')
<?php 
$page = "teachers"; 
$tutor_profile_path = config('app.image.tutor.profile');
$default_male_profile_path = config('app.image.default.profile.male');
$default_female_profile_path = config('app.image.default.profile.female');
?>
@section('content')

<div class="hero-wrap hero-wrap-2" style="background-image: url('{{@genius}}/images/bg_2.jpg'); background-attachment:fixed;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-md-8 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span>Teacher</span></p>
                <h1 class="mb-3 bread">Teacher</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate text-center">
                <h2 class="mb-4">Our Experience Advisor</h2>
            </div>
        </div>
        
        <div class="row">
            @foreach($tutors as $tutor)
            <div class="col-lg-4  col-sm-12 mb-sm-4 ftco-animate">
                <div class="staff">
                    <div class="d-flex mb-4">
                        
                        <div class="img" >
                             <img class="img" src="{{asset('storage/'.$tutor_profile_path.$tutor->image)}}">
                        </div>
                        <div class="info ml-4">
                            <h3><a>{{ ucwords($tutor->name) }}</a></h3>
                             <span class="position">Teacher</span> 
                            <!-- <p class="ftco-social d-flex">
                                <a href="#" class="d-flex justify-content-center align-items-center"><span class="icon-twitter"></span></a>
                                <a href="#" class="d-flex justify-content-center align-items-center"><span class="icon-facebook"></span></a>
                                <a href="#" class="d-flex justify-content-center align-items-center"><span class="icon-instagram"></span></a>
                            </p> -->
                        </div>
                        
                    </div>
                     
                    <div class="text">
                         <p>{{$tutor->subjects}}</p> 
                    </div>
                    
                    <div class="text">
                         <p>{{$tutor->experience}}</p> 
                    </div>
                   
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@endsection