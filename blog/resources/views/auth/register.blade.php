@extends('layouts.app')
<?php
$page = 'register';

$site_key = config('app.recaptcha.site_key');
$secret_key = config('app.recaptcha.secret_key');
?>
@section('content')

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<style>
    .hidden {
        display: none;
    }

    .register {
        background: -webkit-linear-gradient(left, #3931af, #00c6ff);
        /* margin-top: 3%; */
        padding: 3%;
    }

    .register-left {
        text-align: center;
        color: #fff;
        margin-top: 4%;
    }

    .register-left input {
        border: none;
        border-radius: 1.5rem;
        padding: 2%;
        width: 60%;
        background: #f8f9fa;
        font-weight: bold;
        color: #383d41;
        margin-top: 30%;
        margin-bottom: 3%;
        cursor: pointer;
    }

    .register-right {
        background: #f8f9fa;
        border-top-left-radius: 10% 50%;
        border-bottom-left-radius: 10% 50%;
    }

    .register-left img {
        margin-top: 15%;
        margin-bottom: 5%;
        width: 25%;
        -webkit-animation: mover 2s infinite alternate;
        animation: mover 1s infinite alternate;
    }

    @-webkit-keyframes mover {
        0% {
            transform: translateY(0);
        }

        100% {
            transform: translateY(-20px);
        }
    }

    @keyframes mover {
        0% {
            transform: translateY(0);
        }

        100% {
            transform: translateY(-20px);
        }
    }

    .register-left p {
        font-weight: lighter;
        padding: 12%;
        margin-top: -9%;
    }

    .register .register-form {
        padding: 10%;
        margin-top: 10%;
    }

    .btnRegister {
        float: right;
        margin-top: 10%;
        border: none;
        border-radius: 1.5rem;
        padding: 2%;
        background: #0062cc;
        color: #fff;
        font-weight: 600;
        width: 50%;
        cursor: pointer;
    }

    .register .nav-tabs {
        margin-top: 3%;
        border: none;
        background: #0062cc;
        border-radius: 1.5rem;
        width: 28%;
        float: right;
    }

    .register .nav-tabs .nav-link {
        padding: 2%;
        height: 34px;
        font-weight: 600;
        color: #fff;
        border-top-right-radius: 1.5rem;
        border-bottom-right-radius: 1.5rem;
    }

    .register .nav-tabs .nav-link:hover {
        border: none;
    }

    .register .nav-tabs .nav-link.active {
        /* width: 100px; */
        color: #0062cc;
        border: 2px solid #0062cc;
        border-top-left-radius: 1.5rem;
        border-bottom-left-radius: 1.5rem;
    }

    .register-heading {
        text-align: center;
        margin-top: 8%;
        margin-bottom: -15%;
        color: #495057;
    }
</style>


<div class=" register">
    <div class="row">

        <div class="col-3 register-left">
            <!-- <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt="" /> -->

            <img src="{{@genius}}/images/rocket.png" alt="" />

            <h3>Welcome</h3>
            <p>It takes only 30 seconds to register.</p>
            <!-- <input type="submit" name="" value="Login" /><br /> -->
        </div>

        <div class="col-9 register-right">
            <!--<ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">-->
            <!--    <li class="nav-item">-->
            <!--        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Student</a>-->
            <!--    </li>-->
            <!--    <li class="nav-item">-->
            <!--        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Teacher</a>-->
            <!--    </li>-->
            <!--</ul>-->

            <div class="tab-content" id="myTabContent">

                <!-- Student -->
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <form method="POST" action="{{ route('register.submit') }}">
                        @csrf
                        <!--<input type="hidden" name="role" id="role" value="student">-->
                        <div>
                            <h3 class="register-heading" id="fold_p">Sign Up</h3>
                        </div>

                        <div class="row register-form">
                            <div class="col-md-6 col-sm-12">
                                
                                <div class="form-group">
                                    <select class="form-control @error('role') is-invalid @enderror" name="role" id="role" required>
                                        <option class="hidden" selected disabled>I am a...</option>
                                        <option value="student">Student</option>
                                        <option value="tutor">Tutor</option>
                                    </select>

                                    @error('role')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                
                                <div class="form-group">
                                    <!-- <label for="name">{{ __('Full Name') }}</label> -->
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Name" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus />
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input type="text" minlength="10" maxlength="10" id="phone_no" name="phone_no" class="form-control @error('phone_no') is-invalid @enderror" required placeholder="Your Phone" value="{{old('phone_no')}}" />
                                    @error('phone_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <!-- <label for="email">{{ __('Email') }}</label> -->
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus />
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <!-- <label for="password">{{ __('Password') }}</label> -->
                                    <input type="password" class="form-control" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" />
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <!-- <label for="password_confirm">{{ __('Confirm Password') }}</label> -->
                                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Confirm Password" id="password_confirmation" name="password_confirmation" required autocomplete="new-password" />
                                    @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">

                                <div class="form-group">
                                     <select class="form-control @error('gender') is-invalid @enderror" name="gender" id="gender" required>
                                        <option class="hidden" selected disabled>Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                    
                                    @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <!-- <label for="nationality">{{ __('Nationality') }}</label> -->
                                    <select class="form-control @error('nationality') is-invalid @enderror" name="nationality" id="nationalityS" required>
                                        <option class="hidden" selected disabled>Nationality...</option>
                                        <?php
                                        foreach ($nationalities as $k) {
                                            echo  "<option value='$k->id'>$k->nationality</option>";
                                        }
                                        ?>
                                    </select>

                                    @error('nationality')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group" id="id_no">
                                    <input class="form-control" name="id_no" placeholder="MyKad/Passport No." disabled>
                                </div>

                                <div class="form-group hidden" id="mykadDivS">
                                    <input id="mykadS" name="mykad" maxlength="12" type="text" placeholder="MyKad" class="form-control @error('mykad') is-invalid @enderror" value="{{ old('mykad') }}" autocomplete="mykad" autofocus>
                                    @error('mykad')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>


                                <div class="form-group hidden" id="passportDivS">
                                    <input id="passportS" type="text" class="form-control @error('passport') is-invalid @enderror" placeholder="Passport Number" name="passport" value="{{ old('passport') }}" autocomplete="passport" autofocus>
                                    @error('passport')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="g-recaptcha" data-callback="captchaVerified" data-sitekey="{{ $site_key }}"></div>
                                    <span id="required_captcha" style="color:red;">* required.</span>
                                </div>
                                
                                 <div class="form-group">
                                    <input type="checkbox" class="checkbox" id="terms" name="terms" required> I agree to the <a href="{{ route('terms') }}" target="_blank">Terms and Conditions</a>
                                </div>

                                <div class="form-group">
                                    <button type="submit" id="register" class="btn btnRegister" disabled>Register</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

</div>


<script type="text/javascript">
    window.onload = function() {
        var $recaptcha = document.querySelector('#g-recaptcha-response');

        if ($recaptcha) {
            $recaptcha.setAttribute("required", "required");
        }
    };

    function captchaVerified() {
        var submitBtn = document.querySelector('#register');
        submitBtn.removeAttribute('disabled');
    }

    $(document).ready(function() {
        $('#home-tab').click(function() {
            $('#role').val('student');
            $('#fold_p').text('Apply as Student');
        });

        $('#profile-tab').click(function() {
            $('#role').val('tutor');
            $('#fold_p').text('Apply as Teacher');
        });

        $("#nationalityS").trigger("change");
        $("#nationalityS").change(function() {

            $('#id_no').addClass('hidden');

            if ($(this).val() == "109") {
                // $('#mykadDivS').show();
                $('#mykadDivS').removeClass('hidden');
                $('#mykadS').attr('required', '');
                $('#mykadDivS').attr('data-error', 'This field is required.');
                // $('#passportDivS').hide();
                $('#passportDivS').addClass('hidden');
                $('#passportS').removeAttr('required');
                $('#passportDivS').removeAttr('data-error');
            } else {
                // $('#passportDivS').show();
                $('#passportDivS').removeClass('hidden');
                $('#passportS').attr('required', '');
                $('#passportDivS').attr('data-error', 'This field is required.');
                // $('#mykadDivS').hide();
                $('#mykadDivS').addClass('hidden');
                $('#mykadS').removeAttr('required');
                $('#mykadDivS').removeAttr('data-error');
            }
        });
    });
</script>

@endsection